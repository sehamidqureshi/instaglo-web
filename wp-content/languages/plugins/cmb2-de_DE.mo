��    s      �  �   L      �	     �	     �	     �	  	   �	     
     
     
     3
     :
  	   ?
  Q   I
  	   �
     �
  	   �
     �
     �
     �
     �
     �
     �
     �
                 	   %  
   /     :     G     M     m     y     ~  :   �  `   �     (  :   5     p     �     �     �     �     �     �     �     �     �  
   �       
             /     4     B     Q     X     e     r       
   �     �     �     �     �     �     �     �       !     >   A     �     �     �  !   �  ,   �  
           
        #  
   7     B     T     `     }     �  	   �     �     �     �     �     �       	        '     9     F     T     p  	   u          �     �  *   �     �     �  
   �       9     X   T     �  (   �     �     �          ,    3     K     e  "   }     �     �      �      �                 b   *  
   �  
   �  
   �     �     �     �     �     �     �            	     
        &  
   2     =     J  +   Q     }     �     �  ;   �  ^   �     =  C   J  ,   �     �     �     �     �  /   �          /     6     P     V     _     h     q     �     �     �  	   �     �     �     �     �            	   6     @  "   H     k     y     �     �  !   �  A   �     /     =     N  #   a  4   �  	   �     �     �     �  
             #      /     P     e  	   {     �     �  3   �     �     �       	             1     >     M     m  	   r     |     �  5   �  ;   �                *     D     b     k     q  4   }     �     �     �     �     D   E   c   e   1   8       j   #          @                      I      n      !           R       X      _      C           a   3       >   F   f   /       [   T       =         ]      o   A   ,   h   S   Y   r             7      K          O       U   5   W   :   %   "      J   g      .         k         ;   H       *      Z   V   
       	   `   4       )   '   ?       6   2   Q          L   &   m           9   l           P      b           N   (   i   +              -           0   $       <                  B      G       q                      p   \      s   ^   d              M    %s &mdash; Updated! About Page Metabox Add Another Entry Add Group Add Row Add or Upload File Add or Upload Files Avatar CMB2 CMB2 team CMB2 will create metaboxes and forms with custom fields that will blow your mind. Check One Check Three Check Two Choose Time Clear Click to toggle Current Color Default Description Done Download Entry Image Entry Title Entry {#} Extra Info Facebook URL File: Generates reusable form entries Google+ URL Hour Image Caption Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec January, February, March, April, May, June, July, August, September, October, November, December Linkedin URL Metabox configuration is required to have an ID parameter. Method '%s' must be overridden. Minute Multiple Files Next No boxes found. No field found by that id. No terms None Nothing to update. Now Option One Option Three Option Two Please Try Again Prev REST Test Box REST Test Text Remove Remove Embed Remove Entry Remove Group Remove Image Remove Row Repeating Field Group Save Second Select / Deselect All Select Color Settings updated. Site Background Color Su, Mo, Tu, We, Th, Fr, Sa Sun, Mon, Tue, Wed, Thu, Fri, Sat Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday Test Checkbox Test Color Picker Test Date Picker Test Date Picker (UNIX timestamp) Test Date/Time Picker Combo (UNIX timestamp) Test Image Test Metabox Test Money Test Multi Checkbox Test Radio Test Radio inline Test Select Test Taxonomy Multi Checkbox Test Taxonomy Radio Test Taxonomy Select Test Text Test Text Area Test Text Area Small Test Text Area for Code Test Text Email Test Text Medium Test Text Small Test Time Test Title Weeeee Test wysiwyg Theme Options This is a title description Time Time zone Today Twitter URL Upload an image or enter a URL. Upload or add multiple images/attachments. Use these files Use this file User Field User Profile Metabox Valid formatDate string for jquery-ui datepickermm/dd/yy Valid formatting string, as per http://trentrichardson.com/examples/timepicker/hh:mm TT Website URL Write a short description for this entry field description (optional) https://cmb2.io https://github.com/CMB2/CMB2 oEmbed PO-Revision-Date: 2019-08-31 11:55:06+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: de
Project-Id-Version: Plugins - CMB2 - Development (trunk)
 %s &mdash; Aktualisiert!  Über die Metabox Seite Füge einen weiteren Eintrag hinzu Füge eine Gruppe hinzu Zeile hinzufügen Datei hinzufügen oder hochladen Datei hinzufügen oder hochladen Avatar CMB2 Das Team von CMB2 CMB2 erstellt Metaboxen und Formulare mit benutzerdefinierten Feldern, die dich begeistern werden. 1. Auswahl 3. Auswahl 2. Auswahl Zeit auswählen Leeren Zum umschalten klicken Aktuelle Farbe Standard Beschreibung Fertig Download Bild Name Titel Name Eintrag {#} Zusatzinfo Facebook URL Datei: Erzeugt wiederverwendbare Formulareinträge Google+ URL Stunde Bild Beschreibung Jan, Feb, Mär, Apr, Mai, Jun, Jul, Aug, Sep, Okt, Nov, Dez Januar, Februar, März, April, Mai, Juni, Juli, August, September, Oktober, November, Dezember Linkedin URL Bei der Metabox-Konfiguration ist die Eingabe einer ID erforderlich Die '%s'-Methode muss überschrieben werden. Minute Mehrere Dateien Weiter Keine Boxen gefunden. Für diese ID konnte kein Feld gefunden werden. Keine Einträge Nichts Nichts zum Aktualisieren. Jetzt Option 1 Option 3 Option 2 Bitte versuche es noch einmal Zurück REST-Test-Box REST-Test-Text Entfernen Eingebettete Datei entfernen Eintrag entfernen Gruppe entfernen Bild entfernen Zeile entfernen Duplizierbare Feldgruppe Speichern Sekunde Alle auswählen oder ausschliessen Farbe wählen Einstellungen aktualisiert. Hintergrundfarbe der Seite So, Mo, Di, Mi, Do, Fr, Sa Son, Mon, Die, Mit, Don, Fre, Sam Sonntag, Montag, Dienstag, Mittwoch, Donnerstag, Freitag, Samstag Test Checkbox Test Farb Picker Test Datums Picker Test Datums Picker (UNIX timestamp) Test Datums/Zeit Picker-Kombination (UNIX timestamp) Test Bild Test Metabox Test Geld mit Währungssymbol Test mehrfach Checkbox Test Radio Test Radio inline Test Select Test Kategorie mehrfach Checkbox Test Kategorie Radio Test Kategorie Select Test Text Test Text Bereich Test Text Bereich Klein Test Text Textbereich für die Programmcode Eingabe Test Text E-Mail Test Text Mittel Test Text Klein Test Zeit Test Titel Weeeee Test wysiwyg Theme-Optionen Dies ist ein Titel Beschreibung Zeit Zeit Zone Heute Twitter URL Laden Sie ein Bild hoch, oder geben Sie eine URL ein. Hochladen oder mehrfaches hinzufügen von Bildern/Anhängen Benutze diese Dateien Benutze diese Datei Benutzer definiertes Feld Metabox für Benutzer Profile dd.mm.yy HH:mm Website URL Schreibe eine kurze Beschreibung für diesen Eintrag Feld Beschreibung (optional) https://cmb2.io https://github.com/CMB2/CMB2 oEmbed 