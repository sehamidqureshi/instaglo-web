<?php get_header();


    $heading1 = get_post_meta( get_the_ID(), 'heading1', true );
    $heading2 = get_post_meta( get_the_ID(), 'heading2', true );
    $description = get_post_meta( get_the_ID(), 'description', true );
    $beforeAfterSlider = get_post_meta( get_the_ID(), 'slider', true );
    $productSlider = get_post_meta( get_the_ID(), 'products', true );
    $price = get_post_meta( get_the_ID(), 'price', true );
    $faq = get_post_meta( get_the_ID(), 'faq', true );
    $id = get_post_meta( get_the_ID(), 'id', true );
    $postImage = get_post_meta( get_the_ID(), 'postimage', true );

	$global_breadcrumb = cs_get_option( 'show-breadcrumb' );

    $settings = get_post_meta($post->ID,'_dt_post_settings',TRUE);
    $settings = is_array( $settings ) ?  array_filter( $settings )  : array();

	if( empty($settings) || ( array_key_exists( 'layout', $settings ) && $settings['layout'] == 'global-site-layout' ) ) {
		if( empty($settings) ) { $settings['enable-sub-title'] = true; }
		$settings['layout'] = cs_get_option( 'site-global-sidebar-layout' );
		$settings['show-standard-sidebar-left'] = true;
		$settings['show-standard-sidebar-right'] = true;
	}

    $header_class = '';
    if( !isset( $settings['enable-sub-title'] ) || !$settings['enable-sub-title']  ) {
        if( isset( $settings['show_slider'] ) && $settings['show_slider'] ) {
            if( isset( $settings['slider_type'] ) ) {
                $header_class =  $settings['slider_position'];
            }
        }
    }

    if( !empty( $global_breadcrumb ) ) {
        if( isset( $settings['enable-sub-title'] ) && $settings['enable-sub-title'] ) {
            $header_class = isset( $settings['breadcrumb_position'] ) ? $settings['breadcrumb_position'] : '';
		}
	}?>
<!-- ** Header Wrapper ** -->
<div id="header-wrapper" class="<?php echo esc_attr($header_class); ?>">

    <!-- **Header** -->
    <header id="header">

        <div class="container"><?php
            /**
             * meni_header hook.
             * 
             * @hooked meni_vc_header_template - 10
             *
             */
            do_action( 'meni_header' ); 

            ?>
        </div>
    </header><!-- **Header - End ** -->

    <!-- ** Breadcrumb ** -->
    <?php
    	# Global Breadcrumb
		if( array_key_exists( 'single-post-style', $settings ) ) {

			// Breadcrumb fixed style...
			if( $settings['single-post-style'] == 'breadcrumb-fixed' ) {

				echo '<div class="single-post-header-wrapper aligncenter">';

					if( has_post_thumbnail( $post->ID ) ):
						$bgimage = get_the_post_thumbnail_url( $post->ID, 'full' );
						echo '<div class="main-title-section-bg" style="background-image: url('.$bgimage.');"></div>';
					else:
						echo '<div class="main-title-section-bg"></div>';
					endif;

					echo '<div class="container">';
						echo the_title( '<h1 class="single-post-title">', '</h1>', false );

						$template = 'framework/templates/single/entry-categories.php';
						$template_args['post_ID'] = $post->ID;
						$template_args['post_Style'] = 'breadcrumb-fixed'; ?>

                        <div class="post-meta-data">
                            <div class="post-categories"><?php meni_get_template( $template, $template_args ); ?></div>
                            <div class="date"><?php echo sprintf( esc_html__( '%s ago', 'meni' ), human_time_diff( get_the_date('U'), current_time('timestamp') ) ); ?></div>
						</div><?php
					echo '</div>';

				echo '</div>';
			} elseif( $settings['single-post-style'] == 'breadcrumb-parallax' ) {

				// Breadcrumb parallax style...
				echo '<div class="single-post-header-wrapper dt-parallax-bg aligncenter">';

					if( has_post_thumbnail( $post->ID ) ):
						$bgimage = get_the_post_thumbnail_url( $post->ID, 'full' );
						echo '<div class="main-title-section-bg" style="background-image: url('.$bgimage.');"></div>';
					else:
						echo '<div class="main-title-section-bg"></div>';
					endif;

					echo '<div class="container">';
						$template = 'framework/templates/single/entry-categories.php';
						$template_args['post_ID'] = $post->ID;
						$template_args['post_Style'] = 'breadcrumb-parallax'; ?>

                        <div class="post-categories"><?php meni_get_template( $template, $template_args ); ?></div><?php

						echo the_title( '<h1 class="single-post-title">', '</h1>', false ); ?>

                        <div class="post-meta">
							<div class="post-author"><?php
								$auth = get_post( $post->ID );
								$authid = $auth->post_author;
								echo '<span>'.esc_html__('By ', 'meni').'</span>';
                            	echo '<a href="'.get_author_posts_url($authid).'" title="'.esc_attr__('View all posts by ', 'meni').get_the_author_meta('display_name', $authid).'">'.get_the_author_meta('display_name', $authid).'</a>'; ?>
                            </div>

							<div class="post-date"><?php
								$template = 'framework/templates/single/entry-date.php';
								meni_get_template( $template, $template_args ); ?></div>

                            <div class="post-comments"><?php
								if( ! post_password_required() ) {
									comments_popup_link( esc_html__('No Comments', 'meni'), esc_html__('1 Comment', 'meni'), esc_html__('% Comments', 'meni'), '', esc_html__('Comments Off', 'meni'));
								} else {
									echo esc_html__('Enter your password to view comments.', 'meni');
								} ?>
                            </div>
                        </div><?php
					echo '</div>';
				echo '</div>';
			} else {
				if( !empty( $global_breadcrumb ) ) {
					if( isset( $settings['enable-sub-title'] ) && $settings['enable-sub-title'] ) {

						$breadcrumbs = array();
						$bstyle = meni_cs_get_option( 'breadcrumb-style', 'default' );
						$separator = '<span class="'.meni_cs_get_option( 'breadcrumb-delimiter', 'fa default' ).'"></span>';

						$cat = get_the_category();
						if( $cat ) {
							$cat = $cat[0];
							$breadcrumbs[] = get_category_parents( $cat, true, $separator );
						}
						
						$breadcrumbs[] = the_title( '<span class="current">', '</span>', false );
						$bcsettings = isset( $settings['breadcrumb_background'] ) ? $settings['breadcrumb_background'] : array();
						$style = meni_breadcrumb_css( $bcsettings );

						meni_breadcrumb_output ( the_title( '<h1>', '</h1>',false ), $breadcrumbs, $bstyle, $style );
					}
				}
			}
		} else {
			if( !empty( $global_breadcrumb ) ) {
				if( isset( $settings['enable-sub-title'] ) && $settings['enable-sub-title'] ) {

					$breadcrumbs = array();
					$bstyle = meni_cs_get_option( 'breadcrumb-style', 'default' );
					$separator = '<span class="'.meni_cs_get_option( 'breadcrumb-delimiter', 'fa default' ).'"></span>';

					$cat = get_the_category();
					if( $cat ) {
						$cat = $cat[0];
						$breadcrumbs[] = get_category_parents( $cat, true, $separator );
					}

					$breadcrumbs[] = the_title( '<span class="current">', '</span>', false );
					$bcsettings = isset( $settings['breadcrumb_background'] ) ? $settings['breadcrumb_background'] : array();
					$style = meni_breadcrumb_css( $bcsettings );

					meni_breadcrumb_output ( the_title( '<h1>', '</h1>',false ), $breadcrumbs, $bstyle, $style );
				}
			}
		}
		

    ?><!-- ** Breadcrumb End ** -->

</div><!-- ** Header Wrapper - End ** -->
<!-- **Main** -->
<div id="main">

    <!-- ** Container ** -->
<div class="container">
    <!-- Secondary Left -->
    <section id="secondary-left" class="secondary-sidebar secondary-has-left-sidebar">
        <div class="type9">


	
	
		
		<?php if (is_array($productSlider)): ?>
            <aside id="text-2" class="widget widget_text">
                <div class="textwidget">
                    <div class="dt-sc-images-wrapper  " data-delay="0">
                        <div class="caroufredsel_wrapper" style="display: block; text-align: center; float: left; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 285px; height: 381px; margin: 0px; overflow: hidden; cursor: move;">
                            <ul class="dt-sc-images-carousel" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 1995px; height: 381px; z-index: auto; opacity: 1;">
                                
                                <?php foreach ($productSlider as $index => $value ): ?>
                                    <li style="width: 285px;"><img width="900" height="1200" src="<?php echo $value['slider'] ?>" class="attachment-full" alt="" srcset="<?php echo $value['slider'] ?> 900w, <?php echo $value['slider'] ?> 225w, <?php echo $value['slider'] ?> 768w, <?php echo $value['slider'] ?> 750w, <?php echo $value['slider'] ?> 540w" sizes="(max-width: 900px) 100vw, 900px"></li>
                                <?php endforeach; ?>
                               
                            </ul>
                        </div>
                        <div class="carousel-arrows">
                            <a href="JavaScript:void(0);" class="images-prev" style="display: inline-block;"> </a>
                            <a href="JavaScript:void(0);" class="images-next" style="display: inline-block;"> </a>
                        </div>
                    </div>
                    <p style="text-align: left" class="vc_custom_heading uppercase dt-sc-skin-color vc_custom_1560952143936">100% Natural</p>
                    <h5 style="text-align: left" class="vc_custom_heading vc_custom_1562837388977">Products we Use</h5>
                </div>
            </aside>
            <?php endif; ?>
            
            <?php if (is_array($beforeAfterSlider)): ?>
            <aside id="text-3" class="widget widget_text">
                <div class="textwidget">
                    <div class="dt-sc-images-wrapper  " data-delay="0">
                        <div class="caroufredsel_wrapper" style="display: block; text-align: center; float: left; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 285px; height: 222px; margin: 0px; overflow: hidden; cursor: move;">
                            <ul class="dt-sc-images-carousel" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 1995px; height: 223px; z-index: auto; opacity: 1;">
                                <?php foreach ($beforeAfterSlider as $index => $value ): ?>
                                <li style="width: 285px;"><img width="450" height="347" src="<?php echo $value['slider'] ?>" class="attachment-full" alt="" srcset="<?php echo $value['slider'] ?> 450w, <?php echo $value['slider'] ?> 300w" sizes="(max-width: 450px) 100vw, 450px"></li>
                                 <?php endforeach; ?>
                               
                            </ul>
                        </div>
                        <div class="carousel-arrows">
                            <a href="JavaScript:void(0);" class="images-prev" style="display: inline-block;"> </a>
                            <a href="JavaScript:void(0);" class="images-next" style="display: inline-block;"> </a>
                        </div>
                    </div>
                    <p style="text-align: left" class="vc_custom_heading uppercase dt-sc-skin-color vc_custom_1560952143936">Before &amp; After</p>
                    <h5 style="text-align: left" class="vc_custom_heading vc_custom_1562837388977">Treatment Results</h5>
                </div>
            </aside>
            <?php endif; ?>
            <aside id="text-4" class="widget widget_text">
                <div class="textwidget">
                    <p><img width="450" height="587" src="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/article-img.jpg" class="alignnone attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/article-img.jpg 450w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/article-img-230x300.jpg 230w" sizes="(max-width: 450px) 100vw, 450px"></p>
                    <p style="font-size: 14px;text-align: left" class="vc_custom_heading uppercase dt-sc-skin-color">Published Books</p><a href="#" target="_self" title="" class="dt-sc-button   xlarge icon-right with-icon    simple" data-delay="0" rel="noopener noreferrer"> Read Articles <span class="fa fa-chevron-right"> </span></a>
                </div>
            </aside>
            <aside id="text-5" class="widget widget_text">
                <div class="textwidget">
                    <div data-delay="0" class="vc_row wpb_row vc_row-fluid vc_custom_1561095103759">
                        <div class="dt-sc-dark-bg wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill" data-delay="0">
                            <div class="vc_column-inner vc_custom_1560949392712">
                                <div class="wpb_wrapper">
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1560950177222 vc_row-has-fill" style="text-align: center; font-size: 25px;">
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper">
                                                    <p style="font-size: 14px;text-align: center" class="vc_custom_heading uppercase vc_custom_1560950121534">Have A Question?</p>
                                                    <h3 style="text-align: center" class="vc_custom_heading vc_custom_1560949993816">Call Us Now</h3><span class="fas fa-phone fa-rotate-90  " data-delay="0"> </span>
                                                    <h5 style="text-align: center" class="vc_custom_heading vc_custom_1560950071017">00-123-456-89</h5></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </section>
    <!-- Secondary Left End -->
    <!-- Primary -->
    <section id="primary" class="page-with-sidebar with-left-sidebar">
        <!-- #post-20297 -->
        <div id="post-20297" class="post-20297 dt_procedures type-dt_procedures status-publish hentry">
            <div data-delay="0" class="vc_row wpb_row vc_row-fluid">
                <div class="rs_col-sm-12 rs_padding_override_default wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-12" data-delay="0">
                    <div class="vc_column-inner vc_custom_1561009782413">
                        <div class="wpb_wrapper">
                            <div class="wpb_single_image wpb_content_element vc_align_left   custom-img-border">

                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="450" height="658" src="<?php echo $postImage ?>" class="vc_single_image-img attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/medical-treatment.jpg 450w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/medical-treatment-205x300.jpg 205w" sizes="(max-width: 450px) 100vw, 450px"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rs_col-sm-12 wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-12" data-delay="0">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="dt-sc-title script-with-sub-title  " data-delay="0">
                                <h6 class="dt-sc-main-heading"><?php echo $heading1 ?></h6>
                                <h3 class="dt-sc-sub-heading"><?php echo $heading2 ?></h3></div>
                            <div id="1560936741021-50dfb741-2c95" class="dt-sc-empty-space" style="margin-top:50px"></div>
                            <div id="1560936887236-5b564e9e-d10b" class="dt-sc-empty-space" style="margin-bottom:30px"></div>
                            <?php echo $description ?>
                            
                            <div id="1560936887236-5b564e9e-d10b" class="dt-sc-empty-space" style="margin-top:30px"></div>
                            <h6 style="font-size: 16px;text-align: left" class="vc_custom_heading dt-sc-skin-color"><a href="#">Contact us to schedule your Treatment today!</a></h6>
                            <h6 style="font-size: 16px;text-align: left" class="vc_custom_heading dt-sc-skin-color"><a href="#">In <?php echo round($price,2); ?>Rs Only</a></h6>
                            <div id="1560936937015-b62f9d36-3521" class="dt-sc-empty-space" style="margin-top:30px"></div>
                            <form method="post" action="<?php echo home_url() ?>/appointment-ii/">
                                <input type="hidden" name="service" value="<?php echo $id ?>">
                                <input class="dt-sc-button   medium icon-right with-icon  bordered  type2" type="submit" value="Get Appointment">
                            </form>
                            
                            <div id="1561007652255-d380dc1d-6be1" class="dt-sc-empty-space" style="margin-top:30px"></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo $post->post_content; ?>
            <?php if (is_array($faq) && count($faq) > 0): ?>
                
                    <div data-delay="0" class="vc_row wpb_row vc_row-fluid vc_custom_1560941677892 vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-12" data-delay="0">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <h3 style="text-align: left" class="vc_custom_heading">Frequently Asked Questions</h3>
                                <div class="dt-sc-toggle-frame-set type2  alignright" data-delay="0">
                                <?php foreach ($faq as $faqindex => $faqdetails): ?>
                                
                                <?php if ($faqindex == 0) {
                                            $active = "active";
                                      } else {
                                            $active = "";
                                      }
                                
                                ?>
                                    <h5 class="dt-sc-toggle-accordion  <?php echo $active ?>"><a href="#"><?php echo $faqdetails['question'] ?></a></h5>
                                    <div class="dt-sc-toggle-content">
                                        <div class="block">
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p><?php echo $faqdetails['answer'] ?></p>
    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            <?php endif; ?>
        </div>
        <!-- #post-20297 -->
    </section>
    <!-- Primary End -->
</div>
    <!-- ** Container End ** -->
    
</div><!-- **Main - End ** -->    

<?php get_footer(); ?>