<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php wp_head(); ?>
	
    <script type="text/javascript">
      var AjaxURL = "<?php echo home_url(); ?>/wp-admin/admin-ajax.php";
    </script>
    
        <!-- The core Firebase JS SDK is always required and must be listed first -->
    <script src="https://www.gstatic.com/firebasejs/4.8.1/firebase.js"></script>
        
    <!-- TODO: Add SDKs for Firebase products that you want to use https://firebase.google.com/docs/web/setup#available-libraries -->
        
    <script>
      // Your web app's Firebase configuration
      var firebaseConfig = {
        apiKey: "AIzaSyCQhVa3nBZ4TV_Xd3hMow8DQq4Z_7MDGBM",
        authDomain: "instaglo-ab269.firebaseapp.com",
        databaseURL: "https://instaglo-ab269.firebaseio.com",
        projectId: "instaglo-ab269",
        storageBucket: "instaglo-ab269.appspot.com",
        messagingSenderId: "156223364377",
        appId: "1:156223364377:web:36bedba76441a8fa157988",
        measurementId: "G-37X6K6S1V8"
      };
      // Initialize Firebase
      firebase.initializeApp(firebaseConfig);
    </script>
<!-- Start of  Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=5f70eb1c-d00a-4988-a100-479cda21ce26"> </script>
<!-- End of  Zendesk Widget script -->
</head>

<body <?php body_class(); ?>>
    <?php
    
        /**
         * meni_hook_top hook.
         *      meni_hook_top - 10
         *      meni_page_loader - 20
         */
         do_action( 'meni_hook_top' );
    ?>
    
    <!-- **Wrapper** -->
    <div class="wrapper">
    
        <!-- ** Inner Wrapper ** -->
        <div class="inner-wrapper">

            <?php
                /**
                 * meni_hook_content_before hook.
                 * 
                 */
                do_action( 'meni_hook_content_before' );
            ?>