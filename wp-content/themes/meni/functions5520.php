<?php
/**
 * Theme Functions
 *
 * @package DTtheme
 * @author DesignThemes
 * @link http://wedesignthemes.com
 */
define( 'MENI_THEME_DIR', get_template_directory() );
define( 'MENI_THEME_URI', get_template_directory_uri() );

if (function_exists ('wp_get_theme')) :
	$themeData = wp_get_theme();
	define( 'MENI_THEME_NAME', $themeData->get('Name'));
	define( 'MENI_THEME_VERSION', $themeData->get('Version'));
endif;

/* ---------------------------------------------------------------------------
 * Loads Kirki
 * ---------------------------------------------------------------------------*/
require_once( MENI_THEME_DIR .'/kirki/index.php' );

/* ---------------------------------------------------------------------------
 * Loads Codestar
 * ---------------------------------------------------------------------------*/

if( !defined( 'CS_OPTION' ) ) {
	define( 'CS_OPTION', '_meni_cs_options' );
}

require_once MENI_THEME_DIR .'/cs-framework/cs-framework.php';

if( !defined( 'CS_ACTIVE_TAXONOMY' ) ) { 
	define( 'CS_ACTIVE_TAXONOMY',   false );
}

if( !defined( 'CS_ACTIVE_SHORTCODE' ) ) {
	define( 'CS_ACTIVE_SHORTCODE',  false );
}
if( !defined( 'CS_ACTIVE_CUSTOMIZE' ) ) {
	define( 'CS_ACTIVE_CUSTOMIZE',  false );
}

/* ---------------------------------------------------------------------------
 * Create function to get theme options
 * --------------------------------------------------------------------------- */
function meni_cs_get_option($key, $value = '') {

	$v = cs_get_option( $key );

	if ( !empty( $v ) ) {
		return $v;
	} else {
		return $value;
	}
}

/* ---------------------------------------------------------------------------
 * Loads Theme Textdomain
 * ---------------------------------------------------------------------------*/ 
define( 'MENI_LANG_DIR', MENI_THEME_DIR. '/languages' );
load_theme_textdomain( 'meni', MENI_LANG_DIR );

/* ---------------------------------------------------------------------------
 * Loads the Admin Panel Style
 * ---------------------------------------------------------------------------*/
function meni_admin_scripts() {
	wp_enqueue_style('meni-admin', MENI_THEME_URI .'/cs-framework-override/style.css');
}
add_action( 'admin_enqueue_scripts', 'meni_admin_scripts' );

/* ---------------------------------------------------------------------------
 * Loads Theme Functions
 * ---------------------------------------------------------------------------*/

// Functions --------------------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-functions.php' );

// Header -----------------------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-head.php' );

// Hooks ------------------------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-hooks.php' );

// Post Functions ---------------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-post-functions.php' );
new meni_post_functions;

// Widgets ----------------------------------------------------------------------
add_action( 'widgets_init', 'meni_widgets_init' );
function meni_widgets_init() {
	require_once( MENI_THEME_DIR .'/framework/register-widgets.php' );
}

// Plugins ---------------------------------------------------------------------- 
require_once( MENI_THEME_DIR .'/framework/register-plugins.php' );

// WooCommerce ------------------------------------------------------------------
if( function_exists( 'is_woocommerce' ) ){
	require_once( MENI_THEME_DIR .'/framework/woocommerce/register-woocommerce.php' );
}

// WP Store Locator -------------------------------------------------------------
if( class_exists( 'WP_Store_locator' ) ) {
	require_once( MENI_THEME_DIR .'/framework/register-storelocator.php' );
}

// Register Templates -----------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-templates.php' );

// Register Gutenberg -----------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-gutenberg-editor.php' );


// functions.php

// custom firebase short code 

add_shortcode( 'firebase_authentication', 'show_firebase_authentication_html' );

add_shortcode( 'load_additional_fields', 'load_additional_fields' );
add_shortcode( 'load_service_content', 'load_service_content' );
add_shortcode( 'load_homepage_main_services', 'load_homepage_main_services' );
add_shortcode( 'load_krisalys_women_services', 'loadWomenCategories' );
add_shortcode( 'load_krisalys_women_category_services', 'loadWomenCategoriesServices' );

add_shortcode( 'load_krisalys_men_services', 'loadMenCategories' );
add_shortcode( 'load_krisalys_men_category_services', 'loadMenCategoriesServices' );

add_shortcode( 'load_krisalys_aesthetics_services', 'loadAestheticsCategories' );
add_shortcode( 'load_krisalys_aesthetics_category_services', 'loadAestheticsCategoriesServices' );

add_shortcode( 'load_krisalys_dental_services', 'loadDentalCategories' );
add_shortcode( 'load_krisalys_dental_category_services', 'loadDentalCategoriesServices' );

add_shortcode( 'load_krisalys_cs_services', 'loadCSCategories' );
add_shortcode( 'load_krisalys_cs_category_services', 'loadCSCategoriesServices' );

add_action( 'wp_ajax_nopriv_create_customer', 'create_customer'); // User registration
add_action( 'wp_ajax_create_customer', 'create_customer');
add_action( 'wp_ajax_nopriv_create_guest_customer', 'create_guest_customer'); // User registration by guest
add_action( 'wp_ajax_create_guest_customer', 'create_guest_customer');

add_action( 'wp_ajax_nopriv_logoutuser', 'logoutuser'); // User registration by guest
add_action( 'wp_ajax_logoutuser', 'logoutuser');

add_action( 'user_register', 'autologgedin' );
//add_action( 'wp_ajax_nopriv_is_user_loggedIn', 'create_customer');
add_action( 'wp_ajax_nopriv_is_user_loggedIn', 'is_user_loggedIn');
add_action( 'wp_ajax_is_user_loggedIn', 'is_user_loggedIn');
add_action( 'wp_ajax_nopriv_signinuser', 'signinuser');
add_action( 'wp_ajax_signinuser', 'signinuser');
add_action( 'wp_ajax_signinuser', 'signinuser');

function loadCSCategoriesServices() {
    
    $slug = isset($_GET['service']) ? $_GET['service']: "";
   
    if ($slug != "") {

        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'cosmetic-surgery-tax',
                                    'field' => 'slug',
                                    'terms' => $slug
                            )
                        ),
            'post_type'=>'cosmetic-surgery',
            'posts_per_page'=>-1
           
        );
       
    } else {
       
        $args = array(
            'post_type'=>'cosmetic-surgery',
            'posts_per_page'=>3
        );

    }
   
    $postsList = get_posts($args);
    
    $html = '';
    
    if (is_array($postsList)) {
        
        
        $html .= '<div id="1561363009893-ddd2acf9-2d3f" class="dt-sc-empty-space" style="margin-top:50px;"></div>
            <h2 style="text-align: center" class="vc_custom_heading">Related Services</h2>
            <p style="text-align: center" class="vc_custom_heading">We encourage your feedback, please feel free to send us a message
                <br> using the following contact form.</p><div id="1561363016947-ee05ec60-ed26" class="dt-sc-empty-space" style="margin-top:50px;"></div>';
        
        $counter = 0;
        
        foreach ($postsList as $index => $post) {
            
            $class = "";
            
            if ($counter == 1) {
                $class = "alternate";
            } 
            
            $image = get_the_post_thumbnail_url($post->ID);

            $html .= '<div class="dt-sc-meni-package-caption '.$class.'" data-delay="">
                        <div class="dt-sc-meni-image">
                            <div class="" style="background-image:url('.$image.')"></div>
                        </div>
                        <div class="dt-sc-meni-content-wrapper">
                            <h5>'.$post->post_title.'</h5>
                            <h6>Recommended for 30’s</h6>
                            <div class="dt-sc-meni-content">Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.
                                <ul>
                                    <li>3 mls of Refyne/Defyne</li>
                                    <li>20 Units of Botox®</li>
                                    <li>Vibraderm With Infusion</li>
                                </ul>
                            </div><a class="dt-sc-button bordered small" href="'.$post->guid.'" title="View More" target=" _blank">View More</a></div>
                    </div>';
                    
            if ($counter == 1) {
                $counter--;
            } else {
                 $counter++;
            }
            

            
        }
    }
    
  echo $html;
    
}

function loadDentalCategoriesServices() {
    
    $slug = isset($_GET['service']) ? $_GET['service']: "";
   
    if ($slug != "") {

        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'dental-tax',
                                    'field' => 'slug',
                                    'terms' => $slug
                            )
                        ),
            'post_type'=>'dental',
            'posts_per_page'=>-1
           
        );
       
    } else {
       
        $args = array(
            'post_type'=>'dental',
            'posts_per_page'=>3
        );

    }
   
    $postsList = get_posts($args);
    
    $html = '';
    
    if (is_array($postsList)) {
        
        
        $html .= '<div id="1561363009893-ddd2acf9-2d3f" class="dt-sc-empty-space" style="margin-top:50px;"></div>
            <h2 style="text-align: center" class="vc_custom_heading">Related Services</h2>
            <p style="text-align: center" class="vc_custom_heading">We encourage your feedback, please feel free to send us a message
                <br> using the following contact form.</p><div id="1561363016947-ee05ec60-ed26" class="dt-sc-empty-space" style="margin-top:50px;"></div>';
        
        $counter = 0;
        
        foreach ($postsList as $index => $post) {
            
            $class = "";
            
            if ($counter == 1) {
                $class = "alternate";
            } 
            
            $image = get_the_post_thumbnail_url($post->ID);

            $html .= '<div class="dt-sc-meni-package-caption '.$class.'" data-delay="">
                        <div class="dt-sc-meni-image">
                            <div class="" style="background-image:url('.$image.')"></div>
                        </div>
                        <div class="dt-sc-meni-content-wrapper">
                            <h5>'.$post->post_title.'</h5>
                            <h6>Recommended for 30’s</h6>
                            <div class="dt-sc-meni-content">Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.
                                <ul>
                                    <li>3 mls of Refyne/Defyne</li>
                                    <li>20 Units of Botox®</li>
                                    <li>Vibraderm With Infusion</li>
                                </ul>
                            </div><a class="dt-sc-button bordered small" href="'.$post->guid.'" title="View More" target=" _blank">View More</a></div>
                    </div>';
                    
            if ($counter == 1) {
                $counter--;
            } else {
                 $counter++;
            }
            

            
        }
    }
    
  echo $html;
    
}

function loadAestheticsCategoriesServices() {
    
    $slug = isset($_GET['service']) ? $_GET['service']: "";
   
    if ($slug != "") {

        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'aesthetics-tax',
                                    'field' => 'slug',
                                    'terms' => $slug
                            )
                        ),
            'post_type'=>'aesthetics',
            'posts_per_page'=>-1
           
        );
       
    } else {
       
        $args = array(
            'post_type'=>'aesthetics',
            'posts_per_page'=>3
        );

    }
   
    $postsList = get_posts($args);
    
    $html = '';
    
    if (is_array($postsList)) {
        
        
        $html .= '<div id="1561363009893-ddd2acf9-2d3f" class="dt-sc-empty-space" style="margin-top:50px;"></div>
            <h2 style="text-align: center" class="vc_custom_heading">Related Services</h2>
            <p style="text-align: center" class="vc_custom_heading">We encourage your feedback, please feel free to send us a message
                <br> using the following contact form.</p><div id="1561363016947-ee05ec60-ed26" class="dt-sc-empty-space" style="margin-top:50px;"></div>';
        
        $counter = 0;
        
        foreach ($postsList as $index => $post) {
            
            $class = "";
            
            if ($counter == 1) {
                $class = "alternate";
            } 
            
            $image = get_the_post_thumbnail_url($post->ID);

            $html .= '<div class="dt-sc-meni-package-caption '.$class.'" data-delay="">
                        <div class="dt-sc-meni-image">
                            <div class="" style="background-image:url('.$image.')"></div>
                        </div>
                        <div class="dt-sc-meni-content-wrapper">
                            <h5>'.$post->post_title.'</h5>
                            <h6>Recommended for 30’s</h6>
                            <div class="dt-sc-meni-content">Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.
                                <ul>
                                    <li>3 mls of Refyne/Defyne</li>
                                    <li>20 Units of Botox®</li>
                                    <li>Vibraderm With Infusion</li>
                                </ul>
                            </div><a class="dt-sc-button bordered small" href="'.$post->guid.'" title="View More" target=" _blank">View More</a></div>
                    </div>';
                    
            if ($counter == 1) {
                $counter--;
            } else {
                 $counter++;
            }
            

            
        }
    }
    
  echo $html;
    
}

function loadMenCategoriesServices() {
    
    $slug = isset($_GET['service']) ? $_GET['service']: "";
   
    if ($slug != "") {

        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'krisalys-men-tax',
                                    'field' => 'slug',
                                    'terms' => $slug
                            )
                        ),
            'post_type'=>'krisalys-men',
            'posts_per_page'=>-1
           
        );
       
    } else {
       
        $args = array(
            'post_type'=>'krisalys-men',
            'posts_per_page'=>3
        );

    }
   
    $postsList = get_posts($args);
    
    $html = '';
    
    if (is_array($postsList)) {
        
        
        $html .= '<div id="1561363009893-ddd2acf9-2d3f" class="dt-sc-empty-space" style="margin-top:50px;"></div>
            <h2 style="text-align: center" class="vc_custom_heading">Related Services</h2>
            <p style="text-align: center" class="vc_custom_heading">We encourage your feedback, please feel free to send us a message
                <br> using the following contact form.</p><div id="1561363016947-ee05ec60-ed26" class="dt-sc-empty-space" style="margin-top:50px;"></div>';
        
        $counter = 0;
        
        foreach ($postsList as $index => $post) {
            
            $class = "";
            
            if ($counter == 1) {
                $class = "alternate";
            } 
            
            $image = get_the_post_thumbnail_url($post->ID);

            $html .= '<div class="dt-sc-meni-package-caption '.$class.'" data-delay="">
                        <div class="dt-sc-meni-image">
                            <div class="" style="background-image:url('.$image.')"></div>
                        </div>
                        <div class="dt-sc-meni-content-wrapper">
                            <h5>'.$post->post_title.'</h5>
                            <h6>Recommended for 30’s</h6>
                            <div class="dt-sc-meni-content">Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.
                                <ul>
                                    <li>3 mls of Refyne/Defyne</li>
                                    <li>20 Units of Botox®</li>
                                    <li>Vibraderm With Infusion</li>
                                </ul>
                            </div><a class="dt-sc-button bordered small" href="'.$post->guid.'" title="View More" target=" _blank">View More</a></div>
                    </div>';
                    
            if ($counter == 1) {
                $counter--;
            } else {
                 $counter++;
            }
            

            
        }
    }
    
  echo $html;
    
}

function loadWomenCategoriesServices() {
    
    $slug = isset($_GET['service']) ? $_GET['service']: "";
   
    if ($slug != "") {

        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'krisalys-women-tax',
                                    'field' => 'slug',
                                    'terms' => $slug
                            )
                        ),
            'post_type'=>'krisalys-women',
            'posts_per_page'=>-1
           
        );
       
    } else {
       
        $args = array(
            'post_type'=>'krisalys-women',
            'posts_per_page'=>3
        );

    }
   
    $postsList = get_posts($args);
    
    $html = '';
    
    if (is_array($postsList)) {
        
        
        $html .= '<div id="1561363009893-ddd2acf9-2d3f" class="dt-sc-empty-space" style="margin-top:50px;"></div>
            <h2 style="text-align: center" class="vc_custom_heading">Related Services</h2>
            <p style="text-align: center" class="vc_custom_heading">We encourage your feedback, please feel free to send us a message
                <br> using the following contact form.</p><div id="1561363016947-ee05ec60-ed26" class="dt-sc-empty-space" style="margin-top:50px;"></div>';
        
        $counter = 0;
        
        foreach ($postsList as $index => $post) {
            
            $class = "";
            
            if ($counter == 1) {
                $class = "alternate";
            } 
            
            $image = get_the_post_thumbnail_url($post->ID);

            $html .= '<div class="dt-sc-meni-package-caption '.$class.'" data-delay="">
                        <div class="dt-sc-meni-image">
                            <div class="" style="background-image:url('.$image.')"></div>
                        </div>
                        <div class="dt-sc-meni-content-wrapper">
                            <h5>'.$post->post_title.'</h5>
                            <h6>Recommended for 30’s</h6>
                            <div class="dt-sc-meni-content">Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi.
                                <ul>
                                    <li>3 mls of Refyne/Defyne</li>
                                    <li>20 Units of Botox®</li>
                                    <li>Vibraderm With Infusion</li>
                                </ul>
                            </div><a class="dt-sc-button bordered small" href="'.$post->guid.'" title="View More" target=" _blank">View More</a></div>
                    </div>';
                    
            if ($counter == 1) {
                $counter--;
            } else {
                 $counter++;
            }
            

            
        }
    }
    
  echo $html;
    
}

function loadDentalCategories() {
    
    $slug = isset($_GET['service']) ? $_GET['service'] : "";
    
    $parentTaxId = 0;
    
    if (!empty($slug)) {
        
        $term = get_term_by('slug', $slug, 'dental-tax');
        $parentTaxId = $term->term_id;
    }
    
    
    
    $args = array('taxonomy' => 'dental-tax',
                  'hide_empty' => false,
                  'parent' => $parentTaxId
                 );

     $customPostTaxonomies = get_terms($args);
     
     $mainCategories = array_chunk($customPostTaxonomies, ceil(count($customPostTaxonomies) / 2));
    

    $html = '<div class="vc_row wpb_row vc_inner vc_row-fluid">';
    
    
    foreach ($mainCategories as $mainIndex => $categories) {
        
        
        $html .= '<div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">';
                        
                        foreach ($categories as $index => $value) {
                            
                            $html .= '<a href="?service='.$value->slug.'"><div class="dt-sc-icon-box type15  " data-delay="0">
                                        <div class="icon-wrapper">
                                            <img width="132" height="154" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/face.png" class="attachment-full" alt="">
                                            <h4>'.$value->name.'<br>
                                            Treatment</h4>
                                        </div>
                                        <div class="icon-content"></div>
                                      </div></a>';
                            
                        }
                                        
                        
        $html .= '</div>
                    </div>
                        </div>';
        
       
        
        
    }

    
    
    $html .= '</div>';
    
    echo $html;
    
    
}

function loadCSCategories() {
    
    $slug = isset($_GET['service']) ? $_GET['service'] : "";
    
    $parentTaxId = 0;
    
    if (!empty($slug)) {
        
        $term = get_term_by('slug', $slug, 'cosmetic-surgery-tax');
        $parentTaxId = $term->term_id;
    }
    
    
    
    $args = array('taxonomy' => 'cosmetic-surgery-tax',
                  'hide_empty' => false,
                  'parent' => $parentTaxId
                 );

     $customPostTaxonomies = get_terms($args);
     
     $mainCategories = array_chunk($customPostTaxonomies, ceil(count($customPostTaxonomies) / 2));
    

    $html = '<div class="vc_row wpb_row vc_inner vc_row-fluid">';
    
    
    foreach ($mainCategories as $mainIndex => $categories) {
        
        
        $html .= '<div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">';
                        
                        foreach ($categories as $index => $value) {
                            
                            $html .= '<a href="?service='.$value->slug.'"><div class="dt-sc-icon-box type15  " data-delay="0">
                                        <div class="icon-wrapper">
                                            <img width="132" height="154" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/face.png" class="attachment-full" alt="">
                                            <h4>'.$value->name.'<br>
                                            Treatment</h4>
                                        </div>
                                        <div class="icon-content"></div>
                                      </div></a>';
                            
                        }
                                        
                        
        $html .= '</div>
                    </div>
                        </div>';
        
       
        
        
    }

    
    
    $html .= '</div>';
    
    echo $html;
    
    
}

function loadAestheticsCategories() {
    
    $slug = isset($_GET['service']) ? $_GET['service'] : "";
    
    $parentTaxId = 0;
    
    if (!empty($slug)) {
        
        $term = get_term_by('slug', $slug, 'aesthetics-tax');
        $parentTaxId = $term->term_id;
    }
    
    
    
    $args = array('taxonomy' => 'aesthetics-tax',
                  'hide_empty' => false,
                  'parent' => $parentTaxId
                 );

     $customPostTaxonomies = get_terms($args);
     
     $mainCategories = array_chunk($customPostTaxonomies, ceil(count($customPostTaxonomies) / 2));
    

    $html = '<div class="vc_row wpb_row vc_inner vc_row-fluid">';
    
    
    foreach ($mainCategories as $mainIndex => $categories) {
        
        
        $html .= '<div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">';
                        
                        foreach ($categories as $index => $value) {
                            
                            $html .= '<a href="?service='.$value->slug.'"><div class="dt-sc-icon-box type15  " data-delay="0">
                                        <div class="icon-wrapper">
                                            <img width="132" height="154" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/face.png" class="attachment-full" alt="">
                                            <h4>'.$value->name.'<br>
                                            Treatment</h4>
                                        </div>
                                        <div class="icon-content"></div>
                                      </div></a>';
                            
                        }
                                        
                        
        $html .= '</div>
                    </div>
                        </div>';
        
       
        
        
    }

    
    
    $html .= '</div>';
    
    echo $html;
    
    
}

function loadMenCategories() {
    
    $slug = isset($_GET['service']) ? $_GET['service'] : "";
    
    $parentTaxId = 0;
    
    if (!empty($slug)) {
        
        $term = get_term_by('slug', $slug, 'krisalys-men-tax');
        $parentTaxId = $term->term_id;
    }
    
    
    
    $args = array('taxonomy' => 'krisalys-men-tax',
                  'hide_empty' => false,
                  'parent' => $parentTaxId
                 );

     $customPostTaxonomies = get_terms($args);
     
     $mainCategories = array_chunk($customPostTaxonomies, ceil(count($customPostTaxonomies) / 2));
    

    $html = '<div class="vc_row wpb_row vc_inner vc_row-fluid">';
    
    
    foreach ($mainCategories as $mainIndex => $categories) {
        
        
        $html .= '<div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">';
                        
                        foreach ($categories as $index => $value) {
                            
                            $html .= '<a href="?service='.$value->slug.'"><div class="dt-sc-icon-box type15  " data-delay="0">
                                        <div class="icon-wrapper">
                                            <img width="132" height="154" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/face.png" class="attachment-full" alt="">
                                            <h4>'.$value->name.'<br>
                                            Treatment</h4>
                                        </div>
                                        <div class="icon-content"></div>
                                      </div></a>';
                            
                        }
                                        
                        
        $html .= '</div>
                    </div>
                        </div>';
        
       
        
        
    }

    
    
    $html .= '</div>';
    
    echo $html;
    
    
}

function loadWomenCategories() {
    
    $slug = isset($_GET['service']) ? $_GET['service'] : "";
    
    $parentTaxId = 0;
    
    if (!empty($slug)) {
        
        $term = get_term_by('slug', $slug, 'krisalys-women-tax');
        $parentTaxId = $term->term_id;
    }
    
    
    
    $args = array('taxonomy' => 'krisalys-women-tax',
                  'hide_empty' => false,
                  'parent' => $parentTaxId
                 );

     $customPostTaxonomies = get_terms($args);
     
     $mainCategories = array_chunk($customPostTaxonomies, ceil(count($customPostTaxonomies) / 2));
    

    $html = '<div class="vc_row wpb_row vc_inner vc_row-fluid">';
    
    
    foreach ($mainCategories as $mainIndex => $categories) {
        
        
        $html .= '<div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">';
                        
                        foreach ($categories as $index => $value) {
                            
                            $html .= '<a href="?service='.$value->slug.'"><div class="dt-sc-icon-box type15  " data-delay="0">
                                        <div class="icon-wrapper">
                                            <img width="132" height="154" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/face.png" class="attachment-full" alt="">
                                            <h4>'.$value->name.'<br>
                                            Treatment</h4>
                                        </div>
                                        <div class="icon-content"></div>
                                      </div></a>';
                            
                        }
                                        
                        
        $html .= '</div>
                    </div>
                        </div>';
        
       
        
        
    }

    
    
    $html .= '</div>';
    
    echo $html;
    
    
}

function load_homepage_main_services() {
    
    
    $mainCategories = get_option("krisalys_categories");
    
    //if (is_array($mainCategories)) {
        
        $mainCategories = array_chunk($mainCategories, ceil(count($mainCategories) / 2));
        
        
        ob_start();
        
        $counter = 1;

        foreach ($mainCategories as $index => $category) {
            
            ?>
            
                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                    
                <?php foreach ($category as $catindex => $catvalue ) { 
                
                    if($catvalue['name'] == 'krisalys-women') {
                        
                       $catvalueName = 'women-salon';
                       
                    } else if ($catvalue['name'] == 'krisalys-men') {
                        
                        $catvalueName = 'men-salon';
                        
                    } elseif ($catvalue['name'] == 'aesthetics') {
                        
                        $catvalueName = 'krisalys-aesthetics';
                    }else {
                        $catvalueName = $catvalue['name'];
                    }
                        
                ?>
                <div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="dt-sc-image-caption type4  " data-delay="0">
                                <div class="dt-sc-image-wrapper"><img width="450" height="568" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/service-img-cap1.jpg" class="attachment-full" alt="" srcset="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/service-img-cap1.jpg 450w, http://instaglo.novatoresols.online/wp-content/uploads/2019/06/service-img-cap1-238x300.jpg 238w" sizes="(max-width: 450px) 100vw, 450px"></div>
                                <div class="dt-sc-image-content">
                                    <div class="dt-sc-image-title">
                                        <h3><a href="<?php echo home_url(); ?>/<?php echo $catvalueName ?>/" target=" _blank" title="<?php echo $catvalue['label'] ?>"><?php echo ucfirst($catvalue['label']) ?></a></h3>
                                        <h6><span>0<?php echo $counter ?>.</span>Your appearance</h6></div>
                                    <p>In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede.</p>
                                    <p><a href="<?php echo home_url(); ?>/<?php echo $catvalueName ?>/" target=" _blank" title="Cosmetic" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> View Services <span class="fa fa-chevron-right"> </span></a></p>
                                </div>
                            </div>
                            <div id="1587022811374-119ee39b-d641" class="dt-sc-empty-space" style="margin-bottom:65px;"></div>
                        </div>
                    </div>
                </div>
                <?php $counter++; } ?>
            
            
            </div>
           <?php 
            
        }
        
     $html = ob_get_clean();
     
     return $html;
    //}
}

function load_service_content() {
    
    
    $servicesHTML = '';
    
    $baseURL = home_url();
    
     $args = array(
                'post_type' => 'dt_procedures',
                'posts_per_page' => -1,
                'order' => $sort_by,
                'post_status' => 'publish',
                'ignore_sticky_posts' => 1,
                );
                
    $procedures = get_posts($args);
    
    
    $servicesHTML = '<div style="margin:80px;" class="dt-sc-empty-space"></div>
<div class="vc_row wpb_row vc_inner vc_row-fluid">
    <div class="wpb_column vc_column_container vc_col-sm-6">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="dt-sc-image-caption type4  " data-delay="0">
                    <div class="dt-sc-image-wrapper"><img width="600" height="270" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/service-img1.jpg" class="attachment-full" alt="" srcset="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/service-img1.jpg 600w, http://instaglo.novatoresols.online/wp-content/uploads/2019/06/service-img1-300x135.jpg 300w, http://instaglo.novatoresols.online/wp-content/uploads/2019/06/service-img1-540x243.jpg 540w" sizes="(max-width: 600px) 100vw, 600px"></div>
                    <div class="dt-sc-image-content">
                        <div class="dt-sc-image-title">
                            <h3>
                        <a href="'.$baseURL.'/procedure/wedding-valima-makeover-expert/" target=" _blank" title="Wedding Makeover Expert">Wedding Makeover Expert</a></h3></div>
                        <p>Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum…</p>
                        <p><a href="'.$baseURL.'/procedure/wedding-valima-makeover-expert/" target=" _blank" title="Cosmetic" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> View Services <span class="fa fa-chevron-right"> </span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-6">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="dt-sc-image-caption type4  " data-delay="0">
                    <div class="dt-sc-image-wrapper"><img width="600" height="270" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-2.jpg" class="attachment-full" alt="" srcset="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-2.jpg 600w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-2-300x135.jpg 300w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-2-540x243.jpg 540w" sizes="(max-width: 600px) 100vw, 600px"></div>
                    <div class="dt-sc-image-content">
                        <div class="dt-sc-image-title">
                            <h3><a href="'.$baseURL.'/procedure/wedding-valima-makeover-star/" target=" _blank" title="Wedding Makeover Star">Wedding Makeover Star</a></h3></div>
                        <p>Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum…</p>
                        <p><a href="'.$baseURL.'/procedure/wedding-valima-makeover-star/" target=" _blank" title="Cosmetic" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> View Services <span class="fa fa-chevron-right"> </span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="margin:80px;" class="dt-sc-empty-space"></div>
<div class="vc_row wpb_row vc_inner vc_row-fluid">
    <div class="wpb_column vc_column_container vc_col-sm-6">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="dt-sc-image-caption type4  " data-delay="0">
                    <div class="dt-sc-image-wrapper"><img width="600" height="270" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-3.jpg" class="attachment-full" alt="" srcset="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-3.jpg 600w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-3-300x135.jpg 300w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-3-540x243.jpg 540w" sizes="(max-width: 600px) 100vw, 600px"></div>
                    <div class="dt-sc-image-content">
                        <div class="dt-sc-image-title">
                            <h3><a href="'.$baseURL.'/procedure/spa-silver/" target=" _blank" title="SPA Silver">SPA Silver</a></h3></div>
                        <p>Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum…</p>
                        <p><a href="'.$baseURL.'/procedure/spa-silver/" target=" _blank" title="Cosmetic" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> View Services <span class="fa fa-chevron-right"> </span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-6">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="dt-sc-image-caption type4  " data-delay="0">
                    <div class="dt-sc-image-wrapper"><img width="600" height="270" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-5.jpg" class="attachment-full" alt="" srcset="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-5.jpg 600w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-5-300x135.jpg 300w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-5-540x243.jpg 540w" sizes="(max-width: 600px) 100vw, 600px"></div>
                    <div class="dt-sc-image-content">
                        <div class="dt-sc-image-title">
                            <h3><a href="'.$baseURL.'/procedure/spa-gold/" target=" _blank" title="SPA Gold">SPA Gold</a></h3></div>
                        <p>Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum…</p>
                        <p><a href="'.$baseURL.'/procedure/spa-gold/" target=" _blank" title="Cosmetic" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> View Services <span class="fa fa-chevron-right"> </span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="margin:80px;" class="dt-sc-empty-space"></div>
</div>';

    
    
    echo $servicesHTML;
}

function load_additional_fields() {
    
    $service = isset($_POST['service']) ?$_POST['service'] : "";
    
    ?>
    <script>
        
        var serviceOptionSelected = '<?php echo $service; ?>';
        jQuery(document).ready(function() {
            
             if (serviceOptionSelected != '') {
                jQuery('select[name="service"]').find('option[value="<?php echo $service; ?>"]').attr("selected",true).change();
             }           
        });
        

    </script>
    <?
    
}

function signinuser() {

	$userEmail = isset($_POST['userEmail']) ? $_POST['userEmail'] : "";
	$userPass = isset($_POST['userPass']) ? $_POST['userPass'] : "";
	$uid = isset($_POST['uid']) ? $_POST['uid'] : "";

	$autologin_user = wp_signon(array(    'user_login'    => $userEmail,
						'user_password' => $userPass,
						'remember'      => true
					));
					
					
    if ( is_wp_error( $autologin_user ) ) {
        
        $userObject = get_user_by("email",$userEmail);
        
        if (!$userObject) {
            
    		$user_id = wp_create_user( $userEmail, $userPass, $userEmail );
    	  
    		wp_update_user(
    		  array(
    			'ID'          =>    $user_id,
    			'nickname'    =>    $userEmail,
    			'uid'		  =>    $uid
    		  )
    		);
    		
    		update_user_meta($user_id,"uid",$uid);
           
        }
        
    } else {
        
        if (!empty($uid)) {
    	    update_user_meta($user->ID,"uid",$uid);
    	}

    }
	


	return true;
}

function is_user_loggedIn() {

	if ( is_user_logged_in() ) {
		echo "true";
	} else {
		echo "false";
	}
exit;
}

function logoutuser() {
    
    wp_logout();
}

function autologgedin( $user_id ) {
    wp_set_current_user( $user_id );
    wp_set_auth_cookie( $user_id );
     
}


add_filter('pre_get_posts', 'query_post_type');
function query_post_type($query) {
    if((is_category() || is_tag()) && !is_admin()) {
        $query->set('post_type',array('krisalys-women','post'));
        return $query;
    }
}


function create_guest_customer() {

	$customerName = isset($_POST['customerName']) ? $_POST['customerName'] : "";
	$customerEmail = isset($_POST['customerEmail']) ? $_POST['customerEmail'] : "";
	$customerPass = isset($_POST['customerPass']) ? $_POST['customerPass'] : "";
	$uid = isset($_POST['uid']) ? $_POST['uid'] : "";
	$response = "";

	if( null == username_exists( $customerEmail ) ) {

		$user_id = wp_create_user( $customerEmail, $customerPass, $customerEmail );
	  
		wp_update_user(
		  array(
			'ID'          =>    $user_id,
			'nickname'    =>    $customerEmail,
			'uid'		  =>    $uid
		  )
		);
		
		update_user_meta($user_id,"uid",$uid);
	  
		$user = new WP_User( $user_id );

		$user->set_role( 'customer' );

        $curl = curl_init();
        
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/thirdparties",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => array('email' => $customerEmail,'name' => $customerName, 'code_client' => $uid,'client' => '1'),
		  CURLOPT_HTTPHEADER => array(
			"DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
	
		  ),
		));

		$response = curl_exec($curl);

        curl_close($curl);
		

	  
	  }
	

	  return true;


}


function create_customer() {

	$firstName = isset($_POST['firstName']) ? $_POST['firstName'] : "";
	$lastName = isset($_POST['lastName']) ? $_POST['lastName'] : "";
	$customerEmail = isset($_POST['customerEmail']) ? $_POST['customerEmail'] : "";
	$customerPass = isset($_POST['customerPass']) ? $_POST['customerPass'] : "";
	$uid = isset($_POST['uid']) ? $_POST['uid'] : "";
	
	$fullName = $firstName." ".$lastName;
	
	$response = "";

	if( null == username_exists( $customerEmail ) ) {

		$user_id = wp_create_user( $customerEmail, $customerPass, $customerEmail );
	  
		wp_update_user(
		  array(
			'ID'          =>    $user_id,
			'nickname'    =>    $customerEmail,
			'uid'		  =>    $uid
		  )
		);
		
		update_user_meta($user_id,"uid",$uid);
		update_user_meta($user_id,"user_nicename",$fullName);
		update_user_meta($user_id,"first_name",$firstName);
		update_user_meta($user_id,"last_name",$lastName);
		
		
	  
		$user = new WP_User( $user_id );

		$user->set_role( 'customer' );

        $curl = curl_init();
        
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/thirdparties",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => array('email' => $customerEmail,'name' => $fullName, 'code_client' => $uid,'client' => '1'),
		  CURLOPT_HTTPHEADER => array(
			"DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
	
		  ),
		));

		$response = curl_exec($curl);

        curl_close($curl);
		

	  
	  } else {


		$autologin_user = wp_signon(array(  'user_login'    => $customerEmail,
											'user_password' => $customerPass,
											'remember'      => true
										));
										
		    
 
        if ( is_wp_error( $autologin_user ) ) {
            
            $userObject = get_user_by("email",$customerEmail);
            
            if ($userObject) {
                wp_set_password($customerPass,$userObject->ID);
                update_user_meta($userObject->ID,"uid",$uid);
            }
            
            $autologin_user = wp_signon(array(  'user_login'    => $customerEmail,
											'user_password' => $customerPass,
											'remember'      => true
										));
            

            /*$curl = curl_init();
            
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/thirdparties/byEmail/$customerEmail",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
              ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            
            $response = json_decode($response);
            
            $erpuserid = $response->ref;
            
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/thirdparties/$erpuserid",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "PUT",
              CURLOPT_POSTFIELDS =>array('code_client' => $uid),
              CURLOPT_HTTPHEADER => array(
                "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
              ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);  */


        }



	  }
	  

	  return true;
	  

}


function show_firebase_authentication_html( ){
	
	ob_start();
	
	$current_user = wp_get_current_user();

	$userEmail = isset($current_user->user_email) ? $current_user->user_email : "";
	$userName = isset($current_user->display_name) ? $current_user->display_name : "";

    ?>
  
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> 
        <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script>
			var appointment_url = '<?php echo home_url(); ?>/appointment-ii/'; 
			var userEmail = '<?php echo $userEmail; ?>';
			var userName = '<?php echo $userName; ?>';
        </script>
        
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/firebase/style.css" />
        
        <div class="col-md-8 offset-2">
        
        <div class="authentication-login-form">
            <div class="" >
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="authenticationModalLabel">The Krisalys - Login or create account</h5>
                    <button onclick = "googleSignin()">SignIn <i class="fa fa-google" aria-hidden="true"></i></button>
                    
              </div>
              
              
            <div class="modal-body">
                <div id="login_div" class="main-div">
                  <input type="text" name="firstName" placeholder="First Name..." id="first_name_field" />
                  <input type="text" name="lastName" placeholder="Last Name..." id="last_name_field" />
                  <input type="email" name="email" placeholder="Email..." id="email_field" />
                  <input type="password" placeholder="Password..." id="password_field" />
                  <button onclick="signup()">Create Account</button>
                </div>
              
                <div id="user_div" class="loggedin-div">
                  
                  <input type="email" name="email" placeholder="Email..." id="login_email" />
                  <input type="password" placeholder="Password..." id="login_password" />
                  <button onclick="login()">Login</button>
                </div>
        
                <div id="loggedin-user-area" >
                  <p id="user_para" class="alert alert-success">Great! you are logged in successfully. </p>
                  
                  <button onclick="logout()">Logout</button>
                </div>
              <div class="footer-buttons">
            
                <button type="button" class="login-btn">Login</button>
                <button type="button" class="create-account-btn">Create Account</button>
            </div>
             </div>
             </div>
             </div>
             </div>
         </div>
         </div>
         
         
         
         
    

        
        <script src="<?php echo get_template_directory_uri(); ?>/js/firebase/index.js"></script>
        


    <?php
    $content = ob_get_clean();
	
	return $content;
	
	
}

//add_action('init','create_main_posts_type');
//add_action('init','load_content_form_erp');

add_action('init','loadPostTypes');

function loadPostTypes() {
    
    $mainCategories = get_option("krisalys_categories");
    
    foreach ($mainCategories as $index => $category) {
        
                    $label = $category['label'];
                    $labelPost = $category['name'];
    
                    $labels = array(
                        'name'               => $label,
                        'singular_name'      => $label,
                        'menu_name'          => $label,
                        'name_admin_bar'     => $label,
                        'add_new'            => 'Add New',
                        'add_new_item'       => 'Add New '.$label,
                        'new_item'           => 'New '.$label,
                        'edit_item'          => 'Edit '.$label,
                        'view_item'          => 'View '.$label,
                        'all_items'          => 'All '.$label,
                        'search_items'       => 'Search '.$label,
                        'not_found'          => 'No '.$label.' Found',
                        'not_found_in_trash' => 'No '.$label.' Found in Trash'
                      );
                    
                      $args = array(
                        'labels'              => $labels,
                        'public'              => true,
                        'exclude_from_search' => false,
                        'publicly_queryable'  => true,
                        'show_ui'             => true,
                        'show_in_nav_menus'   => true,
                        'show_in_menu'        => true,
                        'has_category'        => true,
                        'show_in_admin_bar'   => true,
                        'menu_position'       => 5,
                        'menu_icon'           => 'dashicons-clipboard',
                        'capability_type'     => 'post',
                        'hierarchical'        => false,
                        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields' ),
                        'has_archive'         => true,
                        'rewrite'             => array( 'slug' => $labelPost ),
                        'query_var'           => true
                      );
                    
                      register_post_type( $labelPost, $args );
                      
                      register_taxonomy("$labelPost-tax", array($labelPost), array("hierarchical" => true, "label" => "Categories", "singular_label" => "Category", "rewrite" => array( 'slug' => $labelPost, 'with_front'=> true )));
                     
    }
    
}

function load_content_form_erp() {
    

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/categories/servicesproductsundercategories",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        
        global $wpdb;
        
        $tableLocations = $wpdb->prefix."ea_locations";          
        
        $response = json_decode($response);
        
        //var_dump("<pre>",$response->data); exit;
        
        $mainCategories = array();
        
        foreach ($response->data as $index => $value) {
            
             $id = $value->id;
             $fkParent = $value->fk_parent;
             $title = $value->title;
             $image = $value->image;
             $children = $value->children;
             $childObjects = $value->childObjects;
             
             
             if ($fkParent == "0") {
                 
                    $label = ucwords(strtolower($value->title));
                    $labelPost = str_replace(" ","-",strtolower($value->title));
                    
                    $mainCategories[] = array("name" => $labelPost, "id" => $value->id, "label" => $label, "description" => $value->description);
                    
                    
                      $labels = array(
                        'name'               => $label,
                        'singular_name'      => $label,
                        'menu_name'          => $label,
                        'name_admin_bar'     => $label,
                        'add_new'            => 'Add New',
                        'add_new_item'       => 'Add New '.$label,
                        'new_item'           => 'New '.$label,
                        'edit_item'          => 'Edit '.$label,
                        'view_item'          => 'View '.$label,
                        'all_items'          => 'All '.$label,
                        'search_items'       => 'Search '.$label,
                        'not_found'          => 'No '.$label.' Found',
                        'not_found_in_trash' => 'No '.$label.' Found in Trash'
                      );
                    
                      $args = array(
                        'labels'              => $labels,
                        'public'              => true,
                        'exclude_from_search' => false,
                        'publicly_queryable'  => true,
                        'show_ui'             => true,
                        'show_in_nav_menus'   => true,
                        'show_in_menu'        => true,
                        'show_in_admin_bar'   => true,
                        'menu_position'       => 5,
                        'menu_icon'           => 'dashicons-clipboard',
                        'capability_type'     => 'post',
                        'hierarchical'        => false,
                        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields' ),
                        'has_archive'         => true,
                        'rewrite'             => array( 'slug' => $labelPost ),
                        'query_var'           => true
                      );
                    
                      register_post_type( $labelPost, $args );
                      
                      register_taxonomy("$labelPost-tax", array($labelPost), array("hierarchical" => true, "label" => "Categories", "singular_label" => "Category", "rewrite" => array( 'slug' => $labelPost, 'with_front'=> true )));
                     
                      if (is_array($children) && count($children) > 0) {
                          
                          //addChildren($labelPost,$children);
                      }
                      
                      if (!empty($childObjects)) {
                          
                         //addServicesAndProducts($labelPost,$childObjects);
                          
                      }
                            
                        $result = isLocationAlreadyExist($label);
                        
                        if (!$result) {
                            
                            $data = array("name" => $label,
                                          "address" => "The Krisalys",
                                          "location" => "Karachi"
                                        );
                            
                            
                            $error = $wpdb->insert($tableLocations,$data);
                            
                        
                        } else {
                            
                                $label = isset($label) ? $label : $result->name;
                                $address = isset($address) ? $address : $result->address;
                                $location = isset($location) ? $location : $result->location;
                             
                                
                                $data = array("name" => $label,
                                              "address"=>$address,
                                              "location" => $location
                                            );
                                    
                                $wpdb->update($tableLocations, $data, array('name'=>$label));
                            
                        }                          
                                         
             }
                 
             }
             
             update_option("krisalys_categories",$mainCategories);
            
}

function addChildren($labelPost, $children, $args = array(), $cat = '') {
    
    foreach ($children as $index => $child) {
        
        $resultTerm = get_term_by('name', $child->title, "$labelPost-tax");
        
        if (!$resultTerm) {
            $termObject = wp_insert_term( $child->title, "$labelPost-tax", $args);
            $termParentId = $termObject->term_id;
        } else {
            
            $termParentId = $resultTerm->term_id;
        } 
        
        $childrenArray = $child->children;
        
        $childObjects = $child->childObjects;
        
        if (!empty($childObjects)) {
            
                          
            //addServicesAndProducts($labelPost,$childObjects,$termParentId);
                          
        }        
 
        
        if (is_array($childrenArray) && count($childrenArray) > 0) {
            
            if ( is_wp_error( $termObject ) ) { 
                
                $result = get_term_by('name', $child->title, "$labelPost-tax");
                
                $termParentId = $result->term_id;
            
                
            } else {
                
                $termParentId = $termObject['term_id'];
            }
          
            //addChildren($labelPost,$childrenArray, array("parent" => $termParentId), $termParentId);
        
            
        }
        
        

    }

    
}

function addServicesAndProducts($labelPost,$childObjects,$category = '') {
    

    global $wpdb;
    
    foreach ($childObjects as $index => $value) {
        
            $erpId = $value->id;
            $erpLabel = $value->label;
            $erpDescription = $value->description;
            $erpPrice = $value->price;
            $erpType = $value->type;
            $erpImage = $value->image;
            
            
            $postObject = array(
              'post_title'    => wp_strip_all_tags( $erpLabel ),
              'post_content'  => $erpDescription,
              'post_status'   => 'publish',
              'post_type'     => $labelPost,
              'post_author'   => 1,
              'post_category' => $category
            );
            
            $tablename = $wpdb->prefix."posts";
            $wpTitle = wp_strip_all_tags( $erpLabel );
            
            $query = $wpdb->prepare(
                "SELECT ID FROM " . $tablename . "
                WHERE post_title = %s
                AND post_type = '".$labelPost."';",
                $wpTitle
            );
           
            $wpdb->query( $query );
            
            if ( $wpdb->num_rows ) { 
                
                $WPPostId = $wpdb->get_var( $query );
                
                update_post_meta($WPPostId,"id",$value->id);
                update_post_meta($WPPostId,"label",$value->label);
                update_post_meta($WPPostId,"description",$value->description);
                update_post_meta($WPPostId,"price",$value->price);
                update_post_meta($WPPostId,"type",$value->type);
                update_post_meta($WPPostId,"image",$value->image);
                update_post_meta($WPPostId,"term",$category);                   
                
                
            } else {
                
                $postId = wp_insert_post( $postObject );
                
                update_post_meta($postId,"id",$value->id);
                update_post_meta($postId,"label",$value->label);
                update_post_meta($postId,"description",$value->description);
                update_post_meta($postId,"price",$value->price);
                update_post_meta($postId,"type",$value->type);
                update_post_meta($postId,"image",$value->image);
                update_post_meta($postId,"term",$category);                
            }
            
            
            //wp_set_object_terms( $postId, $category, "$labelPost-tax" );
            //wp_set_post_categories($postId, $category);
     
            wp_set_post_terms( $postId, $category, "$labelPost-tax", 'true' );
            
            
    }
        
}
    



/*

function create_main_posts_type() {
    
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/categories?sortfield=t.rowid&sortorder=ASC&api_key=ZudEY99Q6GAE81phshBM3mMsqD0y17d1",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
          ),
        ));
        
        $response = curl_exec($curl);
        
        $response = json_decode($response);
        
        curl_close($curl);
        
        global $wpdb;
        
        $tableLocations = $wpdb->prefix."ea_locations";        
        
        if (!empty($response->data) && is_array($response->data)) {
            
            $mainCategories = array();
            
            foreach ($response->data as $index => $category) {
                
                
            
                if ($category->fk_parent == "0") {
                    
                    
                    
                    $label = ucwords(strtolower($category->label));
                    $labelPost = str_replace(" ","-",strtolower($category->label));
                    
                    $mainCategories[] = array("name" => $labelPost, "id" => $category->id, "label" => $label, "description" => $category->description);
                    
                    
                      $labels = array(
                        'name'               => $label,
                        'singular_name'      => $label,
                        'menu_name'          => $label,
                        'name_admin_bar'     => $label,
                        'add_new'            => 'Add New',
                        'add_new_item'       => 'Add New '.$label,
                        'new_item'           => 'New '.$label,
                        'edit_item'          => 'Edit '.$label,
                        'view_item'          => 'View '.$label,
                        'all_items'          => 'All '.$label,
                        'search_items'       => 'Search '.$label,
                        'not_found'          => 'No '.$label.' Found',
                        'not_found_in_trash' => 'No '.$label.' Found in Trash'
                      );
                    
                      $args = array(
                        'labels'              => $labels,
                        'public'              => true,
                        'exclude_from_search' => false,
                        'publicly_queryable'  => true,
                        'show_ui'             => true,
                        'show_in_nav_menus'   => true,
                        'show_in_menu'        => true,
                        'show_in_admin_bar'   => true,
                        'menu_position'       => 5,
                        'menu_icon'           => 'dashicons-clipboard',
                        'capability_type'     => 'post',
                        'hierarchical'        => false,
                        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
                        'has_archive'         => true,
                        'rewrite'             => array( 'slug' => $labelPost ),
                        'query_var'           => true
                      );
                    
                      register_post_type( $labelPost, $args );
                      
                      register_taxonomy("$labelPost-tax", array($labelPost), array("hierarchical" => true, "label" => "Categories", "singular_label" => "Category", "rewrite" => array( 'slug' => $labelPost, 'with_front'=> true )));
                     
                          
                            
                        $result = isLocationAlreadyExist($label);
                        
                        if (!$result) {
                            
                            $data = array("name" => $label,
                                          "address" => "The Krisalys",
                                          "location" => "Karachi"
                                        );
                            
                            
                            $error = $wpdb->insert($tableLocations,$data);
                            
                        
                        } else {
                            
                                $label = isset($label) ? $label : $result->name;
                                $address = isset($address) ? $address : $result->address;
                                $location = isset($location) ? $location : $result->location;
                             
                                
                                $data = array("name" => $label,
                                              "address"=>$address,
                                              "location" => $location
                                            );
                                    
                                $wpdb->update($tableLocations, $data, array('name'=>$label));
                            
                        }                          
                        
                        
                      
                    
                }
            }
            
            update_option("krisalys_categories",$mainCategories);
        }

}*/

function isLocationAlreadyExist($name) {
    
    global $wpdb;
    
    $tableLocations = $wpdb->prefix."ea_locations";  
    
    $result = $wpdb->get_results("SELECT * FROM $tableLocations WHERE lower(`name`) = '$name';");
    
    if (sizeof($result) > 0) {
        
        return $result;
   
    } else {
        
        return false;
    }

}

/********************/
/**
 * This snippet has been updated to reflect the official supporting of options pages by CMB2
 * in version 2.2.5.
 *
 * If you are using the old version of the options-page registration,
 * it is recommended you swtich to this method.
 */
add_action( 'cmb2_admin_init', 'myprefix_register_theme_options_metabox' );

/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function myprefix_register_theme_options_metabox() {

/**
* Registers options page menu item and form.
*/
	$postPages = new_cmb2_box( array(
		'id'            => 'mainposttypefields',
		'title'         => __( 'Test Metabox', 'cmb2' ),
		'object_types'  => array( 'krisalys-women', 'krisalys-men', 'cosmetic-surgery', 'aesthetics', 'dental' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	
	    
    $postPages->add_field( array(
        'name'       => __( 'Heading 1', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading1',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
    
    
    $postPages->add_field( array(
        'name'       => __( 'Heading 2', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading2',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
    
   
    $postPages->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );  
    
    
    $group_field_id = $postPages->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Before After Slider Images', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $postPages->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $postPages->add_field( array(
    	'id'          => 'products',
    	'type'        => 'group',
    	'description' => __( 'Product Slider Images', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $postPages->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );    
    
    
    
    $group_field_id = $postPages->add_field( array(
    	'id'          => 'faq',
    	'type'        => 'group',
    	'description' => __( 'Frequently Asked Questions', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Questions {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Question', 'cmb2' ),
    		'remove_button'     => __( 'Remove Question', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $postPages->add_group_field( $group_field_id, array(
        'name'    => 'Question',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'question',
        'type'    => 'text',
    ) );  
    
    $postPages->add_group_field( $group_field_id, array(
        'name'    => 'Answer',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'answer',
        'type'    => 'wysiwyg',
    ) );     
      

    /**
     * Initiate the metabox
     */
    $cmb1 = new_cmb2_box( array(
        'id'            => 'main-services-1',
        'title'         => __( 'Women Salon Options', 'cmb2' ),
        'object_types' => array( 'options-page' ),
        'option_key'      => 'krisalys_women_options',
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    // Regular text field
    $cmb1->add_field( array(
        'name'       => __( 'Women Salon Options', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
   
    $cmb1->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );
   
    $cmb1->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $cmb1->add_field( array(
    	'id'          => 'documents',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Link {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Link', 'cmb2' ),
    		'remove_button'     => __( 'Remove Link', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
      // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb1->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Title',
    	'id'   => 'title',
    	'type' => 'text',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb1->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Link',
    	'id'   => 'url',
    	'type' => 'text_url',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    
    $group_field_id = $cmb1->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $cmb1->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
  
    
    
   
    /**
     * Initiate the metabox
     */
    $cmb2 = new_cmb2_box( array(
        'id'            => 'main-services-2',
        'title'         => __( 'Men Salon Options', 'cmb2' ),
        'object_types' => array( 'options-page' ),
        'option_key'      => 'krisalys_men_options',
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    // Regular text field
    $cmb2->add_field( array(
        'name'       => __( 'Men Salon Options', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
   
    $cmb2->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );
   
    $cmb2->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $cmb2->add_field( array(
    	'id'          => 'documents',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Link {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Link', 'cmb2' ),
    		'remove_button'     => __( 'Remove Link', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
      // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb2->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Title',
    	'id'   => 'title',
    	'type' => 'text',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb2->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Link',
    	'id'   => 'url',
    	'type' => 'text_url',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    
    $group_field_id = $cmb2->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $cmb2->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
   
    /**
     * Initiate the metabox
     */
    $cmb3 = new_cmb2_box( array(
        'id'            => 'main-services-3',
        'title'         => __( 'Aesthetics Options', 'cmb2' ),
        'object_types' => array( 'options-page' ),
        'option_key'      => 'krisalys_aesthetics_options',
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );
    
    // Regular text field
    $cmb3->add_field( array(
        'name'       => __( 'Men Salon Options', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
   
    $cmb3->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );
   
    $cmb3->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $cmb3->add_field( array(
    	'id'          => 'documents',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Link {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Link', 'cmb2' ),
    		'remove_button'     => __( 'Remove Link', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
      // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb3->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Title',
    	'id'   => 'title',
    	'type' => 'text',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb3->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Link',
    	'id'   => 'url',
    	'type' => 'text_url',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    
    $group_field_id = $cmb3->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $cmb3->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );

   
   
    /**
     * Initiate the metabox
     */
    $cmb4 = new_cmb2_box( array(
        'id'            => 'main-services-4',
        'title'         => __( 'Cosmetic Surgery Options', 'cmb2' ),
        'object_types' => array( 'options-page' ),
        'option_key'      => 'krisalys_cosmetic_surgery_options',
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );
    
    // Regular text field
    $cmb4->add_field( array(
        'name'       => __( 'Men Salon Options', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
   
    $cmb4->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );
   
    $cmb4->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $cmb4->add_field( array(
    	'id'          => 'documents',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Link {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Link', 'cmb2' ),
    		'remove_button'     => __( 'Remove Link', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
      // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb4->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Title',
    	'id'   => 'title',
    	'type' => 'text',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb4->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Link',
    	'id'   => 'url',
    	'type' => 'text_url',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    
    $group_field_id = $cmb4->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $cmb4->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );    
    
    /**
     * Initiate the metabox
     */
    $cmb5 = new_cmb2_box( array(
        'id'            => 'main-services-5',
        'title'         => __( 'Dental Options', 'cmb2' ),
        'object_types' => array( 'options-page' ),
        'option_key'      => 'krisalys_dental_options',
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );    
    
    
        // Regular text field
    $cmb5->add_field( array(
        'name'       => __( 'Men Salon Options', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
   
    $cmb5->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );
   
    $cmb5->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $cmb5->add_field( array(
    	'id'          => 'documents',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Link {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Link', 'cmb2' ),
    		'remove_button'     => __( 'Remove Link', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
      // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb5->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Title',
    	'id'   => 'title',
    	'type' => 'text',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb5->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Link',
    	'id'   => 'url',
    	'type' => 'text_url',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    
    $group_field_id = $cmb5->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $cmb5->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );

 

   
   


}


