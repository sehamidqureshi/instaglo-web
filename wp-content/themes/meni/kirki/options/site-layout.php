<?php
$config = meni_kirki_config();

MENI_Kirki::add_section( 'dt_site_layout_section', array(
	'title' => esc_html__( 'Site Layout', 'meni' ),
	'priority' => 20
) );

	# site-layout
	MENI_Kirki::add_field( $config, array(
		'type'     => 'radio-image',
		'settings' => 'site-layout',
		'label'    => esc_html__( 'Site Layout', 'meni' ),
		'section'  => 'dt_site_layout_section',
		'default'  => meni_defaults('site-layout'),
		'choices' => array(
			'boxed' =>  MENI_THEME_URI.'/kirki/assets/images/site-layout/boxed.png',
			'wide' => MENI_THEME_URI.'/kirki/assets/images/site-layout/wide.png',
		)
	));

	# site-boxed-layout
	MENI_Kirki::add_field( $config, array(
		'type'     => 'switch',
		'settings' => 'site-boxed-layout',
		'label'    => esc_html__( 'Customize Boxed Layout?', 'meni' ),
		'section'  => 'dt_site_layout_section',
		'default'  => '1',
		'choices'  => array(
			'on'  => esc_attr__( 'Yes', 'meni' ),
			'off' => esc_attr__( 'No', 'meni' )
		),
		'active_callback' => array(
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
		)			
	));

	# body-bg-type
	MENI_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-type',
		'label'    => esc_html__( 'Background Type', 'meni' ),
		'section'  => 'dt_site_layout_section',
		'multiple' => 1,
		'default'  => 'none',
		'choices'  => array(
			'pattern' => esc_attr__( 'Predefined Patterns', 'meni' ),
			'upload' => esc_attr__( 'Set Pattern', 'meni' ),
			'none' => esc_attr__( 'None', 'meni' ),
		),
		'active_callback' => array(
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-pattern
	MENI_Kirki::add_field( $config, array(
		'type'     => 'radio-image',
		'settings' => 'body-bg-pattern',
		'label'    => esc_html__( 'Predefined Patterns', 'meni' ),
		'description'    => esc_html__( 'Add Background for body', 'meni' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-image' )
		),
		'choices' => array(
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern1.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern1.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern2.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern2.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern3.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern3.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern4.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern4.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern5.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern5.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern6.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern6.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern7.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern7.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern8.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern8.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern9.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern9.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern10.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern10.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern11.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern11.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern12.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern12.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern13.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern13.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern14.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern14.jpg',
			MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern15.jpg'=> MENI_THEME_URI.'/kirki/assets/images/site-layout/pattern15.jpg',
		),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => '==', 'value' => 'pattern' ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)						
	));

	# body-bg-image
	MENI_Kirki::add_field( $config, array(
		'type' => 'image',
		'settings' => 'body-bg-image',
		'label'    => esc_html__( 'Background Image', 'meni' ),
		'description'    => esc_html__( 'Add Background Image for body', 'meni' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-image' )
		),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => '==', 'value' => 'upload' ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-position
	MENI_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-position',
		'label'    => esc_html__( 'Background Position', 'meni' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-position' )
		),
		'default' => 'center',
		'multiple' => 1,
		'choices' => meni_image_positions(),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => 'contains', 'value' => array( 'pattern', 'upload') ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-repeat
	MENI_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-repeat',
		'label'    => esc_html__( 'Background Repeat', 'meni' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-repeat' )
		),
		'default' => 'repeat',
		'multiple' => 1,
		'choices' => meni_image_repeats(),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => 'contains', 'value' => array( 'pattern', 'upload' ) ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));	