<?php
function meni_kirki_config() {
	return 'meni_kirki_config';
}

function meni_defaults( $key = '' ) {
	$defaults = array();

	# site identify
	$defaults['use-custom-logo'] = '1';
	$defaults['custom-logo'] = MENI_THEME_URI.'/images/logo.png';
	$defaults['custom-light-logo'] = MENI_THEME_URI.'/images/light-logo.png';
	$defaults['site_icon'] = MENI_THEME_URI.'/images/favicon.ico';
	$defaults['custom-title-color'] = '#00000';

	# site layout
	$defaults['site-layout'] = 'wide';
	
	# site skin
	$defaults['primary-color']   	= '#7b5f43';
	$defaults['secondary-color'] 	= '#050505';
	$defaults['tertiary-color']  	= '#705539';
	$defaults['body-bg-color']  	= '#ffffff';
	$defaults['body-content-color'] = '#555555';
    $defaults['body-a-color'] 		= '#7b5f43';
	$defaults['body-a-hover-color'] = '#050505';

	# site breadcrumb
	$defaults['customize-breadcrumb-title-typo'] = '1';
	$defaults['breadcrumb-title-typo'] = array( 'font-family' => 'Montserrat',
		'variant' => '500',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '70px',
		'line-height' => 'normal',
		'letter-spacing' => '0',
		'color' => '#ffffff',
		'text-align' => 'unset',
		'text-transform' => 'none' );
	$defaults['customize-breadcrumb-typo'] = '1';
	$defaults['breadcrumb-typo'] = array( 'font-family' => 'Montserrat',
		'variant' => '500',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '18px',
		'line-height' => 'normal',
		'letter-spacing' => '3px',
		'color' => '#898989',
		'text-align' => 'unset',
		'text-transform' => 'uppercase' );

	# custom content
	$defaults['customize-custom-content-typo'] = '1';
	$defaults['custom-content-typo'] = array( 
		'font-family' => 'Montserrat',
		'font-weight' => '500' 
	);
	$defaults['customize-custom-heading-typo'] = '1';
	$defaults['custom-heading-typo'] = array( 
		'font-family' => 'Montserrat',
		'font-weight' => '500' 
	);

	# site footer
	$defaults['customize-footer-title-typo'] = '1';
	$defaults['footer-title-typo'] = array( 'font-family' => 'Montserrat',
		'variant' => '500',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '30px',
		'line-height' => 'normal',	
		'letter-spacing' => '0',
		'color' => '#050505',
		'text-align' => 'left',
		'text-transform' => 'none' );
	$defaults['customize-footer-content-typo'] = '1';
	$defaults['footer-content-typo'] = array( 'font-family' => 'Montserrat',
		'variant' => '500',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '16px',
		'line-height' => '28px',
		'letter-spacing' => '0',
		'color' => '#888888',
		'text-align' => 'left',
		'text-transform' => 'none' );	

	# site typography
	$defaults['customize-body-h1-typo'] = '1';
	$defaults['h1'] = array(
		'font-family' => 'Montserrat',
		'variant' => '300',
		'font-size' => '80px',
		'line-height' => 'normal',
		'letter-spacing' => '0',
		'color' => '#050505',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h2-typo'] = '1';
	$defaults['h2'] = array(
		'font-family' => 'Montserrat',
		'variant' => '500',
		'font-size' => '52px',
		'line-height' => 'normal',
		'letter-spacing' => '0',
		'color' => '#050505',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h3-typo'] = '1';
	$defaults['h3'] = array(
		'font-family' => 'Montserrat',
		'variant' => '500',
		'font-size' => '32px',
		'line-height' => 'normal',
		'letter-spacing' => '0',
		'color' => '#050505',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h4-typo'] = '1';
	$defaults['h4'] = array(
		'font-family' => 'Montserrat',
		'variant' => '500',
		'font-size' => '28px',
		'line-height' => 'normal',
		'letter-spacing' => '0',
		'color' => '#050505',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h5-typo'] = '1';
	$defaults['h5'] = array(
		'font-family' => 'Montserrat',
		'variant' => '500',
		'font-size' => '24px',
		'line-height' => 'normal',
		'letter-spacing' => '0',
		'color' => '#050505',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h6-typo'] = '1';
	$defaults['h6'] = array(
		'font-family' => 'Montserrat',
		'variant' => '500',
		'font-size' => '22px',
		'line-height' => 'normal',
		'letter-spacing' => '0',
		'color' => '#050505',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-content-typo'] = '1';
	$defaults['body-content-typo'] = array(
		'font-family' => 'Montserrat',
		'variant' => '500',
		'font-size' => '16px',
		'line-height' => '32px',
		'letter-spacing' => '0',
		'color' => '#555555',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);

	$defaults['footer-content-a-color'] = '';
	$defaults['footer-content-a-hover-color'] = '';
	

	if( !empty( $key ) && array_key_exists( $key, $defaults) ) {
		return $defaults[$key];
	}

	return '';
}

function meni_image_positions() {

	$positions = array( "top left" => esc_attr__('Top Left','meni'),
		"top center"    => esc_attr__('Top Center','meni'),
		"top right"     => esc_attr__('Top Right','meni'),
		"center left"   => esc_attr__('Center Left','meni'),
		"center center" => esc_attr__('Center Center','meni'),
		"center right"  => esc_attr__('Center Right','meni'),
		"bottom left"   => esc_attr__('Bottom Left','meni'),
		"bottom center" => esc_attr__('Bottom Center','meni'),
		"bottom right"  => esc_attr__('Bottom Right','meni'),
	);

	return $positions;
}

function meni_image_repeats() {

	$image_repeats = array( "repeat" => esc_attr__('Repeat','meni'),
		"repeat-x"  => esc_attr__('Repeat in X-axis','meni'),
		"repeat-y"  => esc_attr__('Repeat in Y-axis','meni'),
		"no-repeat" => esc_attr__('No Repeat','meni')
	);

	return $image_repeats;
}

function meni_border_styles() {

	$image_repeats = array(
		"none"	 => esc_attr__('None','meni'),
		"dotted" => esc_attr__('Dotted','meni'),
		"dashed" => esc_attr__('Dashed','meni'),
		"solid"	 => esc_attr__('Solid','meni'),
		"double" => esc_attr__('Double','meni'),
		"groove" => esc_attr__('Groove','meni'),
		"ridge"	 => esc_attr__('Ridge','meni'),
	);

	return $image_repeats;
}

function meni_animations() {

	$animations = array(
		'' 					 => esc_html__('Default','meni'),	
		"bigEntrance"        =>  esc_attr__("bigEntrance",'meni'),
        "bounce"             =>  esc_attr__("bounce",'meni'),
        "bounceIn"           =>  esc_attr__("bounceIn",'meni'),
        "bounceInDown"       =>  esc_attr__("bounceInDown",'meni'),
        "bounceInLeft"       =>  esc_attr__("bounceInLeft",'meni'),
        "bounceInRight"      =>  esc_attr__("bounceInRight",'meni'),
        "bounceInUp"         =>  esc_attr__("bounceInUp",'meni'),
        "bounceOut"          =>  esc_attr__("bounceOut",'meni'),
        "bounceOutDown"      =>  esc_attr__("bounceOutDown",'meni'),
        "bounceOutLeft"      =>  esc_attr__("bounceOutLeft",'meni'),
        "bounceOutRight"     =>  esc_attr__("bounceOutRight",'meni'),
        "bounceOutUp"        =>  esc_attr__("bounceOutUp",'meni'),
        "expandOpen"         =>  esc_attr__("expandOpen",'meni'),
        "expandUp"           =>  esc_attr__("expandUp",'meni'),
        "fadeIn"             =>  esc_attr__("fadeIn",'meni'),
        "fadeInDown"         =>  esc_attr__("fadeInDown",'meni'),
        "fadeInDownBig"      =>  esc_attr__("fadeInDownBig",'meni'),
        "fadeInLeft"         =>  esc_attr__("fadeInLeft",'meni'),
        "fadeInLeftBig"      =>  esc_attr__("fadeInLeftBig",'meni'),
        "fadeInRight"        =>  esc_attr__("fadeInRight",'meni'),
        "fadeInRightBig"     =>  esc_attr__("fadeInRightBig",'meni'),
        "fadeInUp"           =>  esc_attr__("fadeInUp",'meni'),
        "fadeInUpBig"        =>  esc_attr__("fadeInUpBig",'meni'),
        "fadeOut"            =>  esc_attr__("fadeOut",'meni'),
        "fadeOutDownBig"     =>  esc_attr__("fadeOutDownBig",'meni'),
        "fadeOutLeft"        =>  esc_attr__("fadeOutLeft",'meni'),
        "fadeOutLeftBig"     =>  esc_attr__("fadeOutLeftBig",'meni'),
        "fadeOutRight"       =>  esc_attr__("fadeOutRight",'meni'),
        "fadeOutUp"          =>  esc_attr__("fadeOutUp",'meni'),
        "fadeOutUpBig"       =>  esc_attr__("fadeOutUpBig",'meni'),
        "flash"              =>  esc_attr__("flash",'meni'),
        "flip"               =>  esc_attr__("flip",'meni'),
        "flipInX"            =>  esc_attr__("flipInX",'meni'),
        "flipInY"            =>  esc_attr__("flipInY",'meni'),
        "flipOutX"           =>  esc_attr__("flipOutX",'meni'),
        "flipOutY"           =>  esc_attr__("flipOutY",'meni'),
        "floating"           =>  esc_attr__("floating",'meni'),
        "hatch"              =>  esc_attr__("hatch",'meni'),
        "hinge"              =>  esc_attr__("hinge",'meni'),
        "lightSpeedIn"       =>  esc_attr__("lightSpeedIn",'meni'),
        "lightSpeedOut"      =>  esc_attr__("lightSpeedOut",'meni'),
        "pullDown"           =>  esc_attr__("pullDown",'meni'),
        "pullUp"             =>  esc_attr__("pullUp",'meni'),
        "pulse"              =>  esc_attr__("pulse",'meni'),
        "rollIn"             =>  esc_attr__("rollIn",'meni'),
        "rollOut"            =>  esc_attr__("rollOut",'meni'),
        "rotateIn"           =>  esc_attr__("rotateIn",'meni'),
        "rotateInDownLeft"   =>  esc_attr__("rotateInDownLeft",'meni'),
        "rotateInDownRight"  =>  esc_attr__("rotateInDownRight",'meni'),
        "rotateInUpLeft"     =>  esc_attr__("rotateInUpLeft",'meni'),
        "rotateInUpRight"    =>  esc_attr__("rotateInUpRight",'meni'),
        "rotateOut"          =>  esc_attr__("rotateOut",'meni'),
        "rotateOutDownRight" =>  esc_attr__("rotateOutDownRight",'meni'),
        "rotateOutUpLeft"    =>  esc_attr__("rotateOutUpLeft",'meni'),
        "rotateOutUpRight"   =>  esc_attr__("rotateOutUpRight",'meni'),
        "shake"              =>  esc_attr__("shake",'meni'),
        "slideDown"          =>  esc_attr__("slideDown",'meni'),
        "slideExpandUp"      =>  esc_attr__("slideExpandUp",'meni'),
        "slideLeft"          =>  esc_attr__("slideLeft",'meni'),
        "slideRight"         =>  esc_attr__("slideRight",'meni'),
        "slideUp"            =>  esc_attr__("slideUp",'meni'),
        "stretchLeft"        =>  esc_attr__("stretchLeft",'meni'),
        "stretchRight"       =>  esc_attr__("stretchRight",'meni'),
        "swing"              =>  esc_attr__("swing",'meni'),
        "tada"               =>  esc_attr__("tada",'meni'),
        "tossing"            =>  esc_attr__("tossing",'meni'),
        "wobble"             =>  esc_attr__("wobble",'meni'),
        "fadeOutDown"        =>  esc_attr__("fadeOutDown",'meni'),
        "fadeOutRightBig"    =>  esc_attr__("fadeOutRightBig",'meni'),
        "rotateOutDownLeft"  =>  esc_attr__("rotateOutDownLeft",'meni')
    );

	return $animations;
}

function meni_custom_fonts( $standard_fonts ){

	$custom_fonts = array();

	$fonts = cs_get_option('custom_font_fields');
	if( count( $fonts ) > 0 ):
		foreach( $fonts as $font ):
			$custom_fonts[$font['custom_font_name']] = array(
				'label' => $font['custom_font_name'],
				'variants' => array( '100', '100italic', '200', '200italic', '300', '300italic', 'regular', 'italic', '500', '500italic', '600', '600italic', '700', '700italic', '800', '800italic', '900', '900italic' ),
				'stack' => $font['custom_font_name'] . ', sans-serif'
			);
		endforeach;
	endif;

	return array_merge_recursive( $custom_fonts, $standard_fonts );
}
add_filter( 'kirki/fonts/standard_fonts', 'meni_custom_fonts', 20 );