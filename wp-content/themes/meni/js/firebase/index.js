var fbProvider = new firebase.auth.FacebookAuthProvider();
var gMailProvider = new firebase.auth.GoogleAuthProvider();
var phoneProvider = new firebase.auth.PhoneAuthProvider();
var twitterProvider = new firebase.auth.TwitterAuthProvider();


function phoneSignin() {
    
    jQuery("#loadphonedialog").click();
    
    window.recaptchaVerifier=new firebase.auth.RecaptchaVerifier('recaptcha-container');
    recaptchaVerifier.render();
}

function phoneAuth() {
    //get the number
    var number=document.getElementById('number').value;
    //phone number authentication function of firebase
    //it takes two parameter first one is number,,,second one is recaptcha
    firebase.auth().signInWithPhoneNumber(number,window.recaptchaVerifier).then(function (confirmationResult) {
        //s is in lowercase
        window.confirmationResult=confirmationResult;
        coderesult=confirmationResult;
        console.log(coderesult);
        jQuery(".step2").show();
        jQuery(".step1").hide();
        alert("Message sent");
        
    }).catch(function (error) {
        
        jQuery(".step2").hide();
        jQuery(".step1").show();
        alert(error.message);
    });
}
function codeverify() {
    var code=document.getElementById('verificationCode').value;
    coderesult.confirm(code).then(function (result) {
        alert("Successfully registered");
        var user=result.user;
        
      jQuery.ajax({

			type: 'POST',
			url: AjaxURL,
			data: {"action": "create_phone_customer", "name" : user.displayName,"phoneNumber":user.phoneNumber,"customerEmail":user.email,"customerPass":user.uid,"uid":user.uid},

			success: function(result)
			{
			    window.location.replace(appointment_url);

			}
		});        
        
        
        
        jQuery(".close").click();
    }).catch(function (error) {
        alert(error.message);
    });
}


function googleSignin() {
   
   firebase.auth().signInWithPopup(gMailProvider).then(function(result) {
      
      var token = result.credential.accessToken;
      var user = result.user;

      jQuery.ajax({

			type: 'POST',
			url: AjaxURL,
			data: {"action": "create_social_customer", "firstName" : user.displayName,"lastName":user.displayName,"customerEmail":user.email,"customerPass":user.uid,"uid":user.uid},

			success: function(result)
			{
			    window.location.replace(appointment_url);

			}
		});

   }).catch(function(error) {
      var errorCode = error.code;
      var errorMessage = error.message;
		
      console.log(error.code)
      console.log(error.message)
   });
}

function twitterSignin() {
   firebase.auth().signInWithPopup(twitterProvider).then(function(result) {
      var token = result.credential.accessToken;
      var user = result.user;
		
      console.log(token)
      console.log(user)
   }).catch(function(error) {
      console.log(error.code)
      console.log(error.message)
   });
}

function facebookSignin() {
   firebase.auth().signInWithPopup(fbProvider).then(function(result) {
      var token = result.credential.accessToken;
      var user = result.user;
		
      console.log(token)
      console.log(user)
   }).catch(function(error) {
      console.log(error.code);
      console.log(error.message);
   });
}



firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.

    document.getElementById("loggedin-user-area").style.display = "block";
    document.getElementById("login_div").style.display = "none";
    document.getElementById("user_div").style.display = "none";
    var user = firebase.auth().currentUser;
    
    console.log(user);

    if(user != null){

      var email_id = user.email;
      document.getElementById("user_para").innerHTML = "Welcome";

    }

    jQuery(".login-btn").hide();

  } else {
    // No user is signed in.

    document.getElementById("loggedin-user-area").style.display = "none";
    document.getElementById("login_div").style.display = "block";

    jQuery(".login-btn").show();

  }
});

jQuery(document).on("click",".launch-appointment",function () {


  firebase.auth().onAuthStateChanged(function(user) {

    if (user) {

        jQuery.ajax({

          type: 'POST',
          url: AjaxURL,
          data: {"action": "is_user_loggedIn"},
          success: function(result)
          {


            if (result == "true") {
              window.location.replace(appointment_url);
            } else {
              
              firebase.auth().signOut();
              jQuery(".launchfirebaseauthenticationform").click();
            }

            jQuery(".launch-appointment").html('Appointment');
    
          }
        });

    } else {

      jQuery(".launchfirebaseauthenticationform").click();
      jQuery(".launch-appointment").html('Appointment');

    }
  });
    
});

jQuery(document).on("click",".login-btn", function() {

    document.getElementById("user_div").style.display = "block";
    document.getElementById("login_div").style.display = "none";
    
    jQuery(".login-btn").hide();
    jQuery(".create-account-btn").show();

});

jQuery(document).on("click",".create-account-btn", function() {
  document.getElementById("user_div").style.display = "none";
  document.getElementById("login_div").style.display = "block";
  
  jQuery(".login-btn").show();
  jQuery(".create-account-btn").hide();
  
});

function isUserLoggedIn() {

  $status = false;

  jQuery.ajax({

      type: 'POST',
      url: AjaxURL,
      data: {"action": "is_user_loggedIn"},
      success: function(result)
      {
         $status = result;

      }
  });

  return $status;
}

function login(){

  var userEmail = document.getElementById("login_email").value;
  var userPass = document.getElementById("login_password").value;

  firebase.auth().signInWithEmailAndPassword(userEmail, userPass).then(function(user){
      
    document.getElementById("user_para").innerHTML = "Welcome : " + user.email + " please wait "+'<i class="fa fa-circle-o-notch fa-spin"></i>';
    
    jQuery.ajax({

      type: 'POST',
      url: AjaxURL,
      data: {"action": "signinuser", "userEmail":userEmail,"userPass":userPass, "uid":user.uid},
      success: function(result)
      {
        
          window.location.replace(appointment_url);
      

      }
  });
    
    //console.log('uid',user.uid);
  }).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;

    window.alert("Error : " + errorMessage);

    // ...
  });

}

function signup() {
    
    
  var firstName = document.getElementById("first_name_field").value;
  var lastName = document.getElementById("last_name_field").value;
  var customerEmail = document.getElementById("email_field").value;
  var customerPass = document.getElementById("password_field").value;

  firebase.auth().createUserWithEmailAndPassword(customerEmail, customerPass).then(function(user){
      
      document.getElementById("user_para").innerHTML = "Welcome : " + user.email + " please wait "+'<i class="fa fa-circle-o-notch fa-spin"></i>';
      
      jQuery.ajax({

			type: 'POST',
			url: AjaxURL,
			data: {"action": "create_customer", "firstName" : firstName,"lastName":lastName,"customerEmail":customerEmail,"customerPass":customerPass,"uid":user.uid},

			success: function(result)
			{
			    window.location.replace(appointment_url);

			}
		});
      
    //console.log('uid',user.uid);
  }).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;

    window.alert("Error : " + errorMessage);
    // ...
  });

}

function logout(){
    
      firebase.auth().signOut();
  
      jQuery.ajax({

          type: 'POST',
          url: AjaxURL,
          data: {"action": "logoutuser"},
          success: function(result)
          {
            
              
          
    
          }
      });
}



