<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => constant('MENI_THEME_NAME').' '.esc_html__('Options', 'meni'),
  'menu_type'       => 'theme', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'cs-framework',
  'ajax_save'       => true,
  'show_reset_all'  => false,
	'framework_title' => sprintf(esc_html__('Designthemes Framework %1$s', 'meni'), '<small>'.esc_html__('by Designthemes', 'meni').'</small>'),
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();

$options[]      = array(
  'name'        => 'general',
  'title'       => esc_html__('General', 'meni'),
  'icon'        => 'fa fa-gears',

  'fields'      => array(

	array(
	  'type'    => 'subheading',
	  'content' => esc_html__( 'General Options', 'meni' ),
	),
	
	array(
		'id'	=> 'header',
		'type'	=> 'select',
		'title'	=> esc_html__('Site Header', 'meni'),
		'class'	=> 'chosen',
		'options'	=> 'posts',
		'query_args'	=> array(
			'post_type'	=> 'dt_headers',
			'orderby'	=> 'title',
			'order'	=> 'ASC',
			'posts_per_page' => -1
		),
		'default_option'	=> esc_attr__('Select Header', 'meni'),
		'attributes'	=> array ( 'style'	=> 'width:50%'),
		'info'	=> esc_html__('Select default header.','meni'),
	),
	
	array(
		'id'	=> 'footer',
		'type'	=> 'select',
		'title'	=> esc_html__('Site Footer', 'meni'),
		'class'	=> 'chosen',
		'options'	=> 'posts',
		'query_args'	=> array(
			'post_type'	=> 'dt_footers',
			'orderby'	=> 'title',
			'order'	=> 'ASC',
			'posts_per_page' => -1
		),
		'default_option'	=> esc_attr__('Select Footer', 'meni'),
		'attributes'	=> array ( 'style'	=> 'width:50%'),
		'info'	=> esc_html__('Select defaultfooter.','meni'),
	),

	array(
		'id'      => 'use-site-loader',
		'type'    => 'switcher',
		'title'   => esc_html__('Site Loader', 'meni'),
		'info'    => esc_html__('YES! to use site loader.', 'meni'),
		'default' => true,	  
	),	

	array(
	  'id'  	 => 'show-pagecomments',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Globally Show Page Comments', 'meni'),
	  'info'	 => esc_html__('YES! to show comments on all the pages. This will globally override your "Allow comments" option under your page "Discussion" settings.', 'meni'),
	  'default'  => true,
	),

	array(
	  'id'  	 => 'showall-pagination',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Show all pages in Pagination', 'meni'),
	  'info'	 => esc_html__('YES! to show all the pages instead of dots near the current page.', 'meni'),
	  'default'  => false,
	),

	array(
	  'id'      => 'google-map-key',
	  'type'    => 'text',
	  'title'   => esc_html__('Google Map API Key', 'meni'),
	  'after' 	=> '<p class="cs-text-info">'.esc_html__('Put a valid google account api key here', 'meni').'</p>',
	),

	array(
	  'id'      => 'mailchimp-key',
	  'type'    => 'text',
	  'title'   => esc_html__('Mailchimp API Key', 'meni'),
	  'after' 	=> '<p class="cs-text-info">'.esc_html__('Put a valid mailchimp account api key here', 'meni').'</p>',
	),

	array(
	  'id'  	 => 'enable-totop',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Enable To Top', 'meni'),
	  'info'	 => esc_html__('YES! to enable to top for your site.', 'meni'),
	  'default'  => true,
	),


  ),
);

$options[]      = array(
  'name'        => 'layout_options',
  'title'       => esc_html__('Layout Options', 'meni'),
  'icon'        => 'dashicons dashicons-exerpt-view',
  'sections' => array(

	// -----------------------------------------
	// Header Options
	// -----------------------------------------
	array(
	  'name'      => 'breadcrumb_options',
	  'title'     => esc_html__('Breadcrumb Options', 'meni'),
	  'icon'      => 'fa fa-sitemap',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Breadcrumb Options", 'meni' ),
		  ),

		  array(
			'id'  		 => 'show-breadcrumb',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Breadcrumb', 'meni'),
			'info'		 => esc_html__('YES! to display breadcrumb for all pages.', 'meni'),
			'default' 	 => true,
		  ),

		  array(
			'id'           => 'breadcrumb-delimiter',
			'type'         => 'icon',
			'title'        => esc_html__('Breadcrumb Delimiter', 'meni'),
			'info'         => esc_html__('Choose delimiter style to display on breadcrumb section.', 'meni'),
		  ),

		  array(
			'id'           => 'breadcrumb-style',
			'type'         => 'select',
			'title'        => esc_html__('Breadcrumb Style', 'meni'),
			'options'      => array(
			  'default' 							=> esc_html__('Default', 'meni'),
			  'aligncenter'    						=> esc_html__('Align Center', 'meni'),
			  'alignright'  						=> esc_html__('Align Right', 'meni'),
			  'breadcrumb-left'    					=> esc_html__('Left Side Breadcrumb', 'meni'),
			  'breadcrumb-right'     				=> esc_html__('Right Side Breadcrumb', 'meni'),
			  'breadcrumb-top-right-title-center'  	=> esc_html__('Top Right Title Center', 'meni'),
			  'breadcrumb-top-left-title-center'  	=> esc_html__('Top Left Title Center', 'meni'),
			),
			'class'        => 'chosen',
			'default'      => 'aligncenter',
			'info'         => esc_html__('Choose alignment style to display on breadcrumb section.', 'meni'),
		  ),

		  array(
			  'id'                 => 'breadcrumb-position',
			  'type'               => 'select',
			  'title'              => esc_html__('Position', 'meni' ),
			  'options'            => array(
				  'header-top-absolute'    => esc_html__('Behind the Header','meni'),
				  'header-top-relative'    => esc_html__('Default','meni'),
			  ),
			  'class'        => 'chosen',
			  'default'      => 'header-top-relative',
			  'info'         => esc_html__('Choose position of breadcrumb section.', 'meni'),
		  ),

		  array(
			'id'    => 'breadcrumb_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'meni'),
			'desc'  => esc_html__('Choose background options for breadcrumb title section.', 'meni')
		  ),

		),
	),

  ),
);

$options[]      = array(
  'name'        => 'allpage_options',
  'title'       => esc_html__('All Page Options', 'meni'),
  'icon'        => 'fa fa-files-o',
  'sections' => array(

	// -----------------------------------------
	// Global Sidebar Options
	// -----------------------------------------
	array(
	  'name'      => 'global_sidebar_options',
	  'title'     => esc_html__('Global Sidebar Options', 'meni'),
	  'icon'      => 'dashicons dashicons-format-aside',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Global Sidebar Options", 'meni' ),
		  ),

		  array(
			'id'      => 'site-global-sidebar-layout',
			'type'    => 'image_select',
			'title'   => esc_html__('Global Sidebar Layout', 'meni'),
			'options' => array(
				'content-full-width' => MENI_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
				'with-left-sidebar'  => MENI_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
				'with-right-sidebar' => MENI_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
				'with-both-sidebar'  => MENI_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'    => 'content-full-width',
			'attributes' => array(
			  'data-depend-id' => 'site-global-sidebar-layout',
			),
			'desc'  => esc_html__('Choose sidebar layout for site wide.', 'meni')
		  ),
		),
	),

	// -----------------------------------------
	// Blog Archive Options
	// -----------------------------------------
	array(
	  'name'      => 'blog_archive_options',
	  'title'     => esc_html__('Blog Archive Options', 'meni'),
	  'icon'      => 'fa fa-file-archive-o',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Blog Archives Page Layout", 'meni' ),
		  ),

		  array(
			'id'      	 => 'blog-archives-page-layout',
			'type'       => 'image_select',
			'title'      => esc_html__('Page Layout', 'meni'),
			'options'    => array(
			  'content-full-width'   => MENI_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => MENI_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => MENI_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => MENI_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'with-left-sidebar',
			'attributes'   => array(
			  'data-depend-id' => 'blog-archives-page-layout',
			),
		  ),

		  array(
			'id'      	   => 'blog-post-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Post Layout', 'meni'),
			'options'      => array(
			  'entry-grid'  => MENI_THEME_URI . '/cs-framework-override/images/entry-grid.png',
			  'entry-list'  => MENI_THEME_URI . '/cs-framework-override/images/entry-list.png',			  
			  'entry-cover' => MENI_THEME_URI . '/cs-framework-override/images/entry-cover.png',
			),
			'default'      => 'entry-grid',
			'attributes'   => array(
			  'data-depend-id' => 'blog-post-layout',
			),
		  ),

		  array(
			'id'           => 'blog-post-grid-list-style',
			'type'         => 'select',
			'title'        => esc_html__('Post Style', 'meni'),
			'options'      => array(
			  'dt-sc-boxed' 			=> esc_html__('Boxed', 'meni'),
			  'dt-sc-simple'      		=> esc_html__('Simple', 'meni'),
			  'dt-sc-overlap'      		=> esc_html__('Overlap', 'meni'),
			  'dt-sc-content-overlay' 	=> esc_html__('Content Overlay', 'meni'),
			  'dt-sc-simple-withbg'		=> esc_html__('Simple with Background', 'meni'),
			  'dt-sc-overlay'   	    => esc_html__('Overlay', 'meni'),
			  'dt-sc-overlay-ii'      	=> esc_html__('Overlay II', 'meni'),			  
			  'dt-sc-overlay-iii'      	=> esc_html__('Overlay III', 'meni'),			  
			  'dt-sc-alternate'	 		=> esc_html__('Alternate', 'meni'),
			  'dt-sc-minimal'       	=> esc_html__('Minimal', 'meni'),
			  'dt-sc-modern' 	      	=> esc_html__('Modern', 'meni'),
			  'dt-sc-classic'	 		=> esc_html__('Classic', 'meni'),
			  'dt-sc-classic-ii'	 	=> esc_html__('Classic II', 'meni'),
			  'dt-sc-classic-overlay' 	=> esc_html__('Classic Overlay', 'meni'),
			  'dt-sc-grungy-boxed' 		=> esc_html__('Grungy Boxed', 'meni'),
			  'dt-sc-title-overlap'	 	=> esc_html__('Title Overlap', 'meni'),
			),
			'class'        => 'chosen',
			'default'      => 'dt-sc-classic-ii',
			'info'         => esc_html__('Choose post style to display blog archives pages.', 'meni'),
			'dependency'   => array( 'blog-post-layout', 'any', 'entry-grid,entry-list' ),
		  ),

		  array(
			'id'           => 'blog-post-cover-style',
			'type'         => 'select',
			'title'        => esc_html__('Post Style', 'meni'),
			'options'      => array(
			  'dt-sc-boxed' 			=> esc_html__('Boxed', 'meni'),
			  'dt-sc-canvas'      		=> esc_html__('Canvas', 'meni'),
			  'dt-sc-content-overlay' 	=> esc_html__('Content Overlay', 'meni'),
			  'dt-sc-overlay'   	    => esc_html__('Overlay', 'meni'),
			  'dt-sc-overlay-ii'      	=> esc_html__('Overlay II', 'meni'),
			  'dt-sc-overlay-iii'      	=> esc_html__('Overlay III', 'meni'),
			  'dt-sc-trendy' 			=> esc_html__('Trendy', 'meni'),
			  'dt-sc-mobilephone' 		=> esc_html__('Mobile Phone', 'meni'),
			),
			'class'        => 'chosen',
			'default'      => 'dt-sc-boxed',
			'info'         => esc_html__('Choose post style to display blog archives pages.', 'meni'),
			'dependency'   => array( 'blog-post-layout', '==', 'entry-cover' ),
		  ),

		  array(
			'id'      	   => 'blog-post-columns',
			'type'         => 'image_select',
			'title'        => esc_html__('Columns', 'meni'),
			'options'      => array(
			  'one-column' 		  => MENI_THEME_URI . '/cs-framework-override/images/one-column.png',
			  'one-half-column'   => MENI_THEME_URI . '/cs-framework-override/images/one-half-column.png',
			  'one-third-column'  => MENI_THEME_URI . '/cs-framework-override/images/one-third-column.png',
			  //'one-fourth-column' => MENI_THEME_URI . '/cs-framework-override/images/one-fourth-column.png',
			),
			'default'      => 'one-third-column',
			'attributes'   => array(
			  'data-depend-id' => 'blog-post-columns',
			),
			'dependency' => array( 'blog-post-layout', 'any', 'entry-grid,entry-cover' ),
		  ),

		  array(
			'id'      	   => 'blog-list-thumb',
			'type'         => 'image_select',
			'title'        => esc_html__('List Type', 'meni'),
			'options'      => array(
			  'entry-left-thumb'  => MENI_THEME_URI . '/cs-framework-override/images/entry-left-thumb.png',
			  'entry-right-thumb' => MENI_THEME_URI . '/cs-framework-override/images/entry-right-thumb.png',
			),
			'default'      => 'entry-left-thumb',
			'attributes'   => array(
			  'data-depend-id' => 'blog-list-thumb',
			),
			'dependency' => array( 'blog-post-layout', '==', 'entry-list' ),
		  ),

		  array(
			'id'           => 'blog-alignment',
			'type'         => 'select',
			'title'        => esc_html__('Elements Alignment', 'meni'),
			'options'      => array(
			  'alignnone'	=> esc_html__('None', 'meni'),
			  'alignleft' 	=> esc_html__('Align Left', 'meni'),
			  'aligncenter' => esc_html__('Align Center', 'meni'),
			  'alignright'  => esc_html__('Align Right', 'meni'),
			),
			'class'        => 'chosen',
			'default'      => 'alignnone',
			'info'         => esc_html__('Choose alignment to display archives pages.', 'meni'),
			'dependency'   => array( 'blog-post-layout', 'any', 'entry-grid,entry-cover' ),
		  ),

		  array(
			'id'         => 'enable-equal-height',
			'type'       => 'switcher',
			'title'      => esc_html__('Enable Equal Height', 'meni'),
			'info'       => esc_html__('YES! to items display as equal height', 'meni'),
			'dependency' => array( 'blog-post-layout', 'any', 'entry-grid,entry-cover' ),
			'default'    => false,
		  ),

		  array(
			'id'         => 'enable-no-space',
			'type'       => 'switcher',
			'title'      => esc_html__('Enable No Space', 'meni'),
			'info'       => esc_html__('YES! to items display as no space', 'meni'),
			'dependency' => array( 'blog-post-layout', 'any', 'entry-grid,entry-cover' ),
			'default'    => false,
		  ),

		  array(
			'id'  		 => 'enable-gallery-slider',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Display Gallery Slider', 'meni'),
			'info'		 => esc_html__('YES! to display gallery slider', 'meni'),
			'default'    => false,
			'dependency' => array( 'blog-post-layout', 'any', 'entry-grid,entry-list' ),
		  ),

		  array(
			'id'             => 'blog-elements-position',
			'type'           => 'sorter',
			'title'          => esc_html__('Elements Positioning', 'meni'),
			'default'        => array(
			  'enabled'      => array(
				'feature_image'	=> esc_html__('Feature Image', 'meni'),
				'date'     		=> esc_html__('Date', 'meni'),
				'title'      	=> esc_html__('Title', 'meni'),
				'content'    	=> esc_html__('Content', 'meni'),
				'read_more'  	=> esc_html__('Read More', 'meni'),
			  ),
			  'disabled'     => array(
				'meta_group' 	=> esc_html__('Meta Group', 'meni'),
				'author'		=> esc_html__('Author', 'meni'),
				'comments' 		=> esc_html__('Comments', 'meni'),
				'categories'    => esc_html__('Categories', 'meni'),
				'tags'  		=> esc_html__('Tags', 'meni'),
				'social_share'  => esc_html__('Social Share', 'meni'),
				'likes_views'   => esc_html__('Likes & Views', 'meni'),
			  ),
			),
			'enabled_title'  	=> esc_html__('Active Elements', 'meni'),
			'disabled_title' 	=> esc_html__('Deactive Elements', 'meni'),
		  ),

		  array(
			'id'             => 'blog-meta-position',
			'type'           => 'sorter',
			'title'          => esc_html__('Meta Group Positioning', 'meni'),
			'default'        => array(
			  'enabled'      => array(
					'author'		=> esc_html__('Author', 'meni'),
					'date'     		=> esc_html__('Date', 'meni'),
			  ),
			  'disabled'     => array(
					'comments' 		=> esc_html__('Comments', 'meni'),
					'categories'    => esc_html__('Categories', 'meni'),					
					'tags'  		=> esc_html__('Tags', 'meni'),
					'social_share'  => esc_html__('Social Share', 'meni'),
					'likes_views'   => esc_html__('Likes & Views', 'meni'),
			  ),
			),
			'enabled_title'  => esc_html__('Active Items', 'meni'),
			'disabled_title' => esc_html__('Deactive Items', 'meni'),
			'desc'  		 => esc_html__('Note: Use max 3 items for better results.', 'meni')
		  ),

		  array(
			'id'  		 => 'enable-post-format',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Enable Post Format', 'meni'),
			'info'		 => esc_html__('YES! to display post format icon', 'meni'),
			'default'    => false,
		  ),

		  array(
			'id'  		 => 'enable-excerpt-text',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Enable Excerpt Text', 'meni'),
			'info'		 => esc_html__('YES! to display excerpt content', 'meni'),
			'default'    => true,
		  ),

		  array(
			'id'  		 => 'blog-excerpt-length',
			'type'  	 => 'number',
			'title' 	 => esc_html__('Excerpt Length', 'meni'),
			'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('Put Excerpt Length', 'meni').'</span>',
			'default' 	 => 15,
			'dependency' => array( 'enable-excerpt-text', '==', 'true' ),
		  ),

		  array(
			'id'  		 => 'enable-video-audio',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Display Video & Audio for Posts', 'meni'),
			'info'		 => esc_html__('YES! to display video & audio, instead of feature image for posts', 'meni'),
			'dependency' => array( 'blog-post-layout', 'any', 'entry-grid,entry-list' ),
			'default'    => false,			
		  ),

		  array(
			'id'  		 => 'blog-readmore-text',
			'type'  	 => 'text',
			'title' 	 => esc_html__('Read More Text', 'meni'),
			'info'		 => esc_html__('Put the read more text here', 'meni'),
			'default'	 => esc_html__('Read More', 'meni')
		  ),

		  array(
			'id'           => 'blog-image-hover-style',
			'type'         => 'select',
			'title'        => esc_html__('Image Hover Style', 'meni'),
			'options'      => array(
			  'dt-sc-default' 			=> esc_html__('Default', 'meni'),
			  'dt-sc-blur'      		=> esc_html__('Blur', 'meni'),
			  'dt-sc-bw'   		   		=> esc_html__('Black and White', 'meni'),
			  'dt-sc-brightness'	 	=> esc_html__('Brightness', 'meni'),
			  'dt-sc-fadeinleft'   	    => esc_html__('Fade In Left', 'meni'),
			  'dt-sc-fadeinright'  	    => esc_html__('Fade In Right', 'meni'),
			  'dt-sc-hue-rotate'   	    => esc_html__('Hue-Rotate', 'meni'),
			  'dt-sc-invert'	   	    => esc_html__('Invert', 'meni'),
			  'dt-sc-opacity'   	    => esc_html__('Opacity', 'meni'),
			  'dt-sc-rotate'	   	    => esc_html__('Rotate', 'meni'),
			  'dt-sc-rotate-alt'   	    => esc_html__('Rotate Alt', 'meni'),
			  'dt-sc-scalein'   	    => esc_html__('Scale In', 'meni'),
			  'dt-sc-scaleout' 	    	=> esc_html__('Scale Out', 'meni'),
			  'dt-sc-sepia'	   	    	=> esc_html__('Sepia', 'meni'),
			  'dt-sc-tint'		   	    => esc_html__('Tint', 'meni'),
			),
			'class'        => 'chosen',
			'default'      => 'dt-sc-fadeinleft',
			'info'         => esc_html__('Choose image hover style to display archives pages.', 'meni'),
		  ),

		  array(
			'id'           => 'blog-image-overlay-style',
			'type'         => 'select',
			'title'        => esc_html__('Image Overlay Style', 'meni'),
			'options'      => array(
			  'dt-sc-default' 			=> esc_html__('None', 'meni'),
			  'dt-sc-fixed' 			=> esc_html__('Fixed', 'meni'),
			  'dt-sc-tb' 				=> esc_html__('Top to Bottom', 'meni'),
			  'dt-sc-bt'   				=> esc_html__('Bottom to Top', 'meni'),
			  'dt-sc-rl'   				=> esc_html__('Right to Left', 'meni'),
			  'dt-sc-lr'				=> esc_html__('Left to Right', 'meni'),
			  'dt-sc-middle'			=> esc_html__('Middle', 'meni'),
			  'dt-sc-middle-radial'		=> esc_html__('Middle Radial', 'meni'),
			  'dt-sc-tb-gradient' 		=> esc_html__('Gradient - Top to Bottom', 'meni'),
			  'dt-sc-bt-gradient'   	=> esc_html__('Gradient - Bottom to Top', 'meni'),
			  'dt-sc-rl-gradient'   	=> esc_html__('Gradient - Right to Left', 'meni'),
			  'dt-sc-lr-gradient'		=> esc_html__('Gradient - Left to Right', 'meni'),
			  'dt-sc-radial-gradient'	=> esc_html__('Gradient - Radial', 'meni'),
			  'dt-sc-flash' 			=> esc_html__('Flash', 'meni'),
			  'dt-sc-circle' 			=> esc_html__('Circle', 'meni'),
			  'dt-sc-hm-elastic'		=> esc_html__('Horizontal Elastic', 'meni'),
			  'dt-sc-vm-elastic'		=> esc_html__('Vertical Elastic', 'meni'),
			),
			'class'        => 'chosen',
			'default'      => 'dt-sc-default',
			'info'         => esc_html__('Choose image overlay style to display archives pages.', 'meni'),
			'dependency' => array( 'blog-post-layout', 'any', 'entry-grid,entry-list' ),
		  ),

		  array(
			'id'           => 'blog-pagination',
			'type'         => 'select',
			'title'        => esc_html__('Pagination Style', 'meni'),
			'options'      => array(
			  'older_newer' 	=> esc_html__('Older & Newer', 'meni'),
			  'numbered'      	=> esc_html__('Numbered', 'meni'),
			  'load_more'      	=> esc_html__('Load More', 'meni'),
			  'infinite_scroll'	=> esc_html__('Infinite Scroll', 'meni'),
			),
			'class'        => 'chosen',
			'default'      => 'numbered',
			'info'         => esc_html__('Choose pagination style to display archives pages.', 'meni'),
		  ),

		),
	),

	// -----------------------------------------
	// Blog Single Options
	// -----------------------------------------
	array(
	  'name'      => 'blog_single_options',
	  'title'     => esc_html__('Blog Single Options', 'meni'),
	  'icon'      => 'fa fa-thumb-tack',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Blog Single Post Options", 'meni' ),
		  ),

		  array(
			'id'             => 'post-elements-position',
			'type'           => 'sorter',
			'title'          => esc_html__('Post Elements Positioning', 'meni'),
			'default'        => array(
			  'enabled'      => array(
				'content'    	=> esc_html__('Content', 'meni'),
				'meta_group' 	=> esc_html__('Meta Group', 'meni'),
				'navigation'    => esc_html__('Navigation', 'meni'),
				'comment_box' 	=> esc_html__('Comment Box', 'meni'),
			  ),
			  'disabled'     => array(
				'author_bio' 	=> esc_html__('Author Bio', 'meni'),
				'feature_image'	=> esc_html__('Feature Image', 'meni'),
				'title'      	=> esc_html__('Title', 'meni'),
				'author'		=> esc_html__('Author', 'meni'),
				'date'     		=> esc_html__('Date', 'meni'),
				'comments' 		=> esc_html__('Comments', 'meni'),
				'tags'  		=> esc_html__('Tags', 'meni'),
				'social_share'  => esc_html__('Social Share', 'meni'),
				'likes_views'   => esc_html__('Likes & Views', 'meni'),
				'related_posts' => esc_html__('Related Posts', 'meni'),
				'related_article' 	=> esc_html__('Related Article( Only Fixed )', 'meni'),
			  ),
			),
			'enabled_title'  => esc_html__('Active Elements', 'meni'),
			'disabled_title' => esc_html__('Deactive Elements', 'meni'),
		  ),

		  array(
			'id'             => 'post-meta-position',
			'type'           => 'sorter',
			'title'          => esc_html__('Meta Group Positioning', 'meni'),
			'default'        => array(
			  'enabled'      => array(
				'tags'  		=> esc_html__('Tags', 'meni'),
				'categories'    => esc_html__('Categories', 'meni')
			  ),
			  'disabled'     => array(
				'date'     		=> esc_html__('Date', 'meni'),
				'author'		=> esc_html__('Author', 'meni'),
				'comments' 		=> esc_html__('Comments', 'meni'),
				'social_share'  => esc_html__('Social Share', 'meni'),
				'likes_views'   => esc_html__('Likes & Views', 'meni'),
			  ),
			),
			'enabled_title'  => esc_html__('Active Items', 'meni'),
			'disabled_title' => esc_html__('Deactive Items', 'meni'),
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Related Post Options", 'meni' ),
		  ),

		  array(
			'id'  		 => 'post-related-title',
			'type'  	 => 'text',
			'title' 	 => esc_html__('Related Posts Section Title', 'meni'),
			'info'		 => esc_html__('Put the related posts section title here', 'meni'),
			'default'	 => esc_html__('Related Posts', 'meni')
		  ),

		  array(
			'id'      	   => 'post-related-columns',
			'type'         => 'image_select',
			'title'        => esc_html__('Columns', 'meni'),
			'options'      => array(
			  'one-column' 		  => MENI_THEME_URI . '/cs-framework-override/images/one-column.png',
			  'one-half-column'   => MENI_THEME_URI . '/cs-framework-override/images/one-half-column.png',
			  'one-third-column'  => MENI_THEME_URI . '/cs-framework-override/images/one-third-column.png',
			  //'one-fourth-column' => MENI_THEME_URI . '/cs-framework-override/images/one-fourth-column.png',
			),
			'default'      => 'one-third-column',
			'attributes'   => array(
			  'data-depend-id' => 'post-related-columns',
			),
		  ),

		  array(
			'id'  		 => 'post-related-count',
			'type'  	 => 'number',
			'title' 	 => esc_html__('No.of Posts to Show', 'meni'),
			'info'		 => esc_html__('Put the no.of related posts to show', 'meni'),
			'default'	 => 3
		  ),

		  array(
			'id'  		 => 'enable-related-excerpt',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Enable Excerpt Text', 'meni'),
			'info'		 => esc_html__('YES! to display excerpt content', 'meni'),
		  ),

		  array(
			'id'  		 => 'post-related-excerpt',
			'type'  	 => 'number',
			'title' 	 => esc_html__('Excerpt Length', 'meni'),
			'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('Put Excerpt Length', 'meni').'</span>',
			'default' 	 => 25,
			'dependency' => array( 'enable-related-excerpt', '==', 'true' ),
		  ),

		  array(
			'id'  		 => 'enable-related-carousel',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Enable Carousel', 'meni'),
			'info'		 => esc_html__('YES! to enable carousel related posts', 'meni'),
		  ),

		  array(
			'id'           => 'related-carousel-nav',
			'type'         => 'select',
			'title'        => esc_html__('Navigation Style', 'meni'),
			'options'      => array(
			  '' 			=> esc_html__('None', 'meni'),
			  'navigation'  => esc_html__('Navigations', 'meni'),
			  'pager'   	=> esc_html__('Pager', 'meni'),
			),
			'class'        => 'chosen',
			'default'      => 'text',
			'info'         => esc_html__('Choose navigation style to display related post carousel.', 'meni'),
			'dependency' => array( 'enable-related-carousel', '==', 'true' ),
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Miscellaneous Post Options", 'meni' ),
		  ),

		  array(
			'id'  		 => 'enable-image-lightbox',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Feature Image Lightbox', 'meni'),
			'info'		 => esc_html__('YES! to enable lightbox for feature image.', 'meni'),
		  ),

		  array(
			'id'           => 'post-comments-list-style',
			'type'         => 'select',
			'title'        => esc_html__('Comments List Style', 'meni'),
			'options'      => array(
			  'rounded' 	=> esc_html__('Rounded', 'meni'),
			  'square'   	=> esc_html__('Square', 'meni'),
			),
			'class'        => 'chosen',
			'default'      => 'rounded',
			'info'         => esc_html__('Choose comments list style to display single post.', 'meni'),
		  ),
		),
	),

	// -----------------------------------------
	// 404 Options
	// -----------------------------------------
	array(
	  'name'      => '404_options',
	  'title'     => esc_html__('404 Options', 'meni'),
	  'icon'      => 'fa fa-warning',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "404 Message", 'meni' ),
		  ),
		  
		  array(
			'id'      => 'enable-404message',
			'type'    => 'switcher',
			'title'   => esc_html__('Enable Message', 'meni' ),
			'info'	  => esc_html__('YES! to enable not-found page message.', 'meni'),
			'default' => true
		  ),

		  array(
			'id'           => 'notfound-style',
			'type'         => 'select',
			'title'        => esc_html__('Template Style', 'meni'),
			'options'      => array(
			  'type1' 	   => esc_html__('Modern', 'meni'),
			  'type2'      => esc_html__('Classic', 'meni'),
			  'type4'  	   => esc_html__('Diamond', 'meni'),
			  'type5'      => esc_html__('Shadow', 'meni'),
			  'type6'      => esc_html__('Diamond Alt', 'meni'),
			  'type7'  	   => esc_html__('Stack', 'meni'),
			  'type8'  	   => esc_html__('Minimal', 'meni'),
			),
			'class'        => 'chosen',
			'default'      => 'type5',
			'info'         => esc_html__('Choose the style of not-found template page.', 'meni')
		  ),

		  array(
			'id'      => 'notfound-darkbg',
			'type'    => 'switcher',
			'title'   => esc_html__('404 Dark BG', 'meni' ),
			'info'	  => esc_html__('YES! to use dark bg notfound page for this site.', 'meni'),
			'default' => true			
		  ),

		  array(
			'id'           => 'notfound-pageid',
			'type'         => 'select',
			'title'        => esc_html__('Custom Page', 'meni'),
			'options'      => 'pages',
			'class'        => 'chosen',
			'default_option' => esc_html__('Choose the page', 'meni'),
			'info'       	 => esc_html__('Choose the page for not-found content.', 'meni')
		  ),
		  
		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Background Options", 'meni' ),
		  ),

		  array(
			'id'    => 'notfound_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'meni'),
			'default' => array( "color"=>"#000000" ) 
		  ),

		  array(
			'id'  		 => 'notfound-bg-style',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Custom Styles', 'meni'),
			'info'		 => esc_html__('Paste custom CSS styles for not found page.', 'meni')
		  ),

		),
	),

	// -----------------------------------------
	// Underconstruction Options
	// -----------------------------------------
	array(
	  'name'      => 'comingsoon_options',
	  'title'     => esc_html__('Under Construction Options', 'meni'),
	  'icon'      => 'fa fa-thumbs-down',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Under Construction", 'meni' ),
		  ),
	
		  array(
			'id'      => 'enable-comingsoon',
			'type'    => 'switcher',
			'title'   => esc_html__('Enable Coming Soon', 'meni' ),
			'info'	  => esc_html__('YES! to check under construction page of your website.', 'meni')
		  ),
	
		  array(
			'id'           => 'comingsoon-style',
			'type'         => 'select',
			'title'        => esc_html__('Template Style', 'meni'),
			'options'      => array(
			  'type1' 	   => esc_html__('Diamond', 'meni'),
			  'type2'      => esc_html__('Teaser', 'meni'),
			  'type3'  	   => esc_html__('Minimal', 'meni'),
			  'type4'      => esc_html__('Counter Only', 'meni'),
			  'type5'      => esc_html__('Belt', 'meni'),
			  'type6'  	   => esc_html__('Classic', 'meni'),
			  'type7'  	   => esc_html__('Boxed', 'meni')
			),
			'class'        => 'chosen',
			'default'      => 'type1',
			'info'         => esc_html__('Choose the style of coming soon template.', 'meni'),
		  ),

		  array(
			'id'      => 'uc-darkbg',
			'type'    => 'switcher',
			'title'   => esc_html__('Coming Soon Dark BG', 'meni' ),
			'info'	  => esc_html__('YES! to use dark bg coming soon page for this site.', 'meni'),
			'default' => true
		  ),

		  array(
			'id'           => 'comingsoon-pageid',
			'type'         => 'select',
			'title'        => esc_html__('Custom Page', 'meni'),
			'options'      => 'pages',
			'class'        => 'chosen',
			'default_option' => esc_html__('Choose the page', 'meni'),
			'info'       	 => esc_html__('Choose the page for comingsoon content.', 'meni')
		  ),

		  array(
			'id'      => 'show-launchdate',
			'type'    => 'switcher',
			'title'   => esc_html__('Show Launch Date', 'meni' ),
			'info'	  => esc_html__('YES! to show launch date text.', 'meni'),
		  ),

		  array(
			'id'      => 'comingsoon-launchdate',
			'type'    => 'text',
			'title'   => esc_html__('Launch Date', 'meni'),
			'attributes' => array( 
			  'placeholder' => '10/30/2016 12:00:00'
			),
			'after' 	=> '<p class="cs-text-info">'.esc_html__('Put Format: 12/30/2016 12:00:00 month/day/year hour:minute:second', 'meni').'</p>',
		  ),

		  array(
			'id'           => 'comingsoon-timezone',
			'type'         => 'select',
			'title'        => esc_html__('UTC Timezone', 'meni'),
			'options'      => array(
			  '-12' => '-12', '-11' => '-11', '-10' => '-10', '-9' => '-9', '-8' => '-8', '-7' => '-7', '-6' => '-6', '-5' => '-5', 
			  '-4' => '-4', '-3' => '-3', '-2' => '-2', '-1' => '-1', '0' => '0', '+1' => '+1', '+2' => '+2', '+3' => '+3', '+4' => '+4',
			  '+5' => '+5', '+6' => '+6', '+7' => '+7', '+8' => '+8', '+9' => '+9', '+10' => '+10', '+11' => '+11', '+12' => '+12'
			),
			'class'        => 'chosen',
			'default'      => '0',
			'info'         => esc_html__('Choose utc timezone, by default UTC:00:00', 'meni'),
		  ),

		  array(
			'id'    => 'comingsoon_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'meni'),
			'default' => array( "color"=>"#000000" ) 			
		  ),

		  array(
			'id'  		 => 'comingsoon-bg-style',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Custom Styles', 'meni'),
			'info'		 => esc_html__('Paste custom CSS styles for under construction page.', 'meni'),
		  ),

		),
	),

  ),
);

// -----------------------------------------
// Widget area Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'widgetarea_options',
  'title'       => esc_html__('Widget Area', 'meni'),
  'icon'        => 'fa fa-trello',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Custom Widget Area for Sidebar", 'meni' ),
	  ),

	  array(
		'id'           => 'wtitle-style',
		'type'         => 'select',
		'title'        => esc_html__('Sidebar widget Title Style', 'meni'),
		'options'      => array(
		  'type1' 	   => esc_html__('Double Border', 'meni'),
		  'type2'      => esc_html__('Tooltip', 'meni'),
		  'type3'  	   => esc_html__('Title Top Border', 'meni'),
		  'type4'      => esc_html__('Left Border & Pattren', 'meni'),
		  'type5'      => esc_html__('Bottom Border', 'meni'),
		  'type6'  	   => esc_html__('Tooltip Border', 'meni'),
		  'type7'  	   => esc_html__('Boxed Modern', 'meni'),
		  'type8'  	   => esc_html__('Elegant Border', 'meni'),
		  'type9' 	   => esc_html__('Needle', 'meni'),
		  'type10' 	   => esc_html__('Ribbon', 'meni'),
		  'type11' 	   => esc_html__('Content Background', 'meni'),
		  'type12' 	   => esc_html__('Classic BG', 'meni'),
		  'type13' 	   => esc_html__('Tiny Boders', 'meni'),
		  'type14' 	   => esc_html__('BG & Border', 'meni'),
		  'type15' 	   => esc_html__('Classic BG Alt', 'meni'),
		  'type16' 	   => esc_html__('Left Border & BG', 'meni'),
		  'type17' 	   => esc_html__('Basic', 'meni'),
		  'type18' 	   => esc_html__('BG & Pattern', 'meni'),
		),
		'class'          => 'chosen',
		'default_option' => esc_html__('Choose any type', 'meni'),
		'default' => 'type9',
		'info'           => esc_html__('Choose the style of sidebar widget title.', 'meni')
	  ),

	  array(
		'id'              => 'widgetarea-custom',
		'type'            => 'group',
		'title'           => esc_html__('Custom Widget Area', 'meni'),
		'button_title'    => esc_html__('Add New', 'meni'),
		'accordion_title' => esc_html__('Add New Widget Area', 'meni'),
		'fields'          => array(

		  array(
			'id'          => 'widgetarea-custom-name',
			'type'        => 'text',
			'title'       => esc_html__('Name', 'meni'),
		  ),

		)
	  ),

	),
);

// -----------------------------------------
// Sociable Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'sociable_options',
  'title'       => esc_html__('Sociable', 'meni'),
  'icon'        => 'fa fa-share-square',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Sociable", 'meni' ),
	  ),

	  array(
		'id'              => 'sociable_fields',
		'type'            => 'group',
		'title'           => esc_html__('Sociable', 'meni'),
		'info'            => esc_html__('Click button to add type of social & url.', 'meni'),
		'button_title'    => esc_html__('Add New Social', 'meni'),
		'accordion_title' => esc_html__('Adding New Social Field', 'meni'),
		'fields'          => array(
		  array(
			'id'          => 'sociable_fields_type',
			'type'        => 'select',
			'title'       => esc_html__('Select Social', 'meni'),
			'options'      => array(
			  'delicious' 	 => esc_html__('Delicious', 'meni'),
			  'deviantart' 	 => esc_html__('Deviantart', 'meni'),
			  'digg' 	  	 => esc_html__('Digg', 'meni'),
			  'dribbble' 	 => esc_html__('Dribbble', 'meni'),
			  'envelope' 	 => esc_html__('Envelope', 'meni'),
			  'facebook' 	 => esc_html__('Facebook', 'meni'),
			  'flickr' 		 => esc_html__('Flickr', 'meni'),
			  'google-plus'  => esc_html__('Google Plus', 'meni'),
			  'gtalk'  		 => esc_html__('GTalk', 'meni'),
			  'instagram'	 => esc_html__('Instagram', 'meni'),
			  'lastfm'	 	 => esc_html__('Lastfm', 'meni'),
			  'linkedin'	 => esc_html__('Linkedin', 'meni'),
			  'myspace'		 => esc_html__('Myspace', 'meni'),
			  'picasa'		 => esc_html__('Picasa', 'meni'),
			  'pinterest'	 => esc_html__('Pinterest', 'meni'),
			  'reddit'		 => esc_html__('Reddit', 'meni'),
			  'rss'		 	 => esc_html__('RSS', 'meni'),
			  'skype'		 => esc_html__('Skype', 'meni'),
			  'stumbleupon'	 => esc_html__('Stumbleupon', 'meni'),
			  'technorati'	 => esc_html__('Technorati', 'meni'),
			  'tumblr'		 => esc_html__('Tumblr', 'meni'),
			  'twitter'		 => esc_html__('Twitter', 'meni'),
			  'viadeo'		 => esc_html__('Viadeo', 'meni'),
			  'vimeo'		 => esc_html__('Vimeo', 'meni'),
			  'yahoo'		 => esc_html__('Yahoo', 'meni'),
			  'youtube'		 => esc_html__('Youtube', 'meni'),
			),
			'class'        => 'chosen',
			'default'      => 'delicious',
		  ),

		  array(
			'id'          => 'sociable_fields_url',
			'type'        => 'text',
			'title'       => esc_html__('Enter URL', 'meni')
		  ),
		)
	  ),

   ),
);

// -----------------------------------------
// Hook Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'hook_options',
  'title'       => esc_html__('Hooks', 'meni'),
  'icon'        => 'fa fa-paperclip',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Top Hook", 'meni' ),
	  ),

	  array(
		'id'  	=> 'enable-top-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Top Hook', 'meni'),
		'info'	=> esc_html__("YES! to enable top hook.", 'meni')
	  ),

	  array(
		'id'  		 => 'top-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Top Hook', 'meni'),
		'info'		 => esc_html__('Paste your top hook, Executes after the opening &lt;body&gt; tag.', 'meni')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Content Before Hook", 'meni' ),
	  ),

	  array(
		'id'  	=> 'enable-content-before-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Content Before Hook', 'meni'),
		'info'	=> esc_html__("YES! to enable content before hook.", 'meni')
	  ),

	  array(
		'id'  		 => 'content-before-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Content Before Hook', 'meni'),
		'info'		 => esc_html__('Paste your content before hook, Executes before the opening &lt;#primary&gt; tag.', 'meni')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Content After Hook", 'meni' ),
	  ),

	  array(
		'id'  	=> 'enable-content-after-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Content After Hook', 'meni'),
		'info'	=> esc_html__("YES! to enable content after hook.", 'meni')
	  ),

	  array(
		'id'  		 => 'content-after-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Content After Hook', 'meni'),
		'info'		 => esc_html__('Paste your content after hook, Executes after the closing &lt;/#main&gt; tag.', 'meni')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Bottom Hook", 'meni' ),
	  ),

	  array(
		'id'  	=> 'enable-bottom-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Bottom Hook', 'meni'),
		'info'	=> esc_html__("YES! to enable bottom hook.", 'meni')
	  ),

	  array(
		'id'  		 => 'bottom-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Bottom Hook', 'meni'),
		'info'		 => esc_html__('Paste your bottom hook, Executes after the closing &lt;/body&gt; tag.', 'meni')
	  ),

  array(
		'id'  	=> 'enable-analytics-code',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Tracking Code', 'meni'),
		'info'	=> esc_html__("YES! to enable site tracking code.", 'meni')
	  ),

	  array(
		'id'  		 => 'analytics-code',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Google Analytics Tracking Code', 'meni'),
		'info'		 => esc_html__('Either enter your Google tracking id (UA-XXXXX-X) here. If you want to offer your visitors the option to stop being tracked you can place the shortcode [dt_sc_privacy_google_tracking] somewhere on your site', 'meni')
	  ),

   ),
);

// -----------------------------------------
// Custom Font Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'font_options',
  'title'       => esc_html__('Custom Fonts', 'meni'),
  'icon'        => 'fa fa-font',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Custom Fonts", 'meni' ),
	  ),

	  array(
		'id'              => 'custom_font_fields',
		'type'            => 'group',
		'title'           => esc_html__('Custom Font', 'meni'),
		'info'            => esc_html__('Click button to add font name & urls.', 'meni'),
		'button_title'    => esc_html__('Add New Font', 'meni'),
		'accordion_title' => esc_html__('Adding New Font Field', 'meni'),
		'fields'          => array(
		  array(
			'id'          => 'custom_font_name',
			'type'        => 'text',
			'title'       => esc_html__('Font Name', 'meni')
		  ),

		  array(
			'id'      => 'custom_font_woof',
			'type'    => 'upload',
			'title'   => esc_html__('Upload File (*.woff)', 'meni'),
			'after'   => '<p class="cs-text-muted">'.esc_html__('You can upload custom font family (*.woff) file here.', 'meni').'</p>',
		  ),

		  array(
			'id'      => 'custom_font_woof2',
			'type'    => 'upload',
			'title'   => esc_html__('Upload File (*.woff2)', 'meni'),
			'after'   => '<p class="cs-text-muted">'.esc_html__('You can upload custom font family (*.woff2) file here.', 'meni').'</p>',
		  )
		)
	  ),

   ),
);

// ------------------------------
// backup                       
// ------------------------------
if( !function_exists('dt_booking_check_plugin_active') ) {
	$options[]   = array(
		'name'   => 'backup_section',
		'title'  => esc_html__('Backup', 'meni'),
		'icon'   => 'fa fa-shield',
		'fields' => array(
			array(
				'type'    => 'notice',
				'class'   => 'warning',
				'content' => esc_html__('You can save your current options. Download a Backup and Import.', 'meni')
			),

			array(
				'type'    => 'backup',
			),
		)
	);
}
// ------------------------------
// license
// ------------------------------
$options[]   = array(
  'name'     => 'theme_version',
  'title'    => constant('MENI_THEME_NAME').esc_html__(' Log', 'meni'),
  'icon'     => 'fa fa-info-circle',
  'fields'   => array(

    array(
      'type'    => 'heading',
      'content' => constant('MENI_THEME_NAME').esc_html__(' Theme Change Log', 'meni')
    ),
    array(
      'type'    => 'content',
      'content' => '<pre>
      
2020.02.06 - version 1.3

	* Compatible with wordpress 5.3.2
	* Updated: All premium plugins
	* Updated: All wordpress theme standards
	* Updated: Privacy and Cookies concept
	* Updated: Gutenberg editor support for custom post types

	* Fixed: Google Analytics issue
	* Fixed: Mailchimp email client issue
	* Fixed: Privacy Button Issue
	* Fixed: Gutenberg check for old wordpress version

	* Improved: Tags taxonomy added for portfolio
	* Improved: Single product breadcrumb section
	* Improved: Revisions options added for all custom posts

2019.11.13 - version 1.2
 * Compatible with wordpress 5.2.4
 * Added compatible for Gutenberg editor.
2019.08.05 - version 1.1
 * Optimized demo content </pre> <pre>
2019.08.03 - version 1.0
 * First release!  </pre>',
    ),

  )
);

// ------------------------------
// Seperator
// ------------------------------
$options[] = array(
  'name'   => 'seperator_1',
  'title'  => esc_html__('Plugin Options', 'meni'),
  'icon'   => 'fa fa-plug'
);

// -----------------------------------------
// Woocommerce Options
// -----------------------------------------
if( function_exists( 'is_woocommerce' ) ) {

	$product_style_templates = cs_get_option( 'dt-woo-product-style-templates' );
	$product_style_templates = (is_array($product_style_templates) && !empty($product_style_templates)) ? $product_style_templates : false;

	$product_style_templates_arr = array ();
	if($product_style_templates) {
		foreach($product_style_templates as $product_style_template_key => $product_style_template) {
			$product_style_templates_arr[$product_style_template_key] = $product_style_template['template-title'];
		}
	}

	$woo_page_layouts = array(
		'content-full-width'   => MENI_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
		'with-left-sidebar'    => MENI_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
		'with-right-sidebar'   => MENI_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
	);

	$social_follow = array (
			  'delicious' 	 => esc_html__('Delicious', 'meni'),
			  'deviantart' 	 => esc_html__('Deviantart', 'meni'),
			  'digg' 	  	 => esc_html__('Digg', 'meni'),
			  'dribbble' 	 => esc_html__('Dribbble', 'meni'),
			  'envelope' 	 => esc_html__('Envelope', 'meni'),
			  'facebook' 	 => esc_html__('Facebook', 'meni'),
			  'flickr' 		 => esc_html__('Flickr', 'meni'),
			  'google-plus'  => esc_html__('Google Plus', 'meni'),
			  'gtalk'  		 => esc_html__('GTalk', 'meni'),
			  'instagram'	 => esc_html__('Instagram', 'meni'),
			  'lastfm'	 	 => esc_html__('Lastfm', 'meni'),
			  'linkedin'	 => esc_html__('Linkedin', 'meni'),
			  'myspace'		 => esc_html__('Myspace', 'meni'),
			  'picasa'		 => esc_html__('Picasa', 'meni'),
			  'pinterest'	 => esc_html__('Pinterest', 'meni'),
			  'reddit'		 => esc_html__('Reddit', 'meni'),
			  'rss'		 	 => esc_html__('RSS', 'meni'),
			  'skype'		 => esc_html__('Skype', 'meni'),
			  'stumbleupon'	 => esc_html__('Stumbleupon', 'meni'),
			  'technorati'	 => esc_html__('Technorati', 'meni'),
			  'tumblr'		 => esc_html__('Tumblr', 'meni'),
			  'twitter'		 => esc_html__('Twitter', 'meni'),
			  'viadeo'		 => esc_html__('Viadeo', 'meni'),
			  'vimeo'		 => esc_html__('Vimeo', 'meni'),
			  'yahoo'		 => esc_html__('Yahoo', 'meni'),
			  'youtube'		 => esc_html__('Youtube', 'meni')
			);

	$social_follow_options = array ();
	foreach($social_follow as $socialfollow_key => $socialfollow) {

		$social_follow_option = array(
							'id'    => 'dt-single-product-show-follow-'.$socialfollow_key,
							'type'  => 'switcher',
							'title' => sprintf(esc_html__('Show %1$s Follow', 'meni'), $socialfollow),
						);
		array_push($social_follow_options, $social_follow_option);

	}

	array_push($social_follow_options, array(
						  'type'    => 'notice',
						  'class'   => 'info',
						  'content' => esc_html__('For Sociables Follow - links must be defined under "Sociables" tab. Sociables Share & Sociables Follow option is used for "Custom Template" single product settings.', 'meni' )
						));


	$options[] = array(
		'name'     => 'dtwoo',
		'title'    => esc_html__( 'WooCommerce', 'meni' ),
		'icon'     => 'fa fa-shopping-cart',
		'sections' => array(

			// Shop
				array(
					'name'	=> 'dt-woo-shop',
					'title'	=> esc_html__('Shop', 'meni'),
					'icon'  => 'fa fa-angle-double-right',
					'fields'=> array(
						array(	
							'type'    => 'subheading',
							'content' => esc_html__( 'Shop Page Settings', 'meni' ),
						),

						array(
							'id'         => 'shop-page-layout',
							'type'       => 'image_select',
							'title'      => esc_html__('Page Layout', 'meni'),
							'options'    => $woo_page_layouts,
							'default'    => 'content-full-width',
							'attributes' => array( 'data-depend-id' => 'dt-woo-shop-page-layout' )
						),

						array(
							'id'         => 'shop-page-show-standard-sidebar',
							'type'       => 'switcher',
							'title'      => esc_html__('Show Standard Sidebar', 'meni'),
							'dependency' => array( 'dt-woo-shop-page-layout', 'any', 'with-left-sidebar,with-right-sidebar' )
						),

						array(
							'id'         => 'shop-page-widgetareas',
							'type'       => 'select',
							'title'      => esc_html__('Choose Custom Widget Area', 'meni'),
							'class'      => 'chosen',
							'options'    => meni_custom_widgets(),
							'dependency' => array( 'dt-woo-shop-page-layout', 'any', 'with-left-sidebar,with-right-sidebar' ),
							'attributes' => array(
								'multiple'         => 'multiple',
								'data-placeholder' => esc_attr__('Select Widget Areas', 'meni'),
								'style'            => 'width: 400px;'
							),
						),

						array(
							'id'      => 'shop-product-per-page',
							'type'    => 'number',
							'title'   => esc_html__('Products Per Page', 'meni'),
							'after'   => '<span class="cs-text-desc">&nbsp;'.esc_html__('Number of products to show in catalog / shop page', 'meni').'</span>',
							'default' => 12,
						),

						array(
							'id'         => 'shop-page-product-layout',
							'type'       => 'image_select',
							'title'      => esc_html__('Product Layout', 'meni'),
							'options'    => array(
								1 => MENI_THEME_URI . '/cs-framework-override/images/one-column.png',
								2 => MENI_THEME_URI . '/cs-framework-override/images/one-half-column.png',
								3 => MENI_THEME_URI . '/cs-framework-override/images/one-third-column.png',
								4 => MENI_THEME_URI . '/cs-framework-override/images/one-fourth-column.png'
							),
							'default'    => 4,							
						),

				        array(
				          'id'         => 'shop-page-product-style-template',
				          'type'       => 'select',
				          'title'      => esc_html__('Product Style Template', 'meni'),
									'options'    => $product_style_templates_arr,
									'default'    => 0
				        ), 											

								array(
									'id'         => 'shop-page-enable-breadcrumb',
									'type'       => 'switcher',
									'title'      => esc_html__('Enable Breadcrumb', 'meni'),
									'info'       => esc_html__('YES! to show breadcrumb on shop page.', 'meni'),
									'default'    => true
								),

								array(
									'id'  		 => 'shop-page-bottom-hook',
									'type'  	 => 'textarea',
									'title' 	 => esc_html__('Bottom Hook', 'meni'),
									'info'		 => esc_html__('Content added here will be shown below shop listings.', 'meni')
								),

						array(
							'type'    => 'subheading',
							'content' => esc_html__( "Shop Page Sorter Settings", 'meni' ),
						),

						array(
							'id'         => 'product-show-sorter-on-header',
							'type'       => 'switcher',
							'title'      => esc_html__('Show Sorter On Header', 'meni'),
							'attributes' => array( 'data-depend-id' => 'dt-woo-shop-product-sorter-on-header' ),
							'info'       => esc_html__('YES! to show sorter bar on header.', 'meni')
						),

						array(
							'id'             => 'product-sorter-header-elements',
							'type'           => 'sorter',
							'title'          => esc_html__('Sorter Header Elements', 'meni'),
							'default'        => array(
								'enabled'      => array(
									'filter'       => esc_html__('Filter', 'meni'),
									'result_count' => esc_html__('Result Count', 'meni'),
									'pagination'   => esc_html__('Pagination', 'meni'),
								),
								'disabled'     => array(
									'display_mode'         => esc_html__('Display Mode', 'meni'),
									'display_mode_options' => esc_html__('Display Mode Options', 'meni'),
								),
							),
							'enabled_title'  => esc_html__('Active Elements', 'meni'),
							'disabled_title' => esc_html__('Deatcive Elements', 'meni'),
							'dependency' => array( 'dt-woo-shop-product-sorter-on-header', '==', 'true' )
						),

						array(
							'id'         => 'product-show-sorter-on-footer',
							'type'       => 'switcher',
							'title'      => esc_html__('Show Sorter On Footer', 'meni'),
							'attributes' => array( 'data-depend-id' => 'dt-woo-shop-product-sorter-on-footer' ),
							'info'       => esc_html__('YES! to show sorter bar on footer.', 'meni')
						),

						array(
							'id'             => 'product-sorter-footer-elements',
							'type'           => 'sorter',
							'title'          => esc_html__('Sorter Footer Elements', 'meni'),
							'default'        => array(
								'enabled'      => array(
									'filter'       => esc_html__('Filter', 'meni'),
									'result_count' => esc_html__('Result Count', 'meni'),
									'pagination'   => esc_html__('Pagination', 'meni'),
								),
								'disabled'     => array(
									'display_mode'         => esc_html__('Display Mode', 'meni'),
									'display_mode_options' => esc_html__('Display Mode Options', 'meni'),
								),
							),
							'enabled_title'  => esc_html__('Active Elements', 'meni'),
							'disabled_title' => esc_html__('Deatcive Elements', 'meni'),
							'dependency' => array( 'dt-woo-shop-product-sorter-on-footer', '==', 'true' )
						)

					)
				),

			// Product Category
				array(
					'name'	=> 'dt-woo-cat-archive',
					'title'	=> esc_html__('Category Archive', 'meni'),
					'icon'  => 'fa fa-angle-double-right',
					'fields'=> array(

						array(
							'type'    => 'subheading',
							'content' => esc_html__( 'Category Archive Settings', 'meni' ),
						),

						array(
							'id'         => 'dt-woo-category-archive-layout',
							'type'       => 'image_select',
							'title'      => esc_html__('Page Layout', 'meni'),
							'options'    => $woo_page_layouts,
							'default'    => 'with-left-sidebar',
							'attributes' => array( 'data-depend-id' => 'dt-woo-category-archive-layout' )
						),

						array(
							'id'         => 'dt-woo-category-archive-show-standard-sidebar',
							'type'       => 'switcher',
							'title'      => esc_html__('Show Standard Sidebar', 'meni'),
							'dependency' => array( 'dt-woo-category-archive-layout', 'any', 'with-left-sidebar,with-right-sidebar' )
						),

						array(
							'id'         => 'dt-woo-category-archive-widgetareas',
							'type'       => 'select',
							'title'      => esc_html__('Choose Custom Widget Area', 'meni'),
							'class'      => 'chosen',
							'options'    => meni_custom_widgets(),
							'dependency' => array( 'dt-woo-category-archive-layout', 'any', 'with-left-sidebar,with-right-sidebar' ),
							'attributes' => array(
								'multiple'         => 'multiple',
								'data-placeholder' => esc_attr__('Select Widget Areas', 'meni'),
								'style'            => 'width: 400px;'
							),
						),

						array(
							'id'      => 'dt-woo-category-archive-product-per-page',
							'type'    => 'number',
							'title'   => esc_html__('Products Per Page', 'meni'),
							'after'   => '<span class="cs-text-desc">&nbsp;'.esc_html__('Number of products to show in product category archive page', 'meni').'</span>',
							'default' => get_option( 'posts_per_page' ),
						),

						array(
							'id'      => 'dt-woo-category-archive-product-column',
							'type'    => 'image_select',
							'title'   => esc_html__('Product Layout', 'meni'),
							'options' => array(
								1 => MENI_THEME_URI . '/cs-framework-override/images/one-column.png',
								2 => MENI_THEME_URI . '/cs-framework-override/images/one-half-column.png',
								3 => MENI_THEME_URI . '/cs-framework-override/images/one-third-column.png',
								4 => MENI_THEME_URI . '/cs-framework-override/images/one-fourth-column.png'
							),
							'default' => 4,
						),

				        array(
				          'id'         => 'dt-woo-category-product-style-template',
				          'type'       => 'select',
				          'title'      => esc_html__('Product Style Template', 'meni'),
									'options'    => $product_style_templates_arr,
									'default'    => 0
								),

								array(
									'id'         => 'dt-woo-category-archive-enable-breadcrumb',
									'type'       => 'switcher',
									'title'      => esc_html__('Enable Breadcrumb', 'meni'),
									'info'       => esc_html__('YES! to show breadcrumb on category archive page.', 'meni'),
									'default'    => true
								)

					)
				),

			// Product Tag
				array(
					'name'	=> 'dt-woo-tag-archive',
					'title'	=> esc_html__('Tag Archive', 'meni'),
					'icon'  => 'fa fa-angle-double-right',
					'fields'=> array(

						array(
							'type'    => 'subheading',
							'content' => esc_html__( 'Tag Archive Settings', 'meni' ),
						),

						array(
							'id'         => 'dt-woo-tag-archive-layout',
							'type'       => 'image_select',
							'title'      => esc_html__('Page Layout', 'meni'),
							'options'    => $woo_page_layouts,
							'default'    => 'with-left-sidebar',
							'attributes' => array( 'data-depend-id' => 'dt-woo-tag-archive-layout' )
						),

						array(
							'id'         => 'dt-woo-tag-archive-show-standard-sidebar',
							'type'       => 'switcher',
							'title'      => esc_html__('Show Standard Sidebar', 'meni'),
							'dependency' => array( 'dt-woo-tag-archive-layout', 'any', 'with-left-sidebar,with-right-sidebar' )
						),

						array(
							'id'         => 'dt-woo-tag-archive-widgetareas',
							'type'       => 'select',
							'title'      => esc_html__('Choose Custom Widget Area', 'meni'),
							'class'      => 'chosen',
							'options'    => meni_custom_widgets(),
							'dependency' => array( 'dt-woo-tag-archive-layout', 'any', 'with-left-sidebar,with-right-sidebar' ),
							'attributes' => array(
								'multiple'         => 'multiple',
								'data-placeholder' => esc_attr__('Select Widget Areas', 'meni'),
								'style'            => 'width: 400px;'
							),
						),

						array(
							'id'      => 'dt-woo-tag-archive-product-per-page',
							'type'    => 'number',
							'title'   => esc_html__('Products Per Page', 'meni'),
							'after'   => '<span class="cs-text-desc">&nbsp;'.esc_html__('Number of products to show in product tag archive page', 'meni').'</span>',
							'default' => get_option( 'posts_per_page' ),
						),

						array(
							'id'      => 'dt-woo-tag-archive-product-column',
							'type'    => 'image_select',
							'title'   => esc_html__('Product Layout', 'meni'),
							'options' => array(
								2 => MENI_THEME_URI . '/cs-framework-override/images/one-half-column.png',
								3 => MENI_THEME_URI . '/cs-framework-override/images/one-third-column.png',
								4 => MENI_THEME_URI . '/cs-framework-override/images/one-fourth-column.png'							
							),
							'default' => 4,
						),	

				        array(
				          'id'         => 'dt-woo-tag-product-style-template',
				          'type'       => 'select',
				          'title'      => esc_html__('Product Style Template', 'meni'),
									'options'    => $product_style_templates_arr,
									'default'    => 0
								),

								array(
									'id'         => 'dt-woo-tag-archive-enable-breadcrumb',
									'type'       => 'switcher',
									'title'      => esc_html__('Enable Breadcrumb', 'meni'),
									'info'       => esc_html__('YES! to show breadcrumb on tag archive page.', 'meni'),
									'default'    => true
								)								

					)
				),

			// Product
				array(
					'name'   => 'dt-woo-single-product',
					'title'  => esc_html__('Product', 'meni'),
					'icon'   => 'fa fa-angle-double-right',
					'fields' => array_merge ( 
						array(

							array(	
								'type'    => 'subheading',
								'content' => esc_html__( 'Single Product Page Default Settings', 'meni' ),
							),

							array(
								'id'      => 'dt-single-product-default-template',
								'type'    => 'select',
								'title'   => esc_html__('Product Template', 'meni'),
								'class'   => 'chosen',
								'options' => array(
									'woo-default'     => esc_html__( 'WooCommerce Default', 'meni' ),
									'custom-template' => esc_html__( 'Custom Template', 'meni' )
								),
								'default'    => 'woo-default',
								'info'       => esc_html__('Don\'t use product shortcodes in content area when "WooCommerce Default" template is chosen.', 'meni')
							),

							array(
								'id'         => 'dt-single-product-default-layout',
								'type'       => 'image_select',
								'title'      => esc_html__('Page Layout', 'meni'),
								'options'    => $woo_page_layouts,			
								'default'    => 'with-left-sidebar',
								'attributes' => array( 'data-depend-id' => 'dt-single-product-default-layout' )
							),

							array(
								'id'         => 'dt-single-product-show-default-sidebar',
								'type'       => 'switcher',
								'title'      => esc_html__('Show Standard Sidebar', 'meni'),
								'dependency' => array( 'dt-single-product-default-layout', 'any', 'with-left-sidebar,with-right-sidebar' )
							),

							array(
								'id'         => 'dt-single-product-widgetareas',
								'type'       => 'select',
								'title'      => esc_html__('Choose Custom Widget Area', 'meni'),
								'class'      => 'chosen',
								'options'    => meni_custom_widgets(),
								'dependency' => array( 'dt-single-product-default-layout', 'any', 'with-left-sidebar,with-right-sidebar' ),
								'attributes' => array(
									'multiple'         => 'multiple',
									'data-placeholder' => esc_attr__('Select Widget Areas', 'meni'),
									'style'            => 'width: 400px;'
								),
							),

							array(
								'id'		 => 'dt-single-product-sale-countdown-timer',
								'type'		 => 'switcher',
								'title'		 => esc_html__('Enable Sale Countdown Timer', 'meni'),
								'info'       => esc_html__('This option is applicable for "WooCommerce Default" template page only.', 'meni')
							),

							array(
								'id'		 => 'dt-single-product-enable-size-guide',
								'type'		 => 'switcher',
								'title'		 => esc_html__('Enable Size Guide Button', 'meni'),
								'info'       => esc_html__('This option is applicable for "WooCommerce Default" template page only.', 'meni')
							),

							array(
								'id'		 => 'dt-single-product-enable-ajax-addtocart',
								'type'		 => 'switcher',
								'title'		 => esc_html__('Enable Ajax Add To Cart', 'meni'),
								'info'       => esc_html__('If you wish, you can have ajax functionality in single page add to cart button.', 'meni')
							),

							array(
								'id'         => 'dt-single-product-enable-breadcrumb',
								'type'       => 'switcher',
								'title'      => esc_html__('Enable Breadcrumb', 'meni'),
								'info'       => esc_html__('YES! to show breadcrumb on single product page.', 'meni'),
								'default'    => true
							),

							array(	
								'type'    => 'subheading',
								'content' => esc_html__( 'Single Page Upsell Products Settings', 'meni' ),
							),
							
							array(
								'id'		 => 'dt-single-product-default-show-upsell',
								'type'		 => 'switcher',
								'title'		 => esc_html__('Show Upsell Products', 'meni'),
								'default' 	 => true
							),

							array(
								'id'  		=> 'dt-single-product-upsell-title',
								'type'  	=> 'wysiwyg',
								'title' 	=> esc_html__('Upsell Title', 'meni'),
								'default'	=> '<h2>'.esc_html__('You may also like&hellip;', 'meni').'</h2>'
							),

							array(
								'id'      => 'dt-single-product-upsell-column',
								'type'    => 'image_select',
								'title'   => esc_html__('Upsell Column', 'meni'),
								'after'   => '<span class="cs-text-desc">&nbsp;'.esc_html__('Select upsell products column layout', 'meni').'</span>',
								'default' => 4,
								'options' => array(
									1 => MENI_THEME_URI . '/cs-framework-override/images/one-column.png',
									2 => MENI_THEME_URI . '/cs-framework-override/images/one-half-column.png',
									3 => MENI_THEME_URI . '/cs-framework-override/images/one-third-column.png',
									4 => MENI_THEME_URI . '/cs-framework-override/images/one-fourth-column.png',
								)
							),

							array(
								'id'      => 'dt-single-product-upsell-limit',
								'type'    => 'select',
								'title'   => esc_html__('Upsell Limit', 'meni'),
								'after'   => '<span class="cs-text-desc">&nbsp;'.esc_html__('Select upsell products limit', 'meni').'</span>',
								'default' => 4,
								'options' => array(
									1 => esc_html__( '1', 'meni' ),
									2 => esc_html__( '2', 'meni' ),
									3 => esc_html__( '3', 'meni' ),
									4 => esc_html__( '4', 'meni' ),
									5 => esc_html__( '5', 'meni' ),
									6 => esc_html__( '6', 'meni' ),
									7 => esc_html__( '7', 'meni' ),
									8 => esc_html__( '8', 'meni' ),	
									9 => esc_html__( '9', 'meni' ),
									10 => esc_html__( '10', 'meni' ),									
								)
							),

					        array(
					          'id'         => 'dt-woo-single-product-upsell-style-template',
					          'type'       => 'select',
					          'title'      => esc_html__('Product Style Template', 'meni'),
					          'options'    => $product_style_templates_arr
					        ),

							array(	
								'type'    => 'subheading',
								'content' => esc_html__( 'Single Page Related Products Settings', 'meni' ),
							),

							array(
								'id'    => 'dt-single-product-default-show-related',
								'type'  => 'switcher',
								'title' => esc_html__('Show Related Products', 'meni'),
								'default' 	 => true
							),

							array(
								'id'      => 'dt-single-product-related-title',
								'type'    => 'wysiwyg',
								'title'   => esc_html__('Related Product Title', 'meni'),
								'default' => '<h2>'.esc_html__('Related products', 'meni').'</h2>'
							),

							array(
								'id'      => 'dt-single-product-related-column',
								'type'    => 'image_select',
								'title'   => esc_html__('Related Column', 'meni'),
								'after'   => '<span class="cs-text-desc">&nbsp;'.esc_html__('Select related products column layout', 'meni').'</span>',
								'default' => 4,
								'options' => array(
									2 => MENI_THEME_URI . '/cs-framework-override/images/one-half-column.png',
									3 => MENI_THEME_URI . '/cs-framework-override/images/one-third-column.png',
									4 => MENI_THEME_URI . '/cs-framework-override/images/one-fourth-column.png',
								)							
							),

							array(
								'id'      => 'dt-single-product-related-limit',
								'type'    => 'select',
								'title'   => esc_html__('Related Limit', 'meni'),
								'after'   => '<span class="cs-text-desc">&nbsp;'.esc_html__('Select related products limit', 'meni').'</span>',
								'default' => 4,
								'options' => array(
									1 => esc_html__( '1', 'meni' ),
									2 => esc_html__( '2', 'meni' ),
									3 => esc_html__( '3', 'meni' ),
									4 => esc_html__( '4', 'meni' ),
									5 => esc_html__( '5', 'meni' ),
									6 => esc_html__( '6', 'meni' ),
									7 => esc_html__( '7', 'meni' ),
									8 => esc_html__( '8', 'meni' ),	
									9 => esc_html__( '9', 'meni' ),
									10 => esc_html__( '10', 'meni' ),									
								)
							),

					        array(
					          'id'         => 'dt-woo-single-product-related-style-template',
					          'type'       => 'select',
					          'title'      => esc_html__('Product Style Template', 'meni'),
					          'options'    => $product_style_templates_arr
					        ),

							array(	
								'type'    => 'subheading',
								'content' => esc_html__( 'Single Product Page Settings', 'meni' ),
							),

							array(
								'id'    => 'dt-single-product-addtocart-sticky',
								'type'  => 'switcher',
								'title' => esc_html__('Sticky Add to Cart', 'meni'),
							),

							array(
								'id'    => 'dt-single-product-show-360-viewer',
								'type'  => 'switcher',
								'title' => esc_html__('Show Product 360 Viewer', 'meni'),
							),

							array(	
								'type'    => 'subheading',
								'content' => esc_html__( 'Sociables Share', 'meni' ),
							),

							array(
								'id'    => 'dt-single-product-show-sharer-facebook',
								'type'  => 'switcher',
								'title' => esc_html__('Show Facebook Sharer', 'meni'),
							),

							array(
								'id'    => 'dt-single-product-show-sharer-delicious',
								'type'  => 'switcher',
								'title' => esc_html__('Show Delicious Sharer', 'meni'),
							),

							array(
								'id'    => 'dt-single-product-show-sharer-digg',
								'type'  => 'switcher',
								'title' => esc_html__('Show Digg Sharer', 'meni'),
							),

							array(
								'id'    => 'dt-single-product-show-sharer-stumbleupon',
								'type'  => 'switcher',
								'title' => esc_html__('Show Stumble Upon Sharer', 'meni'),
							),

							array(
								'id'    => 'dt-single-product-show-sharer-twitter',
								'type'  => 'switcher',
								'title' => esc_html__('Show Twitter Sharer', 'meni'),
							),

							array(
								'id'    => 'dt-single-product-show-sharer-googleplus',
								'type'  => 'switcher',
								'title' => esc_html__('Show Google Plus Sharer', 'meni'),
							),

							array(
								'id'    => 'dt-single-product-show-sharer-linkedin',
								'type'  => 'switcher',
								'title' => esc_html__('Show Linkedin Sharer', 'meni'),
							),

							array(
								'id'    => 'dt-single-product-show-sharer-pinterest',
								'type'  => 'switcher',
								'title' => esc_html__('Show Pinterest Sharer', 'meni'),
							),	

							array(	
								'type'    => 'subheading',
								'content' => esc_html__( 'Sociables Follow', 'meni' ),
							),							

						),

						$social_follow_options

					)
				),
			
			// Size Guide
				array(
					'name'   => 'dt-woo-size-guides',
					'title'  => esc_html__( 'Size Guides', 'meni' ),
					'icon'   => 'fa fa-angle-double-right',
					'fields' => array(
						array(
							'id'              => 'dt-woo-size-guides',
							'type'            => 'group',
							'title'           => esc_html__( 'Size Guides List', 'meni' ),
							'button_title'    => esc_html__('Add New', 'meni'),
							'accordion_title' => esc_html__('Add New Size Guide', 'meni'),
							'fields'          => array(
								array(
									'id'    => 'title',
									'type'  => 'text',
									'title' => esc_html__('Title', 'meni'),
								),
								array(
									'id'    => 'size-guide',
									'type'  => 'upload',
									'title' => esc_html__('Size Guide', 'meni'),
								)
							)
						)
					)
				),
			
			// Product Style Templates
				array(
					'name'   => 'dt-woo-product-style-templates-holder',
					'title'  => esc_html__( 'Product Style Templates', 'meni' ),
					'icon'   => 'fa fa-angle-double-right',
					'fields' => array(
						array(
							'id'              => 'dt-woo-product-style-templates',
							'type'            => 'group',
							'title'           => esc_html__( 'Product Style Templates', 'meni' ),
							'button_title'    => esc_html__('Add New', 'meni'),
							'accordion_title' => esc_html__('Add New Template', 'meni'),
							'fields'          => array(

								array(
									'id'    => 'template-title',
									'type'  => 'text',
									'title' => esc_html__('Template Title', 'meni'),
								),
								array(
									'id'         => 'product-style',
									'type'       => 'select',
									'title'      => esc_html__('Product Style', 'meni'),
									'options'    => array(
														'product-style-default'              => esc_html__('Default', 'meni'),
														'product-style-cornered'             => esc_html__('Cornered', 'meni'),
														'product-style-title-eg-highlighter' => esc_html__('Title & Element Group Highlighter', 'meni'),
														'product-style-content-highlighter'  => esc_html__('Content Highlighter', 'meni'),
														'product-style-egrp-overlap-pc'      => esc_html__('Element Group Overlap Product Content', 'meni'),
														'product-style-egrp-reveal-pc'       => esc_html__('Element Group Reveal Product Content', 'meni'),
														'product-style-igrp-over-pc'         => esc_html__('Icon Group over Product Content', 'meni'),
														'product-style-egrp-over-pc'         => esc_html__('Element Group over Product Content', 'meni')
													),
									'default'    => 'product-style-default'
								),								

							// "Product Style" Hover Options

								array(
									'type'    => 'notice',
									'class'   => 'info',
									'content' => esc_html__('"Product Style" Hover Options.', 'meni')
								),

								array(
									'id'         => 'product-hover-styles',
									'type'       => 'select',
									'title'      => esc_html__('Hover Styles', 'meni'),
									'options'    => array(
														''                                        => esc_html__('None', 'meni'),
														'product-hover-fade-border'               => esc_html__('Fade - Border', 'meni'),
														'product-hover-fade-skinborder'           => esc_html__('Fade - Skin Border', 'meni'),
														'product-hover-fade-gradientborder'       => esc_html__('Fade - Gradient Border', 'meni'),
														'product-hover-fade-shadow'               => esc_html__('Fade - Shadow', 'meni'),
														'product-hover-fade-inshadow'             => esc_html__('Fade - InShadow', 'meni'),
														'product-hover-thumb-fade-border'         => esc_html__('Fade Thumb Border', 'meni'),
														'product-hover-thumb-fade-skinborder'     => esc_html__('Fade Thumb SkinBorder', 'meni'),
														'product-hover-thumb-fade-gradientborder' => esc_html__('Fade Thumb Gradient Border', 'meni'),
														'product-hover-thumb-fade-shadow'         => esc_html__('Fade Thumb Shadow', 'meni'),
														'product-hover-thumb-fade-inshadow'       => esc_html__('Fade Thumb InShadow', 'meni')
													),
									'default'    => 'product-hover-fade-border'
								),

								array(
									'id'         => 'product-overlay-bgcolor',
									'type'       => 'color_picker',
									'title'      => esc_html__('Overlay Background Color', 'meni')
								),

								array(
									'id'         => 'product-overlay-dark-bgcolor',
									'type'       => 'switcher',
									'title'      => esc_html__('Overlay Dark Background', 'meni'),
								),

								array(
									'id'         => 'product-overlay-effects',
									'type'       => 'select',
									'title'      => esc_html__('Overlay Effects', 'meni'),
									'options'    => array(
														''                                    => esc_html__('None', 'meni'),
														'product-overlay-fixed'               => esc_html__('Fixed', 'meni'),
														'product-overlay-toptobottom'         => esc_html__('Top to Bottom', 'meni'),
														'product-overlay-bottomtotop'         => esc_html__('Bottom to Top', 'meni'),
														'product-overlay-righttoleft'         => esc_html__('Right to Left', 'meni'),
														'product-overlay-lefttoright'         => esc_html__('Left to Right', 'meni'),
														'product-overlay-middle'              => esc_html__('Middle', 'meni'),
														'product-overlay-middleradial'        => esc_html__('Middle Radial', 'meni'),
														'product-overlay-gradienttoptobottom' => esc_html__('Gradient - Top to Bottom', 'meni'),
														'product-overlay-gradientbottomtotop' => esc_html__('Gradient - Bottom to Top', 'meni'),
														'product-overlay-gradientrighttoleft' => esc_html__('Gradient - Right to Left', 'meni'),
														'product-overlay-gradientlefttoright' => esc_html__('Gradient - Left to Right', 'meni'),
														'product-overlay-gradientradial'      => esc_html__('Gradient - Radial', 'meni'),
														'product-overlay-flash'               => esc_html__('Flash', 'meni'),
														'product-overlay-scale'               => esc_html__('Scale', 'meni'),
														'product-overlay-horizontalelastic'   => esc_html__('Horizontal - Elastic', 'meni'),
														'product-overlay-verticalelastic'     => esc_html__('Vertical - Elastic', 'meni')
													),
									'default'    => ''
								),

								array(
									'id'         => 'product-hover-image-effects',
									'type'       => 'select',
									'title'      => esc_html__('Hover Image Effects', 'meni'),
									'options'    => array(
														''                                => esc_html__('None', 'meni'),
														'product-hover-image-blur'        => esc_html__('Blur', 'meni'),
														'product-hover-image-blackwhite'  => esc_html__('Black & White', 'meni'),
														'product-hover-image-fadeinleft'  => esc_html__('Fade In Left', 'meni'),
														'product-hover-image-fadeinright' => esc_html__('Fade In Right', 'meni'),
														'product-hover-image-rotate'      => esc_html__('Rotate', 'meni'),
														'product-hover-image-rotatealt'   => esc_html__('Rotate - Alt', 'meni'),
														'product-hover-image-scalein'     => esc_html__('Scale In', 'meni'),
														'product-hover-image-scaleout'    => esc_html__('Scale Out', 'meni'),
														'product-hover-image-floatout'    => esc_html__('Float Up', 'meni')
													),
									'default'    => ''
								),

								array(
									'id'         => 'product-hover-secondary-image-effects',
									'type'       => 'select',
									'title'      => esc_html__('Hover Secondary Image Effects', 'meni'),
									'options'    => array(
														'product-hover-secimage-fade'              => esc_html__('Fade', 'meni'),
														'product-hover-secimage-zoomin'            => esc_html__('Zoom In', 'meni'),
														'product-hover-secimage-zoomout'           => esc_html__('Zoom Out', 'meni'),
														'product-hover-secimage-zoomoutup'         => esc_html__('Zoom Out Up', 'meni'),
														'product-hover-secimage-zoomoutdown'       => esc_html__('Zoom Out Down', 'meni'),
														'product-hover-secimage-zoomoutleft'       => esc_html__('Zoom Out Left', 'meni'),
														'product-hover-secimage-zoomoutright'      => esc_html__('Zoom Out Right', 'meni'),
														'product-hover-secimage-pushup'            => esc_html__('Push Up', 'meni'),
														'product-hover-secimage-pushdown'          => esc_html__('Push Down', 'meni'),
														'product-hover-secimage-pushleft'          => esc_html__('Push Left', 'meni'),
														'product-hover-secimage-pushright'         => esc_html__('Push Right', 'meni'),
														'product-hover-secimage-slideup'           => esc_html__('Slide Up', 'meni'),
														'product-hover-secimage-slidedown'         => esc_html__('Slide Down', 'meni'),
														'product-hover-secimage-slideleft'         => esc_html__('Slide Left', 'meni'),
														'product-hover-secimage-slideright'        => esc_html__('Slide Right', 'meni'),		
														'product-hover-secimage-hingeup'           => esc_html__('Hinge Up', 'meni'),
														'product-hover-secimage-hingedown'         => esc_html__('Hinge Down', 'meni'),
														'product-hover-secimage-hingeleft'         => esc_html__('Hinge Left', 'meni'),
														'product-hover-secimage-hingeright'        => esc_html__('Hinge Right', 'meni'),		
														'product-hover-secimage-foldup'            => esc_html__('Fold Up', 'meni'),
														'product-hover-secimage-folddown'          => esc_html__('Fold Down', 'meni'),
														'product-hover-secimage-foldleft'          => esc_html__('Fold Left', 'meni'),
														'product-hover-secimage-foldright'         => esc_html__('Fold Right', 'meni'),
														'product-hover-secimage-fliphoriz'         => esc_html__('Flip Horizontal', 'meni'),
														'product-hover-secimage-flipvert'          => esc_html__('Flip Vertical', 'meni')
													),
									'default'    => 'product-hover-secimage-fade'
								),

								array(
									'id'         => 'product-content-hover-effects',
									'type'       => 'select',
									'title'      => esc_html__('Content Hover Effects', 'meni'),
									'options'    => array(
														''                                   => esc_html__('None', 'meni'),
														'product-content-hover-fade'         => esc_html__('Fade', 'meni'),
														'product-content-hover-zoom'         => esc_html__('Zoom', 'meni'),
														'product-content-hover-slidedefault' => esc_html__('Slide Default', 'meni'),
														'product-content-hover-slideleft'    => esc_html__('Slide From Left', 'meni'),
														'product-content-hover-slideright'   => esc_html__('Slide From Right', 'meni'),
														'product-content-hover-slidetop'     => esc_html__('Slide From Top', 'meni'),
														'product-content-hover-slidebottom'  => esc_html__('Slide From Bottom', 'meni')
													),
									'default'    => ''
								),

								array(
									'id'         => 'product-icongroup-hover-effects',
									'type'       => 'select',
									'title'      => esc_html__('Icon Group Hover Effects', 'meni'),
									'options'    => array(
														''                               => esc_html__('None', 'meni'),
														'product-icongroup-hover-flipx'  => esc_html__('Flip X', 'meni'),
														'product-icongroup-hover-flipy'  => esc_html__('Flip Y', 'meni'),
														'product-icongroup-hover-bounce' => esc_html__('Bounce', 'meni')
													),
									'default'    => ''
								),

							// "Product Style" Common Options

								array(
									'type'    => 'notice',
									'class'   => 'info',
									'content' => esc_html__('"Product Style" Common Options.', 'meni')
								),	
								array(
									'id'         => 'product-borderorshadow',
									'type'       => 'select',
									'title'      => esc_html__('Border or Shadow', 'meni'),
									'options'    => array(
														''                              => esc_html__('None', 'meni'),
														'product-borderorshadow-border' => esc_html__('Border', 'meni'),
														'product-borderorshadow-shadow' => esc_html__('Shadow', 'meni')
													),
									'default'    => '',
									'desc'      => esc_html__('Choose either Border or Shadow for your product listing.', 'meni')
								),										
								array(
									'id'         => 'product-border-type',
									'type'       => 'select',
									'title'      => esc_html__('Border - Type', 'meni'),
									'options'    => array(
														'product-border-type-default' => esc_html__('Default', 'meni'),
														'product-border-type-thumb'   => esc_html__('Thumb', 'meni')
													),
									'default'    => 'product-border-type-default',
								),													
								array(
									'id'         => 'product-border-position',
									'type'       => 'select',
									'title'      => esc_html__('Border - Position', 'meni'),
									'options'    => array(
														'product-border-position-default'      => esc_html__('Default', 'meni'),
														'product-border-position-left'         => esc_html__('Left', 'meni'),
														'product-border-position-right'        => esc_html__('Right', 'meni'),
														'product-border-position-top'          => esc_html__('Top', 'meni'),
														'product-border-position-bottom'       => esc_html__('Bottom', 'meni'),
														'product-border-position-top-left'     => esc_html__('Top Left', 'meni'),
														'product-border-position-top-right'    => esc_html__('Top Right', 'meni'),
														'product-border-position-bottom-left'  => esc_html__('Bottom Left', 'meni'),
														'product-border-position-bottom-right' => esc_html__('Bottom Right', 'meni')														
													),
									'default'    => 'product-border-position-default',
								),	
								array(
									'id'         => 'product-shadow-type',
									'type'       => 'select',
									'title'      => esc_html__('Shadow - Type', 'meni'),
									'options'    => array(
														'product-shadow-type-default' => esc_html__('Default', 'meni'),
														'product-shadow-type-thumb'   => esc_html__('Thumb', 'meni')
													),
									'default'    => 'product-shadow-type-default',
								),
								array(
									'id'         => 'product-shadow-position',
									'type'       => 'select',
									'title'      => esc_html__('Shadow - Position', 'meni'),
									'options'    => array(
														'product-shadow-position-default'      => esc_html__('Default', 'meni'),
														'product-shadow-position-top-left'     => esc_html__('Top Left', 'meni'),
														'product-shadow-position-top-right'    => esc_html__('Top Right', 'meni'),
														'product-shadow-position-bottom-left'  => esc_html__('Bottom Left', 'meni'),
														'product-shadow-position-bottom-right' => esc_html__('Bottom Right', 'meni')
													),
									'default'    => 'product-shadow-position-default',
								),

								array(
									'id'         => 'product-bordershadow-highlight',
									'type'       => 'select',
									'title'      => esc_html__('Border / Shadow - Highlight', 'meni'),
									'options'    => array(
														''                                       => esc_html__('None', 'meni'),
														'product-bordershadow-highlight-default' => esc_html__('Default', 'meni'),
														'product-bordershadow-highlight-onhover' => esc_html__('On Hover', 'meni')
													),
									'default'    => '',
								),

								array(
									'id'         => 'product-background-bgcolor',
									'type'       => 'color_picker',
									'title'      => esc_html__('Background - Background Color', 'meni')
								),

								array(
									'id'         => 'product-background-dark-bgcolor',
									'type'       => 'switcher',
									'title'      => esc_html__('Background - Dark Background', 'meni')
								),
							
								array(
									'id'         => 'product-padding',
									'type'       => 'select',
									'title'      => esc_html__('Padding', 'meni'),
									'options'    => array(
														'product-padding-default' => esc_html__('Default', 'meni'),
														'product-padding-overall' => esc_html__('Product', 'meni'),
														'product-padding-thumb'   => esc_html__('Thumb', 'meni'),
														'product-padding-content' => esc_html__('Content', 'meni'),
													),
									'default'    => 'product-padding-default'
								),
								array(
									'id'         => 'product-space',
									'type'       => 'select',
									'title'      => esc_html__('Space', 'meni'),
									'options'    => array(
														'product-without-space' => esc_html__('False', 'meni'),
														'product-with-space'  => esc_html__('True', 'meni')
													),
									'default'    => 'product-with-space'
								),
								array(
									'id'         => 'product-display-type',
									'type'       => 'select',
									'title'      => esc_html__('Display Type', 'meni'),
									'options'    => array(
														'grid' => esc_html__('Grid', 'meni'),
														'list'  => esc_html__('List', 'meni')
													),
									'default'    => 'grid'
								),
								array(
									'id'         => 'product-display-type-list-options',
									'type'       => 'select',
									'title'      => esc_html__('List Options', 'meni'),
									'options'    => array(
														'left-thumb'  => esc_html__('Left Thumb', 'meni'),
														'right-thumb' => esc_html__('Right Thumb', 'meni')
													),
									'default'    => 'left-thumb'
								),	
								array(
									'id'         => 'product-show-labels',
									'type'       => 'select',
									'title'      => esc_html__('Show Product Labels', 'meni'),
									'options'    => array(
														'true'  => esc_html__('True', 'meni'),
														'false' => esc_html__('False', 'meni')
													),
									'default'    => 'true'
								),															
								array(
									'id'         => 'product-label-design',
									'type'       => 'select',
									'title'      => esc_html__('Product Label Design', 'meni'),
									'options'    => array(
														'product-label-boxed'      => esc_html__('Boxed', 'meni'),
														'product-label-circle'  => esc_html__('Circle', 'meni'),
														'product-label-rounded'   => esc_html__('Rounded', 'meni'),
														'product-label-angular'   => esc_html__('Angular', 'meni'),
														'product-label-ribbon'   => esc_html__('Ribbon', 'meni'),
													),
									'default'    => 'product-label-boxed',
								),

								array(
									'id'         => 'product-custom-class',
									'type'       => 'text',
									'title'      => esc_html__('Custom Class', 'meni')
								),	

							// "Product Style - Thumb" Options

								array(
									'type'    => 'notice',
									'class'   => 'info',
									'content' => esc_html__('"Product Style - Thumb" Options.', 'meni')
								),

								array(
									'id'         => 'product-thumb-secondary-image-onhover',
									'type'       => 'switcher',
									'title'      => esc_html__('Show Secondary Image On Hover', 'meni'),
									'desc'	 => esc_html__('YES! to show secondary image on product hover. First image in the gallery will be used as secondary image.', 'meni')
								),

								array(
									'id'             => 'product-thumb-content',
									'type'           => 'sorter',
									'title'          => esc_html__('Content', 'meni'),
									'default'        => array(
										'enabled'      => array(
											'title'          => esc_html__('Title', 'meni'),
											'category'       => esc_html__('Category', 'meni'),
											'price'          => esc_html__('Price', 'meni'),
											'button_element' => esc_html__('Button Element', 'meni'),
											'icons_group'    => esc_html__('Icons Group', 'meni'),
										),
										'disabled'     => array(
											'excerpt'       => esc_html__('Excerpt', 'meni'),
											'rating'        => esc_html__('Rating', 'meni'),
											'countdown'     => esc_html__('Count Down', 'meni'),
											'separator'     => esc_html__('Separator', 'meni'),
											'element_group' => esc_html__('Element Group', 'meni'),
											'swatches'      => esc_html__('Swatches', 'meni'),
										),
									),
									'enabled_title'  => esc_html__('Active Elements', 'meni'),
									'disabled_title' => esc_html__('Deatcive Elements', 'meni'),
								),

								array(
									'id'         => 'product-thumb-alignment',
									'type'       => 'select',
									'title'      => esc_html__('Alignment', 'meni'),
									'options'    => array(
														'product-thumb-alignment-top'          => esc_html__('Top', 'meni'),
														'product-thumb-alignment-top-left'     => esc_html__('Top Left', 'meni'),
														'product-thumb-alignment-top-right'    => esc_html__('Top Right', 'meni'),
														'product-thumb-alignment-middle'       => esc_html__('Middle', 'meni'),
														'product-thumb-alignment-bottom'       => esc_html__('Bottom', 'meni'),
														'product-thumb-alignment-bottom-left'  => esc_html__('Bottom Left', 'meni'),
														'product-thumb-alignment-bottom-right' => esc_html__('Bottom Right', 'meni')
													),
									'default'    => 'product-thumb-alignment-top'
								),

								array(
									'id'         => 'product-thumb-iconsgroup-icons',
									'type'       => 'select',
									'title'      => esc_html__('Icons Group - Icons', 'meni'),
									'options'    => array(
														'cart'      => esc_html__('Cart', 'meni'),
														'wishlist'  => esc_html__('Wishlist', 'meni'),
														'compare'   => esc_html__('Compare', 'meni'),
														'quickview' => esc_html__('Quick View', 'meni')
													),
									'class'         => 'chosen',
									'attributes'    => array(
										'multiple'    => 'multiple',
									),							
								),

								array(
									'id'         => 'product-thumb-iconsgroup-style',
									'type'       => 'select',
									'title'      => esc_html__('Icons Group - Style', 'meni'),
									'options'    => array(
														'product-thumb-iconsgroup-style-simple'  => esc_html__('Simple', 'meni'),
														'product-thumb-iconsgroup-style-bgfill-square'  => esc_html__('Background Fill Square', 'meni'),
														'product-thumb-iconsgroup-style-bgfill-rounded-square' => esc_html__('Background Fill Rounded Square', 'meni'),
														'product-thumb-iconsgroup-style-bgfill-rounded'  => esc_html__('Background Fill Rounded', 'meni'),
														'product-thumb-iconsgroup-style-brdrfill-square'  => esc_html__('Border Fill Square', 'meni'),
														'product-thumb-iconsgroup-style-brdrfill-rounded-square' => esc_html__('Border Fill Rounded Square', 'meni'),
														'product-thumb-iconsgroup-style-brdrfill-rounded'  => esc_html__('Border Fill Rounded', 'meni'),
														'product-thumb-iconsgroup-style-skinbgfill-square'  => esc_html__('Skin Background Fill Square', 'meni'),
														'product-thumb-iconsgroup-style-skinbgfill-rounded-square' => esc_html__('Skin Background Fill Rounded Square', 'meni'),
														'product-thumb-iconsgroup-style-skinbgfill-rounded'  => esc_html__('Skin Background Fill Rounded', 'meni'),
														'product-thumb-iconsgroup-style-skinbrdrfill-square'  => esc_html__('Skin Border Fill Square', 'meni'),
														'product-thumb-iconsgroup-style-skinbrdrfill-rounded-square' => esc_html__('Skin Border Fill Rounded Square', 'meni'),
														'product-thumb-iconsgroup-style-skinbrdrfill-rounded'  => esc_html__('Skin Border Fill Rounded', 'meni')																											
													),
									'default'    => 'product-thumb-iconsgroup-style-simple'
								),

								array(
									'id'         => 'product-thumb-iconsgroup-position',
									'type'       => 'select',
									'title'      => esc_html__('Icons Group - Position', 'meni'),
									'options'    => array(

													''                                                                              => esc_html__('Default', 'meni'),

													'product-thumb-iconsgroup-position-horizontal horizontal-position-top'          => esc_html__('Horizontal Top', 'meni'),
													'product-thumb-iconsgroup-position-horizontal horizontal-position-top-left'     => esc_html__('Horizontal Top Left', 'meni'),
													'product-thumb-iconsgroup-position-horizontal horizontal-position-top-right'    => esc_html__('Horizontal Top Right', 'meni'),
													'product-thumb-iconsgroup-position-horizontal horizontal-position-middle'       => esc_html__('Horizontal Middle', 'meni'),
													'product-thumb-iconsgroup-position-horizontal horizontal-position-bottom'       => esc_html__('Horizontal Bottom', 'meni'),
													'product-thumb-iconsgroup-position-horizontal horizontal-position-bottom-left'  => esc_html__('Horizontal Bottom Left', 'meni'),
													'product-thumb-iconsgroup-position-horizontal horizontal-position-bottom-right' => esc_html__('Horizontal Bottom Right', 'meni'),

													'product-thumb-iconsgroup-position-vertical vertical-position-top-left'         => esc_html__('Vertical Top Left', 'meni'),
													'product-thumb-iconsgroup-position-vertical vertical-position-top-right'        => esc_html__('Vertical Top Right', 'meni'),
													'product-thumb-iconsgroup-position-vertical vertical-position-middle-left'      => esc_html__('Vertical Middle Left', 'meni'),
													'product-thumb-iconsgroup-position-vertical vertical-position-middle-right'     => esc_html__('Vertical Middle Right', 'meni'),
													'product-thumb-iconsgroup-position-vertical vertical-position-bottom-left'      => esc_html__('Vertical Bottom Left', 'meni'),
													'product-thumb-iconsgroup-position-vertical vertical-position-bottom-right'     => esc_html__('Vertical Bottom Right', 'meni')

												),
									'default'    => ''
								),

								array(
									'id'         => 'product-thumb-buttonelement-button',
									'type'       => 'select',
									'title'      => esc_html__('Button Element - Button', 'meni'),
									'options'    => array(
														''          => esc_html__('None', 'meni'),
														'cart'      => esc_html__('Cart', 'meni'),
														'wishlist'  => esc_html__('Wishlist', 'meni'),
														'compare'   => esc_html__('Compare', 'meni'),
														'quickview' => esc_html__('Quick View', 'meni')
													)
								),	

								array(
									'id'         => 'product-thumb-buttonelement-secondary-button',
									'type'       => 'select',
									'title'      => esc_html__('Button Element - Secondary Button', 'meni'),
									'options'    => array(
														''          => esc_html__('None', 'meni'),
														'cart'      => esc_html__('Cart', 'meni'),
														'wishlist'  => esc_html__('Wishlist', 'meni'),
														'compare'   => esc_html__('Compare', 'meni'),
														'quickview' => esc_html__('Quick View', 'meni')
													)
								),

								array(
									'id'         => 'product-thumb-buttonelement-style',
									'type'       => 'select',
									'title'      => esc_html__('Button Element - Style', 'meni'),
									'options'    => array(
														'product-thumb-buttonelement-style-simple'  => esc_html__('Simple', 'meni'),
														'product-thumb-buttonelement-style-bgfill-square'  => esc_html__('Background Fill Square', 'meni'),
														'product-thumb-buttonelement-style-bgfill-rounded-square' => esc_html__('Background Fill Rounded Square', 'meni'),
														'product-thumb-buttonelement-style-bgfill-rounded'  => esc_html__('Background Fill Rounded', 'meni'),
														'product-thumb-buttonelement-style-brdrfill-square'  => esc_html__('Border Fill Square', 'meni'),
														'product-thumb-buttonelement-style-brdrfill-rounded-square' => esc_html__('Border Fill Rounded Square', 'meni'),
														'product-thumb-buttonelement-style-brdrfill-rounded'  => esc_html__('Border Fill Rounded', 'meni'),
														'product-thumb-buttonelement-style-skinbgfill-square'  => esc_html__('Skin Background Fill Square', 'meni'),
														'product-thumb-buttonelement-style-skinbgfill-rounded-square' => esc_html__('Skin Background Fill Rounded Square', 'meni'),
														'product-thumb-buttonelement-style-skinbgfill-rounded'  => esc_html__('Skin Background Fill Rounded', 'meni'),
														'product-thumb-buttonelement-style-skinbrdrfill-square'  => esc_html__('Skin Border Fill Square', 'meni'),
														'product-thumb-buttonelement-style-skinbrdrfill-rounded-square' => esc_html__('Skin Border Fill Rounded Square', 'meni'),
														'product-thumb-buttonelement-style-skinbrdrfill-rounded'  => esc_html__('Skin Border Fill Rounded', 'meni')																
													),
									'default'    => 'product-thumb-buttonelement-style-simple'
								),

								array(
									'id'         => 'product-thumb-buttonelement-stretch',
									'type'       => 'select',
									'title'      => esc_html__('Button Element - Stretch', 'meni'),
									'options'    => array(
														''                                    => esc_html__('False', 'meni'),
														'product-thumb-buttonelement-stretch' => esc_html__('True', 'meni')
													)
								),

								array(
									'id'             => 'product-thumb-element-group',
									'type'           => 'sorter',
									'title'          => esc_html__('Element Group Content', 'meni'),
									'default'        => array(
										'enabled'      => array(
											'title' => esc_html__('Title', 'meni'),
											'price' => esc_html__('Price', 'meni')
										),
										'disabled'     => array(
											'cart'           => esc_html__('Cart', 'meni'),
											'wishlist'       => esc_html__('Wishlist', 'meni'),
											'compare'        => esc_html__('Compare', 'meni'),
											'quickview'      => esc_html__('Quick View', 'meni'),
											'category'       => esc_html__('Category', 'meni'),
											'button_element' => esc_html__('Button Element', 'meni'),
											'icons_group'    => esc_html__('Icons Group', 'meni'),
											'excerpt'        => esc_html__('Excerpt', 'meni'),
											'rating'         => esc_html__('Rating', 'meni'),
											'separator'      => esc_html__('Separator', 'meni'),
											'swatches'       => esc_html__('Swatches', 'meni')
										),
									),
									'enabled_title'  => esc_html__('Active Elements', 'meni'),
									'disabled_title' => esc_html__('Deatcive Elements', 'meni'),
								),


							// "Product Style - Content" Options
								
								array(
									'type'    => 'notice',
									'class'   => 'info',
									'content' => esc_html__('"Product Style - Content" Options.', 'meni')
								),

								array(
									'id'         => 'product-content-enable',
									'type'       => 'switcher',
									'title'      => esc_html__('Enable Content Section', 'meni'),
									'desc'	 => esc_html__('YES! to enable content section.', 'meni')
								),

								array(
									'id'             => 'product-content-content',
									'type'           => 'sorter',
									'title'          => esc_html__('Content', 'meni'),
									'default'        => array(
										'enabled'      => array(
											'title'          => esc_html__('Title', 'meni'),
											'category'       => esc_html__('Category', 'meni'),
											'price'          => esc_html__('Price', 'meni'),
											'button_element' => esc_html__('Button Element', 'meni'),
											'icons_group'    => esc_html__('Icons Group', 'meni'),
										),
										'disabled'     => array(
											'excerpt'       => esc_html__('Excerpt', 'meni'),
											'rating'        => esc_html__('Rating', 'meni'),
											'countdown'     => esc_html__('Count Down', 'meni'),
											'separator'     => esc_html__('Separator', 'meni'),
											'element_group' => esc_html__('Element Group', 'meni'),
											'swatches'      => esc_html__('Swatches', 'meni'),
										),
									),
									'enabled_title'  => esc_html__('Active Elements', 'meni'),
									'disabled_title' => esc_html__('Deatcive Elements', 'meni'),
								),

								array(
									'id'         => 'product-content-alignment',
									'type'       => 'select',
									'title'      => esc_html__('Alignment', 'meni'),
									'options'    => array(
														'product-content-alignment-left'   => esc_html__('Left', 'meni'),
														'product-content-alignment-right'  => esc_html__('Right', 'meni'),
														'product-content-alignment-center' => esc_html__('Center', 'meni')
													),
									'default'    => 'product-content-alignment-left'
								),

								array(
									'id'         => 'product-content-iconsgroup-icons',
									'type'       => 'select',
									'title'      => esc_html__('Icons Group - Icons', 'meni'),
									'options'    => array(
														'cart'      => esc_html__('Cart', 'meni'),
														'wishlist'  => esc_html__('Wishlist', 'meni'),
														'compare'   => esc_html__('Compare', 'meni'),
														'quickview' => esc_html__('Quick View', 'meni')
													),
									'class'         => 'chosen',
									'attributes'    => array(
										'multiple'    => 'multiple',
									),							
								),

								array(
									'id'         => 'product-content-iconsgroup-style',
									'type'       => 'select',
									'title'      => esc_html__('Icons Group - Style', 'meni'),
									'options'    => array(
														'product-content-iconsgroup-style-simple'  => esc_html__('Simple', 'meni'),
														'product-content-iconsgroup-style-bgfill-square'  => esc_html__('Background Fill Square', 'meni'),
														'product-content-iconsgroup-style-bgfill-rounded-square' => esc_html__('Background Fill Rounded Square', 'meni'),
														'product-content-iconsgroup-style-bgfill-rounded'  => esc_html__('Background Fill Rounded', 'meni'),
														'product-content-iconsgroup-style-brdrfill-square'  => esc_html__('Border Fill Square', 'meni'),
														'product-content-iconsgroup-style-brdrfill-rounded-square' => esc_html__('Border Fill Rounded Square', 'meni'),
														'product-content-iconsgroup-style-brdrfill-rounded'  => esc_html__('Border Fill Rounded', 'meni'),
														'product-content-iconsgroup-style-skinbgfill-square'  => esc_html__('Skin Background Fill Square', 'meni'),
														'product-content-iconsgroup-style-skinbgfill-rounded-square' => esc_html__('Skin Background Fill Rounded Square', 'meni'),
														'product-content-iconsgroup-style-skinbgfill-rounded'  => esc_html__('Skin Background Fill Rounded', 'meni'),
														'product-content-iconsgroup-style-skinbrdrfill-square'  => esc_html__('Skin Border Fill Square', 'meni'),
														'product-content-iconsgroup-style-skinbrdrfill-rounded-square' => esc_html__('Skin Border Fill Rounded Square', 'meni'),
														'product-content-iconsgroup-style-skinbrdrfill-rounded'  => esc_html__('Skin Border Fill Rounded', 'meni')																													
													),
									'default'    => 'product-content-iconsgroup-style-simple'
								),

								array(
									'id'         => 'product-content-buttonelement-button',
									'type'       => 'select',
									'title'      => esc_html__('Button Element - Button', 'meni'),
									'options'    => array(
														''          => esc_html__('None', 'meni'),
														'cart'      => esc_html__('Cart', 'meni'),
														'wishlist'  => esc_html__('Wishlist', 'meni'),
														'compare'   => esc_html__('Compare', 'meni'),
														'quickview' => esc_html__('Quick View', 'meni')
													)
								),	

								array(
									'id'         => 'product-content-buttonelement-secondary-button',
									'type'       => 'select',
									'title'      => esc_html__('Button Element - Secondary Button', 'meni'),
									'options'    => array(
														''          => esc_html__('None', 'meni'),
														'cart'      => esc_html__('Cart', 'meni'),
														'wishlist'  => esc_html__('Wishlist', 'meni'),
														'compare'   => esc_html__('Compare', 'meni'),
														'quickview' => esc_html__('Quick View', 'meni')
													)
								),

								array(
									'id'         => 'product-content-buttonelement-style',
									'type'       => 'select',
									'title'      => esc_html__('Button Element - Style', 'meni'),
									'options'    => array(
														'product-content-buttonelement-style-simple'  => esc_html__('Simple', 'meni'),
														'product-content-buttonelement-style-bgfill-square'  => esc_html__('Background Fill Square', 'meni'),
														'product-content-buttonelement-style-bgfill-rounded-square' => esc_html__('Background Fill Rounded Square', 'meni'),
														'product-content-buttonelement-style-bgfill-rounded'  => esc_html__('Background Fill Rounded', 'meni'),
														'product-content-buttonelement-style-brdrfill-square'  => esc_html__('Border Fill Square', 'meni'),
														'product-content-buttonelement-style-brdrfill-rounded-square' => esc_html__('Border Fill Rounded Square', 'meni'),
														'product-content-buttonelement-style-brdrfill-rounded'  => esc_html__('Border Fill Rounded', 'meni'),
														'product-content-buttonelement-style-skinbgfill-square'  => esc_html__('Skin Background Fill Square', 'meni'),
														'product-content-buttonelement-style-skinbgfill-rounded-square' => esc_html__('Skin Background Fill Rounded Square', 'meni'),
														'product-content-buttonelement-style-skinbgfill-rounded'  => esc_html__('Skin Background Fill Rounded', 'meni'),
														'product-content-buttonelement-style-skinbrdrfill-square'  => esc_html__('Skin Border Fill Square', 'meni'),
														'product-content-buttonelement-style-skinbrdrfill-rounded-square' => esc_html__('Skin Border Fill Rounded Square', 'meni'),
														'product-content-buttonelement-style-skinbrdrfill-rounded'  => esc_html__('Skin Border Fill Rounded', 'meni')																													
													),
									'default'    => 'product-content-buttonelement-style-simple'
								),

								array(
									'id'         => 'product-content-buttonelement-stretch',
									'type'       => 'select',
									'title'      => esc_html__('Button Element - Stretch', 'meni'),
									'options'    => array(
														''                                    => esc_html__('False', 'meni'),
														'product-content-buttonelement-stretch' => esc_html__('True', 'meni')
													)
								),

								array(
									'id'             => 'product-content-element-group',
									'type'           => 'sorter',
									'title'          => esc_html__('Element Group Content', 'meni'),
									'default'        => array(
										'enabled'      => array(
											'title'          => esc_html__('Title', 'meni'),
											'price'          => esc_html__('Price', 'meni')
										),
										'disabled'     => array(
											'cart'           => esc_html__('Cart', 'meni'),
											'wishlist'       => esc_html__('Wishlist', 'meni'),
											'compare'        => esc_html__('Compare', 'meni'),
											'quickview'      => esc_html__('Quick View', 'meni'),
											'category'       => esc_html__('Category', 'meni'),
											'button_element' => esc_html__('Button Element', 'meni'),
											'icons_group'    => esc_html__('Icons Group', 'meni'),
											'excerpt'        => esc_html__('Excerpt', 'meni'),
											'rating'         => esc_html__('Rating', 'meni'),
											'separator'      => esc_html__('Separator', 'meni'),
											'swatches'       => esc_html__('Swatches', 'meni')
										),
									),
									'enabled_title'  => esc_html__('Active Elements', 'meni'),
									'disabled_title' => esc_html__('Deactive Elements', 'meni')
								),

							
							),

							'default' => array (
														dt_sc_woo_default_product_settings()
												),							

						)					
					)
				),

			// Others
				array(
					'name'   => 'dt-woo-other-settings',
					'title'  => esc_html__('Others', 'meni'),
					'icon'   => 'fa fa-angle-double-right',
					'fields' => array(

						array(	
							'type'    => 'subheading',
							'content' => esc_html__( 'Other Settings', 'meni' ),
						),

						array(
							'id'    => 'dt-woo-quantity-plusnminus',
							'type'  => 'switcher',
							'title' => esc_html__('Enable Plus / Minus Button - Quantity', 'meni'),
						),

						array(	
							'type'    => 'subheading',
							'content' => esc_html__( 'Cart Settings', 'meni' ),
						),

						array(
							'id'         => 'dt-woo-addtocart-custom-action',
							'type'       => 'select',
							'title'      => esc_html__('Add To Cart Custom Action', 'meni'),
							'options'    => array(
												''                    => esc_html__('None', 'meni'),
												'sidebar_widget'      => esc_html__('Sidebar Widget', 'meni'),
												'notification_widget' => esc_html__('Notification Widget', 'meni'),
											),
							'default'    => '',
						),

						array(
							'id'      => 'dt-woo-cross-sell-column',
							'type'    => 'image_select',
							'title'   => esc_html__('Cross Sell Prodcut Column', 'meni'),
							'options' => array(
								2 => MENI_THEME_URI . '/cs-framework-override/images/one-half-column.png',
								3 => MENI_THEME_URI . '/cs-framework-override/images/one-third-column.png',
								4 => MENI_THEME_URI . '/cs-framework-override/images/one-fourth-column.png',
							),
							'default' => 2,
						),

						array(
							'id'  		=> 'dt-cross-sell-title',
							'type'  	=> 'wysiwyg',
							'title' 	=> esc_html__('Cross Sell Title', 'meni'),
							'default'	=> '<h2>You may be interested in&hellip;</h2>'
						),

						array(
							'id'         => 'dt-woo-cross-sell-style-template',
							'type'       => 'select',
							'title'      => esc_html__('Product Style Template', 'meni'),
							'options'    => $product_style_templates_arr
						),

					)
				),
		)
	);
}

CSFramework::instance( $settings, $options );