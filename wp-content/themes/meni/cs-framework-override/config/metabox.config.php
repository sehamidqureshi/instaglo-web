<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// -----------------------------------------
// Layer Sliders
// -----------------------------------------
function meni_layersliders() {
  $layerslider = array(  esc_html__('Select a slider','meni') );

  if( class_exists('LS_Sliders') ) {

    $sliders = LS_Sliders::find(array('limit' => 50));

    if(!empty($sliders)) {
      foreach($sliders as $key => $item){
        $layerslider[ $item['id'] ] = $item['name'];
      }
    }
  }

  return $layerslider;
}

// -----------------------------------------
// Revolution Sliders
// -----------------------------------------
function meni_revolutionsliders() {
  $revolutionslider = array( '' => esc_html__('Select a slider','meni') );

  if( class_exists('RevSlider') ) {  
    $sld = new RevSlider();
    $sliders = $sld->getArrSliders();
    if(!empty($sliders)){
      foreach($sliders as $key => $item) {
        $revolutionslider[$item->getAlias()] = $item->getTitle();
      }
    }    
  }

  return $revolutionslider;  
}

// -----------------------------------------
// Meta Layout Section
// -----------------------------------------

$cart_page_id = get_option('woocommerce_cart_page_id');
$checkout_page_id = get_option('woocommerce_checkout_page_id');
$myaccount_page_id = get_option('woocommerce_myaccount_page_id');
$wishlist_page_id = get_option( 'yith_wcwl_wishlist_page_id' );

if( isset( $_GET['post'] ) && ( $_GET['post'] == $cart_page_id || $_GET['post'] == $checkout_page_id || $_GET['post'] == $myaccount_page_id || $_GET['post'] == $wishlist_page_id ) ) {	

  $page_layout_options = array(
      'global-site-layout' 	 => MENI_THEME_URI . '/cs-framework-override/images/admin-option.png',
      'content-full-width'   => MENI_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
      'with-left-sidebar'    => MENI_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
      'with-right-sidebar'   => MENI_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
  );

} else {

  $page_layout_options = array(
    'global-site-layout' 	 => MENI_THEME_URI . '/cs-framework-override/images/admin-option.png',
    'content-full-width'   => MENI_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
    'with-left-sidebar'    => MENI_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
    'with-right-sidebar'   => MENI_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
    'with-both-sidebar'    => MENI_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
  );

}

$meta_layout_section =array(
  'name'  => 'layout_section',
  'title' => esc_html__('Layout', 'meni'),
  'icon'  => 'fa fa-columns',
  'fields' =>  array(
    array(
      'id'  => 'layout',
      'type' => 'image_select',
      'title' => esc_html__('Page Layout', 'meni' ),
      'options'      => $page_layout_options,
      'default'      => 'global-site-layout',
      'attributes'   => array( 'data-depend-id' => 'page-layout' )
    ),
    array(
      'id'         => 'show-standard-sidebar-left',
      'type'       => 'switcher',
      'title'      => esc_html__('Show Standard Left Sidebar', 'meni' ),
      'dependency' => array( 'page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
    'default'	   => true,
    ),
    array(
      'id'        => 'widget-area-left',
      'type'      => 'select',
      'title'     => esc_html__('Choose Left Widget Areas', 'meni' ),
      'class'     => 'chosen',
      'options'   => meni_custom_widgets(),
      'attributes'  => array( 
        'multiple'  => 'multiple',
        'data-placeholder' => esc_html__('Select Left Widget Areas','meni'),
        'style' => 'width: 400px;'
      ),
      'dependency'  => array( 'page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
    ),
    array(
      'id'          => 'show-standard-sidebar-right',
      'type'        => 'switcher',
      'title'       => esc_html__('Show Standard Right Sidebar', 'meni' ),
      'dependency'  => array( 'page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
    'default'	  	=> true,
    ),
    array(
      'id'        => 'widget-area-right',
      'type'      => 'select',
      'title'     => esc_html__('Choose Right Widget Areas', 'meni' ),
      'class'     => 'chosen',
      'options'   => meni_custom_widgets(),
      'attributes'    => array( 
        'multiple' => 'multiple',
        'data-placeholder' => esc_html__('Select Right Widget Areas','meni'),
        'style' => 'width: 400px;'
      ),
      'dependency'  => array( 'page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
    )
  )
);

// -----------------------------------------
// Meta Breadcrumb Section
// -----------------------------------------
$meta_breadcrumb_section = array(
  'name'  => 'breadcrumb_section',
  'title' => esc_html__('Breadcrumb', 'meni'),
  'icon'  => 'fa fa-sitemap',
  'fields' =>  array(
    array(
      'id'      => 'enable-sub-title',
      'type'    => 'switcher',
      'title'   => esc_html__('Show Breadcrumb', 'meni' ),
      'default' => true
    ),
    array(
    	'id'                 => 'breadcrumb_position',
	'type'               => 'select',
      'title'              => esc_html__('Position', 'meni' ),
      'options'            => array(
        'header-top-absolute'    => esc_html__('Behind the Header','meni'),
        'header-top-relative' 	   => esc_html__('Default','meni'),
		),
		'default'            => 'header-top-relative',	
      'dependency'         => array( 'enable-sub-title', '==', 'true' ),
    ),    
    array(
      'id'    => 'breadcrumb_background',
      'type'  => 'background',
      'title' => esc_html__('Background', 'meni' ),
      'dependency'   => array( 'enable-sub-title', '==', 'true' ),
    ),
  )
);

// -----------------------------------------
// Meta Slider Section
// -----------------------------------------
$meta_slider_section = array(
  'name'  => 'slider_section',
  'title' => esc_html__('Slider', 'meni'),
  'icon'  => 'fa fa-slideshare',
  'fields' =>  array(
    array(
      'id'           => 'slider-notice',
      'type'         => 'notice',
      'class'        => 'danger',
      'content'      => esc_html__('Slider tab works only if breadcrumb disabled.','meni'),
      'class'        => 'margin-30 cs-danger',
      'dependency'   => array( 'enable-sub-title', '==', 'true' ),
    ),

    array(
      'id'           => 'show_slider',
      'type'         => 'switcher',
      'title'        => esc_html__('Show Slider', 'meni' ),
      'dependency'   => array( 'enable-sub-title', '==', 'false' ),
    ),
    array(
    	'id'                 => 'slider_position',
	'type'               => 'select',
	'title'              => esc_html__('Position', 'meni' ),
	'options'            => array(
		'header-top-relative'     => esc_html__('Top Header Relative','meni'),
		'header-top-absolute'    => esc_html__('Top Header Absolute','meni'),
		'bottom-header' 	   => esc_html__('Bottom Header','meni'),
	),
	'default'            => 'bottom-header',
	'dependency'         => array( 'enable-sub-title|show_slider', '==|==', 'false|true' ),
   ),
   array(
      'id'                 => 'slider_type',
      'type'               => 'select',
      'title'              => esc_html__('Slider', 'meni' ),
      'options'            => array(
        ''                 => esc_html__('Select a slider','meni'),
        'layerslider'      => esc_html__('Layer slider','meni'),
        'revolutionslider' => esc_html__('Revolution slider','meni'),
        'customslider'     => esc_html__('Custom Slider Shortcode','meni'),
      ),
      'validate' => 'required',
      'dependency'         => array( 'enable-sub-title|show_slider', '==|==', 'false|true' ),
    ),

    array(
      'id'          => 'layerslider_id',
      'type'        => 'select',
      'title'       => esc_html__('Layer Slider', 'meni' ),
      'options'     => meni_layersliders(),
      'validate'    => 'required',
      'dependency'  => array( 'enable-sub-title|show_slider|slider_type', '==|==|==', 'false|true|layerslider' )
    ),

    array(
      'id'          => 'revolutionslider_id',
      'type'        => 'select',
      'title'       => esc_html__('Revolution Slider', 'meni' ),
      'options'     => meni_revolutionsliders(),
      'validate'    => 'required',
      'dependency'  => array( 'enable-sub-title|show_slider|slider_type', '==|==|==', 'false|true|revolutionslider' )
    ),

    array(
      'id'          => 'customslider_sc',
      'type'        => 'textarea',
      'title'       => esc_html__('Custom Slider Code', 'meni' ),
      'validate'    => 'required',
      'dependency'  => array( 'enable-sub-title|show_slider|slider_type', '==|==|==', 'false|true|customslider' )
    ),
  )  
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// METABOX OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options = array();

// -----------------------------------------
// Page Metabox Options                    -
// -----------------------------------------
array_push( $meta_layout_section['fields'], array(
  'id'        => 'enable-sticky-sidebar',
  'type'      => 'switcher',
  'title'     => esc_html__('Enable Sticky Sidebar', 'meni' ),
  'dependency'  => array( 'page-layout', 'any', 'with-left-sidebar,with-right-sidebar,with-both-sidebar' )
) );

$page_settings = array(
  $meta_layout_section,
  $meta_breadcrumb_section,
  $meta_slider_section,
  array(
    'name'   => 'sidenav_template_section',
    'title'  => esc_html__('Side Navigation Template', 'meni'),
    'icon'   => 'fa fa-th-list',
    'fields' =>  array(
      array(
        'id'      => 'sidenav-tpl-notice',
        'type'    => 'notice',
        'class'   => 'success',
        'content' => esc_html__('Side Navigation Tab Works only if page template set to Side Navigation Template in Page Attributes','meni'),
        'class'   => 'margin-30 cs-success',      
      ),
      array(
        'id'      => 'sidenav-style',
        'type'    => 'select',
        'title'   => esc_html__('Side Navigation Style', 'meni' ),
        'options' => array(
          'type1'  => esc_html__('Type1','meni'),
          'type2'  => esc_html__('Type2','meni'),
          'type3'  => esc_html__('Type3','meni'),
          'type4'  => esc_html__('Type4','meni'),
          'type5'  => esc_html__('Type5','meni')
        ),
      ),
      array(
        'id'    => 'sidenav-align',
        'type'  => 'switcher',
        'title' => esc_html__('Align Right', 'meni' ),
        'info'  => esc_html__('YES! to align right of side navigation.','meni')
      ),
      array(
        'id'    => 'sidenav-sticky',
        'type'  => 'switcher',
        'title' => esc_html__('Sticky Side Navigation', 'meni' ),
        'info'  => esc_html__('YES! to sticky side navigation content.','meni')
      ),
      array(
        'id'    => 'enable-sidenav-content',
        'type'  => 'switcher',
        'title' => esc_html__('Show Content', 'meni' ),
        'info'  => esc_html__('YES! to show content in below side navigation.','meni')
      ),
      array(
        'id'         => 'sidenav-content',
        'type'       => 'textarea',
        'title'      => esc_html__('Side Navigation Content', 'meni' ),
        'info'       => esc_html__('Paste any shortcode content here','meni'),
        'attributes' => array( 'rows' => 6 ),
      ),
    )
  ),  
);

$shop_page_id = get_option('woocommerce_shop_page_id');

if( isset( $_GET['post'] ) && ( $_GET['post'] != $shop_page_id ) || !isset( $_GET['post'] )  ) {	
  $options[] = array(
    'id'        => '_tpl_default_settings',
    'title'     => esc_html__('Page Settings','meni'),
    'post_type' => 'page',
    'context'   => 'normal',
    'priority'  => 'high',
    'sections'  => $page_settings
  );

}

// -----------------------------------------
// Post Metabox Options                    -
// -----------------------------------------
$post_meta_layout_section = $meta_layout_section;
$fields = $post_meta_layout_section['fields'];

	$fields[0]['title'] =  esc_html__('Post Layout', 'meni' );
	#unset( $fields[0]['options']['with-both-sidebar'] );
	#unset( $fields[0]['info'] );
	#unset( $fields[0]['options']['fullwidth'] );
	unset( $fields[5] );
	unset( $post_meta_layout_section['fields'] );
	$post_meta_layout_section['fields']  = $fields;  

	$post_format_section = array(
		'name'  => 'post_format_data_section',
		'title' => esc_html__('Post Format', 'meni'),
		'icon'  => 'fa fa-cog',
		'fields' =>  array(

			array(
				'id'           => 'single-post-style',
				'type'         => 'select',
				'title'        => esc_html__('Post Style', 'meni'),
				'options'      => array(
          'breadcrumb-fixed'    => esc_html__('Breadcrumb Fixed', 'meni'),
          'breadcrumb-parallax' => esc_html__('Breadcrumb Parallax', 'meni'),
          'overlay'             => esc_html__('Overlay', 'meni'),
          'overlap'             => esc_html__('Overlap', 'meni'),
          'custom-classic'      => esc_html__('Classic', 'meni'),
          'custom-classic-ii'   => esc_html__('Classic II', 'meni'),
          'custom-modern'       => esc_html__('Modern', 'meni'),
          'custom'              => esc_html__('Custom', 'meni')
				),
				'class'        => 'chosen',
				'default'      => 'custom-classic-ii',
				'info'         => esc_html__('Choose post style to display single post. If post style is "Custom", display based on Editor content.', 'meni')
			),

			array(
				'id'           => 'single-custom-style',
				'type'         => 'select',
				'title'        => esc_html__('Custom Style', 'meni'),
				'options'      => array(
          'minimal-ii'    => esc_html__('Minimal II', 'meni'),
				),
				'class'        => 'chosen',
				'default'      => 'minimal-ii',
				'info'         => esc_html__('Select type of custom style.', 'meni'),
				'dependency'   => array( 'single-post-style', '==', 'custom' ),
			),

			array(
			    'id'           => 'view_count',
			    'type'         => 'number',
			    'title'        => esc_html__('Views', 'meni' ),
				'info'         => esc_html__('No.of views of this post.', 'meni'),
	          	'attributes' => array(
		           'style'    => 'width: 15%;'
        	    ),
			),

			array(
			    'id'           => 'like_count',
			    'type'         => 'number',
			    'title'        => esc_html__('Likes', 'meni' ),
				'info'         => esc_html__('No.of likes of this post.', 'meni'),
	          	'attributes' => array(
		           'style'    => 'width: 15%;'
        	    ),
			),

			array(
				'id' => 'post-format-type',
				'title'   => esc_html__('Type', 'meni' ),
				'type' => 'select',
				'default' => 'standard',
				'options' => array(
					'standard'  => esc_html__('Standard', 'meni'),
					'status'	=> esc_html__('Status','meni'),
					'quote'		=> esc_html__('Quote','meni'),
					'gallery'	=> esc_html__('Gallery','meni'),
					'image'		=> esc_html__('Image','meni'),
					'video'		=> esc_html__('Video','meni'),
					'audio'		=> esc_html__('Audio','meni'),
					'link'		=> esc_html__('Link','meni'),
					'aside'		=> esc_html__('Aside','meni'),
					'chat'		=> esc_html__('Chat','meni')
				),
				'info'         => esc_html__('Post Format & Type should be Same. Check the Post Format from the "Format" Tab, which comes in the Right Side Section', 'meni'),
			),

			array(
				'id' 	  => 'post-gallery-items',
				'type'	  => 'gallery',
				'title'   => esc_html__('Add Images', 'meni' ),
				'add_title'   => esc_html__('Add Images', 'meni' ),
				'edit_title'  => esc_html__('Edit Images', 'meni' ),
				'clear_title' => esc_html__('Remove Images', 'meni' ),
				'dependency' => array( 'post-format-type', '==', 'gallery' ),
			),

			array(
				'id' 	  => 'media-type',
				'type'	  => 'select',
				'title'   => esc_html__('Select Type', 'meni' ),
				'dependency' => array( 'post-format-type', 'any', 'video,audio' ),
		      	'options'	=> array(
					'oembed' => esc_html__('Oembed','meni'),
					'self' => esc_html__('Self Hosted','meni'),
				)
			),

			array(
				'id' 	  => 'media-url',
				'type'	  => 'textarea',
				'title'   => esc_html__('Media URL', 'meni' ),
				'dependency' => array( 'post-format-type', 'any', 'video,audio' ),
			),
		)
	);

	$options[] = array(
		'id'        => '_dt_post_settings',
		'title'     => esc_html__('Post Settings','meni'),
		'post_type' => 'post',
		'context'   => 'normal',
		'priority'  => 'high',
		'sections'  => array(
			$post_meta_layout_section,
			$meta_breadcrumb_section,
			$post_format_section			
		)
	);

// -----------------------------------------
// Tribe Events Post Metabox Options
// -----------------------------------------
  array_push( $post_meta_layout_section['fields'], array(
    'id' => 'event-post-style',
    'title'   => esc_html__('Post Style', 'meni' ),
    'type' => 'select',
    'default' => 'type1',
    'options' => array(
      'type1'  => esc_html__('Classic', 'meni'),
      'type2'  => esc_html__('Full Width','meni'),
      'type3'  => esc_html__('Minimal Tab','meni'),
      'type4'  => esc_html__('Clean','meni'),
      'type5'  => esc_html__('Modern','meni'),
    ),
	'class'    => 'chosen',
	'info'     => esc_html__('Your event post page show at most selected style.', 'meni')
  ) );

  $options[] = array(
    'id'        => '_custom_settings',
    'title'     => esc_html__('Settings','meni'),
    'post_type' => 'tribe_events',
    'context'   => 'normal',
    'priority'  => 'high',
    'sections'  => array(
      $post_meta_layout_section,
      $meta_breadcrumb_section
    )
  );

  if( function_exists( 'is_woocommerce' ) ) {

    $woo_size_guides = cs_get_option( 'dt-woo-size-guides' );
    $woo_size_guides = (is_array($woo_size_guides) && !empty($woo_size_guides)) ? $woo_size_guides : false;
  
    $size_guides[] = esc_html__('None', 'meni');
    if($woo_size_guides) {
      foreach($woo_size_guides as $woo_size_guide_key => $woo_size_guide) {
        $size_guides[$woo_size_guide_key] = $woo_size_guide['title'];
      }
    }
    
    $product_meta_layout_section = array(
      'name'   => 'general_section',
      'title'  => esc_html__('General', 'meni'),
      'icon'   => 'fa fa-angle-double-right',
      'fields' =>  array(
          array(
              'id'         => 'page-layout',
              'type'       => 'image_select',
              'title'      => esc_html__('Page Layout', 'meni'),
              'options'    => array(
                  'admin-option'       => MENI_THEME_URI . '/cs-framework-override/images/admin-option.png',
                  'content-full-width' => MENI_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
                  'with-left-sidebar'  => MENI_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
                  'with-right-sidebar' => MENI_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
              ),
              'default'    => 'admin-option',
              'attributes' => array( 'data-depend-id' => 'page-layout' )
          ),
          array(
              'id'         => 'show-standard-sidebar',
              'type'       => 'switcher',
              'title'      => esc_html__('Show Product Standard Sidebar', 'meni'),
              'dependency' => array( 'page-layout', 'any', 'with-left-sidebar,with-right-sidebar' )
          ),
          array(
              'id'         => 'product-widgetareas',
              'type'       => 'select',
              'title'      => esc_html__('Choose Custom Widget Area', 'meni'),
              'class'      => 'chosen',
              'options'    => meni_custom_widgets(),
              'dependency' => array( 'page-layout', 'any', 'with-left-sidebar,with-right-sidebar' ),
              'attributes' => array(
                  'multiple'         => 'multiple',
                  'data-placeholder' => esc_attr__('Select Widget Areas', 'meni'),
                  'style'            => 'width: 400px;'
              ),
          ),
  
          # Product Template
          array(
              'id'      => 'product-template',
              'type'    => 'select',
              'title'   => esc_html__('Product Template', 'meni'),
              'class'   => 'chosen',
              'options' => array(
                  'admin-option'    => esc_html__( 'Admin Option', 'meni' ),
                  'woo-default'     => esc_html__( 'WooCommerce Default', 'meni' ),
                  'custom-template' => esc_html__( 'Custom Template', 'meni' )
              ),
              'default'    => 'admin-option',
              'info'       => esc_html__('Don\'t use product shortcodes in content area when "WooCommerce Default" template is chosen.', 'meni')
          ),
                 
          array(
              'id'         => 'show-upsell',
              'type'       => 'select',
              'title'      => esc_html__('Show Upsell Products', 'meni'),
              'class'      => 'chosen',
              'default'    => 'admin-option',
              'attributes' => array( 'data-depend-id' => 'show-upsell' ),
              'options'    => array(
                  'admin-option' => esc_html__( 'Admin Option', 'meni' ),
                  'true'         => esc_html__( 'Show', 'meni'),
                  null           => esc_html__( 'Hide', 'meni'),
              ),
              'dependency' => array( 'product-template', '!=', 'custom-template')
          ),
          array(
              'id'         => 'upsell-column',
              'type'       => 'select',
              'title'      => esc_html__('Choose Upsell Column', 'meni'),
              'class'      => 'chosen',
              'default'    => 4,
              'options'    => array(
                  'admin-option' => esc_html__( 'Admin Option', 'meni' ),
                  1              => esc_html__( 'One Column', 'meni' ),
                  2              => esc_html__( 'Two Columns', 'meni' ),
                  3              => esc_html__( 'Three Columns', 'meni' ),
                  4              => esc_html__( 'Four Columns', 'meni' ),
              ),
              'dependency' => array( 'product-template|show-upsell', '!=|==', 'custom-template|true')
          ),
          array(
              'id'         => 'upsell-limit',
              'type'       => 'select',
              'title'      => esc_html__('Choose Upsell Limit', 'meni'),
              'class'      => 'chosen',
              'default'    => 4,
              'options'    => array(
                  'admin-option' => esc_html__( 'Admin Option', 'meni' ),
                  1              => esc_html__( 'One', 'meni' ),
                  2              => esc_html__( 'Two', 'meni' ),
                  3              => esc_html__( 'Three', 'meni' ),
                  4              => esc_html__( 'Four', 'meni' ),
                  5              => esc_html__( 'Five', 'meni' ),
                  6              => esc_html__( 'Six', 'meni' ),
                  7              => esc_html__( 'Seven', 'meni' ),
                  8              => esc_html__( 'Eight', 'meni' ),
                  9              => esc_html__( 'Nine', 'meni' ),
                  10              => esc_html__( 'Ten', 'meni' ),                                                
              ),
              'dependency' => array( 'product-template|show-upsell', '!=|==', 'custom-template|true')
          ),        
          array(
              'id'         => 'show-related',
              'type'       => 'select',
              'title'      => esc_html__('Show Related Products', 'meni'),
              'class'      => 'chosen',
              'default'    => 'admin-option',
              'attributes' => array( 'data-depend-id' => 'show-related' ),
              'options'    => array(
                  'admin-option' => esc_html__( 'Admin Option', 'meni' ),
                  'true'         => esc_html__( 'Show', 'meni'),
                  null           => esc_html__( 'Hide', 'meni'),
              ),
              'dependency' => array( 'product-template', '!=', 'custom-template')
          ),
          array(
              'id'         => 'related-column',
              'type'       => 'select',
              'title'      => esc_html__('Choose Related Column', 'meni'),
              'class'      => 'chosen',
              'default'    => 4,
              'options'    => array(
                  'admin-option' => esc_html__( 'Admin Option', 'meni' ),
                  2              => esc_html__( 'Two Columns', 'meni' ),
                  3              => esc_html__( 'Three Columns', 'meni' ),
                  4              => esc_html__( 'Four Columns', 'meni' ),
              ),
              'dependency' => array( 'product-template|show-related', '!=|==', 'custom-template|true')
          ),
          array(
              'id'         => 'related-limit',
              'type'       => 'select',
              'title'      => esc_html__('Choose Related Limit', 'meni'),
              'class'      => 'chosen',
              'default'    => 4,
              'options'    => array(
                  'admin-option' => esc_html__( 'Admin Option', 'meni' ),
                  1              => esc_html__( 'One', 'meni' ),
                  2              => esc_html__( 'Two', 'meni' ),
                  3              => esc_html__( 'Three', 'meni' ),
                  4              => esc_html__( 'Four', 'meni' ),
                  5              => esc_html__( 'Five', 'meni' ),
                  6              => esc_html__( 'Six', 'meni' ),
                  7              => esc_html__( 'Seven', 'meni' ),
                  8              => esc_html__( 'Eight', 'meni' ),
                  9              => esc_html__( 'Nine', 'meni' ),
                  10              => esc_html__( 'Ten', 'meni' ),                                                
              ),
              'dependency' => array( 'product-template|show-related', '!=|==', 'custom-template|true')
          ),
  
          # Product Additional Tabs
          array(
            'id'              => 'product-additional-tabs',
            'type'            => 'group',
            'title'           => esc_html__('Additional Tabs', 'meni'),
            'info'            => esc_html__('Click button to add title and description.', 'meni'),
            'button_title'    => esc_html__('Add New Tab', 'meni'),
            'accordion_title' => esc_html__('Adding New Tab Field', 'meni'),
            'fields'          => array(
              array(
              'id'          => 'tab_title',
              'type'        => 'text',
              'title'       => esc_html__('Title', 'meni'),
              ),
  
              array(
              'id'          => 'tab_description',
              'type'        => 'textarea',
              'title'       => esc_html__('Description', 'meni')
              ),
            )
          ),
  
          # Product New Label
           array(
              'id'         => 'product-new-label',
              'type'       => 'switcher',
              'title'      => esc_html__('Add "New" label', 'meni'),
          ), 
  
          array(
            'id'         => 'dt-single-product-size-guides',
            'type'       => 'select',
            'title'      => esc_html__('Product Size Guides', 'meni'),
            'options'    => $size_guides,
            //'info'       => esc_html__('Choose product size guide that defined in admin section.', 'meni')
          ),              
  
          array(
            'id'          => 'description',
            'type'        => 'textarea',
            'title'       => esc_html__('Description', 'meni'),
            'info'       => esc_html__('This content will be used in description tab, when "Custom Template" is chosen.', 'meni')
            ),
  
      )
    );
  
    $options[] = array(
      'id'        => '_custom_settings',
      'title'     => esc_html__('Product Settings','meni'),
      'post_type' => 'product',
      'context'   => 'normal',
      'priority'  => 'high',
      'sections'  => array(
        $product_meta_layout_section
      )
    );
  
    $options[] = array(
      'id'        => '_360viewer_gallery',
      'title'     => esc_html__('Product 360 View Gallery','meni'),
      'post_type' => 'product',
      'context'   => 'side',
      'priority'  => 'low',
      'sections'  => array(
                      array(
                      'name'   => '360view_section',
                      'fields' =>  array(
                                      array (
                                        'id'          => 'product-360view-gallery',
                                        'type'        => 'gallery',
                                        'title'       => esc_html__('Gallery Images', 'meni'),
                                        'desc'        => esc_html__('Simply add images to gallery items.', 'meni'),
                                        'add_title'   => esc_html__('Add Images', 'meni'),
                                        'edit_title'  => esc_html__('Edit Images', 'meni'),
                                        'clear_title' => esc_html__('Remove Images', 'meni'),
                                      )
                                  )
                      )
                    )
      );
  
  
  }
  
// -----------------------------------------
// Header And Footer Options Metabox
// -----------------------------------------
$post_types = apply_filters( 'meni_header_footer_default_cpt' , array ( 'post', 'page', 'product' )  );
$options[] = array(
	'id'	=> '_dt_custom_settings',
	'title'	=> esc_html__('Header & Footer','meni'),
	'post_type' => $post_types,
	'priority'  => 'high',
	'context'   => 'side', 
	'sections'  => array(
	
		# Header Settings
		array(
			'name'  => 'header_section',
			'title' => esc_html__('Header', 'meni'),
			'icon'  => 'fa fa-angle-double-right',
			'fields' =>  array(
			
				# Header Show / Hide
				array(
					'id'		=> 'show-header',
					'type'		=> 'switcher',
					'title'		=> esc_html__('Show Header', 'meni'),
					'default'	=>  true,
				),
				
				# Header
				array(
					'id'  		 => 'header',
					'type'  	 => 'select',
					'title' 	 => esc_html__('Choose Header', 'meni'),
					'class'		 => 'chosen',
					'options'	 => 'posts',
					'query_args' => array(
						'post_type'	 => 'dt_headers',
						'orderby'	 => 'ID',
						'order'		 => 'ASC',
						'posts_per_page' => -1,
					),
					'default_option' => esc_attr__('Select Header', 'meni'),
					'attributes' => array( 'style'	=> 'width:50%' ),
					'info'		 => esc_html__('Select custom header for this page.','meni'),
					'dependency'	=> array( 'show-header', '==', 'true' )
				),							
			)			
		),
		# Header Settings

		# Footer Settings
		array(
			'name'  => 'footer_settings',
			'title' => esc_html__('Footer', 'meni'),
			'icon'  => 'fa fa-angle-double-right',
			'fields' =>  array(
			
				# Footer Show / Hide
				array(
					'id'		=> 'show-footer',
					'type'		=> 'switcher',
					'title'		=> esc_html__('Show Footer', 'meni'),
					'default'	=>  true,
				),
				
				# Footer
		        array(
					'id'         => 'footer',
					'type'       => 'select',
					'title'      => esc_html__('Choose Footer', 'meni'),
					'class'      => 'chosen',
					'options'    => 'posts',
					'query_args' => array(
						'post_type'  => 'dt_footers',
						'orderby'    => 'ID',
						'order'      => 'ASC',
						'posts_per_page' => -1,
					),
					'default_option' => esc_attr__('Select Footer', 'meni'),
					'attributes' => array( 'style'  => 'width:50%' ),
					'info'       => esc_html__('Select custom footer for this page.','meni'),
					'dependency'    => array( 'show-footer', '==', 'true' )
				),			
			)			
		),
		# Footer Settings
		
	)	
);
	
CSFramework_Metabox::instance( $options );