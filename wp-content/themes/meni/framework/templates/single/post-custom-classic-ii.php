<?php
	$format = !empty( $Post_Meta['post-format-type'] ) ? $Post_Meta['post-format-type'] : 'standard';

	$template_args['post_ID'] = $ID;
	$template_args['meta'] = $Post_Meta;
	$template_args['post_Style'] = $Post_Style; ?>

		<div class="entry-top-meta-group">
		    <!-- Entry Author -->
		    <div class="entry-author">
	    	    <?php meni_get_template( 'framework/templates/single/entry-author.php', $template_args ); ?>
	    	</div><!-- Entry Author -->

        	<!-- Entry Date -->
			<div class="entry-date">
				<?php meni_get_template( 'framework/templates/single/entry-date.php', $template_args ); ?>
			</div><!-- Entry Date -->
		</div>

	    <!-- Featured Image -->
	    <div class="entry-thumb single-preview-img">
	        <div class="blog-image">
				<?php meni_get_template( 'framework/templates/single/entry-image.php', $template_args ); ?>
	        </div>

	        <!-- Entry Date -->
			<div class="entry-date">
				<?php meni_get_template( 'framework/templates/single/entry-categories.php', $template_args ); ?>
			</div><!-- Entry Date -->

	        <!-- Entry Comment -->
			<div class="entry-comments">
				<?php meni_get_template( 'framework/templates/single/entry-comment.php', $template_args ); ?>
			</div><!-- Entry Comment -->
	    </div><!-- Featured Image -->
<?php
	// Getting values from theme options...
	$template = 'framework/templates/single/entry-elements-loop.php';
	$template_args['ID'] = $ID;
	$template_args['Post_Style'] = $Post_Style;
	$template_args['Post_Meta'] = $Post_Meta;

	meni_get_template( $template, $template_args ); ?>	