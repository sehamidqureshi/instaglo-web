<?php
/**
 * Theme Functions
 *
 * @package DTtheme
 * @author DesignThemes
 * @link http://wedesignthemes.com
 */
define( 'MENI_THEME_DIR', get_template_directory() );
define( 'MENI_THEME_URI', get_template_directory_uri() );

if (function_exists ('wp_get_theme')) :
	$themeData = wp_get_theme();
	define( 'MENI_THEME_NAME', $themeData->get('Name'));
	define( 'MENI_THEME_VERSION', $themeData->get('Version'));
endif;

/* ---------------------------------------------------------------------------
 * Loads Kirki
 * ---------------------------------------------------------------------------*/
require_once( MENI_THEME_DIR .'/kirki/index.php' );

/* ---------------------------------------------------------------------------
 * Loads Codestar
 * ---------------------------------------------------------------------------*/

if( !defined( 'CS_OPTION' ) ) {
	define( 'CS_OPTION', '_meni_cs_options' );
}

require_once MENI_THEME_DIR .'/cs-framework/cs-framework.php';

if( !defined( 'CS_ACTIVE_TAXONOMY' ) ) { 
	define( 'CS_ACTIVE_TAXONOMY',   false );
}

if( !defined( 'CS_ACTIVE_SHORTCODE' ) ) {
	define( 'CS_ACTIVE_SHORTCODE',  false );
}
if( !defined( 'CS_ACTIVE_CUSTOMIZE' ) ) {
	define( 'CS_ACTIVE_CUSTOMIZE',  false );
}

/* ---------------------------------------------------------------------------
 * Create function to get theme options
 * --------------------------------------------------------------------------- */
function meni_cs_get_option($key, $value = '') {

	$v = cs_get_option( $key );

	if ( !empty( $v ) ) {
		return $v;
	} else {
		return $value;
	}
}

/* ---------------------------------------------------------------------------
 * Loads Theme Textdomain
 * ---------------------------------------------------------------------------*/ 
define( 'MENI_LANG_DIR', MENI_THEME_DIR. '/languages' );
load_theme_textdomain( 'meni', MENI_LANG_DIR );

/* ---------------------------------------------------------------------------
 * Loads the Admin Panel Style
 * ---------------------------------------------------------------------------*/
function meni_admin_scripts() {
	wp_enqueue_style('meni-admin', MENI_THEME_URI .'/cs-framework-override/style.css');
}
add_action( 'admin_enqueue_scripts', 'meni_admin_scripts' );

/* ---------------------------------------------------------------------------
 * Loads Theme Functions
 * ---------------------------------------------------------------------------*/

// Functions --------------------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-functions.php' );

// Header -----------------------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-head.php' );

// Hooks ------------------------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-hooks.php' );

// Post Functions ---------------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-post-functions.php' );
new meni_post_functions;

// Widgets ----------------------------------------------------------------------
add_action( 'widgets_init', 'meni_widgets_init' );
function meni_widgets_init() {
	require_once( MENI_THEME_DIR .'/framework/register-widgets.php' );
}

// Plugins ---------------------------------------------------------------------- 
require_once( MENI_THEME_DIR .'/framework/register-plugins.php' );

// WooCommerce ------------------------------------------------------------------
if( function_exists( 'is_woocommerce' ) ){
	require_once( MENI_THEME_DIR .'/framework/woocommerce/register-woocommerce.php' );
}

// WP Store Locator -------------------------------------------------------------
if( class_exists( 'WP_Store_locator' ) ) {
	require_once( MENI_THEME_DIR .'/framework/register-storelocator.php' );
}

// Register Templates -----------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-templates.php' );

// Register Gutenberg -----------------------------------------------------------
require_once( MENI_THEME_DIR .'/framework/register-gutenberg-editor.php' );


// functions.php

// custom firebase short code 

add_shortcode( 'firebase_authentication', 'show_firebase_authentication_html' );

add_shortcode( 'load_additional_fields', 'load_additional_fields' );
add_shortcode( 'load_service_content', 'load_service_content' );
add_shortcode( 'load_homepage_main_services', 'load_homepage_main_services' );
add_shortcode( 'load_krisalys_women_services', 'loadWomenCategories' );
add_shortcode( 'load_krisalys_women_category_services', 'loadWomenCategoriesServices' );

add_shortcode( 'load_krisalys_men_services', 'loadMenCategories' );
add_shortcode( 'load_krisalys_men_category_services', 'loadMenCategoriesServices' );

add_shortcode( 'load_krisalys_aesthetics_services', 'loadAestheticsCategories' );
add_shortcode( 'load_krisalys_aesthetics_category_services', 'loadAestheticsCategoriesServices' );

add_shortcode( 'load_krisalys_dental_services', 'loadDentalCategories' );
add_shortcode( 'load_krisalys_dental_category_services', 'loadDentalCategoriesServices' );

add_shortcode( 'load_krisalys_cs_services', 'loadCSCategories' );
add_shortcode( 'load_krisalys_cs_category_services', 'loadCSCategoriesServices' );

add_shortcode( 'load_krisalys_women_packages', 'loadWomenPackages' );
add_shortcode( 'load_krisalys_women_doit', 'loadWomenDoit' );

add_shortcode( 'load_krisalys_men_packages', 'loadMenPackages' );
add_shortcode( 'load_krisalys_men_doit', 'loadMenDoit' );

add_shortcode( 'load_krisalys_aesthetics_packages', 'loadAestheticsPackages' );
add_shortcode( 'load_krisalys_aesthetics_doit', 'loadAestheticsDoit' );

add_shortcode( 'load_krisalys_cs_packages', 'loadCSPackages' );
add_shortcode( 'load_krisalys_cs_doit', 'loadCSDoit' );

add_shortcode( 'load_krisalys_dental_packages', 'loadDentalPackages' );
add_shortcode( 'load_krisalys_dental_doit', 'loadDentalDoit' );



add_action( 'wp_ajax_nopriv_create_customer', 'create_customer'); // User registration
add_action( 'wp_ajax_create_customer', 'create_customer');

add_action( 'wp_ajax_nopriv_create_social_customer', 'create_social_customer'); // User registration
add_action( 'wp_ajax_create_social_customer', 'create_social_customer');

add_action( 'wp_ajax_nopriv_create_phone_customer', 'create_phone_customer'); // User registration
add_action( 'wp_ajax_create_phone_customer', 'create_phone_customer');


add_action( 'wp_ajax_nopriv_create_guest_customer', 'create_guest_customer'); // User registration by guest
add_action( 'wp_ajax_create_guest_customer', 'create_guest_customer');

add_action( 'wp_ajax_nopriv_save_promotion_history', 'SavePromotionHistory'); // User registration
add_action( 'wp_ajax_save_promotion_history', 'SavePromotionHistory');

add_action( 'wp_ajax_nopriv_logoutuser', 'logoutuser'); // User registration by guest
add_action( 'wp_ajax_logoutuser', 'logoutuser');

//add_action( 'user_register', 'autologgedin' );
//add_action( 'wp_ajax_nopriv_is_user_loggedIn', 'create_customer');
add_action( 'wp_ajax_nopriv_is_user_loggedIn', 'is_user_loggedIn');
add_action( 'wp_ajax_is_user_loggedIn', 'is_user_loggedIn');
add_action( 'wp_ajax_nopriv_signinuser', 'signinuser');
add_action( 'wp_ajax_signinuser', 'signinuser');
add_action( 'wp_ajax_signinuser', 'signinuser');

add_filter( 'query_vars', 'ingsThemeQueryVars' );
add_action( 'template_redirect', 'executeThemeActions' );

add_action( 'wp_dashboard_setup', 'register_my_dashboard_widget' );
function register_my_dashboard_widget() {
	wp_add_dashboard_widget(
		'promotion_dashboard_widget',
		'Promotions Overview',
		'promotionsStatistics'
	);

}

function getPromotionById($postId) {
    
    global $wpdb;
    
    $promotion = get_post($postId);
    
    $promoheading1 = get_post_meta( $promotion->ID, 'promoheading1', true );
    $promoheading2 = get_post_meta( $promotion->ID, 'promoheading2', true );
    
    $link = get_edit_post_link($postId);
    
    $data = array("h1"=>$promoheading1,"h2"=>$promoheading2,"editlink"=>$link);
    
    return $data;
}

function promotionsStatistics() {
    
    global $wpdb;
    
    $totalviewsPromotions = $wpdb->get_results("SELECT SUM(`count`) as totalcount FROM `promotions_count`;");
    
    $totalauthorizedUsers = $wpdb->get_results("SELECT COUNT(fk_user_id) as totalusers FROM `promotions_history` WHERE fk_user_id <> 0;");
    
    $totalUnauthorizedUsers = $wpdb->get_results("SELECT COUNT(fk_user_id) as totalusers FROM `promotions_history` WHERE fk_user_id = 0;");
    
    $mostlyViewedPromotion = $wpdb->get_results("SELECT fk_promotion_id FROM promotions_count WHERE count = (SELECT MAX(count) FROM promotions_count);");
    
    $lessViewedPromotion = $wpdb->get_results("SELECT fk_promotion_id FROM promotions_count WHERE count = (SELECT MIN(count) FROM promotions_count);");
    
    $mostlyViewedPromotionId = $mostlyViewedPromotion[0]->fk_promotion_id;
    
    $lessViewedPromotionId = $lessViewedPromotion[0]->fk_promotion_id;
    
    $mostViewed = getPromotionById($mostlyViewedPromotionId);
    $lessViewed = getPromotionById($lessViewedPromotionId);

 
    
    $args = array(
        'post_type'=>'promotions',
        'posts_per_page'=>-1
    );
    
    $promotionsList = get_posts($args);   
    
    $promotionsCount = count($promotionsList);
    
    $totalViewedPromotions = $totalviewsPromotions[0]->totalcount;
    
    $totalAuthorized = $totalauthorizedUsers[0]->totalusers;
    $totalUnAuthorized = $totalUnauthorizedUsers[0]->totalusers;
    
    
    ob_start(); ?>
    

<div class="overview-module-wrap clear">
	<div class="mycred-type clear first">

		<div class="overview clear">
			<div class="section border" style="width: 50%;">
				<p>
					<strong style="color:lightgreen;">Promotions Total</strong>
				</p>
			</div>
			<div class="section border" style="width: 50%; margin-left: -1px;">
				<p>
					<strong style="color:#a9b6bd;"><?php echo $promotionsCount ?></strong>
				</p>
			</div>
			
			<div class="section border" style="width: 50%;">
				<p>
					<strong style="color:lightblue;">Promotions</strong> <i class="fa fa-eye" style="color:grey" aria-hidden="true"></i>
				</p>
			</div>
			<div class="section border" style="width: 50%; margin-left: -1px;">
				<p>
					<strong style="color:#a9b6bd;"><?php echo $totalViewedPromotions ?></strong>
				</p>
			</div>	
			<div class="section border" style="width: 50%;">
				<p>
					<strong style="color:pink;">Unauthorized Users</strong> <i class="fa fa-eye" style="color:grey" aria-hidden="true"></i>
				</p>
			</div>
			<div class="section border" style="width: 50%; margin-left: -1px;">
				<p>
					<strong style="color:#a9b6bd;"><?php echo $totalUnAuthorized ?></strong>
				</p>
			</div>	
			<div class="section border" style="width: 50%;">
				<p>
					<strong style="color:brown;">Authorized Users</strong> <i class="fa fa-eye" style="color:grey" aria-hidden="true"></i>
				</p>
			</div>
			<div class="section border" style="width: 50%; margin-left: -1px;">
				<p>
					<strong style="color:#a9b6bd;"><?php echo $totalAuthorized ?></strong>
				</p>
			</div>	
			
			<div class="section border" style="width: 50%;">
				<p>
					<strong style="color:black;">Trending <i class="fa fa-arrow-up" aria-hidden="true"></i></strong>
				</p>
			</div>
			<div class="section border" style="width: 50%; margin-left: -1px;">
				<p>
					<strong style="color:#a9b6bd;"><a href="<?php echo $mostViewed['editlink'] ?>"><?php echo $mostViewed['h2'] ?></a></strong>
				</p>
			</div>
			
			<div class="section border" style="width: 50%;">
				<p>
					<strong style="color:orange;">Grossing <i class="fa fa-arrow-down" aria-hidden="true"></i> </strong>
				</p>
			</div>
			<div class="section border" style="width: 50%; margin-left: -1px;">
				<p>
					<strong style="color:#a9b6bd;"><a href="<?php echo $lessViewed['editlink'] ?>"> <?php echo $lessViewed['h2'] ?></a></strong>
				</p>
			</div>			
		</div>
		

	</div>

	<div class="clear"></div>
</div>

<style>
    #promotion_dashboard_widget .inside {
        
        padding: 0;
        vertical-align: middle;
        text-align: center;  
        margin:0px;
    }
    
    

</style>
   
    
    <?php $promotionsOverviewHTML = ob_get_clean();
    echo $promotionsOverviewHTML;
}

function SavePromotionHistory() {
    
    $promotionId = isset($_POST['promotionId']) ? $_POST['promotionId'] : "";
    $promotionName = isset($_POST['promotionName']) ? $_POST['promotionName'] : "";
    $date = date("Y-m-d h:i:s");
    
    if (!empty($promotionId)) {
        
        global $wpdb;
        
        $IpAddress = $_SERVER['REMOTE_ADDR'];
        $userId = get_current_user_id();
        
            
        $data = array("fk_user_id" => $userId,
                      "fk_promotion_id" => $promotionId,
                      "promotion" => $promotionName,
                      "ip_address" => $IpAddress,
                      "added_date" => $date
                    );
            
            
        $error = $wpdb->insert("promotions_history",$data);
            
        $result = $wpdb->get_results("SELECT * FROM promotions_count WHERE fk_promotion_id = '$promotionId';");
            
        if (sizeof($result) > 0) {
            
                $total = $result[0]->count + 1;

                $data = array("fk_promotion_id" => $promotionId,
                              "count" => $total,
                              "updated_date" => $date,
                            );
                    
                $wpdb->update("promotions_count", $data, array('fk_promotion_id'=>$promotionId));                
            
        
        } else {
            
            $data = array("fk_promotion_id" => $promotionId,
                          "count" => 1,
                          "added_date" => $date,
                        );
            
            
            $error = $wpdb->insert("promotions_count",$data);                    
            
        }             
            
       
        
    }
    
}

add_role( "women", "Krisalys Women", array('read' => true,'publish_posts' => true, 'edit_posts' => true, 'delete_posts' => false, 'edit_users' => true ));
add_role( "men", "Krisalys Men", array('read' => true,'publish_posts' => false, 'edit_posts' => false, 'delete_posts' => false, 'edit_users' => true ));
add_role( "cosmeticsurgery", "Krisalys Cosmetic Surgery", array('read' => true,'publish_posts' => false, 'edit_posts' => false, 'delete_posts' => false, 'edit_users' => true ));
add_role( "aesthetics", "Krisalys Aesthetics", array('read' => true,'publish_posts' => false, 'edit_posts' => false, 'delete_posts' => false, 'edit_users' => true ));
add_role( "dental", "Krisalys Dental", array('read' => true,'publish_posts' => false, 'edit_posts' => false, 'delete_posts' => false, 'edit_users' => true ));

$mainCategories = get_option("krisalys_categories");

foreach ($mainCategories as $index => $category) {
    
    $label = $category['label'];
    $labelPost = $category['name'];
    
    $role = get_role( 'administrator' );
    
    $role->add_cap('edit_'.$labelPost,true);
    $role->add_cap('read_'.$labelPost,true);
    $role->add_cap('edit_'.$labelPost,true);
    $role->add_cap('delete_'.$labelPost,true);
    $role->add_cap('publish_'.$labelPost,false);
    $role->add_cap('delete_others_'.$labelPost,false);
    $role->add_cap('edit_others_'.$labelPost,false);
    $role->add_cap('read_private_'.$labelPost,false);
    $role->add_cap('upload_files');
    
   /* if ($labelPost == "krisalys-women") {
        
        $role = get_role( 'women' );
        
        $role->add_cap('edit_'.$labelPost,true);
        $role->add_cap('read_'.$labelPost,true);
        $role->add_cap('edit_'.$labelPost,true);
        $role->add_cap('delete_'.$labelPost,true);
        $role->add_cap('publish_'.$labelPost,false);
        $role->add_cap('delete_others_'.$labelPost,false);
        $role->add_cap('edit_others_'.$labelPost,false);
        $role->add_cap('read_private_'.$labelPost,false);
        $role->add_cap('upload_files');        
        
        
    } else if ("krisalys-men") {
        
        $role = get_role( 'men' );
        
        $role->add_cap('edit_'.$labelPost,true);
        $role->add_cap('read_'.$labelPost,true);
        $role->add_cap('edit_'.$labelPost,true);
        $role->add_cap('delete_'.$labelPost,true);
        $role->add_cap('publish_'.$labelPost,false);
        $role->add_cap('delete_others_'.$labelPost,false);
        $role->add_cap('edit_others_'.$labelPost,false);
        $role->add_cap('read_private_'.$labelPost,false);
        $role->add_cap('upload_files');        
    
        
    } else if ("aesthetics") {
        
        $role = get_role( 'aesthetics' );
        
        $role->add_cap('edit_'.$labelPost,true);
        $role->add_cap('read_'.$labelPost,true);
        $role->add_cap('edit_'.$labelPost,true);
        $role->add_cap('delete_'.$labelPost,true);
        $role->add_cap('publish_'.$labelPost,false);
        $role->add_cap('delete_others_'.$labelPost,false);
        $role->add_cap('edit_others_'.$labelPost,false);
        $role->add_cap('read_private_'.$labelPost,false);
        $role->add_cap('upload_files');
        
    } else if ("cosmetic-surgery") {
        
        $role = get_role( 'cosmetic_surgery' );
        
        $role->add_cap('edit_'.$labelPost,true);
        $role->add_cap('read_'.$labelPost,true);
        $role->add_cap('edit_'.$labelPost,true);
        $role->add_cap('delete_'.$labelPost,true);
        $role->add_cap('publish_'.$labelPost,false);
        $role->add_cap('delete_others_'.$labelPost,false);
        $role->add_cap('edit_others_'.$labelPost,false);
        $role->add_cap('read_private_'.$labelPost,false);
        $role->add_cap('upload_files');
        
    } else if ("dental") {
        
        $role = get_role( 'dental' );
        
        $role->add_cap('edit_'.$labelPost,true);
        $role->add_cap('read_'.$labelPost,true);
        $role->add_cap('edit_'.$labelPost,true);
        $role->add_cap('delete_'.$labelPost,true);
        $role->add_cap('publish_'.$labelPost,false);
        $role->add_cap('delete_others_'.$labelPost,false);
        $role->add_cap('edit_others_'.$labelPost,false);
        $role->add_cap('read_private_'.$labelPost,false);
        $role->add_cap('upload_files');        
        
    }*/

    
    
    
}

function give_seo_yoastToast() {
    $user = new WP_User( $seo_user_id );
    $user->add_cap( 'manage_options');
}
add_action( 'admin_init', 'give_seo_yoastToast');

function loadDentalDoit() {
    
    ob_start();
    
    ?>
    
<div class="wpb_wrapper">
   <div id="1560845229374-9a367728-1b4f" class="dt-sc-empty-space"></div>
   <div class="dt-sc-title script-with-sub-title  aligncenter alter" data-delay="0">
      <h6 class="dt-sc-main-heading">How we do it</h6>
      <h2 class="dt-sc-sub-heading">Treatments &amp; Services</h2>
   </div>
   <div id="1560843967373-edd46969-721c" style="height:50px;" class="dt-sc-empty-space"></div>
   <p style="text-align: center" class="vc_custom_heading vc_custom_1560851306806">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
   <div id="1560843990151-4ca3549b-6cc5" style="height:50px;" class="dt-sc-empty-space"></div>
   <div class="vc_row wpb_row vc_inner vc_row-fluid">
      <div class="custom-video-player wpb_column vc_column_container vc_col-sm-6">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1560843506630">
                  <figure class="wpb_wrapper vc_figure">
                     <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="650" height="260" src="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1.jpg 650w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1-300x120.jpg 300w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1-540x216.jpg 540w" sizes="(max-width: 650px) 100vw, 650px"></div>
                  </figure>
               </div>
               <a title="Video" target=" _blank" href="http://vimeo.com/18439821" class="video-image"><span class="fas fa-play  video-image" data-delay="0"> </span></a>
            </div>
         </div>
      </div>
      <div class="custom-video-player wpb_column vc_column_container vc_col-sm-6">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1560843845545">
                  <figure class="wpb_wrapper vc_figure">
                     <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="650" height="260" src="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2.jpg 650w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2-300x120.jpg 300w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2-540x216.jpg 540w" sizes="(max-width: 650px) 100vw, 650px"></div>
                  </figure>
               </div>
               <a title="Video" target=" _blank" href="http://vimeo.com/18439821" class="video-image"><span class="fas fa-play  video-image" data-delay="0"> </span></a>
            </div>
         </div>
      </div>
   </div>
   <div class="dt-sc-clear "> </div>
   <div class="vc_row wpb_row vc_inner vc_row-fluid aligncenter">
      <div class="wpb_column vc_column_container vc_col-sm-12">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div id="1560845014922-1f02b86f-344f" class="dt-sc-empty-space"></div>
               <a href="#" target="_self" title="" class="dt-sc-button   large   filled  custom-button" data-delay="0"> More Videos </a>
            </div>
         </div>
      </div>
   </div>
   <div id="1560845144081-1d57af4f-cac9" class="dt-sc-empty-space"></div>
</div>    
    
    <?php $html = ob_get_clean();
    
    echo $html;
}
function loadWomenDoit() {
    
    ob_start();
    
    ?>
    
<div class="wpb_wrapper">
   <div id="1560845229374-9a367728-1b4f" class="dt-sc-empty-space"></div>
   <div class="dt-sc-title script-with-sub-title  aligncenter alter" data-delay="0">
      <h6 class="dt-sc-main-heading">How we do it</h6>
      <h2 class="dt-sc-sub-heading">Treatments &amp; Services</h2>
   </div>
   <div id="1560843967373-edd46969-721c" style="height:50px;" class="dt-sc-empty-space"></div>
   <p style="text-align: center" class="vc_custom_heading vc_custom_1560851306806">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
   <div id="1560843990151-4ca3549b-6cc5" style="height:50px;" class="dt-sc-empty-space"></div>
   <div class="vc_row wpb_row vc_inner vc_row-fluid">
      <div class="custom-video-player wpb_column vc_column_container vc_col-sm-6">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1560843506630">
                  <figure class="wpb_wrapper vc_figure">
                     <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="650" height="260" src="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1.jpg 650w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1-300x120.jpg 300w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1-540x216.jpg 540w" sizes="(max-width: 650px) 100vw, 650px"></div>
                  </figure>
               </div>
               <a title="Video" target=" _blank" href="http://vimeo.com/18439821" class="video-image"><span class="fas fa-play  video-image" data-delay="0"> </span></a>
            </div>
         </div>
      </div>
      <div class="custom-video-player wpb_column vc_column_container vc_col-sm-6">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1560843845545">
                  <figure class="wpb_wrapper vc_figure">
                     <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="650" height="260" src="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2.jpg 650w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2-300x120.jpg 300w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2-540x216.jpg 540w" sizes="(max-width: 650px) 100vw, 650px"></div>
                  </figure>
               </div>
               <a title="Video" target=" _blank" href="http://vimeo.com/18439821" class="video-image"><span class="fas fa-play  video-image" data-delay="0"> </span></a>
            </div>
         </div>
      </div>
   </div>
   <div class="dt-sc-clear "> </div>
   <div class="vc_row wpb_row vc_inner vc_row-fluid aligncenter">
      <div class="wpb_column vc_column_container vc_col-sm-12">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div id="1560845014922-1f02b86f-344f" class="dt-sc-empty-space"></div>
               <a href="#" target="_self" title="" class="dt-sc-button   large   filled  custom-button" data-delay="0"> More Videos </a>
            </div>
         </div>
      </div>
   </div>
   <div id="1560845144081-1d57af4f-cac9" class="dt-sc-empty-space"></div>
</div>    
    
    <?php $html = ob_get_clean();
    
    echo $html;
}
function loadCSDoit() {
    
    ob_start();
    
    ?>
    
<div class="wpb_wrapper">
   <div id="1560845229374-9a367728-1b4f" class="dt-sc-empty-space"></div>
   <div class="dt-sc-title script-with-sub-title  aligncenter alter" data-delay="0">
      <h6 class="dt-sc-main-heading">How we do it</h6>
      <h2 class="dt-sc-sub-heading">Treatments &amp; Services</h2>
   </div>
   <div id="1560843967373-edd46969-721c" style="height:50px;" class="dt-sc-empty-space"></div>
   <p style="text-align: center" class="vc_custom_heading vc_custom_1560851306806">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
   <div id="1560843990151-4ca3549b-6cc5" style="height:50px;" class="dt-sc-empty-space"></div>
   <div class="vc_row wpb_row vc_inner vc_row-fluid">
      <div class="custom-video-player wpb_column vc_column_container vc_col-sm-6">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1560843506630">
                  <figure class="wpb_wrapper vc_figure">
                     <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="650" height="260" src="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1.jpg 650w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1-300x120.jpg 300w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1-540x216.jpg 540w" sizes="(max-width: 650px) 100vw, 650px"></div>
                  </figure>
               </div>
               <a title="Video" target=" _blank" href="http://vimeo.com/18439821" class="video-image"><span class="fas fa-play  video-image" data-delay="0"> </span></a>
            </div>
         </div>
      </div>
      <div class="custom-video-player wpb_column vc_column_container vc_col-sm-6">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1560843845545">
                  <figure class="wpb_wrapper vc_figure">
                     <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="650" height="260" src="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2.jpg 650w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2-300x120.jpg 300w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2-540x216.jpg 540w" sizes="(max-width: 650px) 100vw, 650px"></div>
                  </figure>
               </div>
               <a title="Video" target=" _blank" href="http://vimeo.com/18439821" class="video-image"><span class="fas fa-play  video-image" data-delay="0"> </span></a>
            </div>
         </div>
      </div>
   </div>
   <div class="dt-sc-clear "> </div>
   <div class="vc_row wpb_row vc_inner vc_row-fluid aligncenter">
      <div class="wpb_column vc_column_container vc_col-sm-12">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div id="1560845014922-1f02b86f-344f" class="dt-sc-empty-space"></div>
               <a href="#" target="_self" title="" class="dt-sc-button   large   filled  custom-button" data-delay="0"> More Videos </a>
            </div>
         </div>
      </div>
   </div>
   <div id="1560845144081-1d57af4f-cac9" class="dt-sc-empty-space"></div>
</div>    
    
    <?php $html = ob_get_clean();
    
    echo $html;
}
function loadMenDoit() {
    
    ob_start();
    
    ?>
    
<div class="wpb_wrapper">
   <div id="1560845229374-9a367728-1b4f" class="dt-sc-empty-space"></div>
   <div class="dt-sc-title script-with-sub-title  aligncenter alter" data-delay="0">
      <h6 class="dt-sc-main-heading">How we do it</h6>
      <h2 class="dt-sc-sub-heading">Treatments &amp; Services</h2>
   </div>
   <div id="1560843967373-edd46969-721c" style="height:50px;" class="dt-sc-empty-space"></div>
   <p style="text-align: center" class="vc_custom_heading vc_custom_1560851306806">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
   <div id="1560843990151-4ca3549b-6cc5" style="height:50px;" class="dt-sc-empty-space"></div>
   <div class="vc_row wpb_row vc_inner vc_row-fluid">
      <div class="custom-video-player wpb_column vc_column_container vc_col-sm-6">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1560843506630">
                  <figure class="wpb_wrapper vc_figure">
                     <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="650" height="260" src="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1.jpg 650w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1-300x120.jpg 300w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1-540x216.jpg 540w" sizes="(max-width: 650px) 100vw, 650px"></div>
                  </figure>
               </div>
               <a title="Video" target=" _blank" href="http://vimeo.com/18439821" class="video-image"><span class="fas fa-play  video-image" data-delay="0"> </span></a>
            </div>
         </div>
      </div>
      <div class="custom-video-player wpb_column vc_column_container vc_col-sm-6">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1560843845545">
                  <figure class="wpb_wrapper vc_figure">
                     <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="650" height="260" src="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2.jpg 650w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2-300x120.jpg 300w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2-540x216.jpg 540w" sizes="(max-width: 650px) 100vw, 650px"></div>
                  </figure>
               </div>
               <a title="Video" target=" _blank" href="http://vimeo.com/18439821" class="video-image"><span class="fas fa-play  video-image" data-delay="0"> </span></a>
            </div>
         </div>
      </div>
   </div>
   <div class="dt-sc-clear "> </div>
   <div class="vc_row wpb_row vc_inner vc_row-fluid aligncenter">
      <div class="wpb_column vc_column_container vc_col-sm-12">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div id="1560845014922-1f02b86f-344f" class="dt-sc-empty-space"></div>
               <a href="#" target="_self" title="" class="dt-sc-button   large   filled  custom-button" data-delay="0"> More Videos </a>
            </div>
         </div>
      </div>
   </div>
   <div id="1560845144081-1d57af4f-cac9" class="dt-sc-empty-space"></div>
</div>    
    
    <?php $html = ob_get_clean();
    
    echo $html;
}
function loadAestheticsDoit() {
    
    ob_start();
    
    ?>
    
<div class="wpb_wrapper">
   <div id="1560845229374-9a367728-1b4f" class="dt-sc-empty-space"></div>
   <div class="dt-sc-title script-with-sub-title  aligncenter alter" data-delay="0">
      <h6 class="dt-sc-main-heading">How we do it</h6>
      <h2 class="dt-sc-sub-heading">Treatments &amp; Services</h2>
   </div>
   <div id="1560843967373-edd46969-721c" style="height:50px;" class="dt-sc-empty-space"></div>
   <p style="text-align: center" class="vc_custom_heading vc_custom_1560851306806">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
   <div id="1560843990151-4ca3549b-6cc5" style="height:50px;" class="dt-sc-empty-space"></div>
   <div class="vc_row wpb_row vc_inner vc_row-fluid">
      <div class="custom-video-player wpb_column vc_column_container vc_col-sm-6">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1560843506630">
                  <figure class="wpb_wrapper vc_figure">
                     <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="650" height="260" src="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1.jpg 650w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1-300x120.jpg 300w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img1-540x216.jpg 540w" sizes="(max-width: 650px) 100vw, 650px"></div>
                  </figure>
               </div>
               <a title="Video" target=" _blank" href="http://vimeo.com/18439821" class="video-image"><span class="fas fa-play  video-image" data-delay="0"> </span></a>
            </div>
         </div>
      </div>
      <div class="custom-video-player wpb_column vc_column_container vc_col-sm-6">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1560843845545">
                  <figure class="wpb_wrapper vc_figure">
                     <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="650" height="260" src="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2.jpg" class="vc_single_image-img attachment-full" alt="" srcset="https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2.jpg 650w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2-300x120.jpg 300w, https://mk0site10bxu4iq0r2kt.kinstacdn.com/wp-content/uploads/2019/06/video-play-img2-540x216.jpg 540w" sizes="(max-width: 650px) 100vw, 650px"></div>
                  </figure>
               </div>
               <a title="Video" target=" _blank" href="http://vimeo.com/18439821" class="video-image"><span class="fas fa-play  video-image" data-delay="0"> </span></a>
            </div>
         </div>
      </div>
   </div>
   <div class="dt-sc-clear "> </div>
   <div class="vc_row wpb_row vc_inner vc_row-fluid aligncenter">
      <div class="wpb_column vc_column_container vc_col-sm-12">
         <div class="vc_column-inner ">
            <div class="wpb_wrapper">
               <div id="1560845014922-1f02b86f-344f" class="dt-sc-empty-space"></div>
               <a href="#" target="_self" title="" class="dt-sc-button   large   filled  custom-button" data-delay="0"> More Videos </a>
            </div>
         </div>
      </div>
   </div>
   <div id="1560845144081-1d57af4f-cac9" class="dt-sc-empty-space"></div>
</div>    
    
    <?php $html = ob_get_clean();
    
    echo $html;
}
function loadWomenPackages() {
    
    ob_start();
    
        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'promotions-tax',
                                    'field' => 'slug',
                                    'terms' => 'womensaloon'
                            )
                        ),
            'post_type'=>'promotions',
            'posts_per_page'=>4
           
        );
        
        $promotionsList = get_posts($args);
    
    ?>


<?php
    
if (count($promotionsList) > 0) { ?>
    
<div data-delay="0" class="vc_row wpb_row vc_row-fluid vc_custom_1561118998307">
   <div class="wpb_column vc_column_container vc_col-sm-12" data-delay="0">
      <div class="vc_column-inner ">
         <div class="wpb_wrapper">
            <div id="1561118940969-dccb172a-948f" style="height:50px;" class="dt-sc-empty-space"></div>
            <div class="dt-sc-title script-with-sub-title  aligncenter" data-delay="0">
               <h6 class="dt-sc-main-heading">With Best Price</h6>
               <h2 class="dt-sc-sub-heading">Promotions</h2>
            </div>
            <div id="1561109976483-6c398015-b2fd" style="height:75px;" class="dt-sc-empty-space"></div>
            <div class="vc_row wpb_row vc_inner vc_row-fluid">
               
            <?php 
            
                foreach ($promotionsList as $promoindex => $promotion) {
                    
                $promoheading1 = get_post_meta( $promotion->ID, 'promoheading1', true );
                $promoheading2 = get_post_meta( $promotion->ID, 'promoheading2', true );
                $promoimage = get_post_meta( $promotion->ID, 'promoimage', true );
                $promoprice = get_post_meta( $promotion->ID, 'promoprice', true );
                $promodetails = get_post_meta( $promotion->ID, 'promodetails', true );
                $promourl = get_post_meta( $promotion->ID, 'promourl', true );
                
                if ($promoindex == 1) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                

            ?>
               <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-6">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="dt-sc-pr-tb-col type1 standard <?php echo $selected ?>" data-delay="0">
                           <div class="dt-sc-tb-thumb"><img width="80" height="80" src="<?php echo $promoimage ?>" class="attachment-full" alt=""></div>
                           <div class="dt-sc-tb-header">
                              <div class="dt-sc-tb-title">
                                 <p><?php echo $promoheading1 ?></p>
                                 <h5><?php echo $promoheading2 ?></h5>
                              </div>
                              <div class="dt-sc-price">
                                 <h6> <sup>RS</sup><?php echo $promoprice ?></h6>
                              </div>
                           </div>
                           <ul class="dt-sc-tb-content">
                              <?php foreach ($promodetails as $itemindex => $item): ?>
                              
                              <li><?php echo $item['promotionitem'] ?></li>
                              
                              <?php endforeach; ?>
                           </ul>
                           <div class="dt-sc-buy-now"><a class="dt-sc-button promotion-view-btn" promotion-name="<?php echo $promoheading2 ?>" promotion-id="<?php echo $promotion->ID ?>" target="_self" href="<?php echo $promourl ?>">View More</a></div>
                        </div>
                     </div>
                  </div>
               </div>
        
          <?php } ?>
            
            </div>
            <div id="1561118945895-9cb76125-46be" class="dt-sc-empty-space"></div>
         </div>
      </div>
   </div>
</div>  

<script>
    
    jQuery(document).on("click",".promotion-view-btn",function () {
    
            var promotionId = jQuery(this).attr("promotion-id");
            var promotionName = jQuery(this).attr("promotion-name");
            
            jQuery.ajax({
    
              type: 'POST',
              url: AjaxURL,
              data: {"action": "save_promotion_history","promotionId":promotionId,"promotionName":promotionName},
              success: function(result)
              {
    
                // no action required here if any we will add here in future
              }
            });
    
     
    });    
    
</script>
    
    <?php $html = ob_get_clean(); 
        
    }
    
    echo $html;
}
function loadMenPackages() {
    
    ob_start();
    
        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'promotions-tax',
                                    'field' => 'slug',
                                    'terms' => 'mensaloon'
                            )
                        ),
            'post_type'=>'promotions',
            'posts_per_page'=>4
           
        );
        
        $promotionsList = get_posts($args);
    
    ?>


<?php
    
if (count($promotionsList) > 0) { ?>
    
<div data-delay="0" class="vc_row wpb_row vc_row-fluid vc_custom_1561118998307">
   <div class="wpb_column vc_column_container vc_col-sm-12" data-delay="0">
      <div class="vc_column-inner ">
         <div class="wpb_wrapper">
            <div id="1561118940969-dccb172a-948f" style="height:50px;" class="dt-sc-empty-space"></div>
            <div class="dt-sc-title script-with-sub-title  aligncenter" data-delay="0">
               <h6 class="dt-sc-main-heading">With Best Price</h6>
               <h2 class="dt-sc-sub-heading">Promotions</h2>
            </div>
            <div id="1561109976483-6c398015-b2fd" style="height:75px;" class="dt-sc-empty-space"></div>
            <div class="vc_row wpb_row vc_inner vc_row-fluid">
               
            <?php 
            
                foreach ($promotionsList as $promoindex => $promotion) {
                    
                $promoheading1 = get_post_meta( $promotion->ID, 'promoheading1', true );
                $promoheading2 = get_post_meta( $promotion->ID, 'promoheading2', true );
                $promoimage = get_post_meta( $promotion->ID, 'promoimage', true );
                $promoprice = get_post_meta( $promotion->ID, 'promoprice', true );
                $promodetails = get_post_meta( $promotion->ID, 'promodetails', true );
                $promourl = get_post_meta( $promotion->ID, 'promourl', true );
                
                if ($promoindex == 1) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                

            ?>
               <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-6">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="dt-sc-pr-tb-col type1 standard <?php echo $selected ?>" data-delay="0">
                           <div class="dt-sc-tb-thumb"><img width="80" height="80" src="<?php echo $promoimage ?>" class="attachment-full" alt=""></div>
                           <div class="dt-sc-tb-header">
                              <div class="dt-sc-tb-title">
                                 <p><?php echo $promoheading1 ?></p>
                                 <h5><?php echo $promoheading2 ?></h5>
                              </div>
                              <div class="dt-sc-price">
                                 <h6> <sup>RS</sup><?php echo $promoprice ?></h6>
                              </div>
                           </div>
                           <ul class="dt-sc-tb-content">
                              <?php foreach ($promodetails as $itemindex => $item): ?>
                              
                              <li><?php echo $item['promotionitem'] ?></li>
                              
                              <?php endforeach; ?>
                           </ul>
                           <div class="dt-sc-buy-now"><a class="dt-sc-button promotion-view-btn" promotion-name="<?php echo $promoheading2 ?>" promotion-id="<?php echo $promotion->ID ?>" target="_self" href="<?php echo $promourl ?>">View More</a></div>
                        </div>
                     </div>
                  </div>
               </div>
        
          <?php } ?>
            
            </div>
            <div id="1561118945895-9cb76125-46be" class="dt-sc-empty-space"></div>
         </div>
      </div>
   </div>
</div>  

<script>
    
    jQuery(document).on("click",".promotion-view-btn",function () {
    
            var promotionId = jQuery(this).attr("promotion-id");
            var promotionName = jQuery(this).attr("promotion-name");
            
            jQuery.ajax({
    
              type: 'POST',
              url: AjaxURL,
              data: {"action": "save_promotion_history","promotionId":promotionId,"promotionName":promotionName},
              success: function(result)
              {
    
                // no action required here if any we will add here in future
              }
            });
    
     
    });    
    
</script>   
    
    <?php $html = ob_get_clean(); 
        
    }
    
    echo $html;
}
function loadAestheticsPackages() {
    
    ob_start();
    
        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'promotions-tax',
                                    'field' => 'slug',
                                    'terms' => 'aesthetics'
                            )
                        ),
            'post_type'=>'promotions',
            'posts_per_page'=>4
           
        );
        
        $promotionsList = get_posts($args);
    
    ?>


<?php
    
if (count($promotionsList) > 0) { ?>
    
<div data-delay="0" class="vc_row wpb_row vc_row-fluid vc_custom_1561118998307">
   <div class="wpb_column vc_column_container vc_col-sm-12" data-delay="0">
      <div class="vc_column-inner ">
         <div class="wpb_wrapper">
            <div id="1561118940969-dccb172a-948f" style="height:50px;" class="dt-sc-empty-space"></div>
            <div class="dt-sc-title script-with-sub-title  aligncenter" data-delay="0">
               <h6 class="dt-sc-main-heading">With Best Price</h6>
               <h2 class="dt-sc-sub-heading">Promotions</h2>
            </div>
            <div id="1561109976483-6c398015-b2fd" style="height:75px;" class="dt-sc-empty-space"></div>
            <div class="vc_row wpb_row vc_inner vc_row-fluid">
               
            <?php 
            
                foreach ($promotionsList as $promoindex => $promotion) {
                    
                $promoheading1 = get_post_meta( $promotion->ID, 'promoheading1', true );
                $promoheading2 = get_post_meta( $promotion->ID, 'promoheading2', true );
                $promoimage = get_post_meta( $promotion->ID, 'promoimage', true );
                $promoprice = get_post_meta( $promotion->ID, 'promoprice', true );
                $promodetails = get_post_meta( $promotion->ID, 'promodetails', true );
                $promourl = get_post_meta( $promotion->ID, 'promourl', true );
                
                if ($promoindex == 1) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                

            ?>
               <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-6">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="dt-sc-pr-tb-col type1 standard <?php echo $selected ?>" data-delay="0">
                           <div class="dt-sc-tb-thumb"><img width="80" height="80" src="<?php echo $promoimage ?>" class="attachment-full" alt=""></div>
                           <div class="dt-sc-tb-header">
                              <div class="dt-sc-tb-title">
                                 <p><?php echo $promoheading1 ?></p>
                                 <h5><?php echo $promoheading2 ?></h5>
                              </div>
                              <div class="dt-sc-price">
                                 <h6> <sup>RS</sup><?php echo $promoprice ?></h6>
                              </div>
                           </div>
                           <ul class="dt-sc-tb-content">
                              <?php foreach ($promodetails as $itemindex => $item): ?>
                              
                              <li><?php echo $item['promotionitem'] ?></li>
                              
                              <?php endforeach; ?>
                           </ul>
                           <div class="dt-sc-buy-now"><a class="dt-sc-button promotion-view-btn" promotion-name="<?php echo $promoheading2 ?>" promotion-id="<?php echo $promotion->ID ?>" target="_self" href="<?php echo $promourl ?>">View More</a></div>
                        </div>
                     </div>
                  </div>
               </div>
        
          <?php } ?>
            
            </div>
            <div id="1561118945895-9cb76125-46be" class="dt-sc-empty-space"></div>
         </div>
      </div>
   </div>
</div>  

<script>
    
    jQuery(document).on("click",".promotion-view-btn",function () {
    
            var promotionId = jQuery(this).attr("promotion-id");
            var promotionName = jQuery(this).attr("promotion-name");
            
            jQuery.ajax({
    
              type: 'POST',
              url: AjaxURL,
              data: {"action": "save_promotion_history","promotionId":promotionId,"promotionName":promotionName},
              success: function(result)
              {
    
                // no action required here if any we will add here in future
              }
            });
    
     
    });    
    
</script>   
    
    <?php $html = ob_get_clean(); 
        
    }
    
    echo $html;
}
function loadCSPackages() {
    
    ob_start();
    
        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'promotions-tax',
                                    'field' => 'slug',
                                    'terms' => 'cosmeticsurgery'
                            )
                        ),
            'post_type'=>'promotions',
            'posts_per_page'=>4
           
        );
        
        $promotionsList = get_posts($args);
    
    ?>


<?php
    
if (count($promotionsList) > 0) { ?>
    
<div data-delay="0" class="vc_row wpb_row vc_row-fluid vc_custom_1561118998307">
   <div class="wpb_column vc_column_container vc_col-sm-12" data-delay="0">
      <div class="vc_column-inner ">
         <div class="wpb_wrapper">
            <div id="1561118940969-dccb172a-948f" style="height:50px;" class="dt-sc-empty-space"></div>
            <div class="dt-sc-title script-with-sub-title  aligncenter" data-delay="0">
               <h6 class="dt-sc-main-heading">With Best Price</h6>
               <h2 class="dt-sc-sub-heading">Promotions</h2>
            </div>
            <div id="1561109976483-6c398015-b2fd" style="height:75px;" class="dt-sc-empty-space"></div>
            <div class="vc_row wpb_row vc_inner vc_row-fluid">
               
            <?php 
            
                foreach ($promotionsList as $promoindex => $promotion) {
                    
                $promoheading1 = get_post_meta( $promotion->ID, 'promoheading1', true );
                $promoheading2 = get_post_meta( $promotion->ID, 'promoheading2', true );
                $promoimage = get_post_meta( $promotion->ID, 'promoimage', true );
                $promoprice = get_post_meta( $promotion->ID, 'promoprice', true );
                $promodetails = get_post_meta( $promotion->ID, 'promodetails', true );
                $promourl = get_post_meta( $promotion->ID, 'promourl', true );
                
                if ($promoindex == 1) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                

            ?>
               <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-6">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="dt-sc-pr-tb-col type1 standard <?php echo $selected ?>" data-delay="0">
                           <div class="dt-sc-tb-thumb"><img width="80" height="80" src="<?php echo $promoimage ?>" class="attachment-full" alt=""></div>
                           <div class="dt-sc-tb-header">
                              <div class="dt-sc-tb-title">
                                 <p><?php echo $promoheading1 ?></p>
                                 <h5><?php echo $promoheading2 ?></h5>
                              </div>
                              <div class="dt-sc-price">
                                 <h6> <sup>RS</sup><?php echo $promoprice ?></h6>
                              </div>
                           </div>
                           <ul class="dt-sc-tb-content">
                              <?php foreach ($promodetails as $itemindex => $item): ?>
                              
                              <li><?php echo $item['promotionitem'] ?></li>
                              
                              <?php endforeach; ?>
                           </ul>
                           <div class="dt-sc-buy-now"><a class="dt-sc-button promotion-view-btn" promotion-name="<?php echo $promoheading2 ?>" promotion-id="<?php echo $promotion->ID ?>" target="_self" href="<?php echo $promourl ?>">View More</a></div>
                        </div>
                     </div>
                  </div>
               </div>
        
          <?php } ?>
            
            </div>
            <div id="1561118945895-9cb76125-46be" class="dt-sc-empty-space"></div>
         </div>
      </div>
   </div>
</div>  

<script>
    
    jQuery(document).on("click",".promotion-view-btn",function () {
    
            var promotionId = jQuery(this).attr("promotion-id");
            var promotionName = jQuery(this).attr("promotion-name");
            
            jQuery.ajax({
    
              type: 'POST',
              url: AjaxURL,
              data: {"action": "save_promotion_history","promotionId":promotionId,"promotionName":promotionName},
              success: function(result)
              {
    
                // no action required here if any we will add here in future
              }
            });
    
     
    });    
    
</script> 
    
    <?php $html = ob_get_clean(); 
        
    }
    
    echo $html;
}
function loadDentalPackages() {
    
    ob_start();
    
        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'promotions-tax',
                                    'field' => 'slug',
                                    'terms' => 'dental'
                            )
                        ),
            'post_type'=>'promotions',
            'posts_per_page'=>4
           
        );
        
        $promotionsList = get_posts($args);
    
    ?>


<?php
    
if (count($promotionsList) > 0) { ?>
    
<div data-delay="0" class="vc_row wpb_row vc_row-fluid vc_custom_1561118998307">
   <div class="wpb_column vc_column_container vc_col-sm-12" data-delay="0">
      <div class="vc_column-inner ">
         <div class="wpb_wrapper">
            <div id="1561118940969-dccb172a-948f" style="height:50px;" class="dt-sc-empty-space"></div>
            <div class="dt-sc-title script-with-sub-title  aligncenter" data-delay="0">
               <h6 class="dt-sc-main-heading">With Best Price</h6>
               <h2 class="dt-sc-sub-heading">Promotions</h2>
            </div>
            <div id="1561109976483-6c398015-b2fd" style="height:75px;" class="dt-sc-empty-space"></div>
            <div class="vc_row wpb_row vc_inner vc_row-fluid">
               
            <?php 
            
                foreach ($promotionsList as $promoindex => $promotion) {
                    
                $promoheading1 = get_post_meta( $promotion->ID, 'promoheading1', true );
                $promoheading2 = get_post_meta( $promotion->ID, 'promoheading2', true );
                $promoimage = get_post_meta( $promotion->ID, 'promoimage', true );
                $promoprice = get_post_meta( $promotion->ID, 'promoprice', true );
                $promodetails = get_post_meta( $promotion->ID, 'promodetails', true );
                $promourl = get_post_meta( $promotion->ID, 'promourl', true );
                
                if ($promoindex == 1) {
                    $selected = "selected";
                } else {
                    $selected = "";
                }
                

            ?>
               <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-6">
                  <div class="vc_column-inner ">
                     <div class="wpb_wrapper">
                        <div class="dt-sc-pr-tb-col type1 standard <?php echo $selected ?>" data-delay="0">
                           <div class="dt-sc-tb-thumb"><img width="80" height="80" src="<?php echo $promoimage ?>" class="attachment-full" alt=""></div>
                           <div class="dt-sc-tb-header">
                              <div class="dt-sc-tb-title">
                                 <p><?php echo $promoheading1 ?></p>
                                 <h5><?php echo $promoheading2 ?></h5>
                              </div>
                              <div class="dt-sc-price">
                                 <h6> <sup>RS</sup><?php echo $promoprice ?></h6>
                              </div>
                           </div>
                           <ul class="dt-sc-tb-content">
                              <?php foreach ($promodetails as $itemindex => $item): ?>
                              
                              <li><?php echo $item['promotionitem'] ?></li>
                              
                              <?php endforeach; ?>
                           </ul>
                           <div class="dt-sc-buy-now"><a class="dt-sc-button promotion-view-btn" promotion-name="<?php echo $promoheading2 ?>" promotion-id="<?php echo $promotion->ID ?>" target="_self" href="<?php echo $promourl ?>">View More</a></div>
                        </div>
                     </div>
                  </div>
               </div>
        
          <?php } ?>
            
            </div>
            <div id="1561118945895-9cb76125-46be" class="dt-sc-empty-space"></div>
         </div>
      </div>
   </div>
</div>  

<script>
    
    jQuery(document).on("click",".promotion-view-btn",function () {
    
            var promotionId = jQuery(this).attr("promotion-id");
            var promotionName = jQuery(this).attr("promotion-name");
            
            jQuery.ajax({
    
              type: 'POST',
              url: AjaxURL,
              data: {"action": "save_promotion_history","promotionId":promotionId,"promotionName":promotionName},
              success: function(result)
              {
    
                // no action required here if any we will add here in future
              }
            });
    
     
    });    
    
</script> 
    
    <?php $html = ob_get_clean(); 
        
    }
    
    echo $html;
}

function ingsThemeQueryVars($vars){ 

      $vars[] = "getAllDetailedServices";
      $vars[] = "getAllDetailedProducts";
      $vars[] = "getDetailedProduct";
      $vars[] = "getDetailedService";
      $vars[] = "contactUsMobile";
      $vars[] = "getTrends";
      $vars[] = "getGiftCards";
     

      return $vars;

}

 function executeThemeActions($template){ 
        
        global $wp_query;
        global $wpdb;
        
        $data = $_GET;
        
    	if(!isset( $wp_query->query['getGiftCards'] ) && !isset( $wp_query->query['getTrends'] ) && !isset( $wp_query->query['contactUsMobile'] ) && !isset( $wp_query->query['getAllDetailedServices'] ) && !isset( $wp_query->query['getAllDetailedProducts'] ) && !isset( $wp_query->query['getDetailedProduct'] ) && !isset( $wp_query->query['getDetailedService'] ) )
    	    return $template;
           
        if(isset($wp_query->query['getAllDetailedServices'])){

    
           $postType = $data['post_type'];
           
           
           $slug = isset($_GET['service']) ? $_GET['service']: "";
           
            if ($slug != "") {
        
                $args = array(
                'tax_query' => array(
                                    array(
                                            'taxonomy' => "$postType-tax",
                                            'field' => 'slug',
                                            'terms' => $slug
                                    )
                                ),
                    'post_type' => $postType,
                    'posts_per_page' => -1
                   
                );
               
            } else {
               
                $args = array(
                    'post_type' => $postType,
                    'posts_per_page'=>3
                );
        
            }
           
            $postsList = get_posts($args);     
            
            
            $parentTaxId = 0;
            
            if (!empty($slug)) {
                
                $term = get_term_by('slug', $slug, "$postType-tax");
                $parentTaxId = $term->term_id;
            }
            
            
            $args = array('taxonomy' => "$postType-tax",
                          'hide_empty' => false,
                          'parent' => $parentTaxId
                         );
        
             $customPostTaxonomies = get_terms($args);
             
             $mainCategories = $customPostTaxonomies;    
             
             $allPosts[] = array();
             $allCategories[] = array();
             
             if(is_array($postsList)) {
                
                foreach ($postsList as $index => $post) {
                    
                    $erpId = get_post_meta( $post->ID, 'id', true );
                    $serviceLocation = getLocationByServiceId($erpId);
                    
                    $allPosts[$index]["id"] = $post->ID;
                    $allPosts[$index]["service"] = $erpId;
                    $allPosts[$index]["location"] = $serviceLocation;
                    
                    $allPosts[$index]["title"] = $post->post_title;
                    $allPosts[$index]["content"] = $post->post_content;
                    $allPosts[$index]["image"] = get_the_post_thumbnail_url($post->ID);
                    
                    $allPosts[$index]['heading1'] = get_post_meta( $post->ID, 'heading1', true );
                    $allPosts[$index]['heading2'] = get_post_meta( $post->ID, 'heading2', true );
                    $allPosts[$index]['description'] = get_post_meta( $post->ID, 'description', true );
                    $allPosts[$index]['beforeAfterSlider'] = get_post_meta( $post->ID, 'slider', true );
                    $allPosts[$index]['productSlider'] = get_post_meta( $post->ID, 'products', true );
                    $allPosts[$index]['price'] = get_post_meta( $post->ID, 'price', true );
                    $allPosts[$index]['faq'] = get_post_meta( $post->ID, 'faq', true );
                    $allPosts[$index]['postimage'] = get_post_meta( $post->ID, 'postimage', true );
                    $allPosts[$index]['for'] = get_post_meta( $post->ID, 'productfor', true );
                    
                    

                    
                }
                
                
             }
             
             if(is_array($postsList)) { 
                 
                 foreach ($mainCategories as $index => $category) {
                     
                     $allCategories[$index]["term_id"] = $category->term_id;
                     $allCategories[$index]["slug"] = $category->slug;
                     $allCategories[$index]["name"] = $category->name;
                     $allCategories[$index]["count"] = $category->count;
                     $allCategories[$index]["image"] = "";
                     
                     $taxonomy_image_url = get_option('z_taxonomy_image'.$category->term_id);
                     
                     if(!empty($taxonomy_image_url)) {
                        $attachment_id = getAttachmentIdByUrl($taxonomy_image_url);
                        if(!empty($attachment_id)) {
                            $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
                            $taxonomy_image_url = $taxonomy_image_url[0];
                            
                            $allCategories[$index]["image"] = $taxonomy_image_url;
                        }
                     }
                 }
                 
             }

             $responseData = array(
                    			'status' => array(
                    				'code' => 200,
                    				'message' => "Services under categories"
                    			),
                    			'data' => array("categories" => $allCategories, "services" => $allPosts),
                    			);
                    			
            echo wp_send_json($responseData);
            
        } else if (isset( $wp_query->query['getTrends'] )) {
            
            global $wpdb;
            
            $totalviewsPromotions = $wpdb->get_results("SELECT SUM(`count`) as totalcount FROM `promotions_count`;");
            
            $mostlyViewedPromotion = $wpdb->get_results("SELECT * FROM promotions_count WHERE count = (SELECT MAX(count) FROM promotions_count);");
        
            $mostlyViewedPromotionId = $mostlyViewedPromotion[0]->fk_promotion_id;
     
            $mostViewed = getPromotionById($mostlyViewedPromotionId);

            $mostViewedPostUrl = get_post_meta($mostlyViewedPromotionId,"promourl",true);
        
            $totalViewedPromotions = $totalviewsPromotions[0]->totalcount;
            
             $responseData = array(
                    			'status' => array(
                    				'code' => 200,
                    				'message' => "Promotions Statictics"
                    			),
                    			'data' => array("total_promotions" => $totalViewedPromotions, "mostly_viewed" => array("name"=>$mostViewed['h2'],"url"=>$mostViewedPostUrl)),
                    			);
                    			
            echo wp_send_json($responseData); 
            
        } else if (isset( $wp_query->query['getGiftCards'] )) {
            
            
            $query_args = array(
               'post_type' => 'product',
               'tax_query' => array(
                    array(
                        'taxonomy' => 'product_type',
                        'field'    => 'slug',
                        'terms'    => 'wgm_gift_card', 
                    ),
                ),
             );
             
            $products = get_posts( $query_args );
            
            $giftCards = array();
            
            foreach ($products as $index => $gift) {
                
                $title = $gift->post_title;
                $image = get_the_post_thumbnail_url($gift->ID);
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $gift->ID ), 'single-post-thumbnail' );
                $url = get_permalink( $gift->ID );
                

                
                $giftCards[$index] = array("title"=>$title,"image"=>$image[0],"url"=>$url);
            }
            
             $responseData = array(
                    			'status' => array(
                    				'code' => 200,
                    				'message' => "Gift Cards"
                    			),
                    			'data' => $giftCards,
                    			);
                    			
            echo wp_send_json($responseData);     			
                    			
        } else if (isset( $wp_query->query['contactUsMobile'] )) {
            
        
            $args = array("full_name" => $_POST['full_name'],"email" => $_POST['email'],"phone" => $_POST['phone'],"service" => $_POST['service'],"message" => $_POST['message']);
            
            $home_url = home_url();
            $url = $home_url.'/wp-json/contact-form-7/v1/contact-forms/29471/feedback';
            $response = wp_remote_post( $url, array(
                'method'      => 'POST',
                'body'        => $args
                )
            );
        
            if ( is_wp_error( $response ) ) {
                
                $error_message = $response->get_error_message();
                
                $responseData = array(
    			'status' => array(
    				'code' => 404,
    				'message' => "Something went wrong"
    			),
    			'data' => array("error"=>$error_message),
    			);
    			
            echo wp_send_json($responseData);                
            
                
            } else {
                $responseData = array(
    			'status' => array(
    				'code' => 200,
    				'message' => "Success"
    			),
    			'data' => [],
    			);
    			
    		echo wp_send_json($responseData); 
    			
            }
            
            
            
        } else if (isset($wp_query->query['getDetailedService']) ) {
            
            $postId = $_GET['service_id'];
            $postTitle = $_GET['label'];
            
            $post = get_post($postId);
            
            if ( $post ) {
        
                $erpId = get_post_meta( $postId, 'id', true );
                $serviceLocation = getLocationByServiceId($erpId);
                $allPosts["id"] = $postId;
                $allPosts["location"] = $serviceLocation;            
                $allPosts["service"] = $erpId;
                $allPosts["title"] = $post->post_title;
                $allPosts["content"] = $post->post_content;
                $allPosts["image"] = get_the_post_thumbnail_url($postId);
                
                $allPosts['heading1'] = get_post_meta( $postId, 'heading1', true );
                $allPosts['heading2'] = get_post_meta( $postId, 'heading2', true );
                $allPosts['description'] = get_post_meta( $postId, 'description', true );
                $allPosts['beforeAfterSlider'] = get_post_meta( $postId, 'slider', true );
                $allPosts['productSlider'] = get_post_meta( $postId, 'products', true );
                $allPosts['price'] = get_post_meta( $postId, 'price', true );
                $allPosts['faq'] = get_post_meta( $postId, 'faq', true );
                $allPosts['postimage'] = get_post_meta( $postId, 'postimage', true );
                $allPosts['for'] = get_post_meta( $postId, 'productfor', true );
                        
                    
                
                $responseData = array(
                'status' => array(
                	'code' => 200,
                	'message' => "Detailed Service"
                ),
                'data' => $allPosts,
                );
                
                echo wp_send_json($responseData);
                            
            } else {
                
                global $wpdb;
                
                $postId = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s", $postTitle ));
                
                if ($postId) {
               
                    $erpId = get_post_meta( $postId, 'id', true );
                    $serviceLocation = getLocationByServiceId($erpId);
                    $allPosts["id"] = $postId;
                    $allPosts["location"] = $serviceLocation;            
                    $allPosts["service"] = $erpId;
                    $allPosts["title"] = $post->post_title;
                    $allPosts["content"] = $post->post_content;
                    $allPosts["image"] = get_the_post_thumbnail_url($postId);
                    
                    $allPosts['heading1'] = get_post_meta( $postId, 'heading1', true );
                    $allPosts['heading2'] = get_post_meta( $postId, 'heading2', true );
                    $allPosts['description'] = get_post_meta( $postId, 'description', true );
                    $allPosts['beforeAfterSlider'] = get_post_meta( $postId, 'slider', true );
                    $allPosts['productSlider'] = get_post_meta( $postId, 'products', true );
                    $allPosts['price'] = get_post_meta( $postId, 'price', true );
                    $allPosts['faq'] = get_post_meta( $postId, 'faq', true );
                    $allPosts['postimage'] = get_post_meta( $postId, 'postimage', true );
                    $allPosts['for'] = get_post_meta( $postId, 'productfor', true );
                            
                    
                    $responseData = array(
                    'status' => array(
                    	'code' => 200,
                    	'message' => "Detailed Service"
                    ),
                    'data' => $allPosts,
                    );
                    
                    echo wp_send_json($responseData);               
                
                } else {
                    
                    $responseData = array(
                    'status' => array(
                    	'code' => 404,
                    	'message' => "No Service Found"
                    ),
                    'data' => [],
                    ); 
                    
                    echo wp_send_json($responseData);
                    
                    
                }
            }
            
        } else if (isset($wp_query->query['getDetailedProduct']) ) {
            
            
            $data = array();
            
            $productId = $_GET['product_id'];
            
            if ( get_post_status( $productId ) == false ) {	  
                
                
                $args     = array( 'post_type' => 'product', 'posts_per_page' => -1 );
                $products = get_posts( $args );
                
                $data = array();
                
                foreach ($products as $index => $value) {
                    
                    $product_id = $value->ID;
                    
                    $erpId = get_post_meta($value->ID,'productid',true);
                    
                    if ($erpId == $productId) {
                        
                        $productId = $value->ID;
                        
                        $product = new WC_product($productId);  
                        
                        $attachment_ids = $product->get_gallery_image_ids();
                        
                        $image_link = array();
                    
                        foreach( $attachment_ids as $attachment_id ) {
                            $image_link[] = wp_get_attachment_url( $attachment_id );
                        }                
                        
                        $mata = get_post_meta($productId,'',true);
                        
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $productId ), 'single-post-thumbnail' );
            
                        $metadata = array();
                        
                        foreach ($mata as $metaIndex => $metaValue) {
                            
                            if (in_array($metaIndex,array("_stock","_stock_status","_regular_price","_sale_price","_price"))) {
                                
                                $metadata[$metaIndex] = $metaValue[0];
                            }
                                
                        }
                        
                        $data['product_image'] = isset($image[0])?$image[0]:"";
                        $data['product_gallery'] = $image_link;
                        $data['id'] = $productId;
                        $data['title'] = $product->get_name();
                        $data['detail'] = $product->get_short_description();
                        
                        $data['metadata'] = $metadata;  
            
            
            
                        
                                $responseData = array(
                    			'status' => array(
                    				'code' => 200,
                    				'message' => "Product Details"
                    			),
                    			'data' => $data,
                    			);
                    			
                    			echo wp_send_json($responseData);
                    			exit;
                        
                        
                    }
                    
                }    
                $responseData = array(
        			'status' => array(
        				'code' => 404,
        				'message' => "Product Not found"
        			),
        			'data' => $data,
        		);	
                echo wp_send_json($responseData);
                
                exit;
            }   
            
            
            
            $product = new WC_product($productId);  
            
            $attachment_ids = $product->get_gallery_image_ids();
            
            $image_link = array();
        
            foreach( $attachment_ids as $attachment_id ) {
                $image_link[] = wp_get_attachment_url( $attachment_id );
            }                
            
            $mata = get_post_meta($productId,'',true);
            
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $productId ), 'single-post-thumbnail' );

            $metadata = array();
            
            foreach ($mata as $metaIndex => $metaValue) {
                
                if (in_array($metaIndex,array("_stock","_stock_status","_regular_price","_sale_price","_price"))) {
                    
                    $metadata[$metaIndex] = $metaValue[0];
                }
                    
            }
            
            $data['product_image'] = isset($image[0])?$image[0]:"";
            $data['product_gallery'] = $image_link;
            $data['id'] = $productId;
            $data['title'] = $product->get_name();
            $data['detail'] = $product->get_short_description();
            
            $data['metadata'] = $metadata;  
            
            
            
                        
            $responseData = array(
                    			'status' => array(
                    				'code' => 200,
                    				'message' => "Product Details"
                    			),
                    			'data' => $data,
                    			);
                    			
            echo wp_send_json($responseData);
            
            
            
        } else if (isset($wp_query->query['getAllDetailedProducts']) ) {

            $args     = array( 'post_type' => 'product', 'posts_per_page' => -1 );
            $products = get_posts( $args );
            
            $data = array();
            
            foreach ($products as $index => $value) {
                
                $product_id = $value->ID;
                $product = new WC_product($product_id);       
                $attachment_ids = $product->get_gallery_image_ids();
                
                $image_link = array();
            
                foreach( $attachment_ids as $attachment_id ) {
                    $image_link[] = wp_get_attachment_url( $attachment_id );
                }                
                
                $mata = get_post_meta($value->ID,'',true);
                
                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $product_id ), 'single-post-thumbnail' );

                $metadata = array();
                
                foreach ($mata as $metaIndex => $metaValue) {
                    
                    if (in_array($metaIndex,array("_stock","_stock_status","_regular_price","_sale_price","_price"))) {
                        
                        $metadata[$metaIndex] = $metaValue[0];
                    }
                        
                }
                
                $data[$index]['product_image'] = isset($image[0])?$image[0]:"";
                $data[$index]['id'] = $product_id;
                $data[$index]['title'] = $value->post_title;
                $data[$index]['price'] = $product->get_price();
                $data[$index]['detail'] = $value->post_excerpt;
                
                
                //$data[$index]['product_gallery'] = $image_link;
                //$data[$index]['product_details'] = $value;
                //$data[$index]['metadata'] = $metadata;                
       
                    
                    
            }
                
                
                

            
            $responseData = array(
                    			'status' => array(
                    				'code' => 200,
                    				'message' => "All Products"
                    			),
                    			'data' => $data,
                    			);
                    			
            echo wp_send_json($responseData);                    			
            
            
        }
            
        
        exit;
        
    }
    
// get attachment ID by image url
function getAttachmentIdByUrl($image_src) {
    global $wpdb;
    $query = $wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid = %s", $image_src);
    $id = $wpdb->get_var($query);
    return (!empty($id)) ? $id : NULL;
}    

function loadCSCategoriesServices() {
    
    $slug = isset($_GET['service']) ? $_GET['service']: "";
   
    if ($slug != "") {

        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'cosmetic-surgery-tax',
                                    'field' => 'slug',
                                    'terms' => $slug
                            )
                        ),
            'post_type'=>'cosmetic-surgery',
            'posts_per_page'=>-1
           
        );
       
    } else {
       
        $args = array(
            'post_type'=>'cosmetic-surgery',
            'posts_per_page'=>3
        );

    }
   
    $postsList = get_posts($args);
    
    $postsListArray = array_chunk($postsList, 3);
    
    $html = '';
    
    if (is_array($postsList)) {
        
        
        $html .= '<div id="1561363009893-ddd2acf9-2d3f" class="dt-sc-empty-space" style="margin-top:50px;"></div>
            <h2 style="text-align: center" class="vc_custom_heading">Related Services</h2>
            <p style="text-align: center" class="vc_custom_heading">We encourage your appearance, please feel free to login or create account by
                <br> using the following form.</p><div id="1561363016947-ee05ec60-ed26" class="dt-sc-empty-space" style="margin-top:50px;"></div>';
        
        $counter = 0;
        
        
        foreach ($postsListArray as $index => $list) {
            
            $html .= '<div class="vc_row wpb_row vc_inner vc_row-fluid">';
            
            foreach ($list as $index => $post) {
            
                $image = get_the_post_thumbnail_url($post->ID);
    
                
                $html .= '<div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner ">
                        <a href="'.$post->guid.'" target=" _blank" title="'.$post->post_title.'"">
                        <div class="wpb_wrapper">
                            <div class="dt-sc-image-caption type3  " data-delay="0">
                               <div class="dt-sc-image-wrapper"><img style="width:auto" src="'.$image.'" class="attachment-full" alt="" srcset="'.$image.' 450w, '.$image.' 238w" sizes="(max-width: 450px) 100vw, 450px"></div>
                                <div class="dt-sc-image-content">
                                    <div class="dt-sc-image-title">
                                        <h3>'.ucfirst($post->post_title).'</h3>
                                    </div>
                                    <p>Nunc eget feugiat ex. Aliquam commodo dapibus. Duis sed venenatis est. Vestibulum tem ipsum ut molestie imperdiet.</p>
                                    
                                </div>
                            </div>
                            <div id="1587022811374-119ee39b-d641" class="dt-sc-empty-space" style="margin-bottom:65px;"></div>
                        </div>
                        </a>
                    </div>
                    
                </div>';
                
                    
            }
                    
                    
            $html .= '</div>';

            

            
        }
    }
    
  echo $html;
    
}

function loadDentalCategoriesServices() {
    
    $slug = isset($_GET['service']) ? $_GET['service']: "";
   
    if ($slug != "") {

        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'dental-tax',
                                    'field' => 'slug',
                                    'terms' => $slug
                            )
                        ),
            'post_type'=>'dental',
            'posts_per_page'=>-1
           
        );
       
    } else {
       
        $args = array(
            'post_type'=>'dental',
            'posts_per_page'=>3
        );

    }
   
    $postsList = get_posts($args);
    $postsListArray = array_chunk($postsList, 3);
    
    $html = '';
    
    if (is_array($postsList)) {
        
        
        $html .= '<div id="1561363009893-ddd2acf9-2d3f" class="dt-sc-empty-space" style="margin-top:50px;"></div>
            <h2 style="text-align: center" class="vc_custom_heading">Related Services</h2>
            <p style="text-align: center" class="vc_custom_heading">We encourage your feedback, please feel free to send us a message
                <br> using the following contact form.</p><div id="1561363016947-ee05ec60-ed26" class="dt-sc-empty-space" style="margin-top:50px;"></div>';
        
        $counter = 0;
        
        
        foreach ($postsListArray as $index => $list) {
            
            $html .= '<div class="vc_row wpb_row vc_inner vc_row-fluid">';
            
            foreach ($list as $index => $post) {
            
                $image = get_the_post_thumbnail_url($post->ID);
    
                
                $html .= '<div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner ">
                        <a href="'.$post->guid.'" target=" _blank" title="'.$post->post_title.'"">
                        <div class="wpb_wrapper">
                            <div class="dt-sc-image-caption type3  " data-delay="0">
                               <div class="dt-sc-image-wrapper"><img style="width:auto" src="'.$image.'" class="attachment-full" alt="" srcset="'.$image.' 450w, '.$image.' 238w" sizes="(max-width: 450px) 100vw, 450px"></div>
                                <div class="dt-sc-image-content">
                                    <div class="dt-sc-image-title">
                                        <h3>'.ucfirst($post->post_title).'</h3>
                                    </div>
                                    <p>Nunc eget feugiat ex. Aliquam commodo dapibus. Duis sed venenatis est. Vestibulum tem ipsum ut molestie imperdiet.</p>
                                    
                                </div>
                            </div>
                            <div id="1587022811374-119ee39b-d641" class="dt-sc-empty-space" style="margin-bottom:65px;"></div>
                        </div>
                        </a>
                    </div>
                    
                </div>';
                
                    
            }
                    
                    
            $html .= '</div>';

            

            
        }
    }
    
  echo $html;
    
}

function loadAestheticsCategoriesServices() {
    
    $slug = isset($_GET['service']) ? $_GET['service']: "";
   
    if ($slug != "") {

        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'aesthetics-tax',
                                    'field' => 'slug',
                                    'terms' => $slug
                            )
                        ),
            'post_type'=>'aesthetics',
            'posts_per_page'=>-1
           
        );
       
    } else {
       
        $args = array(
            'post_type'=>'aesthetics',
            'posts_per_page'=>3
        );

    }
   
    $postsList = get_posts($args);
    
    $postsListArray = array_chunk($postsList, 3);
    
    $html = '';
    
    if (is_array($postsList)) {
        
        
        $html .= '<div id="1561363009893-ddd2acf9-2d3f" class="dt-sc-empty-space" style="margin-top:50px;"></div>
            <h2 style="text-align: center" class="vc_custom_heading">Related Services</h2>
            <p style="text-align: center" class="vc_custom_heading">We encourage your feedback, please feel free to send us a message
                <br> using the following contact form.</p><div id="1561363016947-ee05ec60-ed26" class="dt-sc-empty-space" style="margin-top:50px;"></div>';
        
        $counter = 0;
        
        
        foreach ($postsListArray as $index => $list) {
            
            $html .= '<div class="vc_row wpb_row vc_inner vc_row-fluid">';
            
            foreach ($list as $index => $post) {
            
                $image = get_the_post_thumbnail_url($post->ID);
    
                
                $html .= '<div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner ">
                        <a href="'.$post->guid.'" target=" _blank" title="'.$post->post_title.'"">
                        <div class="wpb_wrapper">
                            <div class="dt-sc-image-caption type3  " data-delay="0">
                               <div class="dt-sc-image-wrapper"><img style="width:auto" src="'.$image.'" class="attachment-full" alt="" srcset="'.$image.' 450w, '.$image.' 238w" sizes="(max-width: 450px) 100vw, 450px"></div>
                                <div class="dt-sc-image-content">
                                    <div class="dt-sc-image-title">
                                        <h3>'.ucfirst($post->post_title).'</h3>
                                    </div>
                                    <p>Nunc eget feugiat ex. Aliquam commodo dapibus. Duis sed venenatis est. Vestibulum tem ipsum ut molestie imperdiet.</p>
                                    
                                </div>
                            </div>
                            <div id="1587022811374-119ee39b-d641" class="dt-sc-empty-space" style="margin-bottom:65px;"></div>
                        </div>
                        </a>
                    </div>
                    
                </div>';
                
                    
            }
                    
                    
            $html .= '</div>';

            

            
        }
    }
    
  echo $html;
    
}

function loadMenCategoriesServices() {
    
    $slug = isset($_GET['service']) ? $_GET['service']: "";
   
    if ($slug != "") {

        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'krisalys-men-tax',
                                    'field' => 'slug',
                                    'terms' => $slug
                            )
                        ),
            'post_type'=>'krisalys-men',
            'posts_per_page'=>-1
           
        );
       
    } else {
       
        $args = array(
            'post_type'=>'krisalys-men',
            'posts_per_page'=>3
        );

    }
   
    $postsList = get_posts($args);
    
    $postsListArray = array_chunk($postsList, 3);
    
    $html = '';
    
    if (is_array($postsList)) {
        
        
        $html .= '<div id="1561363009893-ddd2acf9-2d3f" class="dt-sc-empty-space" style="margin-top:50px;"></div>
            <h2 style="text-align: center" class="vc_custom_heading">Related Services</h2>
            <p style="text-align: center" class="vc_custom_heading">We encourage your feedback, please feel free to send us a message
                <br> using the following contact form.</p><div id="1561363016947-ee05ec60-ed26" class="dt-sc-empty-space" style="margin-top:50px;"></div>';
        
        $counter = 0;
        
        
        foreach ($postsListArray as $index => $list) {
            
            $html .= '<div class="vc_row wpb_row vc_inner vc_row-fluid">';
            
            foreach ($list as $index => $post) {
            
                $image = get_the_post_thumbnail_url($post->ID);
    
                
                $html .= '<div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner ">
                        <a href="'.$post->guid.'" target=" _blank" title="'.$post->post_title.'"">
                        <div class="wpb_wrapper">
                            <div class="dt-sc-image-caption type3  " data-delay="0">
                               <div class="dt-sc-image-wrapper"><img style="width:auto" src="'.$image.'" class="attachment-full" alt="" srcset="'.$image.' 450w, '.$image.' 238w" sizes="(max-width: 450px) 100vw, 450px"></div>
                                <div class="dt-sc-image-content">
                                    <div class="dt-sc-image-title">
                                        <h3>'.ucfirst($post->post_title).'</h3>
                                    </div>
                                    <p>Nunc eget feugiat ex. Aliquam commodo dapibus. Duis sed venenatis est. Vestibulum tem ipsum ut molestie imperdiet.</p>
                                    
                                </div>
                            </div>
                            <div id="1587022811374-119ee39b-d641" class="dt-sc-empty-space" style="margin-bottom:65px;"></div>
                        </div>
                        </a>
                    </div>
                    
                </div>';
                
                    
            }
                    
                    
            $html .= '</div>';

            

            
        }
    }
    
  echo $html;
    
}

function loadWomenCategoriesServices() {
    
    $slug = isset($_GET['service']) ? $_GET['service']: "";
   
    if ($slug != "") {

        $args = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'krisalys-women-tax',
                                    'field' => 'slug',
                                    'terms' => $slug
                            )
                        ),
            'post_type'=>'krisalys-women',
            'posts_per_page'=>-1
           
        );
       
    } else {
       
        $args = array(
            'post_type'=>'krisalys-women',
            'posts_per_page'=>3
        );

    }
   
    $postsList = get_posts($args);
    
    $postsListArray = array_chunk($postsList, 3);
    
    $html = '';
    
    if (is_array($postsList)) {
        
        
        $html .= '<div id="1561363009893-ddd2acf9-2d3f" class="dt-sc-empty-space" style="margin-top:50px;"></div>
            <h2 style="text-align: center" class="vc_custom_heading">Related Services</h2>
            <p style="text-align: center" class="vc_custom_heading">We encourage your feedback, please feel free to send us a message
                <br> using the following contact form.</p><div id="1561363016947-ee05ec60-ed26" class="dt-sc-empty-space" style="margin-top:50px;"></div>';
        
        $counter = 0;
        
        
        foreach ($postsListArray as $index => $list) {
            
            $html .= '<div class="vc_row wpb_row vc_inner vc_row-fluid">';
            
            foreach ($list as $index => $post) {
            
                $image = get_the_post_thumbnail_url($post->ID);
    
                
                $html .= '<div class="wpb_column vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner ">
                        <a href="'.$post->guid.'" target=" _blank" title="'.$post->post_title.'"">
                        <div class="wpb_wrapper">
                            <div class="dt-sc-image-caption type3  " data-delay="0">
                               <div class="dt-sc-image-wrapper"><img style="width:auto" src="'.$image.'" class="attachment-full" alt="" srcset="'.$image.' 450w, '.$image.' 238w" sizes="(max-width: 450px) 100vw, 450px"></div>
                                <div class="dt-sc-image-content">
                                    <div class="dt-sc-image-title">
                                        <h3>'.ucfirst($post->post_title).'</h3>
                                    </div>
                                    <p>Nunc eget feugiat ex. Aliquam commodo dapibus. Duis sed venenatis est. Vestibulum tem ipsum ut molestie imperdiet.</p>
                                    
                                </div>
                            </div>
                            <div id="1587022811374-119ee39b-d641" class="dt-sc-empty-space" style="margin-bottom:65px;"></div>
                        </div>
                        </a>
                    </div>
                    
                </div>';
                
                    
            }
                    
                    
            $html .= '</div>';

            

            
        }
    }
    
  echo $html;
    
}



function loadDentalCategories() {
    
    $slug = isset($_GET['service']) ? $_GET['service'] : "";
    
    $parentTaxId = 0;
    
    if (!empty($slug)) {
        
        $term = get_term_by('slug', $slug, 'dental-tax');
        $parentTaxId = $term->term_id;
    }
    
    $args = array('taxonomy' => 'dental-tax',
                  'hide_empty' => false,
                  'parent' => $parentTaxId
                 );

     $customPostTaxonomies = get_terms($args);
     
     $mainCategories = array_chunk($customPostTaxonomies, ceil(count($customPostTaxonomies) / 2));
    
    
    foreach ($mainCategories as $mainIndex => $categories) {
        
        
        $html1 .= '<div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">';
                        
                        foreach ($categories as $index => $value) {
                            
                             $categoryImage = "";
                                
                             $taxonomy_image_url = get_option('z_taxonomy_image'.$value->term_id);
                             
                             if(!empty($taxonomy_image_url)) {
                                $attachment_id = getAttachmentIdByUrl($taxonomy_image_url);
                                if(!empty($attachment_id)) {
                                    $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
                                    $taxonomy_image_url = $taxonomy_image_url[0];
                                    
                                    $categoryImage = $taxonomy_image_url;
                                }
                             }                            
                            
                            $html .= '<a href="?service='.$value->slug.'"><div class="dt-sc-icon-box type15  " data-delay="0">
                                        <div class="icon-wrapper">
                                            <img width="132" height="154" src="'.$categoryImage.'" onerror="this.src=\'http://instaglo.novatoresols.online/wp-content/uploads/2020/04/category-default.png\'" class="attachment-thumbnail" alt="">
                                            <h4>'.$value->name.'<br>
                                            Treatment</h4>
                                        </div>
                                        <div class="icon-content"></div>
                                      </div></a>';
                            
                        }
  
        
    }


    
    $html = '
    
  <div class="dtportfolio-container-wrapper dtportfolio-without-fullwidth-wrapper hiddenscrollbar">
   <div class="dtportfolio-container with-space dtportfolio-container-resetter" data-column="1" data-postperpage="-1" style="position: relative; height: 850.812px;">
      <div class="dtportfolio-grid-sizer     dtportfolio-one-column "></div>';
      
      foreach ($customPostTaxonomies as $index => $category) {
          
          
                             $categoryImage = "";
                                
                             $taxonomy_image_url = get_option('z_taxonomy_image'.$category->term_id);
                             
                             if(!empty($taxonomy_image_url)) {
                                $attachment_id = getAttachmentIdByUrl($taxonomy_image_url);
                                if(!empty($attachment_id)) {
                                    $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
                                    $taxonomy_image_url = $taxonomy_image_url[0];
                                    
                                    $categoryImage = $taxonomy_image_url;
                                }
                             }           
      
                          $html .= '<div class="default with-space  dtportfolio-item dtportfolio-column dtportfolio-one-column with-sidebar dtportfolio-hover-modern-title first">
                                     <figure>
                                        <img src="'.$categoryImage.'" alt="'.$category->name.'" title="'.$category->name.'" onerror="this.src=\'http://instaglo.novatoresols.online/wp-content/uploads/2020/04/category-default.png\'">
                                        <div class="dtportfolio-image-overlay ">
                                           <div class="links">
                                              <a title="'.$category->name.'" href="'.home_url().'/dental/?service='.$category->slug.'"> <span class="fas fa-link"> </span> </a>
                                              <a title="'.$category->name.'" data-gal="prettyPhoto[dtgallery]" href="'.$categoryImage.'"> <span class="fas fa-search"> </span> </a>
                                           </div>
                                           <div class="dtportfolio-image-overlay-details">
                                              <h2><a href="'.home_url().'/dental/?service='.$category->slug.'" title="'.$category->name.'">'.$category->name.'</a></h2>
                                              <p class="categories"><a href="'.home_url().'/dental/?service='.$category->slug.'" rel="tag">Treatment</p>
                                           </div>
                                        </div>
                                     </figure>
                                  </div>';
                      
                      }
                $html .= '
                   </div>
                </div> '; 
    
    
    
    
    
    echo $html;
    
    
}

function loadCSCategories() {
    
    $slug = isset($_GET['service']) ? $_GET['service'] : "";
    
    $parentTaxId = 0;
    
    if (!empty($slug)) {
        
        $term = get_term_by('slug', $slug, 'cosmetic-surgery-tax');
        $parentTaxId = $term->term_id;
    }
    
    $args = array('taxonomy' => 'cosmetic-surgery-tax',
                  'hide_empty' => false,
                  'parent' => $parentTaxId
                 );

     $customPostTaxonomies = get_terms($args);
     
     $mainCategories = array_chunk($customPostTaxonomies, ceil(count($customPostTaxonomies) / 2));
    
    
    foreach ($mainCategories as $mainIndex => $categories) {
        
        
        $html1 .= '<div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">';
                        
                        foreach ($categories as $index => $value) {
                            
                             $categoryImage = "";
                                
                             $taxonomy_image_url = get_option('z_taxonomy_image'.$value->term_id);
                             
                             if(!empty($taxonomy_image_url)) {
                                $attachment_id = getAttachmentIdByUrl($taxonomy_image_url);
                                if(!empty($attachment_id)) {
                                    $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
                                    $taxonomy_image_url = $taxonomy_image_url[0];
                                    
                                    $categoryImage = $taxonomy_image_url;
                                }
                             }                            
                            
                            $html .= '<a href="?service='.$value->slug.'"><div class="dt-sc-icon-box type15  " data-delay="0">
                                        <div class="icon-wrapper">
                                            <img width="132" height="154" src="'.$categoryImage.'" onerror="this.src=\'http://instaglo.novatoresols.online/wp-content/uploads/2020/04/category-default.png\'" class="attachment-thumbnail" alt="">
                                            <h4>'.$value->name.'<br>
                                            Treatment</h4>
                                        </div>
                                        <div class="icon-content"></div>
                                      </div></a>';
                            
                        }
  
        
    }


    
    $html = '
    
  <div class="dtportfolio-container-wrapper dtportfolio-without-fullwidth-wrapper hiddenscrollbar">
   <div class="dtportfolio-container with-space dtportfolio-container-resetter" data-column="1" data-postperpage="-1" style="position: relative; height: 850.812px;">
      <div class="dtportfolio-grid-sizer     dtportfolio-one-column "></div>';
      
      foreach ($customPostTaxonomies as $index => $category) {
          
          
                             $categoryImage = "";
                                
                             $taxonomy_image_url = get_option('z_taxonomy_image'.$category->term_id);
                             
                             if(!empty($taxonomy_image_url)) {
                                $attachment_id = getAttachmentIdByUrl($taxonomy_image_url);
                                if(!empty($attachment_id)) {
                                    $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
                                    $taxonomy_image_url = $taxonomy_image_url[0];
                                    
                                    $categoryImage = $taxonomy_image_url;
                                }
                             }           
      
                          $html .= '<div class="default with-space  dtportfolio-item dtportfolio-column dtportfolio-one-column with-sidebar dtportfolio-hover-modern-title first">
                                     <figure>
                                        <img src="'.$categoryImage.'" alt="'.$category->name.'" title="'.$category->name.'" onerror="this.src=\'http://instaglo.novatoresols.online/wp-content/uploads/2020/04/category-default.png\'">
                                        <div class="dtportfolio-image-overlay ">
                                           <div class="links">
                                              <a title="'.$category->name.'" href="'.home_url().'/cosmetic-surgery/?service='.$category->slug.'"> <span class="fas fa-link"> </span> </a>
                                              <a title="'.$category->name.'" data-gal="prettyPhoto[dtgallery]" href="'.$categoryImage.'"> <span class="fas fa-search"> </span> </a>
                                           </div>
                                           <div class="dtportfolio-image-overlay-details">
                                              <h2><a href="'.home_url().'/cosmetic-surgery/?service='.$category->slug.'" title="'.$category->name.'">'.$category->name.'</a></h2>
                                              <p class="categories"><a href="'.home_url().'/cosmetic-surgery/?service='.$category->slug.'" rel="tag">Treatment</p>
                                           </div>
                                        </div>
                                     </figure>
                                  </div>';
                      
                      }
                $html .= '
                   </div>
                </div> '; 
    
    
    
    
    
    echo $html;
    
    
}

function loadAestheticsCategories() {
    
    $slug = isset($_GET['service']) ? $_GET['service'] : "";
    
    $parentTaxId = 0;
    
    if (!empty($slug)) {
        
        $term = get_term_by('slug', $slug, 'aesthetics-tax');
        $parentTaxId = $term->term_id;
    }
    
    $args = array('taxonomy' => 'aesthetics-tax',
                  'hide_empty' => false,
                  'parent' => $parentTaxId
                 );

     $customPostTaxonomies = get_terms($args);
     
     $mainCategories = array_chunk($customPostTaxonomies, ceil(count($customPostTaxonomies) / 2));
    
    
    foreach ($mainCategories as $mainIndex => $categories) {
        
        
        $html1 .= '<div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">';
                        
                        foreach ($categories as $index => $value) {
                            
                             $categoryImage = "";
                                
                             $taxonomy_image_url = get_option('z_taxonomy_image'.$value->term_id);
                             
                             if(!empty($taxonomy_image_url)) {
                                $attachment_id = getAttachmentIdByUrl($taxonomy_image_url);
                                if(!empty($attachment_id)) {
                                    $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
                                    $taxonomy_image_url = $taxonomy_image_url[0];
                                    
                                    $categoryImage = $taxonomy_image_url;
                                }
                             }                            
                            
                            $html .= '<a href="?service='.$value->slug.'"><div class="dt-sc-icon-box type15  " data-delay="0">
                                        <div class="icon-wrapper">
                                            <img width="132" height="154" src="'.$categoryImage.'" onerror="this.src=\'http://instaglo.novatoresols.online/wp-content/uploads/2020/04/category-default.png\'" class="attachment-thumbnail" alt="">
                                            <h4>'.$value->name.'<br>
                                            Treatment</h4>
                                        </div>
                                        <div class="icon-content"></div>
                                      </div></a>';
                            
                        }
  
        
    }


    
    $html = '
    
  <div class="dtportfolio-container-wrapper dtportfolio-without-fullwidth-wrapper hiddenscrollbar">
   <div class="dtportfolio-container with-space dtportfolio-container-resetter" data-column="1" data-postperpage="-1" style="position: relative; height: 850.812px;">
      <div class="dtportfolio-grid-sizer     dtportfolio-one-column "></div>';
      
      foreach ($customPostTaxonomies as $index => $category) {
          
          
                             $categoryImage = "";
                                
                             $taxonomy_image_url = get_option('z_taxonomy_image'.$category->term_id);
                             
                             if(!empty($taxonomy_image_url)) {
                                $attachment_id = getAttachmentIdByUrl($taxonomy_image_url);
                                if(!empty($attachment_id)) {
                                    $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
                                    $taxonomy_image_url = $taxonomy_image_url[0];
                                    
                                    $categoryImage = $taxonomy_image_url;
                                }
                             }           
      
                          $html .= '<div class="default with-space  dtportfolio-item dtportfolio-column dtportfolio-one-column with-sidebar dtportfolio-hover-modern-title first">
                                     <figure>
                                        <img src="'.$categoryImage.'" alt="'.$category->name.'" title="'.$category->name.'" onerror="this.src=\'http://instaglo.novatoresols.online/wp-content/uploads/2020/04/category-default.png\'">
                                        <div class="dtportfolio-image-overlay ">
                                           <div class="links">
                                              <a title="'.$category->name.'" href="'.home_url().'/aesthetics/?service='.$category->slug.'"> <span class="fas fa-link"> </span> </a>
                                              <a title="'.$category->name.'" data-gal="prettyPhoto[dtgallery]" href="'.$categoryImage.'"> <span class="fas fa-search"> </span> </a>
                                           </div>
                                           <div class="dtportfolio-image-overlay-details">
                                              <h2><a href="'.home_url().'/aesthetics/?service='.$category->slug.'" title="'.$category->name.'">'.$category->name.'</a></h2>
                                              <p class="categories"><a href="'.home_url().'/aesthetics/?service='.$category->slug.'" rel="tag">Treatment</p>
                                           </div>
                                        </div>
                                     </figure>
                                  </div>';
                      
                      }
                $html .= '
                   </div>
                </div> '; 
    
    
    
    
    
    echo $html;
    
    
}

function loadMenCategories() {
    
    $slug = isset($_GET['service']) ? $_GET['service'] : "";
    
    $parentTaxId = 0;
    
    if (!empty($slug)) {
        
        $term = get_term_by('slug', $slug, 'krisalys-men-tax');
        $parentTaxId = $term->term_id;
    }
    
    $args = array('taxonomy' => 'krisalys-men-tax',
                  'hide_empty' => false,
                  'parent' => $parentTaxId
                 );

     $customPostTaxonomies = get_terms($args);
     
     $mainCategories = array_chunk($customPostTaxonomies, ceil(count($customPostTaxonomies) / 2));
    
    
    foreach ($mainCategories as $mainIndex => $categories) {
        
        
        $html1 .= '<div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">';
                        
                        foreach ($categories as $index => $value) {
                            
                             $categoryImage = "";
                                
                             $taxonomy_image_url = get_option('z_taxonomy_image'.$value->term_id);
                             
                             if(!empty($taxonomy_image_url)) {
                                $attachment_id = getAttachmentIdByUrl($taxonomy_image_url);
                                if(!empty($attachment_id)) {
                                    $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
                                    $taxonomy_image_url = $taxonomy_image_url[0];
                                    
                                    $categoryImage = $taxonomy_image_url;
                                }
                             }                            
                            
                            $html .= '<a href="?service='.$value->slug.'"><div class="dt-sc-icon-box type15  " data-delay="0">
                                        <div class="icon-wrapper">
                                            <img width="132" height="154" src="'.$categoryImage.'" onerror="this.src=\'http://instaglo.novatoresols.online/wp-content/uploads/2020/04/category-default.png\'" class="attachment-thumbnail" alt="">
                                            <h4>'.$value->name.'<br>
                                            Treatment</h4>
                                        </div>
                                        <div class="icon-content"></div>
                                      </div></a>';
                            
                        }
  
        
    }


    
    $html = '
    
  <div class="dtportfolio-container-wrapper dtportfolio-without-fullwidth-wrapper hiddenscrollbar">
   <div class="dtportfolio-container with-space dtportfolio-container-resetter" data-column="1" data-postperpage="-1" style="position: relative; height: 850.812px;">
      <div class="dtportfolio-grid-sizer     dtportfolio-one-column "></div>';
      
      foreach ($customPostTaxonomies as $index => $category) {
          
          
                             $categoryImage = "";
                                
                             $taxonomy_image_url = get_option('z_taxonomy_image'.$category->term_id);
                             
                             if(!empty($taxonomy_image_url)) {
                                $attachment_id = getAttachmentIdByUrl($taxonomy_image_url);
                                if(!empty($attachment_id)) {
                                    $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
                                    $taxonomy_image_url = $taxonomy_image_url[0];
                                    
                                    $categoryImage = $taxonomy_image_url;
                                }
                             }           
      
                          $html .= '<div class="default with-space  dtportfolio-item dtportfolio-column dtportfolio-one-column with-sidebar dtportfolio-hover-modern-title first">
                                     <figure>
                                        <img src="'.$categoryImage.'" alt="'.$category->name.'" title="'.$category->name.'" onerror="this.src=\'http://instaglo.novatoresols.online/wp-content/uploads/2020/04/category-default.png\'">
                                        <div class="dtportfolio-image-overlay ">
                                           <div class="links">
                                              <a title="'.$category->name.'" href="'.home_url().'/krisalys-men/?service='.$category->slug.'"> <span class="fas fa-link"> </span> </a>
                                              <a title="'.$category->name.'" data-gal="prettyPhoto[dtgallery]" href="'.$categoryImage.'"> <span class="fas fa-search"> </span> </a>
                                           </div>
                                           <div class="dtportfolio-image-overlay-details">
                                              <h2><a href="'.home_url().'/krisalys-men/?service='.$category->slug.'" title="'.$category->name.'">'.$category->name.'</a></h2>
                                              <p class="categories"><a href="'.home_url().'/krisalys-men/?service='.$category->slug.'" rel="tag">Treatment</p>
                                           </div>
                                        </div>
                                     </figure>
                                  </div>';
                      
                      }
                $html .= '
                   </div>
                </div> '; 
    
    
    
    
    
    echo $html;
    
    
}

function loadWomenCategories() {
    
    $slug = isset($_GET['service']) ? $_GET['service'] : "";
    
    $parentTaxId = 0;
    
    if (!empty($slug)) {
        
        $term = get_term_by('slug', $slug, 'krisalys-women-tax');
        $parentTaxId = $term->term_id;
    }
    
    $args = array('taxonomy' => 'krisalys-women-tax',
                  'hide_empty' => false,
                  'parent' => $parentTaxId
                 );

     $customPostTaxonomies = get_terms($args);
     
     $mainCategories = array_chunk($customPostTaxonomies, ceil(count($customPostTaxonomies) / 2));
    
    
    foreach ($mainCategories as $mainIndex => $categories) {
        
        
        $html1 .= '<div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">';
                        
                        foreach ($categories as $index => $value) {
                            
                             $categoryImage = "";
                                
                             $taxonomy_image_url = get_option('z_taxonomy_image'.$value->term_id);
                             
                             if(!empty($taxonomy_image_url)) {
                                $attachment_id = getAttachmentIdByUrl($taxonomy_image_url);
                                if(!empty($attachment_id)) {
                                    $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
                                    $taxonomy_image_url = $taxonomy_image_url[0];
                                    
                                    $categoryImage = $taxonomy_image_url;
                                }
                             }                            
                            
                            $html .= '<a href="?service='.$value->slug.'"><div class="dt-sc-icon-box type15  " data-delay="0">
                                        <div class="icon-wrapper">
                                            <img width="132" height="154" src="'.$categoryImage.'" onerror="this.src=\'http://instaglo.novatoresols.online/wp-content/uploads/2020/04/category-default.png\'" class="attachment-thumbnail" alt="">
                                            <h4>'.$value->name.'<br>
                                            Treatment</h4>
                                        </div>
                                        <div class="icon-content"></div>
                                      </div></a>';
                            
                        }
  
        
    }


    
    $html = '
    
  <div class="dtportfolio-container-wrapper dtportfolio-without-fullwidth-wrapper hiddenscrollbar">
   <div class="dtportfolio-container with-space dtportfolio-container-resetter" data-column="1" data-postperpage="-1" style="position: relative; height: 850.812px;">
      <div class="dtportfolio-grid-sizer     dtportfolio-one-column "></div>';
      
      foreach ($customPostTaxonomies as $index => $category) {
          
          
                             $categoryImage = "";
                                
                             $taxonomy_image_url = get_option('z_taxonomy_image'.$category->term_id);
                             
                             if(!empty($taxonomy_image_url)) {
                                $attachment_id = getAttachmentIdByUrl($taxonomy_image_url);
                                if(!empty($attachment_id)) {
                                    $taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
                                    $taxonomy_image_url = $taxonomy_image_url[0];
                                    
                                    $categoryImage = $taxonomy_image_url;
                                }
                             }           
      
                          $html .= '<div class="default with-space  dtportfolio-item dtportfolio-column dtportfolio-one-column with-sidebar dtportfolio-hover-modern-title first">
                                     <figure>
                                        <img src="'.$categoryImage.'" alt="'.$category->name.'" title="'.$category->name.'" onerror="this.src=\'http://instaglo.novatoresols.online/wp-content/uploads/2020/04/category-default.png\'">
                                        <div class="dtportfolio-image-overlay ">
                                           <div class="links">
                                              <a title="'.$category->name.'" href="'.home_url().'/krisalys-women/?service='.$category->slug.'"> <span class="fas fa-link"> </span> </a>
                                              <a title="'.$category->name.'" data-gal="prettyPhoto[dtgallery]" href="'.$categoryImage.'"> <span class="fas fa-search"> </span> </a>
                                           </div>
                                           <div class="dtportfolio-image-overlay-details">
                                              <h2><a href="'.home_url().'/krisalys-women/?service='.$category->slug.'" title="'.$category->name.'">'.$category->name.'</a></h2>
                                              <p class="categories"><a href="'.home_url().'/krisalys-women/?service='.$category->slug.'" rel="tag">Treatment</p>
                                           </div>
                                        </div>
                                     </figure>
                                  </div>';
                      
                      }
                $html .= '
                   </div>
                </div> '; 
    
    
    
    
    
    echo $html;
    
    
}

function load_homepage_main_services() {
    
    
    $mainCategories = get_option("krisalys_categories");
    
    //if (is_array($mainCategories)) {
        
        $mainCategories = array_chunk($mainCategories, ceil(count($mainCategories) / 2));
        
        
        ob_start();
        
        $counter = 1;
        ?>
        <style>
            .dt-sc-image-caption img:hover {
                transform: scale(1.4);
            }
            .dt-sc-image-caption img {
              transition: all 1.3s;
            }
            
        </style>
        <?php

        foreach ($mainCategories as $index => $category) {
            
            ?>
            
                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                    
                <?php foreach ($category as $catindex => $catvalue ) {
                    
                    if ($catvalue['name'] == 'krisalys-klatch') {
                        
                        $catvalueName = 'krisalysklatch';
                        
                    } else {
                        
                        $catvalueName = $catvalue['name'];
                        
                        
                    }
                
                    if($catvalue['name'] == 'krisalys-women') {
                        
                           $womenOptions = get_option('krisalys_women_options');
                           $image = $womenOptions['image'];
                       
                    } else if ($catvalue['name'] == 'krisalys-men') {
                        
                           $menOptions = get_option('krisalys_men_options');
                           $image = $menOptions['image'];
                        
                    } else if ($catvalue['name'] == 'aesthetics') {
                        
                           $aestheticsOptions = get_option('krisalys_aesthetics_options');
                           $image = $aestheticsOptions['image'];
                        
                    } else if ($catvalue['name'] == 'cosmetic-surgery') {
                        
                           $csOptions = get_option('krisalys_cosmetic_surgery_options');
                           $image = $csOptions['image'];
                        
                    } else if ($catvalue['name'] == 'krisalys-klatch') {
                        
                           
                           $image = "http://instaglo.novatoresols.online/wp-content/uploads/2020/04/lodi-the-garden-restaurant-in-lodhi-colony-201802-1518003702.jpg";
                    } else {
                           $dentalOptions = get_option('krisalys_dental_options');
                           $image = $dentalOptions['image'];
                    }
                        
                ?>
                
                <div class="wpb_column vc_column_container vc_col-sm-4">
                    
                    <div class="vc_column-inner ">
                        <a href="<?php echo home_url(); ?>/<?php echo $catvalueName ?>/" target=" _blank" title="<?php echo $catvalue['label'] ?>">
                        <div class="wpb_wrapper">
                            <div class="dt-sc-image-caption type3  " data-delay="0">
                               <div class="dt-sc-image-wrapper"><img style="width:auto" src="<?php echo $image ?>" class="attachment-full" alt="<?php echo $catvalue['label'] ?>" srcset="<?php echo $image ?> 450w, <?php echo $image ?> 238w" sizes="(max-width: 450px) 100vw, 450px"></div>
                                <div class="dt-sc-image-content">
                                    <div class="dt-sc-image-title">
                                        <h3><?php echo ucfirst($catvalue['label']) ?></h3>
                                    </div>
                                    <p>Nunc eget feugiat ex. Aliquam commodo dapibus. Duis sed venenatis est. Vestibulum tem ipsum ut molestie imperdiet.</p>
                                    
                                </div>
                            </div>
                            <div id="1587022811374-119ee39b-d641" class="dt-sc-empty-space" style="margin-bottom:65px;"></div>
                        </div>
                        </a>
                    </div>
                    
                </div>
          
                <?php $counter++; } ?>
            
            
            </div>
           <?php 
            
        }
        
     $html = ob_get_clean();
     
     return $html;
    //}
}

function load_service_content() {
    
    
    $servicesHTML = '';
    
    $baseURL = home_url();
    
     $args = array(
                'post_type' => 'dt_procedures',
                'posts_per_page' => -1,
                'order' => $sort_by,
                'post_status' => 'publish',
                'ignore_sticky_posts' => 1,
                );
                
    $procedures = get_posts($args);
    
    
    $servicesHTML = '<div style="margin:80px;" class="dt-sc-empty-space"></div>
<div class="vc_row wpb_row vc_inner vc_row-fluid">
    <div class="wpb_column vc_column_container vc_col-sm-6">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="dt-sc-image-caption type4  " data-delay="0">
                    <div class="dt-sc-image-wrapper"><img width="600" height="270" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/service-img1.jpg" class="attachment-full" alt="" srcset="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/service-img1.jpg 600w, http://instaglo.novatoresols.online/wp-content/uploads/2019/06/service-img1-300x135.jpg 300w, http://instaglo.novatoresols.online/wp-content/uploads/2019/06/service-img1-540x243.jpg 540w" sizes="(max-width: 600px) 100vw, 600px"></div>
                    <div class="dt-sc-image-content">
                        <div class="dt-sc-image-title">
                            <h3>
                        <a href="'.$baseURL.'/procedure/wedding-valima-makeover-expert/" target=" _blank" title="Wedding Makeover Expert">Wedding Makeover Expert</a></h3></div>
                        <p>Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum…</p>
                        <p><a href="'.$baseURL.'/procedure/wedding-valima-makeover-expert/" target=" _blank" title="Cosmetic" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> View Services <span class="fa fa-chevron-right"> </span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-6">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="dt-sc-image-caption type4  " data-delay="0">
                    <div class="dt-sc-image-wrapper"><img width="600" height="270" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-2.jpg" class="attachment-full" alt="" srcset="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-2.jpg 600w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-2-300x135.jpg 300w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-2-540x243.jpg 540w" sizes="(max-width: 600px) 100vw, 600px"></div>
                    <div class="dt-sc-image-content">
                        <div class="dt-sc-image-title">
                            <h3><a href="'.$baseURL.'/procedure/wedding-valima-makeover-star/" target=" _blank" title="Wedding Makeover Star">Wedding Makeover Star</a></h3></div>
                        <p>Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum…</p>
                        <p><a href="'.$baseURL.'/procedure/wedding-valima-makeover-star/" target=" _blank" title="Cosmetic" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> View Services <span class="fa fa-chevron-right"> </span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="margin:80px;" class="dt-sc-empty-space"></div>
<div class="vc_row wpb_row vc_inner vc_row-fluid">
    <div class="wpb_column vc_column_container vc_col-sm-6">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="dt-sc-image-caption type4  " data-delay="0">
                    <div class="dt-sc-image-wrapper"><img width="600" height="270" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-3.jpg" class="attachment-full" alt="" srcset="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-3.jpg 600w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-3-300x135.jpg 300w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-3-540x243.jpg 540w" sizes="(max-width: 600px) 100vw, 600px"></div>
                    <div class="dt-sc-image-content">
                        <div class="dt-sc-image-title">
                            <h3><a href="'.$baseURL.'/procedure/spa-silver/" target=" _blank" title="SPA Silver">SPA Silver</a></h3></div>
                        <p>Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum…</p>
                        <p><a href="'.$baseURL.'/procedure/spa-silver/" target=" _blank" title="Cosmetic" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> View Services <span class="fa fa-chevron-right"> </span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="wpb_column vc_column_container vc_col-sm-6">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div class="dt-sc-image-caption type4  " data-delay="0">
                    <div class="dt-sc-image-wrapper"><img width="600" height="270" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-5.jpg" class="attachment-full" alt="" srcset="http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-5.jpg 600w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-5-300x135.jpg 300w, http://instaglo.novatoresols.online/wp-content/uploads/2019/07/service-5-540x243.jpg 540w" sizes="(max-width: 600px) 100vw, 600px"></div>
                    <div class="dt-sc-image-content">
                        <div class="dt-sc-image-title">
                            <h3><a href="'.$baseURL.'/procedure/spa-gold/" target=" _blank" title="SPA Gold">SPA Gold</a></h3></div>
                        <p>Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum…</p>
                        <p><a href="'.$baseURL.'/procedure/spa-gold/" target=" _blank" title="Cosmetic" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> View Services <span class="fa fa-chevron-right"> </span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div style="margin:80px;" class="dt-sc-empty-space"></div>
</div>';

    
    
    echo $servicesHTML;
}

function load_additional_fields() {
    
    $service = isset($_POST['service']) ?$_POST['service'] : "";
    
    ?>
    <script>
        
        var serviceOptionSelected = '<?php echo $service; ?>';
        jQuery(document).ready(function() {
            
             if (serviceOptionSelected != '') {
                jQuery('select[name="service"]').find('option[value="<?php echo $service; ?>"]').attr("selected",true).change();
             }           
        });
        

    </script>
    <?
    
}

function signinuser() {

	$userEmail = isset($_POST['userEmail']) ? $_POST['userEmail'] : "";
	$userPass = isset($_POST['userPass']) ? $_POST['userPass'] : "";
	$uid = isset($_POST['uid']) ? $_POST['uid'] : "";

	$autologin_user = wp_signon(array(    'user_login'    => $userEmail,
						'user_password' => $userPass,
						'remember'      => true
					));
					
					
    if ( is_wp_error( $autologin_user ) ) {
        
        $userObject = get_user_by("email",$userEmail);
        
        if (!$userObject) {
            
    		$user_id = wp_create_user( $userEmail, $userPass, $userEmail );
    	  
    		wp_update_user(
    		  array(
    			'ID'          =>    $user_id,
    			'nickname'    =>    $userEmail,
    			'uid'		  =>    $uid
    		  )
    		);
    		
    		update_user_meta($user_id,"uid",$uid);
           
        }
        
    } else {
        
        if (!empty($uid)) {
    	    update_user_meta($user->ID,"uid",$uid);
    	}

    }
	


	return true;
}

function is_user_loggedIn() {

	if ( is_user_logged_in() ) {
		echo "true";
	} else {
		echo "false";
	}
exit;
}

function logoutuser() {
    
    wp_logout();
}

function autologgedin( $user_id ) {
    wp_set_current_user( $user_id );
    wp_set_auth_cookie( $user_id );
     
}


add_filter('pre_get_posts', 'query_post_type');
function query_post_type($query) {
    if((is_category() || is_tag()) && !is_admin()) {
        $query->set('post_type',array('krisalys-women','post'));
        return $query;
    }
}


function create_guest_customer() {

	$customerName = isset($_POST['customerName']) ? $_POST['customerName'] : "";
	$customerEmail = isset($_POST['customerEmail']) ? $_POST['customerEmail'] : "";
	$customerPass = isset($_POST['customerPass']) ? $_POST['customerPass'] : "";
	$uid = isset($_POST['uid']) ? $_POST['uid'] : "";
	$response = "";

	if( null == username_exists( $customerEmail ) ) {

		$user_id = wp_create_user( $customerEmail, $customerPass, $customerEmail );
	  
		wp_update_user(
		  array(
			'ID'          =>    $user_id,
			'nickname'    =>    $customerEmail,
			'uid'		  =>    $uid
		  )
		);
		
		update_user_meta($user_id,"uid",$uid);
	  
		$user = new WP_User( $user_id );

		$user->set_role( 'customer' );

        $curl = curl_init();
        
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/thirdparties",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => array('email' => $customerEmail,'name' => $customerName, 'code_client' => $uid,'client' => '1'),
		  CURLOPT_HTTPHEADER => array(
			"DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
	
		  ),
		));

		$response = curl_exec($curl);

        curl_close($curl);
		

	  
	  }
	

	  return true;


}

function create_phone_customer() {

	$firstName = isset($_POST['name']) ? $_POST['name'] : "";
	$customerEmail = isset($_POST['customerEmail']) ? $_POST['customerEmail'] : "";
	$customerPass = isset($_POST['customerPass']) ? $_POST['customerPass'] : "";
	$phoneNumber = isset($_POST['phoneNumber']) ? $_POST['phoneNumber'] : "";
	$uid = isset($_POST['uid']) ? $_POST['uid'] : "";
	
	$phoneNumber = str_replace("+","phone",$phoneNumber);
	
	$fullName = $firstName;
	
	$response = "";

	if( null == username_exists( $uid ) ) {

		$user_id = wp_create_user( $uid, $customerPass );
	  
		wp_update_user(
		  array(
			'ID'          =>    $user_id,
			'nickname'    =>    $phoneNumber,
			'uid'		  =>    $uid
		  )
		);
		
		update_user_meta($user_id,"uid",$uid);
		update_user_meta($user_id,"user_nicename",$phoneNumber);
		update_user_meta($user_id,"first_name",$firstName);
		update_user_meta($user_id,"last_name",$lastName);
		
		
	  
		$user = new WP_User( $user_id );

		$user->set_role( 'customer' );

        $curl = curl_init();
        
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/thirdparties",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => array('email' => $customerEmail,'name' => $phoneNumber, 'code_client' => $uid,'client' => '1'),
		  CURLOPT_HTTPHEADER => array(
			"DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
	
		  ),
		));

		$response = curl_exec($curl);

        curl_close($curl);
        
        
		$autologin_user = wp_signon(array(  'user_login'    => $uid,
											'user_password' => $uid,
											'remember'      => true
										));        


	  
	  } else {
	     
	      
		$autologin_user = wp_signon(array(  'user_login'    => $uid,
											'user_password' => $uid,
											'remember'      => true
										));
										
		    
 
        if ( is_wp_error( $autologin_user ) ) {
            
        
            
            $userObject = get_user_by("login",$uid);
            
            if ($userObject) {
                wp_set_password($customerPass,$userObject->ID);
                update_user_meta($userObject->ID,"uid",$uid);
            }
            
            $autologin_user = wp_signon(array('user_login'    => $uid,
											'user_password' => $uid,
											'remember'      => true
										));
            


        }



	  }
	  

	  return true;
	  

}

function create_social_customer() {

	$firstName = isset($_POST['firstName']) ? $_POST['firstName'] : "";
	$lastName = isset($_POST['lastName']) ? $_POST['lastName'] : "";
	$customerEmail = isset($_POST['customerEmail']) ? $_POST['customerEmail'] : "";
	$customerPass = isset($_POST['customerPass']) ? $_POST['customerPass'] : "";
	$uid = isset($_POST['uid']) ? $_POST['uid'] : "";
	
	$fullName = $firstName;
	
	$response = "";

	if( null == username_exists( $uid ) ) {

		$user_id = wp_create_user( $uid, $customerPass );
	  
		wp_update_user(
		  array(
			'ID'          =>    $user_id,
			'nickname'    =>    $customerEmail,
			'uid'		  =>    $uid
		  )
		);
		
		update_user_meta($user_id,"uid",$uid);
		update_user_meta($user_id,"user_nicename",$fullName);
		update_user_meta($user_id,"first_name",$firstName);
		update_user_meta($user_id,"last_name",$lastName);
		
		
	  
		$user = new WP_User( $user_id );

		$user->set_role( 'customer' );

        $curl = curl_init();
        
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/thirdparties",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => array('email' => $customerEmail,'name' => $fullName, 'code_client' => $uid,'client' => '1'),
		  CURLOPT_HTTPHEADER => array(
			"DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
	
		  ),
		));

		$response = curl_exec($curl);

        curl_close($curl);
        
		$autologin_user = wp_signon(array(  'user_login'    => $uid,
											'user_password' => $uid,
											'remember'      => true
										));        


	  
	  } else {
	      
		$autologin_user = wp_signon(array(  'user_login'    => $uid,
											'user_password' => $uid,
											'remember'      => true
										));
										
		    
 
        if ( is_wp_error( $autologin_user ) ) {
            
        
            
            $userObject = get_user_by("login",$uid);
            
            if ($userObject) {
                wp_set_password($customerPass,$userObject->ID);
                update_user_meta($userObject->ID,"uid",$uid);
            }
            
            $autologin_user = wp_signon(array('user_login'    => $uid,
											'user_password' => $uid,
											'remember'      => true
										));
            


        }
        
     



	  }
	  

	  return true;
	  

}



function create_customer() {

	$firstName = isset($_POST['firstName']) ? $_POST['firstName'] : "";
	$lastName = isset($_POST['lastName']) ? $_POST['lastName'] : "";
	$customerEmail = isset($_POST['customerEmail']) ? $_POST['customerEmail'] : "";
	$customerPass = isset($_POST['customerPass']) ? $_POST['customerPass'] : "";
	$uid = isset($_POST['uid']) ? $_POST['uid'] : "";
	
	$fullName = $firstName." ".$lastName;
	
	$response = "";

	if( null == username_exists( $customerEmail ) ) {

		$user_id = wp_create_user( $customerEmail, $customerPass, $customerEmail );
	  
		wp_update_user(
		  array(
			'ID'          =>    $user_id,
			'nickname'    =>    $customerEmail,
			'uid'		  =>    $uid
		  )
		);
		
		update_user_meta($user_id,"uid",$uid);
		update_user_meta($user_id,"user_nicename",$fullName);
		update_user_meta($user_id,"first_name",$firstName);
		update_user_meta($user_id,"last_name",$lastName);
		
		
	  
		$user = new WP_User( $user_id );

		$user->set_role( 'customer' );

        $curl = curl_init();
        
		curl_setopt_array($curl, array(
		  CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/thirdparties",
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => "",
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => "POST",
		  CURLOPT_POSTFIELDS => array('email' => $customerEmail,'name' => $fullName, 'code_client' => $uid,'client' => '1'),
		  CURLOPT_HTTPHEADER => array(
			"DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
	
		  ),
		));

		$response = curl_exec($curl);

        curl_close($curl);
		

	  
	  } else {


		$autologin_user = wp_signon(array(  'user_login'    => $customerEmail,
											'user_password' => $customerPass,
											'remember'      => true
										));
										
		    
 
        if ( is_wp_error( $autologin_user ) ) {
            
            $userObject = get_user_by("email",$customerEmail);
            
            if ($userObject) {
                wp_set_password($customerPass,$userObject->ID);
                update_user_meta($userObject->ID,"uid",$uid);
            }
            
            $autologin_user = wp_signon(array(  'user_login'    => $customerEmail,
											'user_password' => $customerPass,
											'remember'      => true
										));
            

            /*$curl = curl_init();
            
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/thirdparties/byEmail/$customerEmail",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "GET",
              CURLOPT_HTTPHEADER => array(
                "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
              ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
            
            $response = json_decode($response);
            
            $erpuserid = $response->ref;
            
            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/thirdparties/$erpuserid",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "PUT",
              CURLOPT_POSTFIELDS =>array('code_client' => $uid),
              CURLOPT_HTTPHEADER => array(
                "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
              ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);  */


        }



	  }
	  

	  return true;
	  

}


function show_firebase_authentication_html( ){
	
	ob_start();
	
	$current_user = wp_get_current_user();

	$userEmail = isset($current_user->user_email) ? $current_user->user_email : "";
	$userName = isset($current_user->display_name) ? $current_user->display_name : "";

    ?>
  
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"> 
        <link href="https://fonts.googleapis.com/css?family=Nunito:400,600,700" rel="stylesheet">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script>
			var appointment_url = '<?php echo home_url(); ?>/appointment-ii/'; 
			var userEmail = '<?php echo $userEmail; ?>';
			var userName = '<?php echo $userName; ?>';
        </script>
        
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/firebase/style.css" />
        
            <div data-delay="0" class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-12" data-delay="0">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div id="1561377235313-2ab8d1db-6000" class="dt-sc-empty-space"></div>
                            <h2 style="text-align: center" class="vc_custom_heading">Login Form</h2>
                            <p style="text-align: center" class="vc_custom_heading">We encourage your feedback, please feel free to send us a message
                                <br> using the following contact form.</p>
                                        <div class="authentication-login-form">
            <div class="dt-sc-newsletter-section type5" style="width:100%; border:4px solid; border-color:rgba(0, 0, 0, 0.2);">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="authenticationModalLabel">The Krisalys - Login or create account</h5>
                  
              </div>
              
              
            <div class="modal-body">
                <div id="login_div" class="main-div">
                  <div class="column dt-sc-one-half first"><input type="text" name="firstName" placeholder="First Name..." id="first_name_field" /></div>
                  <div class="column dt-sc-one-half"><input type="text" name="lastName" placeholder="Last Name..." id="last_name_field" /></div>
                  <div class="column dt-sc-one-half first"><input type="email" name="email" placeholder="Email..." id="email_field" /></div>
                  <div class="column dt-sc-one-half"><input type="password" placeholder="Password..." id="password_field" /></div>
                  <button onclick="signup()">Create Account</button>
                </div>
              
                <div id="user_div" class="loggedin-div">
                  
                  <input type="email" name="email" placeholder="Email..." id="login_email" />
                  <input type="password" placeholder="Password..." id="login_password" />
                  <button onclick="login()">Login</button>
                </div>
        
                <div id="loggedin-user-area" >
                  <p id="user_para" class="alert alert-success">Great! you are logged in successfully. </p>
                  
                  <button onclick="logout()">Logout</button>
                </div>
              <div class="footer-buttons">
            
                <button type="button" class="login-btn">Login</button>
                <button type="button" class="create-account-btn">Create Account</button>
            </div>
             </div>
             </div>
             </div>
             <div class="dt-sc-diamond-separator aligncenter"></div>
             <ul id="dt-1552298357031-9fdffc28-b367" data-delay="0" class="dt-sc-sociable large" data-default-style="none" data-default-border-radius="no" data-default-shape="" data-hover-style="none" data-hover-border-radius="no" data-hover-shape="">
                 <li class="facebook">  
                    <a style="cursor:pointer;" title="" target="_self" onclick = "facebookSignin()">      
                        <span class="dt-icon-default"> <span></span> </span>      <i></i>      <span class="dt-icon-hover"> <span></span> </span>  
                    </a>
                </li>
                <li class="google-plus">  
                    <a style="cursor:pointer;" title="" target="_self" onclick = "googleSignin()">      
                        <span class="dt-icon-default"> <span></span> </span>      <i></i>      <span class="dt-icon-hover"> <span></span> </span>  
                    </a>
                </li>
                
                <li class="twitter">  
                    <a style="cursor:pointer;" title=""  target="_self" onclick = "twitterSignin()" >      
                        <span class="dt-icon-default"> <span></span> </span>      <i></i>      <span class="dt-icon-hover"> <span></span> </span>  
                    </a>
                </li>

                
            </ul>
            <p onclick="phoneSignin()" style="cursor:pointer" > Login with phone</p>
             </div>
             
             
         </div>
                            
                       
                            <div id="1561377336795-d803f64d-9725" class="dt-sc-empty-space"></div>
                        </div>
                    </div>
                </div>
            
            </div>        
    
         
    
 


<!-- Button trigger modal -->
<button id="loadphonedialog" style="display:none;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#phoneModel"></button>

<!-- Modal -->
<div class="modal fade" id="phoneModel" tabindex="-1" role="dialog" aria-labelledby="phoneModelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="phoneModelLabel">Phone Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="step1">
            <form>
                <input type="text" id="number" placeholder="+923********">
                <div id="recaptcha-container"></div>
                <button class="sendcode" type="button" onclick="phoneAuth();">SendCode</button>
            </form>
        </div>
        <div class="step2" style="display:none;">
            <form>
                <input type="text" id="verificationCode" placeholder="Enter verification code">
                <button class="verify" type="button" onclick="codeverify();">Verify code</button>
            
            </form>  
        </div>
      </div>

    </div>
  </div>
</div>
         
         
        
        <script src="<?php echo get_template_directory_uri(); ?>/js/firebase/index.js"></script>
        


    <?php
    $content = ob_get_clean();
	
	return $content;
	
	
}

//add_action('init','create_main_posts_type');
//add_action('init','load_content_form_erp');

add_action('init','loadPostTypes');
add_action('init','registerPromotions');

function registerPromotions() {

        $label = "Promotions";
        $labelPost = "promotions";

        $labels = array(
            'name'               => $label,
            'singular_name'      => $label,
            'menu_name'          => $label,
            'name_admin_bar'     => $label,
            'add_new'            => 'Add New',
            'add_new_item'       => 'Add New '.$label,
            'new_item'           => 'New '.$label,
            'edit_item'          => 'Edit '.$label,
            'view_item'          => 'View '.$label,
            'all_items'          => 'All '.$label,
            'search_items'       => 'Search '.$label,
            'not_found'          => 'No '.$label.' Found',
            'not_found_in_trash' => 'No '.$label.' Found in Trash'
          );
        
          $args = array(
            'labels'              => $labels,
            'public'              => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'show_ui'             => true,
            'show_in_nav_menus'   => true,
            'show_in_menu'        => true,
            'has_category'        => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-clipboard',
            'capability_type'     => 'post',
            'hierarchical'        => false,
            'supports'            => array('thumbnail','title', ),
            'has_archive'         => true,
            'rewrite'             => array( 'slug' => $labelPost ),
            'query_var'           => true
          );
        
          register_post_type( $labelPost, $args );
          
          register_taxonomy("$labelPost-tax", array($labelPost), array("hierarchical" => true, "label" => "Categories", "singular_label" => "Category", "rewrite" => array( 'slug' => $labelPost, 'with_front'=> true )));
             
    
}

function loadPostTypes() {
    
    $mainCategories = get_option("krisalys_categories");
    
    foreach ($mainCategories as $index => $category) {
        
                    $label = $category['label'];
                    $labelPost = $category['name'];
    
                    $labels = array(
                        'name'               => $label,
                        'singular_name'      => $label,
                        'menu_name'          => $label,
                        'name_admin_bar'     => $label,
                        'add_new'            => 'Add New',
                        'add_new_item'       => 'Add New '.$label,
                        'new_item'           => 'New '.$label,
                        'edit_item'          => 'Edit '.$label,
                        'view_item'          => 'View '.$label,
                        'all_items'          => 'All '.$label,
                        'search_items'       => 'Search '.$label,
                        'not_found'          => 'No '.$label.' Found',
                        'not_found_in_trash' => 'No '.$label.' Found in Trash'
                      );
                    
                      $args = array(
                        'labels'              => $labels,
                        'public'              => true,
                        'exclude_from_search' => false,
                        'publicly_queryable'  => true,
                        'show_ui'             => true,
                        'show_in_nav_menus'   => true,
                        'show_in_menu'        => true,
                        'has_category'        => true,
                        'show_in_admin_bar'   => true,
                        'menu_position'       => 5,
                        'menu_icon'           => 'dashicons-clipboard',
                        'capability_type'     => 'post',
                        'capabilities'        => array(
                                    'publish_posts' => 'publish_'.$labelPost,
                                    'edit_posts' => 'edit_'.$labelPost,
                                    'edit_others_posts' => 'edit_others_'.$labelPost,
                                    'delete_posts' => 'delete_'.$labelPost,
                                    'delete_others_posts' => 'delete_others_'.$labelPost,
                                    'read_private_posts' => 'read_private_'.$labelPost,
                                    'edit_post' => 'edit_'.$labelPost,
                                    'delete_post' => 'delete_'.$labelPost,
                                    'read_post' => 'read_'.$labelPost
                                ),
                        'hierarchical'        => false,
                        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields' ),
                        'has_archive'         => true,
                        'rewrite'             => array( 'slug' => $labelPost ),
                        'query_var'           => true
                      );
                    
                      register_post_type( $labelPost, $args );
                      
                      register_taxonomy("$labelPost-tax", array($labelPost), array("hierarchical" => true, "label" => "Categories", "singular_label" => "Category", "rewrite" => array( 'slug' => $labelPost, 'with_front'=> true )));
                     
    }
    
    
    
}

function load_content_form_erp() {
    

        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/categories/servicesproductsundercategories",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
          ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        
        global $wpdb;
        
        $tableLocations = $wpdb->prefix."ea_locations";          
        
        $response = json_decode($response);
        
        //var_dump("<pre>",$response->data); exit;
        
        $mainCategories = array();
        
        foreach ($response->data as $index => $value) {
            
             $id = $value->id;
             $fkParent = $value->fk_parent;
             $title = $value->title;
             $image = $value->image;
             $children = $value->children;
             $childObjects = $value->childObjects;
             
             
             if ($fkParent == "0") {
                 
                    $label = ucwords(strtolower($value->title));
                    $labelPost = str_replace(" ","-",strtolower($value->title));
                    
                    $mainCategories[] = array("name" => $labelPost, "id" => $value->id, "label" => $label, "description" => $value->description);
                    
                    
                      $labels = array(
                        'name'               => $label,
                        'singular_name'      => $label,
                        'menu_name'          => $label,
                        'name_admin_bar'     => $label,
                        'add_new'            => 'Add New',
                        'add_new_item'       => 'Add New '.$label,
                        'new_item'           => 'New '.$label,
                        'edit_item'          => 'Edit '.$label,
                        'view_item'          => 'View '.$label,
                        'all_items'          => 'All '.$label,
                        'search_items'       => 'Search '.$label,
                        'not_found'          => 'No '.$label.' Found',
                        'not_found_in_trash' => 'No '.$label.' Found in Trash'
                      );
                    
                      $args = array(
                        'labels'              => $labels,
                        'public'              => true,
                        'exclude_from_search' => false,
                        'publicly_queryable'  => true,
                        'show_ui'             => true,
                        'show_in_nav_menus'   => true,
                        'show_in_menu'        => true,
                        'show_in_admin_bar'   => true,
                        'menu_position'       => 5,
                        'menu_icon'           => 'dashicons-clipboard',
                        'capability_type'     => 'post',
                        'hierarchical'        => false,
                        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','custom-fields' ),
                        'has_archive'         => true,
                        'rewrite'             => array( 'slug' => $labelPost ),
                        'query_var'           => true
                      );
                    
                      register_post_type( $labelPost, $args );
                      
                      register_taxonomy("$labelPost-tax", array($labelPost), array("hierarchical" => true, "label" => "Categories", "singular_label" => "Category", "rewrite" => array( 'slug' => $labelPost, 'with_front'=> true )));
                     
                      if (is_array($children) && count($children) > 0) {
                          
                          //addChildren($labelPost,$children);
                      }
                      
                      if (!empty($childObjects)) {
                          
                         //addServicesAndProducts($labelPost,$childObjects);
                          
                      }
                            
                        $result = isLocationAlreadyExist($label);
                        
                        if (!$result) {
                            
                            $data = array("name" => $label,
                                          "address" => "The Krisalys",
                                          "location" => "Karachi"
                                        );
                            
                            
                            $error = $wpdb->insert($tableLocations,$data);
                            
                        
                        } else {
                            
                                $label = isset($label) ? $label : $result->name;
                                $address = isset($address) ? $address : $result->address;
                                $location = isset($location) ? $location : $result->location;
                             
                                
                                $data = array("name" => $label,
                                              "address"=>$address,
                                              "location" => $location
                                            );
                                    
                                $wpdb->update($tableLocations, $data, array('name'=>$label));
                            
                        }                          
                                         
             }
                 
             }
             
             update_option("krisalys_categories",$mainCategories);
            
}

function addChildren($labelPost, $children, $args = array(), $cat = '') {
    
    foreach ($children as $index => $child) {
        
        $resultTerm = get_term_by('name', $child->title, "$labelPost-tax");
        
        if (!$resultTerm) {
            $termObject = wp_insert_term( $child->title, "$labelPost-tax", $args);
            $termParentId = $termObject->term_id;
        } else {
            
            $termParentId = $resultTerm->term_id;
        } 
        
        $childrenArray = $child->children;
        
        $childObjects = $child->childObjects;
        
        if (!empty($childObjects)) {
            
                          
            //addServicesAndProducts($labelPost,$childObjects,$termParentId);
                          
        }        
 
        
        if (is_array($childrenArray) && count($childrenArray) > 0) {
            
            if ( is_wp_error( $termObject ) ) { 
                
                $result = get_term_by('name', $child->title, "$labelPost-tax");
                
                $termParentId = $result->term_id;
            
                
            } else {
                
                $termParentId = $termObject['term_id'];
            }
          
            //addChildren($labelPost,$childrenArray, array("parent" => $termParentId), $termParentId);
        
            
        }
        
        

    }

    
}

function addServicesAndProducts($labelPost,$childObjects,$category = '') {
    

    global $wpdb;
    
    foreach ($childObjects as $index => $value) {
        
            $erpId = $value->id;
            $erpLabel = $value->label;
            $erpDescription = $value->description;
            $erpPrice = $value->price;
            $erpType = $value->type;
            $erpImage = $value->image;
            
            
            $postObject = array(
              'post_title'    => wp_strip_all_tags( $erpLabel ),
              'post_content'  => $erpDescription,
              'post_status'   => 'publish',
              'post_type'     => $labelPost,
              'post_author'   => 1,
              'post_category' => $category
            );
            
            $tablename = $wpdb->prefix."posts";
            $wpTitle = wp_strip_all_tags( $erpLabel );
            
            $query = $wpdb->prepare(
                "SELECT ID FROM " . $tablename . "
                WHERE post_title = %s
                AND post_type = '".$labelPost."';",
                $wpTitle
            );
           
            $wpdb->query( $query );
            
            if ( $wpdb->num_rows ) { 
                
                $WPPostId = $wpdb->get_var( $query );
                
                update_post_meta($WPPostId,"id",$value->id);
                update_post_meta($WPPostId,"label",$value->label);
                update_post_meta($WPPostId,"description",$value->description);
                update_post_meta($WPPostId,"price",$value->price);
                update_post_meta($WPPostId,"type",$value->type);
                update_post_meta($WPPostId,"image",$value->image);
                update_post_meta($WPPostId,"term",$category);                   
                
                
            } else {
                
                $postId = wp_insert_post( $postObject );
                
                update_post_meta($postId,"id",$value->id);
                update_post_meta($postId,"label",$value->label);
                update_post_meta($postId,"description",$value->description);
                update_post_meta($postId,"price",$value->price);
                update_post_meta($postId,"type",$value->type);
                update_post_meta($postId,"image",$value->image);
                update_post_meta($postId,"term",$category);                
            }
            
            
            //wp_set_object_terms( $postId, $category, "$labelPost-tax" );
            //wp_set_post_categories($postId, $category);
     
            wp_set_post_terms( $postId, $category, "$labelPost-tax", 'true' );
            
            
    }
        
}
    



/*

function create_main_posts_type() {
    
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/categories?sortfield=t.rowid&sortorder=ASC&api_key=ZudEY99Q6GAE81phshBM3mMsqD0y17d1",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
          ),
        ));
        
        $response = curl_exec($curl);
        
        $response = json_decode($response);
        
        curl_close($curl);
        
        global $wpdb;
        
        $tableLocations = $wpdb->prefix."ea_locations";        
        
        if (!empty($response->data) && is_array($response->data)) {
            
            $mainCategories = array();
            
            foreach ($response->data as $index => $category) {
                
                
            
                if ($category->fk_parent == "0") {
                    
                    
                    
                    $label = ucwords(strtolower($category->label));
                    $labelPost = str_replace(" ","-",strtolower($category->label));
                    
                    $mainCategories[] = array("name" => $labelPost, "id" => $category->id, "label" => $label, "description" => $category->description);
                    
                    
                      $labels = array(
                        'name'               => $label,
                        'singular_name'      => $label,
                        'menu_name'          => $label,
                        'name_admin_bar'     => $label,
                        'add_new'            => 'Add New',
                        'add_new_item'       => 'Add New '.$label,
                        'new_item'           => 'New '.$label,
                        'edit_item'          => 'Edit '.$label,
                        'view_item'          => 'View '.$label,
                        'all_items'          => 'All '.$label,
                        'search_items'       => 'Search '.$label,
                        'not_found'          => 'No '.$label.' Found',
                        'not_found_in_trash' => 'No '.$label.' Found in Trash'
                      );
                    
                      $args = array(
                        'labels'              => $labels,
                        'public'              => true,
                        'exclude_from_search' => false,
                        'publicly_queryable'  => true,
                        'show_ui'             => true,
                        'show_in_nav_menus'   => true,
                        'show_in_menu'        => true,
                        'show_in_admin_bar'   => true,
                        'menu_position'       => 5,
                        'menu_icon'           => 'dashicons-clipboard',
                        'capability_type'     => 'post',
                        'hierarchical'        => false,
                        'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),
                        'has_archive'         => true,
                        'rewrite'             => array( 'slug' => $labelPost ),
                        'query_var'           => true
                      );
                    
                      register_post_type( $labelPost, $args );
                      
                      register_taxonomy("$labelPost-tax", array($labelPost), array("hierarchical" => true, "label" => "Categories", "singular_label" => "Category", "rewrite" => array( 'slug' => $labelPost, 'with_front'=> true )));
                     
                          
                            
                        $result = isLocationAlreadyExist($label);
                        
                        if (!$result) {
                            
                            $data = array("name" => $label,
                                          "address" => "The Krisalys",
                                          "location" => "Karachi"
                                        );
                            
                            
                            $error = $wpdb->insert($tableLocations,$data);
                            
                        
                        } else {
                            
                                $label = isset($label) ? $label : $result->name;
                                $address = isset($address) ? $address : $result->address;
                                $location = isset($location) ? $location : $result->location;
                             
                                
                                $data = array("name" => $label,
                                              "address"=>$address,
                                              "location" => $location
                                            );
                                    
                                $wpdb->update($tableLocations, $data, array('name'=>$label));
                            
                        }                          
                        
                        
                      
                    
                }
            }
            
            update_option("krisalys_categories",$mainCategories);
        }

}*/

function isLocationAlreadyExist($name) {
    
    global $wpdb;
    
    $tableLocations = $wpdb->prefix."ea_locations";  
    
    $result = $wpdb->get_results("SELECT * FROM $tableLocations WHERE lower(`name`) = '$name';");
    
    if (sizeof($result) > 0) {
        
        return $result;
   
    } else {
        
        return false;
    }

}


function getLocationByServiceId($serviceId) {
    
    global $wpdb;

    $tableLocations = $wpdb->prefix."ea_connections";  
    
    $result = $wpdb->get_results("SELECT location FROM $tableLocations WHERE service = '$serviceId';");
    
    if (sizeof($result) > 0) {
        
        return $result[0]->location;
   
    } else {
        
        return "";
    }

}


add_action('woocommerce_thankyou', 'createCustomerInvoiceInERP', 10, 1);


function createCustomerInvoiceInERP( $order_id ) {

    if ( ! $order_id )
        return;

    $orderDetails = wc_get_order($order_id);
    
    $billing_email = $orderDetails->get_billing_email();
    $billing_last_name = $orderDetails->get_billing_last_name();
    $billing_first_name = $orderDetails->get_billing_first_name();
    $billing_phone = $orderDetails->get_billing_phone(); 
    
    
    $notePrivate = "Product Details : Name - $billing_first_name $billing_last_name, Email - $billing_email, Phone - $billing_phone";
    
    $curl = curl_init();
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/thirdparties/byEmail/$billing_email",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
      ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
   
    $response = json_decode($response);
    
    $erpCustomerId = $response->data->id; 
    
    $curl = curl_init();
    
    curl_setopt_array($curl, array(
      CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/invoices",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => array('socid' => $erpCustomerId, 'name' => 'Krisalys Product','note_public' => $notePrivate,'note_private' => $notePrivate,'module_source' => 'Website','type' => '0'),
      CURLOPT_HTTPHEADER => array(
        "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
      ),
    ));
    
    $response = curl_exec($curl);
    
    curl_close($curl);
    
    $response = json_decode($response);
    
    $invoiceId = $response->data->invoice_id;  


    if($orderDetails->is_paid())
        $paid = 'yes';
    else
        $paid = 'no';

    
    foreach ( $orderDetails->get_items() as $item_id => $item ) {

        if( $item['variation_id'] > 0 ){
            $product_id = $item['variation_id']; // variable product
        } else {
            $product_id = $item['product_id']; // simple product
        }
    
        $product = $item->get_product();
        
        $product_name = $product->get_name(); 
        $item_quantity = $item->get_quantity(); 
        $item_total = $item->get_total(); 
        
        $ERPProductId = get_post_meta( $product_id, 'productid', true );
        
         $curl = curl_init();
            
            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/invoices/$invoiceId/lines",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => array('desc' => '', 'subprice' => $item_total,'qty' => $item_quantity,'tva_tx' => "0.000" , 'localtax1_tx'=> "0.000",'localtax2_tx' => "0.000","fk_product"=>$ERPProductId, "remise_percent"=>"0","product_type"=>"0","label" => $product_name),
              CURLOPT_HTTPHEADER => array(
                "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
              ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);
        
        $productPrice = $product->get_price();

    }
    
    if ($paid == 'yes') {
        
        $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/invoices/$invoiceId/settopaid",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => array('id' => $invoiceId),
              CURLOPT_HTTPHEADER => array(
                "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
              ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl); 
        
    } else {
        
        $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://thekrisalysadminarea.novatoresols.online/htdocs/api/index.php/invoices/$invoiceId/settounpaid",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => true,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => array('id' => $invoiceId),
              CURLOPT_HTTPHEADER => array(
                "DOLAPIKEY: ZudEY99Q6GAE81phshBM3mMsqD0y17d1"
              ),
            ));
            
            $response = curl_exec($curl);
            
            curl_close($curl);         
        
    }

   
}

/********************/
/**
 * This snippet has been updated to reflect the official supporting of options pages by CMB2
 * in version 2.2.5.
 *
 * If you are using the old version of the options-page registration,
 * it is recommended you swtich to this method.
 */
add_action( 'cmb2_admin_init', 'myprefix_register_theme_options_metabox' );
add_action( 'cmb2_admin_init', 'addPromotionsFields' );

function addPromotionsFields() {
    
    $promoPages = new_cmb2_box( array(
		'id'            => 'promotionsDetails',
		'title'         => __( 'Promotions Details', 'cmb2' ),
		'object_types'  => array( 'promotions' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	
    $promoPages->add_field( array(
        'name'       => __( 'Heading 1', 'cmb2' ),
        'desc'       => __( 'Promotion Heading 1', 'cmb2' ),
        'id'         => 'promoheading1',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value

    ) );
    
    $promoPages->add_field( array(
        'name'       => __( 'Heading 2', 'cmb2' ),
        'desc'       => __( 'Promotion Heading 2', 'cmb2' ),
        'id'         => 'promoheading2',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value

    ) ); 
    
    $promoPages->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'promoimage',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );   
    
    $promoPages->add_field( array(
        'name'       => __( 'Price', 'cmb2' ),
        'desc'       => __( 'Promotion Price', 'cmb2' ),
        'id'         => 'promoprice',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value

    ) ); 
    
    $group_field_id = $promoPages->add_field( array(
    	'id'          => 'promodetails',
    	'type'        => 'group',
    	'description' => __( 'Add list items included in the promotions', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Item {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Item', 'cmb2' ),
    		'remove_button'     => __( 'Remove Item', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );    
    
    
    $promoPages->add_group_field( $group_field_id, array(
        'name'    => 'Promotion Items',
        'desc'    => 'Add or remove promotion items.',
        'id'      => 'promotionitem',
        'type'    => 'text',
       
    
     ));
     
    $promoPages->add_field( array(
    	'name' => __( 'Service URL', 'cmb2' ),
    	'id'   => 'promourl',
    	'type' => 'text_url',
    	// 'protocols' => array( 'http', 'https', 'ftp', 'ftps', 'mailto', 'news', 'irc', 'gopher', 'nntp', 'feed', 'telnet' ), // Array of allowed protocols
    ) );
    
}

/**
 * Hook in and register a metabox to handle a theme options page and adds a menu item.
 */
function myprefix_register_theme_options_metabox() {

/**
* Registers options page menu item and form.
*/


	$productPages = new_cmb2_box( array(
		'id'            => 'productadditionalfields',
		'title'         => __( 'Additional ERP Details', 'cmb2' ),
		'object_types'  => array( 'product' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	
	    
    $productPages->add_field( array(
        'name'       => __( 'ERP Product ID', 'cmb2' ),
        'desc'       => __( 'Please map your product id here', 'cmb2' ),
        'id'         => 'productid',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value

    ) );
    
    $productPages->add_field( array(
    	'name'             => 'Type',
    	'id'               => 'productfor',
    	'type'             => 'radio',
    	'show_option_none' => true,
    	'options'          => array(
    		'male' => __( 'Male', 'cmb2' ),
    		'female'   => __( 'Female', 'cmb2' ),
    		'other'     => __( 'Other', 'cmb2' ),
    	),
    ) );
    
    
	$postPages = new_cmb2_box( array(
		'id'            => 'mainposttypefields',
		'title'         => __( 'Krisalys Women', 'cmb2' ),
		'object_types'  => array( 'krisalys-women', 'krisalys-men', 'cosmetic-surgery', 'aesthetics', 'dental' ), // Post type
		'context'       => 'normal',
		'priority'      => 'high',
		'show_names'    => true, // Show field names on the left
		// 'cmb_styles' => false, // false to disable the CMB stylesheet
		// 'closed'     => true, // Keep the metabox closed by default
	) );
	
	    
    $postPages->add_field( array(
        'name'       => __( 'Heading 1', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading1',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
    
    
    $postPages->add_field( array(
        'name'       => __( 'Heading 2', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading2',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
    
    $postPages->add_field( array(
    	'name'             => 'Type',
    	'id'               => 'productfor',
    	'type'             => 'radio',
    	'show_option_none' => true,
    	'options'          => array(
    		'male' => __( 'Male', 'cmb2' ),
    		'female'   => __( 'Female', 'cmb2' ),
    		'other'     => __( 'Other', 'cmb2' ),
    	),
    ) );    
    
   
    $postPages->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );  
    
    $postPages->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'postimage',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );    
    
    
    $group_field_id = $postPages->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Before After Slider Images', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $postPages->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $postPages->add_field( array(
    	'id'          => 'products',
    	'type'        => 'group',
    	'description' => __( 'Product Slider Images', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $postPages->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );    
    
    
    
    $group_field_id = $postPages->add_field( array(
    	'id'          => 'faq',
    	'type'        => 'group',
    	'description' => __( 'Frequently Asked Questions', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Questions {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Question', 'cmb2' ),
    		'remove_button'     => __( 'Remove Question', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $postPages->add_group_field( $group_field_id, array(
        'name'    => 'Question',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'question',
        'type'    => 'text',
    ) );  
    
    $postPages->add_group_field( $group_field_id, array(
        'name'    => 'Answer',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'answer',
        'type'    => 'wysiwyg',
    ) );     
      

    /**
     * Initiate the metabox
     */
    $cmb1 = new_cmb2_box( array(
        'id'            => 'main-services-1',
        'title'         => __( 'Women Salon Options', 'cmb2' ),
        'object_types' => array( 'options-page' ),
        'option_key'      => 'krisalys_women_options',
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    // Regular text field
    $cmb1->add_field( array(
        'name'       => __( 'Women Salon Options', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
   
    $cmb1->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );
   
    $cmb1->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $cmb1->add_field( array(
    	'id'          => 'documents',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Link {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Link', 'cmb2' ),
    		'remove_button'     => __( 'Remove Link', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
      // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb1->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Title',
    	'id'   => 'title',
    	'type' => 'text',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb1->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Link',
    	'id'   => 'url',
    	'type' => 'text_url',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    
    $group_field_id = $cmb1->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $cmb1->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    
    // Classic CMB2 declaration
    $cmbposts = new_cmb2_box( array(
    	'id'           => 'posts_attachments',
    	'title'        => __( 'Related Posts' ),
    	'object_types' => array( 'krisalys-women', ), // Post type
    ) );
    
    // Add new field
    $cmb1->add_field( array(
    	'name'        => __( 'Select max 3 posts' ),
    	'id'          => 'related_posts',
    	'type'        => 'post_search_text', // This field type
    	// post type also as array
    	'post_type'   => 'krisalys-women',
    	// Default is 'checkbox', used in the modal view to select the post type
    	'select_type' => 'checkbox',
    	// Will replace any selection with selection from modal. Default is 'add'
    	'select_behavior' => 'replace',
    ) );

    
   
    /**
     * Initiate the metabox
     */
    $cmb2 = new_cmb2_box( array(
        'id'            => 'main-services-2',
        'title'         => __( 'Men Salon Options', 'cmb2' ),
        'object_types' => array( 'options-page' ),
        'option_key'      => 'krisalys_men_options',
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );

    // Regular text field
    $cmb2->add_field( array(
        'name'       => __( 'Men Salon Options', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
   
    $cmb2->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );
   
    $cmb2->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $cmb2->add_field( array(
    	'id'          => 'documents',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Link {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Link', 'cmb2' ),
    		'remove_button'     => __( 'Remove Link', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
      // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb2->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Title',
    	'id'   => 'title',
    	'type' => 'text',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb2->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Link',
    	'id'   => 'url',
    	'type' => 'text_url',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    
    $group_field_id = $cmb2->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $cmb2->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
   
    /**
     * Initiate the metabox
     */
    $cmb3 = new_cmb2_box( array(
        'id'            => 'main-services-3',
        'title'         => __( 'Aesthetics Options', 'cmb2' ),
        'object_types' => array( 'options-page' ),
        'option_key'      => 'krisalys_aesthetics_options',
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );
    
    // Regular text field
    $cmb3->add_field( array(
        'name'       => __( 'Men Salon Options', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
   
    $cmb3->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );
   
    $cmb3->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $cmb3->add_field( array(
    	'id'          => 'documents',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Link {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Link', 'cmb2' ),
    		'remove_button'     => __( 'Remove Link', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
      // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb3->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Title',
    	'id'   => 'title',
    	'type' => 'text',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb3->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Link',
    	'id'   => 'url',
    	'type' => 'text_url',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    
    $group_field_id = $cmb3->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $cmb3->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );

   
   
    /**
     * Initiate the metabox
     */
    $cmb4 = new_cmb2_box( array(
        'id'            => 'main-services-4',
        'title'         => __( 'Cosmetic Surgery Options', 'cmb2' ),
        'object_types' => array( 'options-page' ),
        'option_key'      => 'krisalys_cosmetic_surgery_options',
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );
    
    // Regular text field
    $cmb4->add_field( array(
        'name'       => __( 'Men Salon Options', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
   
    $cmb4->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );
   
    $cmb4->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $cmb4->add_field( array(
    	'id'          => 'documents',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Link {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Link', 'cmb2' ),
    		'remove_button'     => __( 'Remove Link', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
      // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb4->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Title',
    	'id'   => 'title',
    	'type' => 'text',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb4->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Link',
    	'id'   => 'url',
    	'type' => 'text_url',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    
    $group_field_id = $cmb4->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $cmb4->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );    
    
    /**
     * Initiate the metabox
     */
    $cmb5 = new_cmb2_box( array(
        'id'            => 'main-services-5',
        'title'         => __( 'Dental Options', 'cmb2' ),
        'object_types' => array( 'options-page' ),
        'option_key'      => 'krisalys_dental_options',
        'context'       => 'normal',
        'priority'      => 'high',
        'show_names'    => true, // Show field names on the left
        // 'cmb_styles' => false, // false to disable the CMB stylesheet
        // 'closed'     => true, // Keep the metabox closed by default
    ) );    
    
    
        // Regular text field
    $cmb5->add_field( array(
        'name'       => __( 'Men Salon Options', 'cmb2' ),
        'desc'       => __( 'Heading', 'cmb2' ),
        'id'         => 'heading',
        'type'       => 'text',
        'show_on_cb' => 'cmb2_hide_if_no_cats', // function should return a bool value
        // 'sanitization_cb' => 'my_custom_sanitization', // custom sanitization callback parameter
        // 'escape_cb'       => 'my_custom_escaping',  // custom escaping callback parameter
        // 'on_front'        => false, // Optionally designate a field to wp-admin only
        // 'repeatable'      => true,
    ) );
   
    $cmb5->add_field( array(
        'name'    => 'Description',
        'desc'    => 'field description (optional)',
        'id'      => 'description',
        'type'    => 'wysiwyg',
        'options' => array(),
    ) );
   
    $cmb5->add_field( array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'image',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );
    
    $group_field_id = $cmb5->add_field( array(
    	'id'          => 'documents',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Link {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Link', 'cmb2' ),
    		'remove_button'     => __( 'Remove Link', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
      // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb5->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Title',
    	'id'   => 'title',
    	'type' => 'text',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    // Id's for group's fields only need to be unique for the group. Prefix is not needed.
    $cmb5->add_group_field( $group_field_id, array(
    	'name' => 'Enter Document Link',
    	'id'   => 'url',
    	'type' => 'text_url',
    	// 'repeatable' => true, // Repeatable fields are supported w/in repeatable groups (for most types)
    ) );
    
    
    $group_field_id = $cmb5->add_field( array(
    	'id'          => 'slider',
    	'type'        => 'group',
    	'description' => __( 'Generates reusable form entries', 'cmb2' ),
    	// 'repeatable'  => false, // use false if you want non-repeatable group
    	'options'     => array(
    		'group_title'       => __( 'Image {#}', 'cmb2' ), // since version 1.1.4, {#} gets replaced by row number
    		'add_button'        => __( 'Add Another Image', 'cmb2' ),
    		'remove_button'     => __( 'Remove Image', 'cmb2' ),
    		'sortable'          => true,
    		// 'closed'         => true, // true to have the groups closed by default
    		// 'remove_confirm' => esc_html__( 'Are you sure you want to remove?', 'cmb2' ), // Performs confirmation before removing group.
    	),
    ) );
    
    
    $cmb5->add_group_field( $group_field_id, array(
        'name'    => 'Image',
        'desc'    => 'Upload an image or enter an URL.',
        'id'      => 'slider',
        'type'    => 'file',
        // Optional:
        'options' => array(
        'url' => false, // Hide the text input for the url
        ),
        'text'    => array(
        'add_upload_file_text' => 'Add Image' // Change upload button text. Default: "Add or Upload File"
        ),
        // query_args are passed to wp.media's library query.
        'query_args' => array(
        //'type' => 'application/pdf', // Make library only display PDFs.
        // Or only allow gif, jpg, or png images
        'type' => array(
        'image/gif',
        'image/jpeg',
        'image/png',
        ),
    ),
    'preview_size' => 'small', // Image size to use when previewing in the admin.
    ) );

 

   
   


}


