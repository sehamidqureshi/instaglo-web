<?php
$sidebars = array();
if( is_archive() || is_search() || is_home() ):

	if( is_active_sidebar('post-archives-sidebar-right') ):
		array_push($sidebars, 'post-archives-sidebar-right' );
	endif;	
endif;

if( $sidebars ) {

	$wtstyle = cs_get_option( 'wtitle-style' );

	if(!empty($wtstyle)):
		echo "<div class='{$wtstyle}'>";
	endif;

	foreach( $sidebars as $sidebar ){
		dynamic_sidebar( $sidebar );
	}	

	if(!empty($wtstyle)):	
		echo "</div>";
	endif;
}