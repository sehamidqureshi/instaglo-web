<?php

get_header();

    $dentalOptions = get_option('krisalys_dental_options');
    
    $heading = $dentalOptions['heading'];
    $description = $dentalOptions['description'];
    $image = $dentalOptions['image'];
    $documents = $dentalOptions['documents'];
    $slider = $dentalOptions['slider'];
    
    
     
    $defaultImage = home_url()."/wp-content/uploads/2019/04/package-detail-1.jpg";
    $settings = get_post_meta($post->ID,'_tpl_default_settings',TRUE);
    $settings = is_array( $settings ) ?  array_filter( $settings )  : array();

    $global_breadcrumb = cs_get_option( 'show-breadcrumb' );

    $header_class = '';
    if( !$settings['enable-sub-title'] || !isset( $settings['enable-sub-title'] ) ) {
        if( isset( $settings['show_slider'] ) && $settings['show_slider'] ) {
            if( isset( $settings['slider_type'] ) ) {
                $header_class =  $settings['slider_position'];
            }
        }
    }
    
    if( !empty( $global_breadcrumb ) ) {
        if( isset( $settings['enable-sub-title'] ) && $settings['enable-sub-title'] ) {
            $header_class = isset( $settings['breadcrumb_position'] ) ? $settings['breadcrumb_position'] : '';            
		}
	}?>
<!-- ** Header Wrapper ** -->
<div id="header-wrapper"  class="<?php echo esc_attr($header_class); ?>">

    <!-- **Header** -->
    <header id="header">

        <div class="container"><?php
            /**
             * meni_header hook.
             * 
             * @hooked meni_vc_header_template - 10
             *
             */
            do_action( 'meni_header' ); 
            //echo do_shortcode('[vc_row full_width="stretch_row_content_no_spaces" equal_height="yes" content_placement="middle" css=".vc_custom_1560331193349{background-color: #ffffff !important;}" el_class="dt-header-default"][vc_column width="1/6" el_class="rs_col-sm-3 rs_aligncenter_sm" css=".vc_custom_1559889641102{padding-right: 5% !important;padding-left: 5% !important;}" offset="vc_col-lg-2 vc_col-md-3"][dt_sc_logo logo_type="theme-logo" theme_logo_type="logo" image_width="213" m_image_width="" item_align="center" breakpoint="" el_id="1505717332670-4a33176b-6074" css=".vc_custom_1559889969257{padding-top: 40px !important;padding-bottom: 20px !important;}"][dt_sc_empty_space margin_lg="20" margin_md="" margin_sm="" margin_xs="0" el_id="1555995852058-27784b37-aa9f"][/vc_column][vc_column width="2/3" el_class="rs_col-sm-9" css=".vc_custom_1559889650537{padding-right: 4% !important;padding-left: 4% !important;}" offset="vc_col-lg-8 vc_col-md-6"][dt_sc_empty_space margin_lg="25" margin_md="29" margin_sm="28" margin_xs="15" el_id="1555145499965-46cdbb6d-fda4"][vc_row_inner full_width="stretch_row" css=".vc_custom_1559825544478{padding-bottom: 15px !important;}" el_class="custom-header-topbar"][vc_column_inner el_class="rs_aligncenter_xs flex-col-elements-inline-middle justify-end"][vc_custom_heading text="Welcome to TheKrisalys!" font_container="tag:p|text_align:left" use_theme_fonts="yes" css=".vc_custom_1588051064544{margin-bottom: 0px !important;}"][dt_sc_sociable_new size="small" align="right" default-style="none" default-icon-color="custom" default-icon-custom-color="#555555" hover-style="none" hover-icon-color="primary-color" hide_on_lg="" hide_on_md="" hide_on_sm="" hide_on_xs="" el_id="1552298357031-9fdffc28-b367" social_list="%5B%7B%22social%22%3A%22facebook%22%2C%22link%22%3A%22url%3Ahttps%253A%252F%252Fwww.facebook.com%252Fthekrisalys%252F%7C%7Ctarget%3A%2520_blank%7C%22%7D%2C%7B%22social%22%3A%22twitter%22%2C%22link%22%3A%22url%3Ahttps%253A%252F%252Ftwitter.com%252FTheKrisalys%7C%7Ctarget%3A%2520_blank%7C%22%7D%2C%7B%22social%22%3A%22instagram%22%2C%22link%22%3A%22url%3Ahttps%253A%252F%252Fwww.instagram.com%252Fthekrisalys%252F%7C%7Ctarget%3A%2520_blank%7C%22%7D%2C%7B%22social%22%3A%22youtube%22%2C%22link%22%3A%22url%3A%2523%7C%7C%7C%22%7D%2C%7B%22social%22%3A%22pinterest%22%2C%22link%22%3A%22url%3A%2523%7C%7C%7C%22%7D%5D" title_for_sociable_settings=""][vc_column_text css=".vc_custom_1588049992772{margin-bottom: 0px !important;}"]Call Us  +92 302 2000084[/vc_column_text][/vc_column_inner][/vc_row_inner][vc_row_inner][vc_column_inner el_class="rs_col-xs-nospace-bottom"][dt_sc_empty_space margin_lg="0" margin_md="10" margin_sm="10" margin_xs="15" el_id="1550121395598-dd55cc00-c372"][dt_sc_header_menu nav_id="header-menu" display="simple" visual_nav="" highlighter="none" separator="none" default_item_color="custom" default_custom_item_color="#555555" hover_item_color="primary-color" submenu_indicator="yes" submenu_wrapper="" apply_to_simple_submenu="yes" sub_menu_default_border_radius="square" sub_menu_default_border_style="solid" sub_menu_default_border_width="2px" sub_menu_default_item_color="primary-color" sub_menu_default_bg_color="none" sub_menu_default_border_color="primary-color" sub_menu_hover_border_radius="square" sub_menu_hover_border_style="solid" sub_menu_hover_border_width="2px" sub_menu_hover_item_color="custom" sub_menu_hover_bg_color="primary-color" sub_menu_hover_border_color="primary-color" sub_menu_hover_custom_item_color="#ffffff" mobile_menu_label="Menu" mobile_menu_label_color="#555555" breakpoint="1259" mobile_menu_position="right" mobile_menu_icon_type="fontawesome" mobile_menu_icon_color="#555555" mobile_menu_icon_type_fontawesome="fa fa-bars" use_theme_fonts="yes" font_size="16" items_align="right" text_transform="capitalize" use_theme_fonts_for_sub_menu="yes" sub_menu_font_size="15" sub_menu_items_align="none" sub_menu_text_transform="none" mega_menu_width="mega-menu-column-equal" el_id="1505913242232-e64ea019-90fc"][dt_sc_empty_space margin_lg="0" margin_md="28" margin_sm="28" margin_xs="20" el_id="1548228969026-60e275a1-1e85"][/vc_column_inner][/vc_row_inner][/vc_column][vc_column width="1/6" offset="vc_col-lg-2 vc_col-md-3 vc_hidden-sm" el_class="dt-skin-primary-bg" addstyles="text-align: center;"][dt_sc_empty_space margin_lg="0" margin_md="0" margin_sm="0" margin_xs="30" el_id="1559891544939-12368db1-fb77"][dt_sc_button title="Appointment" size="large" style="bordered" icon_type="fontawesome" iconalign="icon-left with-icon" iconclass="" css=".vc_custom_1586157348619{margin-top: 0px !important;}" link="url:http%3A%2F%2Finstaglo.novatoresols.online%2Fappointment-ii%2F|title:Appointment||rel:nofollow" class="white"][dt_sc_empty_space margin_lg="0" margin_md="0" margin_sm="0" margin_xs="30" el_id="1559891562163-fea5f124-5049"][/vc_column][/vc_row]');
            ?>
        </div>
    </header><!-- **Header - End ** -->

    <!-- ** Slider ** -->
    <?php
        if( !$settings['enable-sub-title'] || !isset( $settings['enable-sub-title'] ) ) {
            if( isset( $settings['show_slider'] ) && $settings['show_slider'] ) {
                if( isset( $settings['slider_type'] ) ) {
                    if( $settings['slider_type'] == 'layerslider' && !empty( $settings['layerslider_id'] ) ) {
                        echo '<div id="slider">';
                        echo '  <div id="dt-sc-layer-slider" class="dt-sc-main-slider">';
                        echo    do_shortcode('[layerslider id="'.$settings['layerslider_id'].'"/]');
                        echo '  </div>';
                        echo '</div>';
					} elseif( $settings['slider_type'] == 'revolutionslider' && !empty( $settings['revolutionslider_id'] ) ) {
                        echo '<div id="slider">';
                        echo '  <div id="dt-sc-rev-slider" class="dt-sc-main-slider">';
                        echo    do_shortcode('[rev_slider '.$settings['revolutionslider_id'].'/]');
                        echo '  </div>';
                        echo '</div>';
					} elseif( $settings['slider_type'] == 'customslider' && !empty( $settings['customslider_sc'] ) ) {
                        echo '<div id="slider">';
                        echo '  <div id="dt-sc-custom-slider" class="dt-sc-main-slider">';
                        echo    do_shortcode( $settings['customslider_sc'] );
                        echo '  </div>';
                        echo '</div>';
					}
                }
            }
        }
    ?><!-- ** Slider End ** -->

    <!-- ** Breadcrumb ** -->
    <?php
    
    $slug = isset($_GET['service']) ? $_GET['service'] : "";
    
    $parentTaxId = 0;
    
    if (!empty($slug)) {
        
        $term = get_term_by('slug', $slug, 'dental-tax');
        
        $parentTaxId = $term->term_id;
        
        $parentTerm = get_term_by('id', $term->parent, 'dental-tax');
        
        $parentName = $parentTerm->name;
        $parentSlug = $parentTerm->slug;        
        
        $termName = $term->name;
        $termSlug = $term->slug;
        
        $args = array(
            'taxonomy' => $term->taxonomy,
            'hide_empty' => false,
            'child_of' => $term->parent,
            'parent' => $parentTaxId
        );
    } else {

        $args = array(
            'taxonomy' => 'dental-tax',
            'hide_empty' => false,
            'parent' => $parentTaxId
        );        
    }
    
    
   
    if ($slug != "") {

        $postArgs = array(
        'tax_query' => array(
                            array(
                                    'taxonomy' => 'dental-tax',
                                    'field' => 'slug',
                                    'terms' => $slug
                            )
                        ),
            'post_type'=>'dental',
            'posts_per_page'=>-1
           
        );
       
    } else {
       
        $postArgs = array(
            'post_type'=>'dental',
            'posts_per_page'=>3
        );

    }
   
    $postsList = get_posts($postArgs); 
    
    $allServices = array_chunk($postsList, ceil(count($postsList) / 2));
     
    $customPostTaxonomies = get_terms($args);
    
    
        # Global Breadcrumb
          
        $breadcrumbs = array();
        $bstyle = meni_cs_get_option( 'breadcrumb-style', 'default' );

        if( $post->post_parent ) {
            $parent_id  = $post->post_parent;
            $parents = array();

            while( $parent_id ) {
                $page = get_page( $parent_id );
                $parents[] = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
                $parent_id  = $page->post_parent;
            }

            $parents = array_reverse( $parents );
            $breadcrumbs = array_merge_recursive($breadcrumbs, $parents);
        }

        $breadcrumbs[] = the_title( '<span class="current">', '</span>', false );
        $style = meni_breadcrumb_css( $settings['breadcrumb_background'] );
        
        $breadcrumbs = array('KRISALYS DENTAL');

        meni_breadcrumb_output ( '', $breadcrumbs, $bstyle, $style );
        
    
    ?><!-- ** Breadcrumb End ** -->                
</div><!-- ** Header Wrapper - End ** -->

<!-- **Main** -->
<div id="main">

    <!-- ** Container ** -->
    <div class="container"><?php
        $page_layout  = array_key_exists( "layout", $settings ) ? $settings['layout'] : "content-full-width";
        $layout = meni_page_layout( $page_layout );
        extract( $layout );

        if( array_key_exists('sidenav-align', $settings ) && $settings ['sidenav-align'] == 'true' ) {
            $page_layout .= ' sidenav-alignright';
        }

        if( array_key_exists('sidenav-sticky', $settings ) && $settings ['sidenav-sticky'] == 'true' ) {
            $page_layout .= ' sidenav-sticky';
        }

        
        ?>
        
        
    <section id="primary" class="content-full-width">

    <div class="side-navigation type2">
        <?php if (isset($termName)): ?>
        <h3 style="text-align: left; font-size:30px;" class="vc_custom_heading"><?php echo $termName ?></h3>
        <?php else: ?>
        <h3 style="text-align: left; font-size:30px;" class="vc_custom_heading">Krisalys Dental</h3>
        <?php endif; ?>
        <div class="side-nav-container">
            <ul class="side-nav">
                
                <?php foreach ($customPostTaxonomies as $index => $category ): ?>
                <?php 
                    $activeClass = ""; 
                    if ($category->slug == $slug) {
                        
                        $activeClass = "current_page_item";
                    }
                ?>
                
                <li class="<?php echo $activeClass ?>"><a href="<?php echo home_url() ?>/dental/?service=<?php echo $category->slug ?>"><?php echo $category->name ?></a></li>
                
                <?php endforeach; ?>
                
                <?php if (isset($parentSlug)): ?>
                    <li style="border-top: 1px solid #e5e5e5; display: inline-block; font-size: 14px; margin: 0 0 20px; padding: 0 0 20px; position: relative; vertical-align: middle; width: 100%;" ><a href="<?php echo home_url() ?>/dental/?service=<?php echo $parentSlug ?>" target="_self" title="" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> Go Back - <?php echo $parentName ?> <span class="fa fa-chevron-right"> </span></a></li>
                <?php endif; ?>
            </ul>
        </div>
        
            
        

        <div class="side-navigation-bottom-content">
            <div data-delay="0" class="vc_row wpb_row vc_row-fluid vc_custom_1561368067297">
                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill" data-delay="0">
                    <div class="vc_column-inner vc_custom_1561367544599">
                        <div class="wpb_wrapper">
                            <h5 style="text-align: left" class="vc_custom_heading">Downloads</h5>
                            <div id="1561367376296-dc39fed9-411b" class="dt-sc-empty-space"></div>
                            <div class="wpb_text_column wpb_content_element  vc_custom_1561367263012 custom-career-siderbar">
                                <div class="wpb_wrapper">
                                    <?php if (is_array($documents)): ?>
                                    
                                    <?php foreach ($documents as $index => $value): ?>
                                        <p><img src="<?php echo home_url() ?>/wp-content/uploads/2020/04/pdf-img.jpg" alt="image"><a href="<?php echo $value['url'] ?>"><?php echo $value['title'] ?></a></p>
                                    <?php endforeach; ?>
                                    <?php endif; ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if (is_array($slider)): ?>
            <aside id="text-3" class="widget widget_text">			
                <div class="textwidget">
                        <div class="dt-sc-images-wrapper " data-delay="0">
                            <div class="caroufredsel_wrapper" style="display: block; text-align: center; float: left; position: relative; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 285px; height: 222px; margin: 0px; overflow: hidden; cursor: move;">
                                    <ul class="dt-sc-images-carousel" style="text-align: left; float: none; position: absolute; top: 0px; right: auto; bottom: auto; left: 0px; margin: 0px; width: 1995px; height: 223px; z-index: auto; opacity: 1;">
                                        <?php foreach ($slider as $index => $value): ?>
                                            <li style="width: 285px;">
                                                <img width="450" height="347" src="<?php echo $value['slider'] ?>" class="attachment-full" alt="" srcset="<?php echo $value['slider'] ?> 450w, <?php echo $value['slider'] ?> 300w" sizes="(max-width: 450px) 100vw, 450px">
                                            </li>
                                        <?php endforeach; ?>
                                        
                                        </ul>
                                        </div><div class="carousel-arrows"><a href="JavaScript:void(0);" class="images-prev" style="display: inline-block;"> </a><a href="JavaScript:void(0);" class="images-next" style="display: inline-block;"> </a></div></div><p style="text-align: left" class="vc_custom_heading uppercase dt-sc-skin-color vc_custom_1560952143936">Before &amp; After</p><h5 style="text-align: left" class="vc_custom_heading vc_custom_1562837388977">Treatment Results</h5>
</div>
		</aside>
	
		<?php endif; ?>
        </div>
    </div>
    
  

    <div class="side-navigation-content">
        <!-- #post-19992 -->
        <div id="post-19992" class="post-19992 page type-page status-publish has-post-thumbnail hentry">
            <div data-delay="0" class="vc_row wpb_row vc_row-fluid">
                <div class="wpb_column vc_column_container vc_col-sm-12" data-delay="0">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <h3 style="text-align: left" class="vc_custom_heading"><?php echo $heading ?></h3>
                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                <div class="rs_col-sm-12 wpb_column vc_column_container vc_col-sm-8 vc_col-lg-8 vc_col-md-8">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                        <?php echo $description ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="rs_col-sm-12 wpb_column vc_column_container vc_col-sm-4 vc_col-lg-4 vc_col-md-4 vc_hidden-sm">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_single_image wpb_content_element vc_align_left">

                                                <figure class="wpb_wrapper vc_figure">
                                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="420" height="570" src="<?php echo $image ?>" class="vc_single_image-img attachment-full" alt="" srcset="<?php echo $image ?> 420w, <?php echo $image ?> 221w" sizes="(max-width: 420px) 100vw, 420px"></div>
                                                </figure>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php if (is_array($allServices)): ?>
            
            <?php //foreach ($allServices as $index => $services): ?>
            
            <div data-delay="0" class="vc_row wpb_row vc_row-fluid">
                
                <?php foreach ($postsList as $serviceIndex => $post) { 
                
                            $postImage = get_the_post_thumbnail_url($post->ID);
                            $postTitle = $post->post_title;
                            $postURL = $post->guid;
                            $postContent = $post->post_excerpt;
                        
                ?>
               
                <div class="wpb_column vc_column_container vc_col-sm-4" data-delay="0">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <a href="<?php echo $postURL ?>"><div class="dt-sc-icon-box type7 aligncenter " data-delay="0">
                                <div class="icon-wrapper"><span class="fa fa-ioxhost"> </span></div>
                                <div class="icon-content">
                                    <h4><?php echo $postTitle ?></h4>
                                    <p><img src="<?php echo $postImage; ?>" onerror="this.src='<?php echo $defaultImage; ?>'"/></p>
                                    <p><?php echo $postContent; ?></p>
                                </div>
                            </div>
                        </div></a>
                    </div>
                </div>
                
                <?php } ?>

                <div class="wpb_column vc_column_container vc_col-sm-12" data-delay="0">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div id="1551517256919-f15bc692-c401" style="margin-top:20px;" class="dt-sc-empty-space"></div>
                        </div>
                    </div>
                </div>
            </div>
            
            <?php //endforeach; ?>
            
            <?php endif; ?>

        </div>
        <!-- #post-19992 -->
    </div>
</section>

    </div>
    <!-- ** Container End ** -->
    
</div><!-- **Main - End ** -->    
<?php wp_footer(); get_footer(); ?>