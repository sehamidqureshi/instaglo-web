<?php
/*
Template Name: Women Salon
*/

get_header();

    $settings = get_post_meta($post->ID,'_tpl_default_settings',TRUE);
    $settings = is_array( $settings ) ?  array_filter( $settings )  : array();

    $global_breadcrumb = cs_get_option( 'show-breadcrumb' );

    $header_class = '';
    if( !$settings['enable-sub-title'] || !isset( $settings['enable-sub-title'] ) ) {
        if( isset( $settings['show_slider'] ) && $settings['show_slider'] ) {
            if( isset( $settings['slider_type'] ) ) {
                $header_class =  $settings['slider_position'];
            }
        }
    }
    
    if( !empty( $global_breadcrumb ) ) {
        if( isset( $settings['enable-sub-title'] ) && $settings['enable-sub-title'] ) {
            $header_class = isset( $settings['breadcrumb_position'] ) ? $settings['breadcrumb_position'] : '';            
		}
	}?>
<!-- ** Header Wrapper ** -->
<div id="header-wrapper"  class="<?php echo esc_attr($header_class); ?>" style="margin:0px;">

    <!-- **Header** -->
    <header id="header">

        <div class="container"><?php
            /**
             * meni_header hook.
             * 
             * @hooked meni_vc_header_template - 10
             *
             */
            do_action( 'meni_header' ); ?>
        </div>
    </header><!-- **Header - End ** -->

    <!-- ** Slider ** -->
    <?php
        if( !$settings['enable-sub-title'] || !isset( $settings['enable-sub-title'] ) ) {
            if( isset( $settings['show_slider'] ) && $settings['show_slider'] ) {
                if( isset( $settings['slider_type'] ) ) {
                    if( $settings['slider_type'] == 'layerslider' && !empty( $settings['layerslider_id'] ) ) {
                        echo '<div id="slider">';
                        echo '  <div id="dt-sc-layer-slider" class="dt-sc-main-slider">';
                        echo    do_shortcode('[layerslider id="'.$settings['layerslider_id'].'"/]');
                        echo '  </div>';
                        echo '</div>';
					} elseif( $settings['slider_type'] == 'revolutionslider' && !empty( $settings['revolutionslider_id'] ) ) {
                        echo '<div id="slider">';
                        echo '  <div id="dt-sc-rev-slider" class="dt-sc-main-slider">';
                        echo    do_shortcode('[rev_slider '.$settings['revolutionslider_id'].'/]');
                        echo '  </div>';
                        echo '</div>';
					} elseif( $settings['slider_type'] == 'customslider' && !empty( $settings['customslider_sc'] ) ) {
                        echo '<div id="slider">';
                        echo '  <div id="dt-sc-custom-slider" class="dt-sc-main-slider">';
                        echo    do_shortcode( $settings['customslider_sc'] );
                        echo '  </div>';
                        echo '</div>';
					}
                }
            }
        }
    ?><!-- ** Slider End ** -->

    <!-- ** Breadcrumb ** -->
    <?php
        # Global Breadcrumb
        if( !empty( $global_breadcrumb ) ) {
            if( isset( $settings['enable-sub-title'] ) && $settings['enable-sub-title'] ) {
                $breadcrumbs = array();
                $bstyle = meni_cs_get_option( 'breadcrumb-style', 'default' );

                if( $post->post_parent ) {
                    $parent_id  = $post->post_parent;
                    $parents = array();

                    while( $parent_id ) {
                        $page = get_page( $parent_id );
                        $parents[] = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
                        $parent_id  = $page->post_parent;
                    }

                    $parents = array_reverse( $parents );
                    $breadcrumbs = array_merge_recursive($breadcrumbs, $parents);
                }

                $breadcrumbs[] = the_title( '<span class="current">', '</span>', false );
                $style = meni_breadcrumb_css( $settings['breadcrumb_background'] );

                meni_breadcrumb_output ( the_title( '<h1>', '</h1>',false ), $breadcrumbs, $bstyle, $style );
            }
        }
    ?><!-- ** Breadcrumb End ** -->                
</div><!-- ** Header Wrapper - End ** -->

<!-- **Main** -->
<div id="main" style="margin-top:0px;">

    <!-- ** Container ** -->
    <div class="container"><?php
        $page_layout  = array_key_exists( "layout", $settings ) ? $settings['layout'] : "content-full-width";
        $layout = meni_page_layout( $page_layout );
        extract( $layout );


      ?>

        <!-- Primary -->
        <section id="primary" class="content-full-width">

        <div id="post-<?php echo $post->ID ?>" class="post-<?php echo $post->ID ?> page type-page status-publish hentry" >
            <!--section 01 -->
            <div  class="vc_row wpb_row vc_row-fluid vc_row-has-fill" style="position: relative; left: -161.5px; box-sizing: border-box; width: 1583px; padding-left: 161.5px; padding-right: 161.5px; padding-top: 50px !important; padding-bottom: 35px !important; background-color: #f4f4f4 !important;">
                <div class="wpb_column vc_column_container vc_col-sm-12" data-delay="0">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div id="1561970905763-a035962f-483a" class="dt-sc-empty-space" style="height: 50px;"></div>
                        </div>
                    </div>
                </div>
                <div class="rs_col-sm-12 wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-12" data-delay="0">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_single_image wpb_content_element vc_align_left   custom-img-border">
            
                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="570" height="664" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/about-doctor-img.jpg" class="vc_single_image-img attachment-full" alt="" srcset="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/about-doctor-img.jpg 570w, http://instaglo.novatoresols.online/wp-content/uploads/2019/06/about-doctor-img-258x300.jpg 258w, http://instaglo.novatoresols.online/wp-content/uploads/2019/06/about-doctor-img-540x629.jpg 540w" sizes="(max-width: 570px) 100vw, 570px"></div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="rs_col-sm-12 wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-12" data-delay="0">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="dt-sc-title script-with-sub-title  " data-delay="0">
                                <h6 class="dt-sc-main-heading">Introduction</h6>
                                <h2 class="dt-sc-sub-heading">About Derma Clinic</h2></div>
                            <div id="1559910757873-ff4e3181-cb65" class="dt-sc-empty-space" style="height: 50px;"></div>
                            <p style="text-align: left" class="vc_custom_heading">Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium.</p>
                            <div id="1559912678981-7e952b29-b8db" class="dt-sc-empty-space" style="height: 50px;"></div>
                            <blockquote class="type3  " data-delay="0"><q>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Integer tincidunt. Cras dapibus. Vivamus elemen tum semper nisi.</q></blockquote>
                            <div id="1559912752491-9c3dd046-385b" class="dt-sc-empty-space" style="height: 50px;"></div><a href="http://dev.themes-demo.com/meni/appointment-i/" target=" _blank" title="Appointment" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> Book an Appointment <span class="fa fa-chevron-right"> </span></a>
                            <div id="1562846058933-49930f46-0343" class="dt-sc-empty-space" style="height: 50px;"></div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-12" data-delay="0">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div id="1561963708952-b043c2b1-8af8" class="dt-sc-empty-space"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--section 01 -->
            
            
<!-- section 02 -->

<div class="vc_row wpb_row vc_row-fluid vc_row-o-equal-height vc_row-flex ui-sortable" style="position: relative; left: -161.5px; box-sizing: border-box; width: 1583px;">
    <div class="rs_col-sm-12 rs_paddingtop-qxlg-none wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-12" data-delay="0">
        <div class="vc_column-inner vc_custom_1564388418825">
            <div class="wpb_wrapper">
                <div id="1560157049692-6b78cb3d-257e" class="dt-sc-empty-space" style="height: 50px;"></div>
                <div class="dt-sc-title script-with-sub-title  " data-delay="0">
                    <h6 class="dt-sc-main-heading">from our best Dermatologist</h6>
                    <h2 class="dt-sc-sub-heading">What We Offer</h2></div>
                <div id="1560150117617-17156b84-3beb" class="dt-sc-empty-space" style="height: 50px;"></div>
                <p style="text-align: left" class="vc_custom_heading vc_custom_1560157711377">Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet.. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet..In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo dapibus in, viverra quis.</p>
                <div class="vc_row wpb_row vc_inner vc_row-fluid ui-sortable">
                    <div class="dt-col-qxlg-6 wpb_column vc_column_container vc_col-sm-4">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <ul class="dt-sc-fancy-list    rounded-circle-tick" data-delay="0">
                                    <li>Acne Scarring</li>
                                    <li>Body Contoring</li>
                                    <li>Age Spots</li>
                                    <li>Brest Birth Marks</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="dt-col-qxlg-6 wpb_column vc_column_container vc_col-sm-4">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <ul class="dt-sc-fancy-list    rounded-circle-tick" data-delay="0">
                                    <li>Allergics</li>
                                    <li>Scar Removels</li>
                                    <li>Hiar Treatment</li>
                                    <li>Facial Redness</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="dt-col-qxlg-6 wpb_column vc_column_container vc_col-sm-4">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <ul class="dt-sc-fancy-list    rounded-circle-tick" data-delay="0">
                                    <li>Double Chin</li>
                                    <li>Angiofibromans</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="1560154387576-d187ebd9-dcea" class="dt-sc-empty-space" style="height: 50px;"></div><a href="http://dev.themes-demo.com/meni/services/" target="_blank" title="Services" class="dt-sc-button   medium icon-right with-icon  bordered  type2" data-delay="0"> More Services <span class="fa fa-chevron-right"> </span></a>
                <div id="1562846084302-ede1f98d-9683" class="dt-sc-empty-space" style="height: 50px;"></div>
            </div>
        </div>
    </div>
    <div class="dt-sc-dark-bg rs_col-sm-12 wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-12 vc_col-has-fill" data-delay="0">
        <div class="vc_column-inner vc_custom_1564149008946">
            <div class="wpb_wrapper">
                <div id="1560157142177-01f75ffa-f15c" class="dt-sc-empty-space" style="height: 50px;"></div>
                <div class="vc_row wpb_row vc_inner vc_row-fluid ui-sortable">
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="dt-sc-icon-box type15  " data-delay="0">
                                    <div class="icon-wrapper"><img width="132" height="154" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/face.png" class="attachment-full" alt="">
                                        <h4>Face<br>
Treatment</h4></div>
                                    <div class="icon-content"></div>
                                </div>
                                <div id="1560148331897-8299e4f7-7b24" class="dt-sc-empty-space" style="height: 50px;"></div>
                                <div class="dt-sc-icon-box type15  " data-delay="0">
                                    <div class="icon-wrapper"><img width="132" height="154" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/breast.png" class="attachment-full" alt="">
                                        <h4>Breast<br>
Treatment</h4></div>
                                    <div class="icon-content"></div>
                                </div>
                                <div id="1560148342426-e04133f3-e6c8" class="dt-sc-empty-space" style="height: 50px;"></div>
                                <div class="dt-sc-icon-box type15  " data-delay="0">
                                    <div class="icon-wrapper"><img width="132" height="154" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/heade.png" class="attachment-full" alt="">
                                        <h4>Hair<br>
Treatment</h4></div>
                                    <div class="icon-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="dt-sc-icon-box type15  " data-delay="0">
                                    <div class="icon-wrapper"><img width="132" height="154" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/belly.png" class="attachment-full" alt="">
                                        <h4>Body<br>
Treatment</h4></div>
                                    <div class="icon-content"></div>
                                </div>
                                <div id="1560148346711-9a919eac-f768" class="dt-sc-empty-space" style="height: 50px;"></div>
                                <div class="dt-sc-icon-box type15  " data-delay="0">
                                    <div class="icon-wrapper"><img width="132" height="154" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/men-head.png" class="attachment-full" alt="">
                                        <h4>Mens<br>
Treatment</h4></div>
                                    <div class="icon-content"></div>
                                </div>
                                <div id="1560148349365-43f26ffc-4b87" class="dt-sc-empty-space" style="height: 50px;"></div>
                                <div class="dt-sc-icon-box type15  " data-delay="0">
                                    <div class="icon-wrapper"><img width="132" height="154" src="http://instaglo.novatoresols.online/wp-content/uploads/2019/06/nose.png" class="attachment-full" alt="">
                                        <h4>Nose<br>
Treatment</h4></div>
                                    <div class="icon-content"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="1560157063281-3aeebcdc-12e9" class="dt-sc-empty-space" style="height: 50px;"></div>
            </div>
        </div>
    </div>
</div>
<!-- section 02 -->
            
            
        </div>
           
        </section><!-- Primary End --><?php

        if ( $show_sidebar ) {
            if ( $show_right_sidebar ) {
                $sticky_class = ( array_key_exists('enable-sticky-sidebar', $settings) && $settings['enable-sticky-sidebar'] == 'true' ) ? ' sidebar-as-sticky' : '';?>

                <!-- Secondary Right -->
                <section id="secondary-right" class="secondary-sidebar <?php echo esc_attr( $sidebar_class.$sticky_class );?>"><?php
                    meni_show_sidebar( 'page', $post->ID, 'right' ); ?>
                </section><!-- Secondary Right End --><?php
            }
        }?>
    </div>
    <!-- ** Container End ** -->
    
</div><!-- **Main - End ** -->    
<?php get_footer(); ?>