    <?php
        /**
         * meni_hook_content_after hook.
         * 
         */
        do_action( 'meni_hook_content_after' );
    ?>

        <!-- **Footer** -->
        <footer id="footer">
            <div class="container">
            <?php
                /**
                 * meni_footer hook.
                 * 
                 * @hooked meni_vc_footer_template - 10
                 *
                 */
                do_action( 'meni_footer' );
            ?>
            </div>
        </footer><!-- **Footer - End** -->

    </div><!-- **Inner Wrapper - End** -->
        
</div><!-- **Wrapper - End** -->




<?php 

	$current_user = wp_get_current_user();

	$userEmail = isset($current_user->user_email) ? $current_user->user_email : "";
	$userName = isset($current_user->display_name) ? $current_user->display_name : "";
?>
    <script>
		var appointment_url = '<?php echo home_url(); ?>/appointment-ii/'; 
		var userEmail = '<?php echo $userEmail; ?>';
		var userName = '<?php echo $userName; ?>';
    </script>
    
    



<?php
    
    do_action( 'meni_hook_bottom' );

    wp_footer();
?>
</body>
</html>