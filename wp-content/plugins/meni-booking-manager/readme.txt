=== Ultimate Booking Manager ===
Contributors: designthemes
Donate link: http://wedesignthemes.com/
Tags: booking, reservation, appointment
Requires at least: 4.9
Tested up to: 5.1
Stable tag: 5.1
Requires PHP: 5.6.24
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

DT Booking Manager is built to be used for booking or reservation functionality. It can be used for any business or niche websites.

== Description ==

The Booking Manager Plugin - A complete booking reservation and management plugin for WordPress.

> [Default Demo](http://nichewpthemes.com/bm/)

###Booking Manager WordPress Themes###

- [Helen Spa](http://preview.themeforest.net/item/helens-spa-beauty-wp-theme/full_screen_preview/21502724?ref=designthemes) - A beautiful Spa theme to showcase your Spa, beauty parlors, hair salons, unisex gyms as well as exclusive male and female gyms, wellness centers, salons of all types, massage centers, yoga studios and just about any hospitality center!

- [Dental](http://preview.themeforest.net/item/dental-a-clean-orthodontist-medical-wp-theme/full_screen_preview/19277048?ref=designthemes) - This Dental Theme is #1 Medical WordPress Theme for Dental Surgeons and Cosmetic Clinics. Dental Theme offers you unique pages for dentists, dental service, procedures, Procedure detail, doctor profile, appointment and Inquiry page.

- [Onelife](http://preview.themeforest.net/item/onelife-medical-medical-health-wordpresstheme/full_screen_preview/16435780?ref=designthemes) - It is a versatile theme that is well suited for the Cosmetic Medical Center, Plastic Surgeons, Hospitals, Beauty Clinics, Health Care Companies, Pharmaceuticalsand all medical related business. One Click Demo install will pre load all the elements with you need to create websites.

- [Spalab](http://preview.themeforest.net/item/spa-lab-beauty-spa-beauty-salon-wordpress-theme/full_screen_preview/8795615?ref=designthemes) - A hand crafted Beauty Salon WordPress Theme for hair salons, wellness centre, yoga / meditation classes and all other health care businesses. The theme includes essential advanced features such as two types of menu card designs, therapists, reservation, gift card, product and shop and many more.

- [LMS](http://preview.themeforest.net/item/lms-learning-management-system-education-lms-wordpress-theme/full_screen_preview/7867581?ref=designthemes) - Powerful Learning management system WordPress theme provides awesome features for creating online courses, teacher profile, extended user profiles, lesson management, quiz System, video hosting, ranking / rating system, questions system, attachments, Tracking course progress etc.,

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/meni-booking-manager` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the Services->Settings screen to configure the plugin
4. Choose desired options & save to finish the configuration.

== Frequently Asked Questions ==

Is it works in Multi-site?

No, It's not only single domain plugin.

== Screenshots ==

1. General Options
2. Time Schedule Options
3. Payments Options
4. Notification Options
5. Service Options
6. Person Options
7. Backup Options
8. Service Post Options
9. Person Post Options
10. Reservation Details

== Changelog ==
= 1.2 =
* WordPress 5.1 compatible issue updated
* Loading image URL bug fixed
* Few issues technical issues updated

= 1.1 =
* Few design & responsive issues updated.

= 1.0 =
* Initial Release.

== Upgrade Notice ==

= 1.0 =
1.0 is initial release.