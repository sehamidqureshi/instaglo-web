<?php
if (! class_exists ( 'DTBooking_Cs_Sc_PersonItem' ) ) {

    class DTBooking_Cs_Sc_PersonItem {

        function DTBooking_sc_PersonItem() {

			$plural_name = '';
			if( function_exists( 'dt_booking_cs_get_option' ) ) :
				$plural_name	=	dt_booking_cs_get_option( 'singular-person-text', esc_html__('Person', 'meni-booking-manager') );
			endif;

			$options = array(
			  'name'      => 'dt_sc_person_item',
			  'title'     => $plural_name,
			  'fields'    => array(

				array(
				  'id'    => 'person_id',
				  'type'  => 'text',
				  'title' => esc_html__( 'Enter Person ID', 'meni-booking-manager' ),
				  'after' => '<div class="cs-text-muted">'.esc_html__('Enter ID of person to display. More than one ids with comma(,) seperated.', 'meni-booking-manager').'</div>',
				),
				array(
				  'id'        => 'type',
				  'type'      => 'select',
				  'title'     => esc_html__('Type', 'meni-booking-manager'),
				  'options'   => array(
					'type1'    => esc_html__('Type - 1', 'meni-booking-manager'),
					'type2'    => esc_html__('Type - 2', 'meni-booking-manager')
				  ),
				  'class'     => 'chosen',
				  'default'   => 'desc',
				  'info'      => esc_html__('Choose type of persons to display.', 'meni-booking-manager')
				),
				array(
				  'id'        => 'show_button',
				  'type'      => 'select',
				  'title'     => esc_html__('Show button?', 'meni-booking-manager'),
				  'options'   => array(
					'yes'   => esc_html__('Yes', 'meni-booking-manager'),
					'no'    => esc_html__('No', 'meni-booking-manager')
				  ),
				  'class'     => 'chosen',
				  'default'   => 'no',
				  'info'      => esc_html__('Choose "Yes" to show button.', 'meni-booking-manager')
				),
				array(
				  'id'    => 'button_text',
				  'type'  => 'text',
				  'title' => esc_html__( 'Button Text', 'meni-booking-manager' ),
				  'default' => esc_html__('Book an appointment', 'meni-booking-manager'),
				  'info'  => esc_html__( 'Enter button text.', 'meni-booking-manager' )
				)
			  ),
			);

			return $options;
		}
	}				
}