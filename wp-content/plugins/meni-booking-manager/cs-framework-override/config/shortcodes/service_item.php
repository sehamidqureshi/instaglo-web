<?php
if (! class_exists ( 'DTBooking_Cs_Sc_ServiceItem' ) ) {

    class DTBooking_Cs_Sc_ServiceItem {

        function DTBooking_sc_ServiceItem() {

			$plural_name = '';
			if( function_exists( 'dt_booking_cs_get_option' ) ) :
				$plural_name	=	dt_booking_cs_get_option( 'singular-service-text', esc_html__('Service', 'meni-booking-manager') );
			endif;

			$options = array(
			  'name'      => 'dt_sc_service_item',
			  'title'     => $plural_name,
			  'fields'    => array(

				array(
				  'id'    => 'service_id',
				  'type'  => 'text',
				  'title' => esc_html__( 'Enter Service ID', 'meni-booking-manager' ),
				  'after' => '<div class="cs-text-muted">'.esc_html__('Enter IDs of services to display. More than one ids with comma(,) seperated.', 'meni-booking-manager').'</div>',
				),
				array(
				  'id'        => 'type',
				  'type'      => 'select',
				  'title'     => esc_html__('Type', 'meni-booking-manager'),
				  'options'   => array(
					'type1'    => esc_html__('Type - 1', 'meni-booking-manager'),
					'type2'    => esc_html__('Type - 2', 'meni-booking-manager')
				  ),
				  'class'     => 'chosen',
				  'default'   => 'desc',
				  'info'      => esc_html__('Choose type of services to display.', 'meni-booking-manager')
				),
				array(
				  'id'        => 'excerpt',
				  'type'      => 'select',
				  'title'     => esc_html__('Show Excerpt?', 'meni-booking-manager'),
				  'options'   => array(
					'yes'   => esc_html__('Yes', 'meni-booking-manager'),
					'no'    => esc_html__('No', 'meni-booking-manager')
				  ),
				  'class'     => 'chosen',
				  'default'   => 'no',
				  'info'      => esc_html__('Choose "Yes" to show excerpt.', 'meni-booking-manager')
				),
				array(
				  'id'    => 'excerpt_length',
				  'type'  => 'text',
				  'title' => esc_html__( 'Excerpt Length', 'meni-booking-manager' ),
				  'default' => 12
				),
				array(
				  'id'        => 'meta',
				  'type'      => 'select',
				  'title'     => esc_html__('Show Meta?', 'meni-booking-manager'),
				  'options'   => array(
					'yes'   => esc_html__('Yes', 'meni-booking-manager'),
					'no'    => esc_html__('No', 'meni-booking-manager')
				  ),
				  'class'     => 'chosen',
				  'default'   => 'no',
				  'info'      => esc_html__('Choose "Yes" to show meta.', 'meni-booking-manager')
				),
				array(
				  'id'    => 'button_text',
				  'type'  => 'text',
				  'title' => esc_html__( 'Button Text', 'meni-booking-manager' ),
				  'default' => esc_html__('View procedure details', 'meni-booking-manager'),
				  'info'  => esc_html__( 'Enter button text.', 'meni-booking-manager' )
				)
			  ),
			);

			return $options;
		}
	}				
}