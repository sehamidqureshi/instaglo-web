<?php
if (! class_exists ( 'DTBooking_Cs_Sc_ReserveAppointment' ) ) {

    class DTBooking_Cs_Sc_ReserveAppointment {

        function DTBooking_sc_ReserveAppointment() {

			$options = array(
			  'name'      => 'dt_sc_reserve_appointment',
			  'title'     => esc_html__('Reserve Appointment', 'meni-booking-manager'),
			  'fields'    => array(

				array(
				  'id'    => 'title',
				  'type'  => 'text',
				  'title' => esc_html__( 'Title', 'meni-booking-manager' )
				),
				array(
				  'id'           => 'type',
				  'type'         => 'select',
				  'title'        => esc_html__('Type', 'meni-booking-manager'),
				  'options'      => array(
					'type1'      => esc_html__('Type - I', 'meni-booking-manager'),
					'type2'      => esc_html__('Type - II', 'meni-booking-manager'),
				  ),
				  'class'        => 'chosen',
				  'default'      => 'type1',
				  'info'         => esc_html__('Choose type of reservation to display.', 'meni-booking-manager')
				),
			  ),
			);

			return $options;
		}
	}				
}