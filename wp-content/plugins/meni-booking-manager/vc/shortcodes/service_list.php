<?php
if (! class_exists ( 'DTBookingServiceList' ) ) {

    class DTBookingServiceList extends DTBaseBookingSC {

        function __construct() {

            add_shortcode( 'dt_sc_service_list', array( $this, 'dt_sc_service_list' ) );
			
			add_filter( 'vc_autocomplete_dt_sc_service_list_terms_callback', array ( $this, 'dt_booking_service_list_vc_autocomplete_terms_field_search' ), 10, 1 );
			add_filter( 'vc_autocomplete_dt_sc_service_list_terms_render', array ( $this, 'dt_booking_service_list_vc_autocomplete_terms_field_render' ), 10, 1 );
        }

		function dt_sc_service_list($attrs, $content = null ){
			extract( shortcode_atts( array(
				'terms' => '',
				'posts_per_page' => '',
				'orderby' => 'date',
				'order' => 'desc',
				'el_class' => ''
			), $attrs ) );
	
			$out = '';
	
			$categories = isset($terms) ? array_filter( explode(",", $terms) ) : array();
	
			$query_args = array();
			if( empty($categories) ):
				$query_args = array( 'posts_per_page' => $posts_per_page, 'orderby' => $orderby, 'order' => $order, 'post_status' => 'publish', 'post_type' => 'dt_service');
			else:
				$query_args = array(
					'post_type'           => 'dt_service',
					'post_status'         => 'publish',
					'posts_per_page'      => $posts_per_page,
					'orderby'             => $orderby,
					'order'               => $order,
					'tax_query' => array(
						array(
							'taxonomy' => 'dt_service_category',
							'field' => 'term_id',
							'operator' => 'IN',
							'terms' => $categories
						)
					)
				);
			endif;
	
			$the_query = new WP_Query($query_args);
			if ( $the_query->have_posts() ) :
	
				$out .= '<div class="dt-services-list '.esc_attr($el_class).'">';
	
					while ( $the_query->have_posts() ) : $the_query->the_post();
						$PID = get_the_ID();
	
						#Meta...
						$service_settings = get_post_meta($PID, '_custom_settings', true);
						$service_settings = is_array ( $service_settings ) ? $service_settings : array ();
	
						$out .= '<div class="dt-sc-service-item dt-sc-one-column column">';
							$out .= '<div class="image">';
								if(has_post_thumbnail()):
									$attr = array('title' => get_the_title(), 'alt' => get_the_title());
									$out .= get_the_post_thumbnail($PID, 'dt-bm-service-type2', $attr);
								else:
									$out .= '<img src="https://place-hold.it/205x205&text='.get_the_title().'" alt="'.get_the_title().'" />';
								endif;

								if( array_key_exists('service-price', $service_settings) && $service_settings['service-price'] != '' ):
									$out .= '<h4>'.dt_booking_get_currency_symbol().$service_settings['service-price'].'</h4>';
								endif;								
							$out .= '</div>';
	
							$out .= '<div class="service-details">';
								$out .= '<h3><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>';
	
								if( array_key_exists('service_opt_flds', $service_settings) ):
									$out .= '<div class="dt-sc-service-meta">';
										$out .= '<ul>';
											for($i = 1; $i <= (sizeof($service_settings['service_opt_flds']) / 2); $i++):

												$title = $service_settings['service_opt_flds']['service_opt_flds_title_'.$i];
												$value = $service_settings['service_opt_flds']['service_opt_flds_value_'.$i];

												if( !empty($value) ):
													$out .= '<li><strong>'.esc_html($title).'</strong> : '.esc_html($value).'</li>';
												endif;
											endfor;
										$out .= '</ul>';
									$out .= '</div>';
								endif;
							$out .= '</div>';

						$out .= '</div>';
					endwhile;

				$out .= '</div>';

				wp_reset_postdata();

			else:
				$out .= '<h2>'.esc_html__("Nothing Found.", "meni-booking-manager").'</h2>';
				$out .= '<p>'.esc_html__("Apologies, but no results were found for the requested archive.", "meni-booking-manager").'</p>';
			endif;

			return $out;
		}
    }
}

new DTBookingServiceList();