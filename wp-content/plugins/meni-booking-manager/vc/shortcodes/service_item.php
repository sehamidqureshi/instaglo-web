<?php
if (! class_exists ( 'DTBookingServiceItem' ) ) {

    class DTBookingServiceItem extends DTBaseBookingSC {

        function __construct() {

            add_shortcode( 'dt_sc_service_item', array( $this, 'dt_sc_service_item' ) );
        }
		
		function dt_sc_service_item($attrs, $content = null ){
			extract( shortcode_atts( array(
				'service_id' => '',
				'type' => 'type1',
				'excerpt' => 'no',
				'excerpt_length' => 12,
				'meta' => 'no',
				'button_text' => esc_html__('View procedure details', 'meni-booking-manager')
			), $attrs ) );

			$out = '';

			#Performing query...
			$args = array('post_type' => 'dt_service', 'post__in' => explode(',', $service_id) );

			$the_query = new WP_Query($args);
			if($the_query->have_posts()):
	
				while($the_query->have_posts()): $the_query->the_post();
					$PID = $service_id;
	
					#Meta...
					$service_settings = get_post_meta($PID, '_custom_settings', true);
					$service_settings = is_array ( $service_settings ) ? $service_settings : array ();
	
					$out .= '<div class="dt-sc-service-item '.$type.'">';
						$out .= '<div class="image">';
								if(has_post_thumbnail()):
									$attr     = array('title' => get_the_title(), 'alt' => get_the_title());
									$img_size = 'full';
									$out .= get_the_post_thumbnail($PID, $img_size, $attr);
								else:
									$img_pros = '615x560';
	
									if( $type == 'type2' ) {
										$img_pros = '205x205';
									}
									$out .= '<img src="https://place-hold.it/'.$img_pros.'&text='.get_the_title().'" alt="'.get_the_title().'" />';
								endif;
						$out .= '</div>';

						$out .= '<div class="service-details">';
							$out .= '<div class="dt-sc-service-info">';
								if( array_key_exists('service-duration', $service_settings) && $service_settings['service-duration'] != '' ):
									$out .= '<h6> <span>'.esc_html__('Duration : ', 'meni-booking-manager').'</span>'.dt_booking_duration_to_string($service_settings['service-duration']).'</h6>';
								endif;

								if( array_key_exists('service-price', $service_settings) && $service_settings['service-price'] != '' ):
									$out .= '<h4>'.dt_booking_get_currency_symbol().$service_settings['service-price'].'</h4>';
								endif;
							$out .= '</div>';

							if( $type == 'type1' ) {
								$out .= '<h3><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>';
							}

							if( $type == 'type2' ) {
								$out .= '<div class="dt-sc-service-title-meta-wrap">';
									$out .= '<h3><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>';
									$out .= '<div class="dt-sc-service-title-meta">';
										$out .= '<h3><a href="'.get_permalink().'" title="'.get_the_title().'">'.get_the_title().'</a></h3>';
										$out .= '<a class="dt-sc-button medium" href="'.get_permalink().'" title="'.get_the_title().'">'.esc_html($button_text).'</a>';
									$out .= '</div>';
								$out .= '</div>';
							}

							if( $excerpt == 'yes' && $excerpt_length > 0 ):
								$out .= dt_booking_post_excerpt($excerpt_length);
							endif;

							if( array_key_exists('service_opt_flds', $service_settings) && $meta == 'yes' ):
								$out .= '<div class="dt-sc-service-meta">';
									$out .= '<ul>';
										for($i = 1; $i <= (sizeof($service_settings['service_opt_flds']) / 2); $i++):

											$title = $service_settings['service_opt_flds']['service_opt_flds_title_'.$i];
											$value = $service_settings['service_opt_flds']['service_opt_flds_value_'.$i];

											if( !empty($value) ):
												$out .= '<li><strong>'.esc_html($title).' : </strong>'.esc_html($value).'</li>';
											endif;
										endfor;
									$out .= '</ul>';
								$out .= '</div>';
							endif;

							if( $type == 'type1' ) {
								$out .= '<a class="dt-sc-button medium filled black" href="'.get_permalink().'" title="'.get_the_title().'">'.esc_html($button_text).'</a>';
							}
	
						$out .= '</div>';
					$out .= '</div>';
				endwhile;

				wp_reset_postdata();

			else:
				$out .= '<h2>'.esc_html__("Nothing Found.", "meni-booking-manager").'</h2>';
				$out .= '<p>'.esc_html__("Apologies, but no results were found for the requested archive.", "meni-booking-manager").'</p>';
			endif;

			return $out;
		}
    }
}

new DTBookingServiceItem();