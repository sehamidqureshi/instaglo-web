<?php
	$plural_name = '';
	if( function_exists( 'dt_booking_cs_get_option' ) ) :
		$plural_name	=	dt_booking_cs_get_option( 'singular-service-text', esc_html__('Service', 'meni-booking-manager') );
	endif;

	vc_map( array(
		"name" => $plural_name,
		"base" => "dt_sc_service_item",
		"icon" => "dt_sc_service_item",
		"category" => esc_html__( 'Booking Manager', 'meni-booking-manager' ),
		"params" => array(

			# ID
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Enter Service ID", "meni-booking-manager" ),
				"param_name" => "service_id",
				"value" => '',
				"description" => esc_html__( 'Enter IDs of services to display. More than one ids with comma(,) seperated.', 'meni-booking-manager' ),
			),

			# Type
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Type','meni-booking-manager'),
				'param_name' => 'type',
				'value' => array(
					esc_html__('Type - 1','meni-booking-manager') => 'type1',
					esc_html__('Type - 2','meni-booking-manager') => 'type2'
				)
			),

			# Excerpt?
			array(
				'type' => 'dropdown',
				'param_name' => 'excerpt',
				'value' => array(
					esc_html__('Yes','meni-booking-manager') => 'yes',
					esc_html__('No','meni-booking-manager') => 'no'
				),
				'heading' => esc_html__( 'Show Excerpt?', 'meni-booking-manager' ),
				'std' => 'no'
			),

			# Excerpt Length
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Excerpt Length', 'meni-booking-manager' ),
				'param_name' => 'excerpt_length',
				'value' => 12
			),

			# Meta?
			array(
				'type' => 'dropdown',
				'param_name' => 'meta',
				'value' => array(
					esc_html__('Yes','meni-booking-manager') => 'yes',
					esc_html__('No','meni-booking-manager') => 'no'
				),
				'heading' => esc_html__( 'Show Meta?', 'meni-booking-manager' ),
				'std' => 'no'
			),

			# Button Text
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Button Text", "meni-booking-manager" ),
				"param_name" => "button_text",
				"value" => esc_html__('View procedure details', 'meni-booking-manager'),
				"description" => esc_html__( 'Enter button text.', 'meni-booking-manager' ),
			)
	     )
	) );