<?php
	vc_map( array(
		"name" => esc_html__( "Reserve Appointment", 'meni-booking-manager' ),
		"base" => "dt_sc_reserve_appointment",
		"icon" => "dt_sc_reserve_appointment",
		"category" => esc_html__( 'Booking Manager', 'meni-booking-manager' ),
		"description" => esc_html__("Show reserve appointment template.",'meni-booking-manager'),
		"params" => array(

			// Title
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', 'meni-booking-manager' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter title here.', 'meni-booking-manager' ),
				'std' => esc_html__('Make an Appointment', 'meni-booking-manager'),
				'admin_label' => true,
				'save_always' => true
			),

			// Type
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Type','meni-booking-manager'),
				'param_name' => 'type',
				'value' => array(
					esc_html__('Type - I','meni-booking-manager') => 'type1' ,
					esc_html__('Type - II','meni-booking-manager') => 'type2'
				),
				'std' => 'type1',
				'save_always' => true
			)
		)
	) );