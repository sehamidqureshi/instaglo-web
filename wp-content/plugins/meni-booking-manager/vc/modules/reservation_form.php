<?php
	vc_map( array(
		"name" => esc_html__( "Reservation Form", 'meni-booking-manager' ),
		"base" => "dt_sc_reservation_form",
		"icon" => "dt_sc_reservation_form",
		"category" => esc_html__( 'Booking Manager', 'meni-booking-manager' ),
		"description" => esc_html__("Show the reservation form.",'meni-booking-manager'),
		"params" => array(

			// Title
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', 'meni-booking-manager' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter title here.', 'meni-booking-manager' ),
				'std' => esc_html__('Appointment', 'meni-booking-manager'),
				'admin_label' => true,
				'save_always' => true
			),

			// Sub Title
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Sub Title', 'meni-booking-manager' ),
				'param_name' => 'sub_title',
				'description' => esc_html__( 'Enter sub title here.', 'meni-booking-manager' ),
				'std' => esc_html__('Office Hours : 07:30 and 19:00 Mon to Sat, Sun - Holiday', 'meni-booking-manager'),
				'admin_label' => true,
				'save_always' => true
			),			

			// Services
			array(
				'type' => 'autocomplete',
				'heading' => __( 'Service IDs', 'meni-booking-manager' ),
				'param_name' => 'serviceids',
				'settings' => array(
					'multiple' => true,
					'min_length' => 1,
					'groups' => true,
					'unique_values' => true,
					'display_inline' => true,
					'delay' => 500,
					'auto_focus' => true,
				),
				'param_holder_class' => 'vc_not-for-custom',
				'description' => __( 'Enter service name & pick.', 'meni-booking-manager' )
			),
			
			// Staffs
			array(
				'type' => 'autocomplete',
				'heading' => __( 'Staff IDs', 'meni-booking-manager' ),
				'param_name' => 'staffids',
				'settings' => array(
					'multiple' => true,
					'min_length' => 1,
					'groups' => true,
					'unique_values' => true,
					'display_inline' => true,
					'delay' => 500,
					'auto_focus' => true,
				),
				'param_holder_class' => 'vc_not-for-custom',
				'description' => __( 'Enter staff name & pick.', 'meni-booking-manager' )
			)
		)
	) );