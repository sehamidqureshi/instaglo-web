<?php
	$plural_name = '';
	if( function_exists( 'dt_booking_cs_get_option' ) ) :
		$plural_name	=	dt_booking_cs_get_option( 'singular-person-text', esc_html__('Person', 'meni-booking-manager') );
	endif;

	vc_map( array(
		"name" => $plural_name,
		"base" => "dt_sc_person_item",
		"icon" => "dt_sc_person_item",
		"category" => esc_html__( 'Booking Manager', 'meni-booking-manager' ),
		"params" => array(

			# ID
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Enter Person ID", "meni-booking-manager" ),
				"param_name" => "person_id",
				"value" => '',
				"description" => esc_html__( 'Enter ID of person to display.', 'meni-booking-manager' ),
			),

			# Type
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Type','meni-booking-manager'),
				'param_name' => 'type',
				'value' => array(
					esc_html__('Type - 1','meni-booking-manager') => 'type1',
					esc_html__('Type - 2','meni-booking-manager') => 'type2'
				)
			),

			# Button?
			array(
				'type' => 'dropdown',
				'param_name' => 'show_button',
				'value' => array(
					esc_html__('Yes','meni-booking-manager') => 'yes',
					esc_html__('No','meni-booking-manager') => 'no'
				),
				'heading' => esc_html__( 'Show button?', 'meni-booking-manager' ),
				'std' => 'no'
			),

			# Button Text
			array(
				"type" => "textfield",
				"heading" => esc_html__( "Button Text", "meni-booking-manager" ),
				"param_name" => "button_text",
				"value" => esc_html__('Book an appointment', 'meni-booking-manager'),
				"description" => esc_html__( 'Enter button text.', 'meni-booking-manager' ),
			)
	     )
	) );