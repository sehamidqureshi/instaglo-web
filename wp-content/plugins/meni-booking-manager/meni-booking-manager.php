<?php
/*
Plugin Name: Meni Booking Manager
Plugin URI: http://dev.themes-demo.com/meni/
Description: A simple wordpress plugin designed to implements reservation features.
Version: 1.1
Author: the DesignThemes team
Author URI: http://wedesignthemes.com/
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Text Domain: meni-booking-manager
*/
if (! class_exists ( 'DTBookingManager' )) {

	class DTBookingManager {

		function __construct() {

			add_action ( 'init', array($this, 'dtBookingManagerTextDomain') );

			define( 'DTBOOKINGMANAGER_PATH', dirname( __FILE__ ) );
			define( 'DTBOOKINGMANAGER_URL', untrailingslashit( plugins_url( '/', __FILE__ ) ) );

			register_activation_hook( __FILE__ , array( $this , 'dtBookingManagerActivate' ) );
			register_deactivation_hook( __FILE__ , array( $this , 'dtBookingManagerDeactivate' ) );

			// Include Functions
			require_once plugin_dir_path ( __FILE__ ) . '/functions/core-functions.php';
			require_once plugin_dir_path ( __FILE__ ) . '/functions/reservation-functions.php';
			require_once plugin_dir_path ( __FILE__ ) . '/functions/template-functions.php';

			// Register Custom Post Types
			require_once plugin_dir_path ( __FILE__ ) . '/post-types/register-post-types.php';
			if(class_exists( 'DTBookingManagerCustomPostTypes' )){
				new DTBookingManagerCustomPostTypes();
			}

			// Register Reservation System
			require_once plugin_dir_path( __FILE__ ).'/reservation/register-reservation-system.php';
			if (class_exists ( 'DTBookingManagerReservationSystem' )) {
				new DTBookingManagerReservationSystem();
			}

			// Register Templates
			require_once plugin_dir_path ( __FILE__ ) . '/templates/register-templates.php';
			if(class_exists('DTBookingManagerTemplates')){
				new DTBookingManagerTemplates();
			}

			// Register Visual Composer
			require_once plugin_dir_path ( __FILE__ ) . '/vc/register-vc.php';
			if(class_exists('DTBookingManagerVcModules')){
				new DTBookingManagerVcModules();
			}

			// Theme Support
			$this->dt_booking_theme_support_includes();
		}

		/**
		 * Load Text Domain
		 */
		function dtBookingManagerTextDomain() {
			load_plugin_textdomain ( 'meni-booking-manager', false, dirname ( plugin_basename ( __FILE__ ) ) . '/languages/' );
		}

		/**
		 * Check Plugin Is Active
		 */
		function dtBookingManagerIsPluginActive( $plugin ) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

			if( is_plugin_active( $plugin ) || is_plugin_active_for_network( $plugin ) ) return true;
			else return false;
		}

		/**
		 * Include Theme Suports
		 */
		function dt_booking_theme_support_includes() {

			include_once plugin_dir_path ( __FILE__ ) . '/theme-support/class-designthemes.php';
		}

		/**
		 * Custom Manager Activate
		 */
		public static function dtBookingManagerActivate() {
		}

		/**
		 * Custom Manager Deactivate
		 */
		public static function dtBookingManagerDeactivate() {
		}

	}

	new DTBookingManager();
}