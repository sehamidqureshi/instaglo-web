<?php
if ( !class_exists( 'DTCustomerPostType' ) ) {

	class DTCustomerPostType {

		function __construct() {
			add_action ( 'init', array (
				$this,
				'dt_init'
			) );
			
			add_filter ( "cs_metabox_options", array(
				$this,
				"dt_customers_cs_metabox_options"
			) );
		}

		function dt_init() {
			$labels = array(
				'name' => __('Customers', 'meni-booking-manager' ),
				'singular_name' => __('Customer', 'meni-booking-manager' ),
				'menu_name' => __('Calendar', 'meni-booking-manager' ),
				'add_new' => __('Add Customer', 'meni-booking-manager' ),
				'add_new_item' => __('Add New Customer', 'meni-booking-manager' ),
				'edit' => __('Edit Customer', 'meni-booking-manager' ),
				'edit_item' => __('Edit Customer', 'meni-booking-manager' ),
				'new_item' => __('New Customer', 'meni-booking-manager' ),
				'view' => __('View Customer', 'meni-booking-manager' ),
				'view_item' => __('View Customer', 'meni-booking-manager' ),
				'search_items' => __('Search Customers', 'meni-booking-manager' ),
				'not_found' => __('No Customers found', 'meni-booking-manager' ),
				'not_found_in_trash' => __('No Customers found in Trash', 'meni-booking-manager' ),
				'parent_item_colon' => __('Parent Customer:', 'meni-booking-manager' ),
			);

			$args = array(
				'labels' => $labels,
				'hierarchical' => false,
				'description' => __('This is Custom Post type named as Customers','meni-booking-manager'),
				'supports' => array('title'),
				'public' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				'show_in_admin_bar' => true,
				'menu_position' => 5,
				'menu_icon' => 'dashicons-clipboard',
			);

			register_post_type('dt_customers', $args );
		}

		function dt_customers_cs_metabox_options( $options ) {

			$options[]    = array(
			  'id'        => '_info',
			  'title'     => esc_html__('Customer Options', 'meni-booking-manager'),
			  'post_type' => 'dt_customers',
			  'context'   => 'normal',
			  'priority'  => 'default',
			  'sections'  => array(
			
				array(
				  'name'  => 'general_section',
			
				  'fields' => array(
			
					array(
					  'id'    => 'emailid',
					  'type'  => 'text',
					  'title' => esc_html__('Email Id', 'meni-booking-manager'),
					  'attributes' => array( 
						'placeholder' => 'testmail@domain.com'
					  )
					),

					array(
					  'id'    => 'phone',
					  'type'  => 'text',
					  'title' => esc_html__('Phone', 'meni-booking-manager'),
					  'attributes' => array( 
						'placeholder' => '022-65265'
					  )
					),

				  ), // end: fields
				), // end: a section

			  ),
			);

			return $options;
		}
	}
}