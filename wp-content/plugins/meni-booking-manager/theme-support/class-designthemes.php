<?php
if( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'DTBookingManagerDesignThemes' ) ) {

	class DTBookingManagerDesignThemes {

		function __construct() {

			add_filter( 'body_class', array( $this, 'dt_booking_dt_body_class' ), 20 );

			add_filter( 'dt_booking_template_metabox_options', array( $this, 'dt_booking_dt_template_metabox_options'), 10, 1 );
			add_filter( 'dt_booking_template_framework_options', array( $this, 'dt_booking_dt_template_framework_options'), 10, 1 );

			add_action( 'wp_enqueue_scripts', array( $this, 'dt_booking_dt_enqueue_styles' ), 104 );

			add_action( 'dt_booking_before_main_content', array( $this, 'dt_booking_dt_before_main_content' ), 10 );
			add_action( 'dt_booking_after_main_content', array( $this, 'dt_booking_dt_after_main_content' ), 10 );

			add_action( 'dt_booking_before_content', array( $this, 'dt_booking_dt_before_content' ), 10 );
			add_action( 'dt_booking_after_content', array( $this, 'dt_booking_dt_after_content' ), 10 );
		}

		function dt_booking_dt_body_class( $classes ) {

			return $classes;

		}

		function dt_booking_dt_template_metabox_options($options) {

			foreach($options as $option_key => $option) {

				if( $option['id'] == '_custom_settings' ) :

					$genetal_section = array(

					  'name'  => 'general_section',
					  'title' => esc_html__('General Options', 'meni-booking-manager'),
					  'icon'  => 'fa fa-cogs',
					  'fields' => array(

						  array(
							  'id'      => 'enable-sub-title',
							  'type'    => 'switcher',
							  'title'   => esc_html__('Show Breadcrumb', 'meni-booking-manager' ),
							  'default' => true
						  ),

						  array(
							  'id'	    => 'breadcrumb_position',
							  'type'    => 'select',
							  'title'   => esc_html__('Position', 'meni-booking-manager' ),
							  'options' => array(
								  'header-top-absolute'  => esc_html__('Behind the Header','meni-booking-manager'),
								  'header-top-relative'  => esc_html__('Default','meni-booking-manager'),
							  ),
							  'default'    => 'header-top-relative',
							  'dependency' => array( 'enable-sub-title', '==', 'true' ),
						  ), 

						  array(
							'id'    => 'breadcrumb_background',
							'type'  => 'background',
							'title' => esc_html__('Background', 'meni-booking-manager'),
							'desc'  => esc_html__('Choose background options for breadcrumb title section.', 'meni-booking-manager'),
							'dependency'  => array( 'enable-sub-title', '==', 'true' ),
						  ),

						  array(
							'id'      	 => 'layout',
							'type'       => 'image_select',
							'title'      => esc_html__('Layout', 'meni-booking-manager'),
							'options'    => array(
							  'content-full-width'   => DTBOOKINGMANAGER_URL . '/cs-framework-override/images/without-sidebar.png',
							  'with-left-sidebar'    => DTBOOKINGMANAGER_URL . '/cs-framework-override/images/left-sidebar.png',
							  'with-right-sidebar'   => DTBOOKINGMANAGER_URL . '/cs-framework-override/images/right-sidebar.png',
							),
							'default'    => 'content-full-width',
							'attributes' => array(
							  'data-depend-id' => 'layout',
							),
						  ),

						  array(
							'id'  		 => 'show-standard-sidebar-left',
							'type'  	 => 'switcher',
							'title' 	 => esc_html__('Show Standard Left Sidebar', 'meni-booking-manager'),
							'dependency' => array( 'layout', 'any', 'with-left-sidebar' ),
						  ),

						  array(
							'id'  		 => 'widget-area-left',
							'type'  	 => 'select',
							'title' 	 => esc_html__('Choose Widget Area - Left Sidebar', 'meni-booking-manager'),
							'class'		 => 'chosen',
							'options'    => meni_custom_widgets(),
							'attributes' => array(
							  'multiple'  	     => 'multiple',
							  'data-placeholder' => esc_attr__('Select Widget Areas', 'meni-booking-manager'),
							  'style' 	 => 'width: 400px;'
							),
							'dependency' => array( 'layout', 'any', 'with-left-sidebar' ),
						  ),

						  array(
							'id'  		 => 'show-standard-sidebar-right',
							'type'  	 => 'switcher',
							'title' 	 => esc_html__('Show Standard Right Sidebar', 'meni-booking-manager'),
							'dependency' => array( 'layout', 'any', 'with-right-sidebar' ),
						  ),
  
						  array(
							'id'  		 => 'widget-area-right',
							'type'  	 => 'select',
							'title' 	 => esc_html__('Choose Widget Area - Right Sidebar', 'meni-booking-manager'),
							'class'		 => 'chosen',
							'options'    => meni_custom_widgets(),
							'attributes' => array(
							  'multiple'	     => 'multiple',
							  'data-placeholder' => esc_attr__('Select Widget Areas', 'meni-booking-manager'),
							  'style' 	 => 'width: 400px;'
							),
							'dependency' => array( 'layout', 'any', 'with-right-sidebar' ),
						  ),

					  ), // end: fields
				  	); // end: a section

					if( $options[$option_key]['sections'][0]['name'] != 'general_section' ) :
						array_unshift($options[$option_key]['sections'], $genetal_section);
					endif;
				endif;
			}

			return $options;
		}

		function dt_booking_dt_template_framework_options($options) {

			foreach($options as $option_key => $option) {

				if( $option['name'] == 'booking-manager' ) :

					$sarchive_layouts = array(
					  array(
						'type'    => 'subheading',
						'content' => esc_html__( 'Service Archives Page Layout', 'meni-booking-manager' ),
					  ),

					  array(
						'id'      	 => 'service-archives-page-layout',
						'type'       => 'image_select',
						'title'      => esc_html__('Page Layout', 'meni-booking-manager'),
						'options'    => array(
						  'content-full-width'   => DTBOOKINGMANAGER_URL . '/cs-framework-override/images/without-sidebar.png',
						  'with-left-sidebar'    => DTBOOKINGMANAGER_URL . '/cs-framework-override/images/left-sidebar.png',
						  'with-right-sidebar'   => DTBOOKINGMANAGER_URL . '/cs-framework-override/images/right-sidebar.png',
						  'with-both-sidebar'    => DTBOOKINGMANAGER_URL . '/cs-framework-override/images/both-sidebar.png',
						),
						'default'     => 'content-full-width',
						'attributes'  => array(
						  'data-depend-id' => 'service-archives-page-layout',
						),
					  ),

					  array(
						'id'  		 => 'show-standard-left-sidebar-for-service-archives',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('Show Standard Left Sidebar', 'meni-booking-manager'),
						'dependency' => array( 'service-archives-page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
					  ),

					  array(
						'id'  		 => 'show-standard-right-sidebar-for-service-archives',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('Show Standard Right Sidebar', 'meni-booking-manager'),
						'dependency' => array( 'service-archives-page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
					  ), // end: fields
				  	); // end: a section

					if( $options[$option_key]['sections'][4]['name'] == 'service_options' && $options[$option_key]['sections'][4]['fields'][1]['id'] != 'service-archives-page-layout' ):
						array_unshift($options[$option_key]['sections'][4]['fields'], $sarchive_layouts[0], $sarchive_layouts[1], $sarchive_layouts[2], $sarchive_layouts[3] );
					endif;

					$parchive_layouts = array(
					  array(
						'type'    => 'subheading',
						'content' => esc_html__( 'Person Archives Page Layout', 'meni-booking-manager' ),
					  ),

					  array(
						'id'      	 => 'person-archives-page-layout',
						'type'       => 'image_select',
						'title'      => esc_html__('Page Layout', 'meni-booking-manager'),
						'options'    => array(
						  'content-full-width'   => DTBOOKINGMANAGER_URL . '/cs-framework-override/images/without-sidebar.png',
						  'with-left-sidebar'    => DTBOOKINGMANAGER_URL . '/cs-framework-override/images/left-sidebar.png',
						  'with-right-sidebar'   => DTBOOKINGMANAGER_URL . '/cs-framework-override/images/right-sidebar.png',
						  'with-both-sidebar'    => DTBOOKINGMANAGER_URL . '/cs-framework-override/images/both-sidebar.png',
						),
						'default'      => 'content-full-width',
						'attributes'   => array(
						  'data-depend-id' => 'person-archives-page-layout',
						),
					  ),

					  array(
						'id'  		 => 'show-standard-left-sidebar-for-person-archives',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('Show Standard Left Sidebar', 'meni-booking-manager'),
						'dependency' => array( 'person-archives-page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
					  ),

					  array(
						'id'  		 => 'show-standard-right-sidebar-for-person-archives',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('Show Standard Right Sidebar', 'meni-booking-manager'),
						'dependency' => array( 'person-archives-page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
					  ),
				  	); // end: a section

					if( $options[$option_key]['sections'][5]['name'] == 'person_options' ) :
						array_unshift($options[$option_key]['sections'][5]['fields'], $parchive_layouts[0], $parchive_layouts[1], $parchive_layouts[2], $parchive_layouts[3] );
					endif;
				endif;
			}

			return $options;
		}

		function dt_booking_dt_enqueue_styles() {

			wp_enqueue_style ( 'dt_booking-designthemes', plugins_url ('meni-booking-manager') . '/css/designthemes.css' );

		}

		function dt_booking_dt_before_main_content() {

			if ( is_singular( 'dt_service' ) || is_singular( 'dt_person' ) ) {

				global $post;

				$global_breadcrumb = cs_get_option( 'show-breadcrumb' );

			    $settings = get_post_meta($post->ID,'_custom_settings',TRUE);
			    $settings = is_array( $settings ) ?  array_filter( $settings )  : array();

			    $header_class = '';
			    if( !empty( $global_breadcrumb ) ) {
			        if( isset( $settings['enable-sub-title'] ) && $settings['enable-sub-title'] ) {
			            $header_class = $settings['breadcrumb_position'];
					}
				} ?>

				<div id="header-wrapper">
				    <header id="header" class="<?php echo esc_attr($header_class); ?>">
				        <div class="container">
				        	<?php do_action( 'meni_header' ); ?>
				        </div>
				    </header><?php

					if( !empty( $global_breadcrumb ) ) {

						if(empty($settings)) { $settings['enable-sub-title'] = true; }

			            if( isset( $settings['enable-sub-title'] ) && $settings['enable-sub-title'] ) {

			                $breadcrumbs = array();
			                $bstyle = meni_cs_get_option( 'breadcrumb-style', 'default' );
							$separator = '<span class="'.meni_cs_get_option( 'breadcrumb-delimiter', 'fa default' ).'"></span>';

			                $tax = '';
							if( is_singular( 'dt_service' ) )
								$tax = 'dt_service_category';
							elseif( is_singular( 'dt_person' ) )
								$tax = 'dt_person_department';

							$cat = get_the_term_list( $post->ID, $tax, '', '$$$', '');
							$cats = array_filter(explode('$$$', $cat));
							if (!empty($cats))
								$breadcrumbs[] = $cats[0];

			                $breadcrumbs[] = the_title( '<span class="current">', '</span>', false );
							$bcsettings = isset( $settings['breadcrumb_background'] ) ? $settings['breadcrumb_background'] : array ();
			                $style = meni_breadcrumb_css( $bcsettings );

			                meni_breadcrumb_output ( the_title( '<h1>', '</h1>',false ), $breadcrumbs, $bstyle, $style );
			    		}

			    	} ?>
				</div><?php
			}

			if( is_post_type_archive('dt_service') || is_tax ( 'dt_service_category' ) || is_post_type_archive('dt_person') || is_tax ( 'dt_person_department' ) ) {

				$global_breadcrumb = cs_get_option( 'show-breadcrumb' );
				$header_class	   = cs_get_option( 'breadcrumb-position' ); ?>

				<div id="header-wrapper">

					<header id="header" class="<?php echo esc_attr($header_class); ?>">
						<div class="container">
							<?php do_action( 'meni_header' ); ?>
					    </div>
					</header><?php

			        if( !empty( $global_breadcrumb ) ) {

			        	$bstyle = meni_cs_get_option( 'breadcrumb-style', 'default' );
			        	$style = meni_breadcrumb_css();

			            $title = '<h1>'.get_the_archive_title().'</h1>';
			            $breadcrumbs = array();

			            if ( is_category() ) {
			                $breadcrumbs[] = '<a href="'. get_category_link( get_query_var('cat') ) .'">' . single_cat_title('', false) . '</a>';
			            } elseif ( is_tag() ) {
			                $breadcrumbs[] = '<a href="'. get_tag_link( get_query_var('tag_id') ) .'">' . single_tag_title('', false) . '</a>';
			            } elseif( is_author() ){
			                $breadcrumbs[] = '<a href="'. get_author_posts_url( get_the_author_meta( 'ID' ) ) .'">' . get_the_author() . '</a>';
			            } elseif( is_day() || is_time() ){
			                $breadcrumbs[] = '<a href="'. get_year_link( get_the_time('Y') ) . '">'. get_the_time('Y') .'</a>';
			                $breadcrumbs[] = '<a href="'. get_month_link( get_the_time('Y'), get_the_time('m') ) .'">'. get_the_time('F') .'</a>';
			                $breadcrumbs[] = '<a href="'. get_day_link( get_the_time('Y'), get_the_time('m'), get_the_time('d') ) .'">'. get_the_time('d') .'</a>';
			            } elseif( is_month() ){
			                $breadcrumbs[] = '<a href="'. get_year_link( get_the_time('Y') ) . '">' . get_the_time('Y') . '</a>';
			                $breadcrumbs[] = '<a href="'. get_month_link( get_the_time('Y'), get_the_time('m') ) .'">'. get_the_time('F') .'</a>';
			            } elseif( is_year() ){
			                $breadcrumbs[] = '<a href="'. get_year_link( get_the_time('Y') ) .'">'. get_the_time('Y') .'</a>';
			            }

			            meni_breadcrumb_output ( $title, $breadcrumbs, $bstyle, $style );
			        } ?>
				</div><?php
			}
		}

		function dt_booking_dt_after_main_content() {

		}

		function dt_booking_dt_before_content() {

			$post_type = '';
			if( is_singular( 'dt_service' ) )
				$post_type = 'dt_service';
			elseif( is_singular( 'dt_person' ) )
				$post_type = 'dt_person';

			if ( $post_type ) {

				echo '<div id="main">';
 
					global $post;
	
					$settings = get_post_meta($post->ID,'_custom_settings',TRUE);
					$settings = is_array( $settings ) ?  array_filter( $settings )  : array();
	
					echo '<div class="container">';
	
						$page_layout  = array_key_exists( "layout", $settings ) ? $settings['layout'] : "content-full-width";
						$layout = meni_page_layout( $page_layout );
						extract( $layout );
		
						if ( $show_sidebar ) {
							if ( $show_left_sidebar ) { ?>
								<section id="secondary-left" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php
									meni_show_sidebar( $post_type, $post->ID, 'left' ); ?>
								</section><?php
							}
						} ?>
		
						<section id="primary" class="<?php echo esc_attr( $page_layout );?>"><?php
			}

			$archive_type = '';
			if( is_post_type_archive('dt_service') || is_tax ( 'dt_service_category' ) )
				$archive_type = 'service';
			elseif( is_post_type_archive('dt_person') || is_tax ( 'dt_person_department' ) )
				$archive_type = 'person';

			if( $archive_type ) {

				echo '<div id="main">';

				    echo '<div class="container">';

						$page_layout  = cs_get_option( $archive_type.'-archives-page-layout' );
						$page_layout  = !empty( $page_layout ) ? $page_layout : "content-full-width";

						if($page_layout != 'content-full-width') {
	
							$layout = meni_page_layout( $page_layout );
							extract( $layout );

							if ( $show_sidebar ) {
								if ( $show_left_sidebar ) { ?>
									<section id="secondary-left" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php
										$wtstyle = cs_get_option( 'wtitle-style' );
										echo !empty( $wtstyle ) ? "<div class='{$wtstyle}'>" : '';

										if( is_active_sidebar('custom-post-'.$archive_type.'-archives-sidebar-left') ):
											dynamic_sidebar('custom-post-'.$archive_type.'-archives-sidebar-left');
										endif;

										$enable = cs_get_option( 'show-standard-left-sidebar-for-'.$archive_type.'-archives' );
										if( $enable ):
											if( is_active_sidebar('standard-sidebar-left') ):
												dynamic_sidebar('standard-sidebar-left');
											endif;
										endif;

										echo !empty( $wtstyle ) ? '</div>' : ''; ?>
									</section><?php
								}
							} ?>

							<section id="primary" class="<?php echo esc_attr( $page_layout );?>"><?php
						}
			}
		}

		function dt_booking_dt_after_content() {

			$post_type = '';
			if( is_singular( 'dt_service' ) )
				$post_type = 'dt_service';
			elseif( is_singular( 'dt_person' ) )
				$post_type = 'dt_person';

			if ( $post_type ) {

					echo '</section>';

			    	global $post;

				    $settings = get_post_meta($post->ID,'_custom_settings',TRUE);
				    $settings = is_array( $settings ) ?  array_filter( $settings )  : array();

			        $page_layout  = array_key_exists( "layout", $settings ) ? $settings['layout'] : "content-full-width";
			        $layout = meni_page_layout( $page_layout );
			        extract( $layout );

			        if ( $show_sidebar ) {
			            if ( $show_right_sidebar ) { ?>
			                <section id="secondary-right" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php
			                    meni_show_sidebar( $post_type, $post->ID, 'right' ); ?>
			                </section><?php
			            }
			        }

					echo '</div>';

				echo '</div>';
			}

			$archive_type = '';
			if( is_post_type_archive('dt_service') || is_tax ( 'dt_service_category' ) )
				$archive_type = 'service';
			elseif( is_post_type_archive('dt_person') || is_tax ( 'dt_person_department' ) )
				$archive_type = 'person';

			if( $archive_type ) {

			    	$page_layout  = cs_get_option( $archive_type.'-archives-page-layout' );
			    	$page_layout  = !empty( $page_layout ) ? $page_layout : "content-full-width";

					if($page_layout != 'content-full-width') {

						$layout = meni_page_layout( $page_layout );
						extract( $layout );

						echo '</section>';

						if ( $show_sidebar ) {
							if ( $show_right_sidebar ) { ?>
								<section id="secondary-right" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php
									$wtstyle = cs_get_option( 'wtitle-style' );
									echo !empty( $wtstyle ) ? "<div class='{$wtstyle}'>" : '';

									if( is_active_sidebar('custom-post-'.$archive_type.'-archives-sidebar-right') ):
										dynamic_sidebar('custom-post-'.$archive_type.'-archives-sidebar-right');
									endif;

									$enable = cs_get_option( 'show-standard-right-sidebar-for-'.$archive_type.'-archives' );
									if( $enable ):
										if( is_active_sidebar('standard-sidebar-right') ):
											dynamic_sidebar('standard-sidebar-right');
										endif;
									endif;

									echo !empty( $wtstyle ) ? '</div>' : ''; ?>
                    			</section><?php
							}
						}
					}

				    echo '</div>';

				echo '</div>';
		    }
		}
	}

	new DTBookingManagerDesignThemes();
}