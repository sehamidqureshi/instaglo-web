=== Ultimate Gift Cards For WooCommerce ===
Contributors: MakeWebBetter
Donate link:  https://makewebbetter.com/
Tags: gift cards, gift certificates, gift voucher, gift card, woocommerce, gift, gift certificates for woocommerce, gift card for woocommerce, woocommerce gift card, gift card plugin for woocommerce, woocommerce gift cards
Requires at least: 4.0
Tested up to: 5.4
WC requires at least: 3.0.0
WC tested up to: 4.0
Stable tag: 2.0.2
Requires PHP: 5.6
License: GPLv3 or later 
License URI: http://www.gnu.org/licenses/gpl-3.0.html

It allows the merchants to sell and manage the use of Gift Cards for WooCommerce stores to provide better user experience.

== Description ==

*You can sell a gift card like any other product on your WooCommerce store!*
 

***Ultimate Gift Cards For WooCommerce*** plugin will add Gift card product as a new product type and manage everything from the selling of Gift Cards to its use. This comes with many gift templates that fit with every occasion like Christmas, Valentine’s Day, birthday and many more.

***Start selling gift cards on your WooCommerce stores in 3 steps.***

<ol>
<li><strong>Start</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Create a gift card product</li>
<li><strong>Modeling</strong>&nbsp;&nbsp;&nbsp;&nbsp;Save your configuration </li>
<li><strong>Showcase</strong>&nbsp;&nbsp;&nbsp;List gift cards products for customers</li>
</ol>


== What makes this plugin the best WooCommerce gift card plugin? ==

* One click gift card product creation as well as many options for customizing according to need makes this setup ***Effortless***

* Easy to purchase Gift Cards, customers need to specify the ***recipient***, ***message*** and ***amount*** when purchasing

* Gift cards touches the actual needs of the customers. Reflects the ***balance*** and ***no. of usages left*** after redemption. For retail stores, facilities available to redeem physical gift cards

* Customers can get Delivery of Gift Cards through email. Plugin offering gift cards predefined email templates layout in free as per event

* Compatible with ***Price Based on Country for WooCommerce*** plugin which enables customers to place an order in their own currency

* 4 types of Pricing Options available - ***Default price***, ***Price Range***, ***Selected Price*** and ***User Price*** in this Ultimate Gift cards plugin

* Store owners can ***ask customers to pay a tax*** on gift card products like other WooCommerce Products

* Shop owners can showcase Gift Card products on the ***shop page*** as well as on a ***separate page***

* Use ***Gift Cards Individually*** , this allows your customers to use gift cards individually. So that the Gift Card coupon cannot be used in conjunction with other coupons

* For gift card coupon several customization options available to -
	&nbsp;&nbsp;&nbsp;&nbsp;1.) Set the coupon code length
	&nbsp;&nbsp;&nbsp;&nbsp;2.) Add a prefix in the Gift Card coupon codes
	&nbsp;&nbsp;&nbsp;&nbsp;3.) Set expiry date of the coupon code
	&nbsp;&nbsp;&nbsp;&nbsp;4.) Set no. of usage before Gift Card is void
	&nbsp;&nbsp;&nbsp;&nbsp;5.) Set least and most amount spent allowed to use the Gift Card Coupons
	
* Predefined email templates are available and ***it can be customized*** according to need

* Shop owners can also ***exclude some products and categories*** if they don’t want to sell them using Gift Cards

&nbsp;&nbsp;&nbsp;&nbsp;[***Free demo of Ultimate Gift Cards for WooCommerce***](https://demo.makewebbetter.com/ultimate-woocommerce-giftcard-lite/product/makewebbetter-gift-card/?utm_source=MWB-giftcard-org&utm_medium=MWB-ORG-Page&utm_campaign=freeDemo)

== Plugin meets your requirement, So ==

&nbsp;&nbsp;&nbsp;&nbsp;[**Download Now**](https://downloads.wordpress.org/plugin/woo-gift-cards-lite.zip)

&nbsp;&nbsp;&nbsp;&nbsp;Visit [**Ultimate Gift Cards for WooCommerce Documentation**](http://docs.makewebbetter.com/woocommerce-gift-cards-lite/?utm_source=MWB-giftcard-org&utm_medium=MWB-ORG-Page&utm_campaign=pluginDoc) and take help to configure it.


== What premium version Ultimate WooCommerce Gift Cards offers ==

Try something out of the box and allow your customers to give surprises to their loved ones in a unique way with our **Ultimate WooCommerce Gift Cards Premium Version**
> Note:// Take advantage of the exclusive features for your [**Ultimate WooCommerce gift cards with the Premium Version**](https://makewebbetter.com/product/giftware-woocommerce-gift-cards/?utm_source=mwb-giftcard-org&utm_medium=mwb-org&utm_campaign=giftcard-org) 

**Generate PDF Format**
The customers can generate Gift Cards in PDF format and share with their dear ones

**Gallery for Gift Card**
Create an Image Gallery for Gift Cards. The user can choose an image from the gallery of their own choice and customize gift card as per need 

**Scheduling**
Allow customers to schedule the delivery of gift cards on their special occasions

**Import/Export Gift Coupons**
WooCommerce gift cards plugin finds an easy way for importing pre-defined coupons without any expert help. Here, you can also export gift coupons

 
**Multiple delivery options**
Shop owners have an option to provide one method for delivery or multiple

Here are some options this plugin covers -
&nbsp;&nbsp;&nbsp;&nbsp;1.)&nbsp;&nbsp; **Email To Recipient**
&nbsp;&nbsp;&nbsp;&nbsp;2.)&nbsp;&nbsp; **Direct Download**
&nbsp;&nbsp;&nbsp;&nbsp;3.)&nbsp;&nbsp; **Shipping** &nbsp; ( *In this feature gift card email is sent to the admin and admin will deliver the gift card to the shipping address of the recipient* )
&nbsp;&nbsp;&nbsp;&nbsp;4.)&nbsp;&nbsp; **Allow Customers**  *to choose any delivery method mentioned above*


**Bonus for your sales team**
Analyze Gift Card Performance via Reporting and enable your sales team to take timely follow-ups. Each detail of the gift card sold like the amount credited, balance remaining, scheduled expiry date is very beneficial for the team


**Provides Integration with Twilio**
Twilio integration empowers the user to share the gift cards via SMS notifications

**Share with  Whatsapp**
Buyer can share the gift card to their whatsapp contacts with predefined messages

== Live Demo of Premium version ==

[**Ultimate WooCommerce Gift Card Frontend demo**](https://demo.makewebbetter.com/giftware-woocommerce-gift-cards/?utm_source=MWB-giftcard-org&utm_medium=MWB-ORG-Page&utm_campaign=frontend)
[**Ultimate WooCommerce Gift Card Backend demo**](http://demo.makewebbetter.com/woocommerce-ultimate-gift-cards/request-for-personal-demo/?utm_source=MWB-giftcard-org&utm_medium=MWB-ORG-Page&utm_campaign=backend)
[**Gift Card Redeem/Recharge demo**](https://demo.makewebbetter.com/giftware-woocommerce-gift-cards/redeem-recharge-gift-card-vouchers/?utm_source=MWB-giftcard-org&utm_medium=MWB-ORG-Page&utm_campaign=redeem)


== Don’t hesitate to reach us! ==

If you need support or have any question then kindly use our online chat window [here](https://makewebbetter.com/?utm_source=MWB-giftcard-org&utm_medium=MWB-org-page&utm_campaign=MWB-giftcard-org) or connect with us then [**Generate a Ticket**](https://makewebbetter.freshdesk.com/support/tickets/new)



== MORE ABOUT MAKEWEBBETTER ==

* [**Our Official Website**](https://makewebbetter.com/?utm_source=MWB-giftcard-org&utm_medium=MWB-org-page&utm_campaign=MWB-giftcard-org)
* [**Follow us on Facebook Page**](https://www.facebook.com/makewebbetter)
* [**Tweet us on @MakeWebBetter**](https://twitter.com/makewebbetter)
* [**Visit our LinkedIn Account**](https://www.linkedin.com/company/makewebbetter)
* [**Subscribe To Our YouTube Channel**](https://www.youtube.com/channel/UC7nYNf0JETOwW3GOD_EW2Ag)
* [**Follow Our SlideShare**](https://www.slideshare.net/MakeWebBetter)
== Installation ==

= Automatic installation =

Automatic installation is the easiest option as WordPress handles the file transfers itself and you don't need to leave your web browser. To do an automatic install of gift voucher, log in to your WordPress dashboard, navigate to the Plugins menu and click Add New.

In the search field type "Ultimate Gift Cards For WooCommerce" and click Search Plugins. Once you've found our plugin you can view details about it such as the point release, rating and description. Most importantly, of course, you can install it by simply clicking "Install Now".

= Manual installation =

The manual installation method involves downloading our Ultimate Gift Cards For WooCommerce and uploading it to your webserver via your favorite FTP application. The WordPress contains [instructions on how to do this here](https://wordpress.org/support/article/managing-plugins/#manual-plugin-installation).

= Updating =

Automatic updates should work like a charm as always though, ensure you backup your site just in case.


== Frequently Asked Questions ==

= How would I allow customers to select gift card product prices on their own? =

This plugin provides you 4 types of pricing options i.e, Default price, Price Range, Selected Price and User Price .

**Default Price** allows you to set the price by default and is not customizable by the customer.
**Price Range** provides a range of prices so that the customer can select it’s price between that range.
**Selected Price** allows your customer to select the price from the list of options which have been set from that particular product edit page.
**User Price** allows your customer to enter the price amount of his choice.

= How will I disable the WooCommerce coupon on gift card type products? =
You can achieve this, first reach to Settings of this plugin. Other Setting Tab -> enable the first setting inside this tab named as “Disable Apply Coupon Fields”

= Where do my customers' gift cards get delivered?=
At the time of purchase your customer gets the option to enter the email id recipient so the same email id gift card gets delivered.

= Can I let my customers schedule their Gift Cards for a particular event date? =
This feature can be easily achieved by our [Gift Card Pro version](https://makewebbetter.com/product/giftware-woocommerce-gift-cards/?utm_source=mwb-giftcard-org&utm_medium=mwb-org&utm_campaign=giftcard-org) plugin.

= How will I edit gift cards? =
Plugins comes with lots of features to edit gift cards and you can completely customize according to your need. Options available to edit the design, delivery method, pricing setting etc., from settings dashboard. 


= Can I let my customers schedule their Gift Card for a particular event date? =

This feature can be easily achieved by our paid version of [Gift Card plugin](https://makewebbetter.com/product/giftware-woocommerce-gift-cards/?utm_source=mwb-giftcard-org&utm_medium=mwb-org&utm_campaign=giftcard-org) .

= Can my customers get a pdf attachment with their Gift Card. =

For this feature please visit our [premium version](https://makewebbetter.com/product/giftware-woocommerce-gift-cards/?utm_source=mwb-giftcard-org&utm_medium=mwb-org&utm_campaign=giftcard-org) of plugin. There you may find many delivery methods for Gift Cards along with PDF attachments.


= Is there any way to create our own custom templates for Gift Cards? =

Yes, this feature is easily available in the premium version, where we have provided 20 email templates. All can be easily customized as well as you may create your own custom email templates.


= How can I redeem gift cards at retail stores like restaurants/spa? =

For redeeming gift cards we have provided an online panel. Whenever a customer comes to your retail store and provides a gift coupon. You just have to scan/fetch the coupon and be marked as redeemed. You can also check the [demo](https://demo.makewebbetter.com/giftware-woocommerce-gift-cards/) once.

= Is your plugin is WPML Compatible and support multiple languages? =

The system is fully compatible with WPML Plugin. We also come with multiple languages supported .po and .mo files like for Germany, France, Spanish and English.

== Changelog ==

= 2.0.2 Released on 11 April 2020 =
* New: Compatibility with WooCommerce 4.0 and WordPress 5.4

= 2.0.1 =
* Fix: Minor issues

= 2.0.0 =
* Tweak: Major code updated for standards and security
* Tweak: Layout of Settings
* New: Gift Card template per product

= 1.2.3 =
* Fix: minor issues

= 1.2.2 =
* New: Christmas template
* Fix: Preview issue

= 1.2.1 =
* Stable version
* Bug fixes

= 1.2.0 =
* New: Compatible with Price Based on Country for WooCommerce
* New: Redeem/Recharge Gift Cards on your Retail Store 

= 1.1.0 =
* New: Disable Apply Coupon Fields   
* New: Downloadable Delivery Method
* New: Custom Email Template
* New: Exclude Product Setting 

= 1.0.2 =
* GDPR compliance for Data Access, Data Erasure
* Preview for Email Templates at Frontend

= 1.0.1 =
New: Email Template for Mothers Day

= 1.0.0 =
* First version 

== Screenshots ==

1. **General Settings** - You can enable gift card plugin along with other features.
2. **Product settings** - You can exclude the products categories for the Gift Cards.
3. **Email Template** - The merchant can manage their email templates with the giving settings and shortcodes.
4. **Product Edit Page** - You can create a new product in WooCommerce by selecting Gift Card as product type
5. **Product Page** - This is Gift Card product page layout. Customer can purchase it by filling the required details.
6. **Selected Price** - This setting helps your customer to select the price from the provided list of options.
7. **Default Price** -  This setting defines the Gift Card Price by default and is not customizable by the customer.
8. **Gift Card Template Listing** - You can also edit the Gift card existing template and provide your own layout to the customers.


== Upgrade Notice ==

= 2.0.2 Released on 11 April 2020 =
* New: Compatibility with WooCommerce 4.0 and WordPress 5.4
