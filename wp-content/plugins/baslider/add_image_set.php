<?php
// BASlider Image Set
function baslider_admin_page() {
	global $message;
?>
<div class="wrap bas-admin-wrap">

    <?php
    if(isset($_GET["edit"]))
	{
    	$slider_name=$_GET['edit'];
    }else{
		$slider_name='slider1';
	}
	?>

	<div class="baslider-heading">
	   	<div id="icon-baslider" class="icon32"></div>
	    <h2>
		<?php
			echo $slider_name . ' &raquo; ';
			_e("Add / Modify Images",'baslider');
		?>
	    </h2>
    </div>

    <div class="msg"></div>

    <div class="baslider-cta">
	        <div class="baslider-cta-shortcode">
	            <h4><?php _e('Shortcode &mdash; ', 'baslider'); ?>
	            	<input type='text' id="shortcode" readonly='readonly' onclick="select();" value='[baslider name="<?php echo esc_attr( $slider_name ); ?>"]'/></h4>
	        	<script type="text/javascript">
					var $mbas5 = jQuery.noConflict();
					var w = $mbas5("#shortcode").val().length;
					var width = (w + 1) * 7 + 'px';
					$mbas5("#shortcode").css('width',width);
				</script>
	        </div>
	        <div class="baslider-cta-button upload-set">
	        <button type="button" class="button button-primary button-large" onclick="window.location='admin.php?page=baslider-settings&setting=<?php echo esc_attr( $slider_name );?>';"><?php _e('Slider Settings', 'baslider') ?></button>
	        </div>
    </div>


<div class="baslider-main-meta">
	<div class="metabox-holder baslider-first-meta">
	    <div class="postbox">
	        <h3><?php _e("Before After Sets in ".$slider_name.":", 'baslider'); ?></h3>
	        <div class="inside sets">
			<?php
				$slider_array = get_option($slider_name);
				$no = count($slider_array);
				if(!empty($slider_array))
				{
					echo "<table width='100%'>";
					$n = 1;
					foreach($slider_array as $set => $slider)
					{
						if(is_numeric($slider['before'])){
							$slider_before_img = wp_get_attachment_image_src($slider['before'],'full');
							$slider_before_img = $slider_before_img[0];
						}
						else
							$slider_before_img = $slider['before'];

						if(is_numeric($slider['after']))
						{
							$slider_after_img = wp_get_attachment_image_src($slider['after'],'full');
							$slider_after_img = $slider_after_img[0];
						}
						else
							$slider_after_img = $slider['after'];
					?>
	                <thead>
						<tr class="<?php echo $set;?>">
	                    	<td colspan="3">
	                        <h4 id="cap_<?php echo $set; ?>"><?php echo stripslashes($slider['caption']);?></h4></td>
						</tr>
	                </thead>
	                <tbody>
						<tr class="<?php echo esc_attr( $set );?> baslider-table-seperator">
	                    	<td width=33%>
	                            <img id="img1_<?php echo esc_attr( $set );?>" src="<?php echo esc_url( $slider_before_img );?>">
	                            <input type="text" readonly="readonly" id="bcap_<?php echo $set ?>" value="<?php echo (isset($slider['caption_before']) && $slider['caption_before'] != '') ? stripslashes($slider['caption_before']) : 'Before'; ?>"/>
	                         </td>
							<td width=33%>
	                            <img id="img2_<?php echo esc_attr( $set );?>" src="<?php echo esc_url( $slider_after_img ); ?>">
	                            <input type="text" readonly="readonly" id="acap_<?php echo $set ?>" value="<?php echo (isset($slider['caption_after']) && $slider['caption_after']!= '') ? stripslashes($slider['caption_after']) : 'After'; ?>"/>
	                        </td>
							<td width=33%>
								<div class="edit">
	                            	<button onclick="editSet('<?php echo esc_attr( $set ); ?>'); return false;" class="button-primary edit-btn"><?php _e('Edit', 'baslider'); ?></button>
									<button onclick="deleteSet('<?php echo $set."','".$slider_name."'"; ?>);" class="button-secondary delete-btn"><?php _e('Delete', 'baslider'); ?></button>
	                                <span id="<?php echo esc_attr( $set ); ?>" ></span>
	                            </div>
								<div class="update">
	                            	<button onclick="return false;" class="button-primary cancel-btn"><?php _e('Cancel', 'baslider'); ?></button>
								</div>
							</td>
						</tr>
					</tbody>
				<?php
					$n++;
					}
					echo "</table>";
				}
			?>
	        </div>
	   </div>
	</div>

	<div class="metabox-holder baslider-second-meta">
		<!-- Upload new set -->
	    <div id="new-set" class="postbox">
			<form id="new_set" method="post" action="">
	        <h3><?php _e("Upload New Set", 'baslider'); ?></h3>
	        <div class="inside">
	        <p><?php _e("Please make sure all images you upload in same slider are of same dimentions", 'baslider'); ?></p>
	        <div class="set-uploader">
	        <table width="100%">
	            <tr class="baslider-second-caption">
	            	<td width=50%>
	            		<strong><?php _e("Caption", 'baslider'); ?>: </strong>
	            	</td>
	                <td>
	                    <input type="text" name="caption" value="" id="slider_name_"/>
	                </td>
				</tr>
	            <tr class="baslider-second-seperator"></tr>
	            <tr>
	            	<td width=50%>
	                	<input id="bthumb" type="hidden" name="before" value="" />
			            <label><strong><?php _e("Before Image:","baslider"); ?> </strong></label>
					</td>
	            	<td>
	            		<input type="button" id="bthumb" class="button-primary thumb-before" name="<?php echo esc_attr( $slider_name );?>_before" value="Upload" />
	              	</td>
				</tr>
	            <tr> <td colspan="2"> <div id="img-thumb-before"></div> </td> </tr>
	            <tr class="baslider-second-before-title">
	            	<th><?php echo _e('Title','baslider'); ?></th>
	                <td><input type="text" name="caption_before" value="Before" /></td>
	            </tr>
	            <tr class="baslider-second-seperator"></tr>
	            <tr>
	            	<td width=50%>
	                	<input id="athumb" type="hidden" name="after" value="" />
			            <label><strong><?php _e("After Image:","baslider"); ?> </strong></label>
	                </td>
	            	<td>
	                	<input type="button" id="athumb" class="button-primary thumb-after" name="<?php echo esc_attr( $slider_name );?>_after" value="Upload" />
	                </td>
	            </tr>
	            <tr><td colspan="2"><div id="img-thumb-after"></div></td></tr>
	            <tr class="baslider-second-after-title">
	            	<th><?php echo _e('Title','baslider'); ?></th>
	                <td><input type="text" name="caption_after" value="After" /></td>
	            </tr>
	            <tr class="baslider-second-seperator"></tr>
	            <tr><td align="right" colspan="2">
	            	<input type="hidden" name="upload_set" value="1" />
	                <input type="hidden" name="slider_name" value="<?php echo esc_attr( $slider_name ); ?>" />
	                <span id="uploading"></span>
	            	<button id="upload_bt_"onclick="uploadSet(); return false;" class="button-primary button"><?php _e('Upload Set', 'baslider') ?></button> </td>
	            </tr>
			</table>
	        </div>
	        </div>
		    </form>
	    </div> <!-- End new set -->

		<!-- Edit Set Box -->
	    <div id="edit-set" class="postbox">
			<form id="edit_set" method="post" action="">
	        <h3  style="line-height: 25px;"><?php _e("Edit Set", 'baslider'); ?></h3>
	        <div class="inside">
	        <p style="color: #E25757;"><?php _e("Please make sure all images you upload in same slider are of same dimentions", 'baslider'); ?></p>
	        <div class="set-uploader">
	        <table width="100%">
	            <tr><td><strong><?php _e("Caption", 'baslider'); ?>: </strong></td>
	                <td>
		                <input id="set" type="hidden" name="set" value=""/>
	                    <input id="edit_cap" type="text" name="caption" value=""/>
	                </td>
				</tr>
	            <tr class="baslider-second-seperator"></tr>
	            <tr>
	            	<td>
	                	<input id="edit_bthumb" type="hidden" name="before" value="" />
			            <label><strong><?php _e("Before Image:","baslider"); ?> </strong></label>
					</td>
	            	<td>
	            		<input type="button" id="edit_bthumb" class="button-primary edit-before" name="<?php echo esc_attr( $slider_name );?>_before" value="Upload" />
					</td>
				</tr>
	            <tr><td colspan="2"  style="text-align: center;"> <div id="edit-thumb-before"></div></td></tr>
	            <tr>
	            	<th><?php echo _e('Title','baslider'); ?></th>
	                <td><input type="text" id="edit_caption_before" name="caption_before" value="Before" /></td>
	            </tr>
	            <tr class="baslider-second-seperator"></tr>
	            <tr>
	            	<td>
	                	<input id="edit_athumb" type="hidden" name="after" value="" />
			            <label><strong><?php _e("After Image:","baslider"); ?> </strong></label>
	                </td>
	            	<td>
	                	<input type="button" id="edit_athumb" class="button-primary edit-after" name="<?php echo esc_attr( $slider_name );?>_after" value="Upload" />
	                </td>
	            </tr>
	            <tr> <td colspan="2"  style="text-align: center;"><div id="edit-thumb-after"></div> </td> </tr>
	            <tr>
	            	<th><?php echo _e('Title','baslider'); ?></th>
	                <td><input type="text" id="edit_caption_after" name="caption_after" value="After" /></td>
	            </tr>
                <tr class="baslider-second-seperator"></tr>
	            <tr> <td align="right" colspan="2">
	            	<input type="hidden" name="update_set" value="1" />
					<input type="hidden" name="slider_name" value="<?php echo esc_attr( $slider_name ); ?>" />
					<span id="updating"></span>
	            	<button onclick="updateSet(); return false;" class="button"><?php _e('Update Set','baslider'); ?></button> </td>
	            </tr>
			</table>
	        </div>
	        </div>
		    </form>
	    </div> <!-- End edit set -->
	</div>
</div>
</div>
<script type="text/javascript">
	var $mbas6 = jQuery.noConflict();
	$mbas6(".update").hide();
	$mbas6("#edit-set").hide();
	$mbas6(".edit-btn").click(
		function()
		{
			$mbas6(".edit").toggle();
			$mbas6("#edit-set").toggle('slow');
			$mbas6("#new-set").toggle('slow');
			$mbas6(this).closest('div').next().toggle();
	});
	$mbas6(".cancel-btn").click(
		function()
		{
			$mbas6(".edit").toggle();
			$mbas6(".update").hide();
			$mbas6("#edit-set").toggle('slow');
			$mbas6("#new-set").toggle('slow');
			$mbas6(this).closest('div').next().toggle();
	});
	// Set values of set to be edited
	function editSet(id)
	{
		var id = id;
		var set_id = id;
		var cap_val = $mbas6('#cap_'+id).html();
		var img1_val = $mbas6('#img1_'+id).attr('src');
		var img2_val = $mbas6('#img2_'+id).attr('src');
		var bcap = $mbas6('#bcap_'+id).val();
		var acap = $mbas6('#acap_'+id).val();
		$mbas6("#set").val(set_id);
		$mbas6("#edit_cap").val(cap_val);
		$mbas6("#edit_bthumb").val(img1_val);
		$mbas6("#edit_athumb").val(img2_val);
		$mbas6("#edit_caption_before").val(bcap);
		$mbas6("#edit_caption_after").val(acap);
		$mbas6("#edit-thumb-before").html('<img src="'+img1_val+'">');
		$mbas6("#edit-thumb-after").html('<img src="'+img2_val+'">');
	}
	// add new set
	function uploadSet()
	{
		$mbas6("#uploading").html('<img src="<?php echo plugins_url().'/baslider/images/load.gif';?>" />');
		var set = $mbas6("#new_set").serialize();
		var form_data = "action=new_set&" + set;
		$mbas6.post(ajaxurl, form_data,function(result){
			if(result)
			{
				if(result == "success")
					$mbas6(".msg").html('<div class="updated" id="message"><p><strong><?php echo _e("Set Added Successfully !", "baslider"); ?></strong></p></div>');
				else if(result == "err")
					$mbas6(".msg").html('<div class="error" id="message"><p><strong><?php echo _e("Please provide both images with caption.", "baslider"); ?></strong></p></div>');
				$mbas6("#uploading").html('');
				if(result == "success")
					window.location.href=window.location.href;
				else
					 $mbas6('html, body').animate({scrollTop:(0)}, '200');

				setTimeout(function() {
					  $mbas6("#message").fadeOut(1000);
				}, 4000);
			}
		});
	}
	// update existing set
	function updateSet()
	{
		$mbas6("#updating").html('<img src="<?php echo plugins_url().'/baslider/images/load.gif';?>" />');

		var set = $mbas6("#edit_set").serialize();

		var form_data = "action=edit_set&" + set;
		$mbas6.post(ajaxurl,form_data,function(result){
			if(result)
			{
				$mbas6(".msg").html('<div class="updated" id="message"><p><strong><?php echo _e("Set Edited Successfully !", "baslider"); ?></strong></p></div>');
				$mbas6("#message").fadeOut(4000);
				window.location.href=window.location.href;
			}
		});
	}
	// delete set
	function deleteSet(set,slider)
	{
		var set = set;
		var slider = slider;
		if (confirm("Are you sure, you want to delete this set?"))
		{
			$mbas6("#"+set).parent('.edit').html('<img src="<?php echo plugins_url().'/baslider/images/load.gif';?>" />');
			var form_data = "action=del_set&set="+set+"&slider="+slider;
			$mbas6.post(ajaxurl,form_data,
				function(result){
					if(result)
					{
						$mbas6("."+set).hide('slow');
						$mbas6(".msg").html('<div class="updated" id="message"><p><strong><?php echo _e("Set Deleted Successfully !", "baslider"); ?></strong></p></div>');
						$mbas6("#message").fadeOut(3000);
					}
			});
		}
	}
</script>
<?php
}
?>