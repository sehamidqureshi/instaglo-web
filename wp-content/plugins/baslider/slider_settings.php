<?php
    // BASlider Settings
    function baslider_setting_page() {
	echo '<div class="wrap bas-admin-wrap">';
    if(isset($_GET["setting"])){
    	$slider_name=$_GET['setting'];
    }else{
		$slider_name='slider1';
	}
    ?>
    <div class="baslider-heading">
        <div id="icon-baslider" class="icon32"></div>
        <h2>
    	<?php
    		echo $slider_name . ' &raquo; ';
    		_e("Settings",'baslider');
    	?>
        </h2>
    </div>

	<div class="msg"></div>

    <div class="baslider-cta">
            <div class="baslider-cta-shortcode">
                <h4><?php _e('Shortcode &mdash; ', 'baslider'); ?>
                	<input type='text' id="shortcode" readonly='readonly' onclick="select();" value='[baslider name="<?php echo esc_attr( $slider_name ); ?>"]'/></h4>
            	<script type="text/javascript">
    				var $mbas8 = jQuery.noConflict();
    				var w = $mbas8("#shortcode").val().length;
    				var width = (w + 1) * 7 + 'px';
    				$mbas8("#shortcode").css('width',width);
    			</script>
            </div>
            <div class="baslider-cta-button add-edit">
    				<button type="button" class="button button-primary button-large" onclick="window.location='admin.php?page=add-baslider&edit=<?php echo esc_attr( $slider_name );?>';"><?php _e('Add / Edit Sets', 'baslider'); ?></button>
            </div>
    </div>


<div class="metabox-holder" style="width: 100%; float:left;">
    <div class="postbox">
    <form id="settings" method="post" action="" style="margin: 2%;">
	<?php $settings = get_option($slider_name.'_setting');?>
    <input type="hidden" name="slider_name" value="<?php echo esc_attr( $slider_name ); ?>" />
<table class="form-table ba-setting-form">
<tbody>
<tr valign="top">
    	<th scope="row"><?php _e("Slider Type", 'baslider'); ?></th>
        <td>
        <div id="ba_type">
            <input id="traditional" class="ba_input_hidden" type="radio" name="type" <?php if ($settings['type'] == 'traditional') echo 'checked="checked"'; ?> value="traditional">
            <label for="traditional" class="<?php if ($settings['type'] == 'traditional') echo 'selected'; ?>">
                <img src="<?php echo esc_url( BASLIDERv1_URL.'images/02.png' );?>" alt="Traditional" title="Traditional" />
            </label>
            <input id="jquery" class="ba_input_hidden" type="radio" name="type" <?php if ($settings['type'] == 'jquery') echo 'checked="checked"'; ?> value="jquery">
            <label for="jquery" class="<?php if ($settings['type'] == 'jquery') echo 'selected'; ?>">
                <img src="<?php echo esc_url( BASLIDERv1_URL.'images/01.png' );?>" alt="jQuery" title="jQuery" />
            </label>
        </div>
        <script type="text/javascript">
		var $mbas9 = jQuery.noConflict();
		$mbas9('#ba_type label').click(function() {
			$mbas9(this).addClass('selected').siblings().removeClass('selected');
		});
		</script>
        </td>
    </tr>
	<tr valign="top">
        <th scope="row"><?php _e("Slider Width", 'baslider'); ?></th>
        <td><input type="text" name="width" value="<?php echo esc_attr( $settings['width'] ); ?>" size="5" /><span>&nbsp;px</span></td>
    </tr>
    <tr valign="top">
        <th><?php _e("Slider Alignment", 'baslider'); ?></th>
        <td>
            <input type="radio" name="sliderAlignment" <?php if (isset($settings['sliderAlignment']) && $settings['sliderAlignment'] == 'left') echo 'checked="checked"'; ?> value="left"><span class="label_span_text"><?php _e('Left', 'baslider'); ?></span>
            <input type="radio" name="sliderAlignment" <?php if (isset($settings['sliderAlignment']) && $settings['sliderAlignment'] == 'center') echo 'checked="checked"'; ?> value="center"><span class="label_span_text"><?php _e('Center', 'baslider'); ?></span>
            <input type="radio" name="sliderAlignment" <?php if (isset($settings['sliderAlignment']) && $settings['sliderAlignment'] == 'right') echo 'checked="checked"'; ?> value="right"><span class="label_span_text"><?php _e('Right', 'baslider'); ?></span>
        </td>
    </tr>
    <tr valign="top" style="display:none;">
    	<th scope="row"><?php _e("Height", 'baslider'); ?></th>
        <td><input type="hidden" name="height" value="<?php echo esc_attr( $settings['height'] ); ?>" size="5" /><span>px</span></td>
    </tr>
    <tr valign="top">
    	<th scope="row"><?php _e("Slider Autoplay", 'baslider'); ?></th>
        <td>
        	<div class="onoffswitch">
                <input type="checkbox" onclick="showSpeed();" name="auto" class="onoffswitch-checkbox" id="myonoffswitch5" <?php if ($settings['auto'] == 'on') echo 'checked="checked"'; ?>>
                <label class="onoffswitch-label" for="myonoffswitch5">
                <div class="onoffswitch-inner">
                    <div class="onoffswitch-active"><div class="onoffswitch-switch"><?php _e('TRUE', 'baslider'); ?></div></div>
                    <div class="onoffswitch-inactive"><div class="onoffswitch-switch"><?php _e('FALSE', 'baslider'); ?></div></div>
                </div>
                </label>
            </div>
        </td>
    </tr>
    <tr class="hideSpeed" valign="top" <?php if ($settings['auto'] != 'on') echo 'style="display:none;"'; ?>>
    	<th scope="row"><?php _e("Autoplay Speed", 'baslider'); ?></th>
        <td><input type="text" name="delay" id="hideSpeedText" value="<?php echo esc_attr( $settings['delay'] ); ?>" size="5" /><span>&nbsp;<?php _e('in MicroSeconds', 'baslider'); ?></span></td>
    </tr>
    <!--<tr valign="top">
    	<th scope="row"><?php _e("Slide Effect", 'baslider'); ?></th>
        <td>
        	<div class="onoffswitch">
                <input type="checkbox" name="effect" class="onoffswitch-checkbox" id="myonoffswitch4" <?php if ($settings['effect'] == 'on') echo 'checked="checked"'; ?>>
                <label class="onoffswitch-label" for="myonoffswitch4">
                <div class="onoffswitch-inner">
                    <div class="onoffswitch-active"><div class="onoffswitch-switch">FADE</div></div>
                    <div class="onoffswitch-inactive"><div class="onoffswitch-switch">SLIDE</div></div>
                </div>
                </label>
            </div>
        </td>
    </tr>-->

    <tr valign="top"><td colspan="2"><hr class="ba-border" /></td></tr>

    <tr valign="top">
    	<th scope="row"><?php _e("Comparison Bar Speed", 'baslider'); ?></th>
        <td><input type="text" name="handelSpeed" value="<?php echo esc_attr( $settings['handelSpeed'] ); ?>" size="5" /><span>&nbsp;<?php _e('in MicroSeconds', 'baslider'); ?></span></td>
    </tr>
    <tr valign="top">
    	<th><?php _e("Comparison Bar Color", 'baslider'); ?></th>
        <td>
            <div class="basliderv1-color-picker">
                <input type="text" name="handelColor" class="handelColor" value="<?php echo esc_attr( $settings['handelColor'] ); ?>" size="5" />
            </div>
        </td>
    </tr>
    <tr valign="top">
    	<th scope="row"><?php _e("Comparison Bar Shadow", 'baslider'); ?></th>
        <td>
        	<div class="onoffswitch">
                <input type="checkbox" name="handelShadow" class="onoffswitch-checkbox" id="myonoffswitch6" <?php if ($settings['handelShadow'] == 'on') echo 'checked="checked"'; ?>>
                <label class="onoffswitch-label" for="myonoffswitch6">
                <div class="onoffswitch-inner">
                    <div class="onoffswitch-active"><div class="onoffswitch-switch"><span><?php _e('SHOW', 'baslider'); ?></span></div></div>
                    <div class="onoffswitch-inactive"><div class="onoffswitch-switch"><span><?php _e('HIDE', 'baslider'); ?></span></div></div>
                </div>
                </label>
            </div>
        </td>
    </tr>

	<tr valign="top"><td colspan="2"><hr class="ba-border" /></td></tr>

    <tr valign="top">
    	<th><?php _e("Border Width", 'baslider'); ?></th>
        <td><input type="number" min="0" max="10" name="borderWidth" value="<?php echo esc_attr( $settings['borderWidth'] ); ?>" size="5" /><span>&nbsp;px</span></td>
    </tr>
    <tr valign="top">
    	<th><?php _e("Border Color", 'baslider'); ?></th>
        <td>
            <div class="basliderv1-color-picker">
                <input type="text" name="borderColor" class="borderColor" value="<?php echo esc_attr( $settings['borderColor'] ); ?>" size="5" />
            </div>
        </td>
    </tr>

	<tr valign="top"><td colspan="2"><hr class="ba-border" /></td></tr>

    <tr valign="top">
    	<th scope="row"><?php _e("Next / Previous Arrows", 'baslider'); ?></th>
        <td>
            <div class="onoffswitch">
                <input type="checkbox" name="controlDisplay" class="onoffswitch-checkbox" id="myonoffswitch1" <?php if ($settings['controlDisplay'] == 'on') echo 'checked="checked"'; ?>>
                <label class="onoffswitch-label" for="myonoffswitch1">
                <div class="onoffswitch-inner">
                    <div class="onoffswitch-active"><div class="onoffswitch-switch"><span><?php _e('SHOW', 'baslider'); ?></span></div></div>
                    <div class="onoffswitch-inactive"><div class="onoffswitch-switch"><span><?php _e('HIDE', 'baslider'); ?></span></div></div>
                </div>
                </label>
            </div>
        </td>
    </tr>
    <tr valign="top">
    	<th scope="row"><?php _e("Paginations", 'baslider'); ?></th>
        <td>
            <div class="onoffswitch">
                <input type="checkbox" name="paginationDisplay" class="onoffswitch-checkbox" id="myonoffswitch2" <?php if ($settings['paginationDisplay'] == 'on') echo 'checked="checked"'; ?>>
                <label class="onoffswitch-label" for="myonoffswitch2">
                <div class="onoffswitch-inner">
                    <div class="onoffswitch-active"><div class="onoffswitch-switch"><span><?php _e('SHOW', 'baslider'); ?></span></div></div>
                    <div class="onoffswitch-inactive"><div class="onoffswitch-switch"><span><?php _e('HIDE', 'baslider'); ?></span></div></div>
                </div>
                </label>
            </div>
        </td>
    </tr>

    <tr valign="top"><td colspan="2"><hr class="ba-border" /></td></tr>

    <tr valign="top">
    	<th scope="row"><?php _e("Caption", 'baslider'); ?></th>
        <td>
            <div class="onoffswitch">
                <input type="checkbox" onclick="showCaptions();" name="cpationDisplay" class="onoffswitch-checkbox" id="myonoffswitch3" <?php if ($settings['cpationDisplay'] == 'on') echo 'checked="checked"'; ?>>
                <label class="onoffswitch-label" for="myonoffswitch3">
                <div class="onoffswitch-inner">
                    <div class="onoffswitch-active"><div class="onoffswitch-switch"><span><?php _e('SHOW', 'baslider'); ?></span></div></div>
                    <div class="onoffswitch-inactive"><div class="onoffswitch-switch"><span><?php _e('HIDE', 'baslider'); ?></span></div></div>
                </div>
                </label>
            </div>
        </td>
    </tr>
    <tr class="hideCaption" valign="top" <?php if ($settings['cpationDisplay'] != 'on') echo 'style="display:none;"'; ?>>
    	<th><?php _e("Caption Size", 'baslider'); ?></th>
        <td><input type="number" min="0" max="30" name="captionSize" value="<?php echo esc_attr( $settings['captionSize'] ); ?>" size="5" /><span>px</span></td>
    </tr>
    <tr class="hideCaption" valign="top" <?php if ($settings['cpationDisplay'] != 'on') echo 'style="display:none;"'; ?>>
    	<th><?php _e("Caption Color", 'baslider'); ?></th>
        <td>
            <div class="basliderv1-color-picker">
                <input type="text" name="captionColor" class="captionColor" value="<?php echo esc_attr( $settings['captionColor'] ); ?>" size="5" />
            </div>
        </td>
    </tr>
    <tr class="hideCaption" valign="top" <?php if ($settings['cpationDisplay'] != 'on') echo 'style="display:none;"'; ?>>
    	<th><?php _e("Caption Background Color", 'baslider'); ?></th>
        <td>
            <div class="basliderv1-color-picker">
                <input type="text" name="captionBG" class="captionBG" value="<?php echo esc_attr( $settings['captionBG'] ); ?>" size="5" />
            </div>
        </td>
    </tr>
    <tr class="hideCaption" valign="top" <?php if ($settings['cpationDisplay'] != 'on') echo 'style="display:none;"'; ?>>
    	<th><?php _e("Caption Alignment", 'baslider'); ?></th>
        <td>
            <input type="radio" name="captionAlignment" <?php if ($settings['captionAlignment'] == 'left') echo 'checked="checked"'; ?> value="left"><span class="label_span_text"><?php _e('Left', 'baslider'); ?></span>
            <input type="radio" name="captionAlignment" <?php if ($settings['captionAlignment'] == 'center') echo 'checked="checked"'; ?> value="center"><span class="label_span_text"><?php _e('Center', 'baslider'); ?></span>
            <input type="radio" name="captionAlignment" <?php if ($settings['captionAlignment'] == 'right') echo 'checked="checked"'; ?> value="right"><span class="label_span_text"><?php _e('Right', 'baslider'); ?></span>
        </td>
    </tr>
    <tr valign="top">
    	<th scope="row"><?php _e("Before / After Caption", 'baslider'); ?></th>
        <td>
            <div class="onoffswitch">
                <input type="checkbox" name="bacpationDisplay" class="onoffswitch-checkbox" id="myonoffswitch7" <?php if ($settings['bacpationDisplay'] == 'on') echo 'checked="checked"'; ?>>
                <label class="onoffswitch-label" for="myonoffswitch7">
                <div class="onoffswitch-inner">
                    <div class="onoffswitch-active"><div class="onoffswitch-switch"><span><?php _e('SHOW', 'baslider'); ?></span></div></div>
                    <div class="onoffswitch-inactive"><div class="onoffswitch-switch"><span><?php _e('HIDE', 'baslider'); ?></span></div></div>
                </div>
                </label>
            </div>
        </td>
    </tr>

    <tr valign="top"><td colspan="2"><hr class="ba-border" /></td></tr>

</tbody>
</table>
</form>
        <div style="width: 96%; margin: 2%;">
            <button type="submit" onclick="updateSettings();" name="update_settings" id="publish" class="button button-primary button-large"><?php _e('Update Settings', 'baslider') ?></button>

            <button type="submit" style="margin-left: 2%;" name="reset_settings" id="reset" class="button button-large" onclick="resetSettings('<?php echo esc_attr( $slider_name ); ?>');"><?php _e('Restore Settings','baslider'); ?></button>
        	<span id="loading"></span>
        </div>
	</div>
</div>
<script type="text/javascript">
var $mbas10 = jQuery.noConflict();
function showSpeed()
{
	$mbas10(".hideSpeed").slideToggle();
}
function hideSpeed()
{
	$mbas10(".hideSpeed").slideToggle();
}
function showCaptions()
{
	$mbas10(".hideCaption").slideToggle(100);
}

function updateSettings()
{
	$mbas10("#loading").html('<img src="<?php echo esc_url( BASLIDERv1_URL.'images/load.gif' );?>" />').fadeIn(500);
	var settings = $mbas10("#settings").serialize();
	settings = 'action=update_setting&' + settings + '&update_settings=1';
	$mbas10.post(ajaxurl,settings,
		function(res)
		{
			if(res == "updated")
				$mbas10(".msg").html('<div class="updated" id="message"><p><strong><?php echo _e("Settings Updated Successfully !", "baslider"); ?></strong></p></div>');
			else if(res == "error")
				$mbas10(".msg").html('<div class="error" id="message"><p><strong><?php echo _e("No settings were updated !", "baslider"); ?></strong></p></div>');
			$mbas10("#message").fadeOut(5000);
			$mbas10("#loading").html(res).fadeOut(2500);
            $mbas10('html,body').animate({ scrollTop: 0 }, 'slow');
		}
	);
}
function resetSettings(slider)
{
	if(confirm('Are you sure you want to restore to default settings?'))
	{
	$mbas10("#loading").html('<img src="<?php echo esc_url( BASLIDERv1_URL.'images/load.gif' );?>" />').fadeIn(500);
		var settings = 'action=reset_setting&slider_name='+slider;
		$mbas10.post(ajaxurl,settings,
			function(res)
			{
				if(res == "restored")
					$mbas10(".msg").html('<div class="updated" id="message"><p><strong><?php echo _e("Settings Restored Successfully !", "baslider"); ?></strong></p></div>');
				else if(res == "error")
					$mbas10(".msg").html('<div class="error" id="message"><p><strong><?php echo _e("Error occured while restoring settings !", "baslider"); ?></strong></p></div>');
				$mbas10("#message").fadeOut(5000);
                $mbas10('html,body').animate({ scrollTop: 0 }, 'slow');
				if(res == "restored")
					window.location.href=window.location.href;
			}
		);
	}
}
</script>
<?php }