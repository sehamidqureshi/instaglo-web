/**

 * BASlider - Multipurpose Before After Slider

 * Copyright 2013, Brainstorm Force, http://brainstormforce.com/

*/
(function($){
	jQuery(document).ready(function() {
		jQuery(document).on('mousemove','.beforeAfterSlidebar',function(e){
			var offset =  $(this).offset();
			var iTopLeft = (e.pageX - offset.left);
			var iTopImgLeft = -(iTopLeft+2);
			// set left of bottomimage div
			if(!jQuery(this).hasClass('traditional_slider'))
			{
				jQuery(this).find(".topImage").css('left',iTopLeft);
				jQuery(this).find(".topImg").css('left',iTopImgLeft);
			}
			else {
				check_for_traditional();
			}
		});

	  function check_for_traditional()
	  {
	    jQuery(".beforeAfterSlidebar").each(function(index,value){
	        if(jQuery(this).hasClass('traditional_slider'))
	            jQuery(this).find(".topImg").css('left','0px');
	    });
	  }
	});
})(jQuery);