<?php
/*
Plugin Name: MultiPurpose Before After Slider
Plugin URI: https://www.brainstormforce.com
Author: Brainstorm Force
Author URI: https://www.brainstormforce.com
Description: Compare images with ease!
Version: 2.7.1
Text Domain: baslider
License: GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
*/
//define('CONCATENATE_SCRIPTS', false );

// Refresh bundled products on activate

register_activation_hook( __FILE__, 'On_baslider_Activate' );

function On_baslider_Activate() {
    update_site_option( 'bsf_force_check_extensions', true );
}

define( 'BSF_REMOVE_5159016_FROM_REGISTRATION_LISTING', true );


define( 'BASLIDERv1_VER', '2.7.1' );
define( 'BASLIDERv1_DIR', plugin_dir_path( __FILE__ ) );
define( 'BASLIDERv1_URL', plugins_url( '/', __FILE__ ) );
define( 'BASLIDERv1_PATH', plugin_basename( __FILE__ ) );

if (!class_exists('ba_slider')) {
    class ba_slider
    {
        function __construct()
        {
            // Runs when plugin is activated and creates new database field
            register_activation_hook(__FILE__, array($this, 'baslider_defaults'));
            // Including JS & CSS for frontend
            add_action('wp_enqueue_scripts', array($this, 'baslider_head_scripts'));
            add_action('wp_enqueue_scripts', array($this, 'baslider_head_styles'));
            // Include styles & scripts for dashboard
            //add_action('admin_enqueue_scripts', array( $this, 'baslider_admin_scripts' ) );
            add_action('admin_enqueue_scripts', array($this, 'baslider_admin_styles'));
            // Including Custom Scripts
            add_action('admin_menu', array($this, 'baslider_plugin_admin_menu'), 99);
            $plugin = plugin_basename(__FILE__);
            add_filter("plugin_action_links_$plugin", array($this, 'bsf_settings_link'));
            // Register new_slider action for ajax call
            add_action('wp_ajax_new_slider', array($this, 'new_slider'));
            add_action('wp_ajax_nopriv_new_slider', array($this, 'new_slider'));
            // Register new_set action for ajax call
            add_action('wp_ajax_new_set', array($this, 'add_slider_set'));
            // Register update_setting action for ajax call
            add_action('wp_ajax_update_setting', array($this, 'update_setting'));
            // Register reset_setting action for ajax call
            add_action('wp_ajax_reset_setting', array($this, 'reset_settings'));
            // Register edit _set action for ajax call
            add_action('wp_ajax_edit_set', array($this, 'edit_set'));
            // Register del_set action for ajax call
            add_action('wp_ajax_del_set', array($this, 'del_set'));
            // Add select box near media button for adding shortcode
            add_action('media_buttons', array($this, 'add_ba_select'), 99);
            add_action('admin_head', array($this, 'button_js'));
            //Add shortcode
            add_shortcode('baslider', array($this, 'baslider_short_code'));
            // Add visual composer component
            add_action('init', array($this, 'baslider_vc'));
            add_action('admin_init', array($this, 'baslider_redirect'));

            // Load Plugin textdomain
            add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );

                        // Add popup license form on plugin list page.
            add_action( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'baslider_license_form_and_links' ) );
            add_action( 'network_admin_plugin_action_links_' . plugin_basename( __FILE__ ),  array( $this, 'baslider_license_form_and_links' ) );
            add_filter( 'bsf_skip_braisntorm_menu', array( $this,  'ba_slider_skip_brainstorm_menu' ) );
            add_filter( 'bsf_registration_page_url_5159016', array( $this, 'ba_slider_bsf_registration_page_url' ) );
        }

        public function load_textdomain() {
            load_plugin_textdomain( 'baslider', false, BASLIDERv1_DIR );
        }

        /**
         * Show action links on the plugin screen.
         *
         * @param   mixed $links Plugin Action links.
         * @return  array        Filtered plugin action links.
         */
        function baslider_license_form_and_links( $links = array() ) {

            if ( function_exists( 'get_bsf_inline_license_form' ) ) {

                $args = array(
                    'product_id'                => '5159016',
                    'popup_license_form'        => true,
                    'bsf_license_allow_email'   => true
                );

                return get_bsf_inline_license_form( $links, $args, 'envato' );
            }

            return $links;
        }
        function ba_slider_skip_brainstorm_menu( $products ) {

            $products[] = '5159016'; // Brainstorm ID of the product.
         
            return $products;
        }

        function ba_slider_bsf_registration_page_url() {
            if ( is_multisite() ) {
                return network_admin_url( 'plugins.php?bsf-inline-license-form=5159016' );
            } else {
                return admin_url( 'plugins.php?bsf-inline-license-form=5159016' );
            }
        }


        function bas_get_defaults()
        {
            $default = array(
                'type' 				=> 'jquery',
                'width' 			=> 600,
                'sliderAlignment' 	=> 'center',
                'height' 			=> 400,
                'auto' 				=> 'on',
                'delay' 			=> 5000,
                'effect' 			=> 'on',
                'handelSpeed' 		=> 0,
                'handelColor' 		=> '#ffffff',
                'handelShadow' 		=> 'on',
                'borderWidth' 		=> 0,
                'borderColor' 		=> '#ffffff',
                'controlDisplay' 	=> 'on',
                'paginationDisplay' => 'on',
                'cpationDisplay' 	=> 'on',
                'captionSize' 		=> 13,
                'captionColor' 		=> '#ffffff',
                'captionAlignment' 	=> 'center',
                'captionBG' 		=> '#222222',
                'bacpationDisplay' 	=> 'on',
            );
            return $default;
        }

        function bas_default_set()
        {
            $set 	= array();
            $set[0] = array(
                "before"	=> BASLIDERv1_URL . 'images/set-1-before.jpg',
                "after"		=> BASLIDERv1_URL . 'images/set-1-after.jpg',
                "caption"	=> 'Image Set 1',
            );
            $set[1] = array(
                "before"	=> BASLIDERv1_URL . 'images/set-2-before.jpg',
                "after"		=> BASLIDERv1_URL . 'images/set-2-after.jpg',
                "caption"	=> 'Image Set 2',
            );
            return $set;
        }

        function baslider_redirect()
        {
            if (get_option('baslider_activate')) {
                $theme_name = get_current_theme();
                if ($theme_name !== "smile") {
                    delete_option('baslider_activate');
                    if (is_multisite()) {
                        $blog_id = get_current_blog_id();
                        $url = get_admin_url($blog_id) . 'admin.php?page=baslider';
                        wp_redirect($url);
                        exit;
                    } else {
                        $url = admin_url() . 'admin.php?page=baslider';
                        wp_redirect($url);
                        exit;
                    }
                }
            }
        }

        // Add balider short code  use baslider as  [baslider name='baslider_options']
        function baslider_short_code($atts)
        {
            wp_enqueue_script('baslider');
            wp_enqueue_script('baslider_event');
            wp_enqueue_style('baslider_main_style');
            add_action( 'wp_footer', array( $this, 'scripts_footer' ) );
            ob_start();
            $sliders = get_option('baslider');
            $default_slider = '';
            foreach ($sliders as $key => $value) {
                if ($atts['name'] == $value) {
                    $default_slider = $value;
                }
            }
            extract(shortcode_atts(array(
                "name" => $default_slider
            ), $atts));
            display_baslider($name);
            $output = ob_get_clean();
            return $output;
        }

        function baslider_vc()
        {
            /*Add Before After Slider Component*/
            $sliders = get_option('baslider');
            if (function_exists('vc_map')) {
                vc_map(array(
                    "name"					=> __("Before After", 'baslider'),
                    "base"					=> "baslider",
                    "class"					=> "before_after",
                    "controls"				=> "full",
                    "show_settings_on_create"=> true,
                    "icon"					=> "icon-before-after",
                    "category"				=> __('Content', 'baslider'),
                    "params"	=> array(
                        array(
                            "type"		=> "dropdown",
                            "class"		=> "",
                            "heading"	=> __("Slider", "baslider"),
                            "param_name"=> "name",
                            "admin_label"=> true,
                            "value"		=> $sliders,
                            "description"=> __("Select the Before After Slider you want to display.", "baslider")
                        ),
                    )// End params array
                ) // End vc_map array
                ); // End vc_map
            } // End if function_exists
        }

        function scripts_footer()
        {
            $uni = uniqid();
            echo '<script type="text/javascript">

						function get_width(id)
						{
							var width = jQuery(id).width();
							var half_div = width/2;
							return half_div;
						}
						function start_slider(id,delay,auto,effect,left,right,pager)
						{
							var slider' . esc_js( $uni ) . ' = jQuery(".slides-"+id).bxSlider({
								auto: auto,
								pause: delay,
								pager: pager,
								nextSelector: "#slider-next-"+id,
								prevSelector: "#slider-prev-"+id,
								mode:effect,
								adaptiveHeight: true,
								onSlideAfter: function(){
									reset_images(id);
									jQuery(window).on("resize", function(event){
										reset_images(id);
									});
								},
								onSliderLoad: function(){
									jQuery(document).trigger("basliderLoadEvent", [ slider' . esc_js( $uni ) . ', id]);
								}
							});

							return slider' . esc_js( $uni ) . ';
						}

						function reset_images(id)
						{
							var width = get_width("#"+id);
							var left = width;
							var right = width+2;
							jQuery("#"+id+" .topImage").css("left",left);
							jQuery("#"+id+" .topImg").css("left",-right);
							check_for_traditional();
						}
						function check_for_traditional()
					  	{
							jQuery(".beforeAfterSlidebar").each(function(index,value){
								if(jQuery(this).hasClass("traditional_slider"))
									jQuery(this).find(".topImg").css("left","0px");
							});
					  	}

				</script>';
        }

        // Generate select box of shortcodes
        function add_ba_select()
        {
            $sliders = get_option('baslider');
            echo '&nbsp;<select id="ba_select" style="padding: 3px; margin: 0; border-radius: 4px; color: #222; outline: none; display: inline-block; border: 1px solid #999;">
				<option disabled="disabled" selected="selected">Before After Slider</option>';
            foreach ($sliders as $slider) {
                ?>
                <option value='[baslider name="<?php echo esc_attr( $slider ); ?>"]'><?php echo esc_attr( $slider ); ?></option>
            <?php }
            echo '</select>';
        }

        // Register jquery for button function
        function button_js()
        {
            echo '<script type="text/javascript">
			(function( $ ) {
    				$(document).ready(function(){
			   		$("#ba_select").change(function() {
					  send_to_editor($("#ba_select :selected").val());
						  return false;
					});
				});
			})( jQuery );
			</script>';
        }

        // Function to handle ajax call for reset setting
        function reset_settings()
        {
            if (isset($_POST['slider_name'])) {
                $slider_name = esc_attr($_POST['slider_name']);
                delete_option($slider_name . '_setting');
                //require_once('config.php');
                $default = $this->bas_get_defaults();
                echo add_option($slider_name . '_setting', $default) ? 'restored' : 'error';
            }
            die();
        }

        // Function to handle ajax call for update setting
        function update_setting()
        {
            $slider_name = esc_attr($_POST['slider_name']);
            $slider_settings = array(
                'type'				=> esc_attr($_POST['type']),
                'width'				=> esc_attr($_POST['width']),
                'sliderAlignment'	=> esc_attr($_POST['sliderAlignment']),
                'height'			=> esc_attr($_POST['height']),
                'delay'				=> esc_attr($_POST['delay']),
                'handelSpeed'		=> esc_attr($_POST['handelSpeed']),
                'handelColor'		=> esc_attr($_POST['handelColor']),
                'handelShadow'		=> isset($_POST['handelShadow']) ? esc_attr($_POST['handelShadow']) : '',
                'auto'				=> isset($_POST['auto']) ? esc_attr($_POST['auto']) : '',
                'effect'			=> isset($_POST['effect']) ? esc_attr($_POST['effect']) : '',
                'borderWidth'		=> esc_attr($_POST['borderWidth']),
                'borderColor'		=> esc_attr($_POST['borderColor']),
                'controlDisplay'	=> isset($_POST['controlDisplay']) ? esc_attr($_POST['controlDisplay']) : '',
                'paginationDisplay'	=> isset($_POST['paginationDisplay']) ? esc_attr($_POST['paginationDisplay']) : '',
                'cpationDisplay'	=> isset($_POST['cpationDisplay']) ? esc_attr($_POST['cpationDisplay']) : '',
                'captionSize'		=> esc_attr($_POST['captionSize']),
                'captionColor'		=> esc_attr($_POST['captionColor']),
                'captionAlignment'	=> esc_attr($_POST['captionAlignment']),
                'captionBG'			=> esc_attr($_POST['captionBG']),
                'bacpationDisplay'	=> isset($_POST['bacpationDisplay']) ? esc_attr($_POST['bacpationDisplay']) : '',

            );
            echo update_option($slider_name . '_setting', $slider_settings) ? 'updated' : 'error';
            die();
        }

        // Function to handle ajax call for adding new slider
        function new_slider()
        {
            // Add new slider
            if (isset($_POST['slider_name']) && trim($_POST['slider_name']) != "") {
                $slider_name =esc_attr( $_POST['slider_name'] );
                $sliders = get_option('baslider');
                if (!is_array($sliders)) {
                    $sliders = array();
                }
                $slider_name = preg_replace('/[^a-z0-9\s]/i', '', $slider_name);
                $slider_name = str_replace(" ", "_", $slider_name);
                if (!in_array($slider_name, $sliders)) {
                    $slider_setting = $slider_name . '_setting';
                    //require_once('config.php');
                    $default = $this->bas_get_defaults();
                    add_option($slider_setting, $default);
                    array_push($sliders, $slider_name);
                    if (update_option('baslider', $sliders))
                        echo 'added';
                    else
                        echo 'err';
                } else {
                    echo 'name';
                }
            } else {
                //echo 'empty';
            }
            die();
        }

        // Add new slider set
        function export_slider()
        {
            if (isset($_POST['export'])) {
                $slider_name	= esc_attr($_POST['export']);
                $output			= '_<single>';
                $settings		= get_option($slider_name . '_setting');
                $data			= get_option($slider_name);
                $export			= array('settings' => $settings, 'slider_name' => $slider_name, 'slider_data' => $data);
                $output			.= json_encode($export);
                $file_url		= BASLIDERv1_DIR . 'export/';
                $file			= $file_url . $slider_name . '.baslider';

                if (file_put_contents($file, $output)) {
                    echo 'exported';//BASLIDERv1_URL.'/'.$slider_name.'.basexp';
                }
            }
            if (isset($_POST['export_all'])) {
                $baslider= get_option('baslider');
                $loc	= $output = '';
                $output = "_<multi>";
                foreach ($baslider as $key => $value) {
                    $output		.= "<slider>";
                    $slider_name= $value;
                    $settings	= get_option($slider_name . '_setting');
                    $data		= get_option($slider_name);
                    $export		= array('settings' => $settings, 'slider_name' => $slider_name, 'slider_data' => $data);
                    $output		.= json_encode($export);
                    $file_url	= BASLIDERv1_DIR . 'export/';
                    $file		= $file_url . get_bloginfo('name') . '_all.baslider';
                }
                if (file_put_contents($file, $output)) {
                    echo 'all_exported';//BASLIDERv1_URL.'/'.$slider_name.'.basexp';
                }
            }
            die();
        }

        function add_slider_set()
        {
            // Upload a new set
            if (isset($_POST['upload_set']) && trim($_POST['before']) != "" && trim($_POST['after']) != "" && trim($_POST['caption']) != "" && trim($_POST['slider_name']) != "") {
                $caption_before = (isset($_POST['caption_before']) && $_POST['caption_before'] != '') ? $_POST['caption_before'] : 'Before';
                $caption_after = (isset($_POST['caption_after']) && $_POST['caption_after'] != '') ? $_POST['caption_after'] : 'After';

                $slider_name = esc_attr( $_POST['slider_name'] );
                $set_info = array(
                    'before'		=> esc_attr( $_POST['before'] ),
                    'after'			=> esc_attr( $_POST['after'] ),
                    'caption'		=> esc_attr( $_POST['caption'] ),
                    'caption_before'=> esc_attr( $caption_before ),
                    'caption_after'	=> esc_attr( $caption_after )
                );
                $slider_set = array();
                $slider_set[] = $set_info;
                $prev_slider_set = get_option($slider_name);
                $sets = count($prev_slider_set);
                if ($prev_slider_set == "")
                    $result = add_option($slider_name, $slider_set);
                else {
                    //$c = count($prev_slider_set)+1;
                    //$new_set = array();
                    //$new_set['set_'.$c] = $set_info;
                    //$prev_slider_set = $prev_slider_set + $new_set;
                    array_push($prev_slider_set, $set_info);
                    $result = update_option($slider_name, $prev_slider_set);
                }
                if ($result) {
                    echo 'success';
                }
            } else {
                echo 'err';
            }
            die();
        }

        function edit_set()
        {
            // Update existing set with new values
            $slider_name = esc_attr( $_POST['slider_name'] );
            $caption_before = (isset($_POST['caption_before']) && esc_attr( $_POST['caption_before'] ) != '') ? esc_attr( $_POST['caption_before'] ): 'Before';
            $caption_after = (isset($_POST['caption_after']) && esc_attr( $_POST['caption_after'] ) != '') ? esc_attr( $_POST['caption_after'] ): 'After';
            if (isset($_POST['update_set'])) {
                $set = sanitize_text_field( $_POST['set'] );
                $set_info = array(
                    'before'		=> esc_attr( $_POST['before'] ),
                    'after'			=> esc_attr( $_POST['after'] ),
                    'caption'		=> esc_attr( $_POST['caption'] ),
                    'caption_before'=> esc_attr( $caption_before ),
                    'caption_after'	=> esc_attr( $caption_after )
                );
                $prev_sets = get_option($slider_name);
                foreach ($prev_sets as $key => $p_set) {
                    if ($key == $set) {
                        $prev_sets[$key]['before']			= esc_attr( $_POST['before'] );
                        $prev_sets[$key]['after']			= esc_attr( $_POST['after'] );
                        $prev_sets[$key]['caption']			= esc_attr( $_POST['caption'] );
                        $prev_sets[$key]['caption_before']	= esc_attr( $caption_before );
                        $prev_sets[$key]['caption_after']	= esc_attr( $caption_after );
                    }
                }
                $result = update_option($slider_name, $prev_sets);
                if ($result) {
                    return $result;
                }
            }
            die();
        }

        function del_set()
        {
            // Delete a set
            if (isset($_POST['set'])) {
                $set = esc_attr( $_POST['set'] );
                $slider_name = esc_attr( $_POST['slider'] );
                $prev_sets = get_option($slider_name);
                foreach ($prev_sets as $key => $p_set) {
                    if ($key == $set) {
                        unset($prev_sets[$key]);
                    }
                }
                $result = update_option($slider_name, $prev_sets);
                if ($result) {
                    return $result;
                }
            }
            die();
        }

        function baslider_head_scripts()
        {
            wp_register_script( 'baslider', BASLIDERv1_URL . 'js/jquery.baslider.min.js',	array( 'jquery' ), BASLIDERv1_VER );
            wp_register_script( 'baslider_event', BASLIDERv1_URL . 'js/baslider_event.js',	array( 'jquery' ), BASLIDERv1_VER );
        }

        function baslider_head_styles()
        {
            wp_register_style( 'baslider_main_style',	BASLIDERv1_URL . 'css/style.css', null, BASLIDERv1_VER );
        }

        function baslider_admin_scripts()
        {
            wp_enqueue_script('media-upload');
            wp_enqueue_script('thickbox');
            wp_enqueue_media();
            wp_register_script( 'new-media', BASLIDERv1_URL . 'js/jquery.media.upload.js',	array( 'jquery' ), BASLIDERv1_VER );
            wp_enqueue_script('new-media');
        }

        function baslider_admin_styles()
        {
            wp_register_script( 'jqform', BASLIDERv1_URL . 'js/jquery.form.js',	array( 'jquery' ), BASLIDERv1_VER );
            wp_enqueue_script('jqform');
            wp_register_style( 'baslider_style',	BASLIDERv1_URL . 'css/admin.css', null, BASLIDERv1_VER );
            wp_enqueue_style('baslider_style');
            wp_enqueue_style('thickbox');
        }

        function baslider_defaults()
        {
            global $wp_version;
            $wp = 3.5;
            if (version_compare($wp_version, $wp, '<')) {
                deactivate_plugins(basename(__FILE__));
                wp_die('<p>The <strong>MultiPurpose Before After Slider </strong> plugin requires WordPress version 3.5 or greater.</p>', 'Plugin Activation Error', array('response' => 200, 'back_link' => TRUE));
            } else {
                if (get_option('baslider')) {
                    $prev_sets = get_option('baslider');
                    foreach ($prev_sets as $value) {
                        $new_indexed_sets = array();
                        $sets = get_option($value);
                        foreach ($sets as $key => $values) {
                            array_push($new_indexed_sets, $values);
                        }
                        delete_option($value);
                        update_option($value, $new_indexed_sets);
                    }
                }
                $sliders = array('slider1');
                if (!get_option('baslider')) {
                    //require_once('config.php');
                    add_option('baslider', $sliders);
                    $default = $this->bas_get_defaults();
                    add_option('slider1_setting', $default);
                    $default_set = $this->bas_default_set();
                    add_option('slider1', $default_set);
                }
                add_option('baslider_activate', 1);
                update_option('bas_tour_status', 'on');
            }
        }

        function baslider_plugin_admin_menu()
        {
            require_once BASLIDERv1_DIR . 'add_slider.php';

            global $submenu;
            if (defined('BSF_MENU_POS'))
                $required_place = BSF_MENU_POS;
            else
                $required_place = 200;


            if (function_exists('bsf_get_free_menu_position'))
                $place = bsf_get_free_menu_position($required_place, 1);
            else
                $place = null;

            $page = add_menu_page(
                'Add BASlider',
                'Before After',
                'edit_pages',
                'baslider',
                'baslider_main',
                plugins_url('baslider/icon-16.png'),
                $place);

            require_once BASLIDERv1_DIR . 'slider_settings.php';
            $page1 = add_submenu_page('baslider', '', '', 'publish_posts', 'add-baslider', 'baslider_admin_page');
            add_action('admin_print_scripts-' . $page1, array($this, 'baslider_admin_scripts'));
            require_once BASLIDERv1_DIR . 'add_image_set.php';
            $page = add_submenu_page('baslider', '', '', 'publish_posts', 'baslider-settings', 'baslider_setting_page');
            add_action('admin_print_scripts-' . $page, array($this, 'iris_enqueue_scripts'));
        }

        function iris_enqueue_scripts()
        {
            wp_enqueue_script('wp-color-picker');
            // load the minified version of custom script
            wp_enqueue_script( 'cp_custom', BASLIDERv1_URL . 'js/cp-script.min.js',	array( 'jquery', 'wp-color-picker' ), '1.1', true );
            wp_enqueue_style('wp-color-picker');
        }
        //add_action( 'admin_enqueue_scripts', 'iris_enqueue_scripts' );
        // Add settings link on plugin page
        function bsf_settings_link($links)
        {
            $settings_link = '<a href="admin.php?page=baslider">Settings</a>';
            array_unshift($links, $settings_link);
            return $links;
        }
    }
}

function baslider_get_color($hex, $opacity)
{
    $rgba = $hex;
    if ('' !== $opacity && 1 !== (int)$opacity) {
        if (strlen($hex) == 3) {
            $r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
            $g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
            $b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
        } else {
            $r = hexdec(substr($hex, 0, 2));
            $g = hexdec(substr($hex, 2, 2));
            $b = hexdec(substr($hex, 4, 2));
        }
        return 'rgba( ' . $r . ', ' . $g . ', ' . $b . ', ' . $opacity . ' )';
    } else {
        return '#' . $hex;
    }
}

// Display Slider
function display_baslider($slider_name)
{
    $slider_array		= get_option($slider_name);
    $ba_setting			= get_option($slider_name . '_setting');

    $captionDisplay		= sanitize_text_field( $ba_setting['cpationDisplay'] );

    $controlDisplay		= sanitize_text_field( $ba_setting['controlDisplay'] );
    $paginationDisplay	= sanitize_text_field( $ba_setting['paginationDisplay'] );
    $effect				= sanitize_text_field( $ba_setting['effect'] );
    $auto				= sanitize_text_field( $ba_setting['auto'] );
    $handelShadow		= sanitize_text_field( $ba_setting['handelShadow'] );
    $bacpationDisplay	= sanitize_text_field( $ba_setting['bacpationDisplay'] );

    $cptd				= ($captionDisplay == 'on') ? '' : 'display:none !important;';
    $ctrld				= ($controlDisplay == 'on') ? '' : 'display:none !important;';
    $pgntd				= ($paginationDisplay == 'on') ? 'true' : 'false';
    //$efct = ($effect == 'on') ? 'fade' : 'horizontal';
    $efct				= 'fade';
    $atply				= ($auto == 'on') ? 'true' : 'false';
    $hndshdw			= ($handelShadow == 'on') ? '' : 'box-shadow:none;';
    $bacptd				= ($bacpationDisplay == 'on') ? '' : 'style="display: none !important;"';

    $baid = $slider_name . '-' . uniqid();
    if (!empty($slider_array)) {
        if ('' !== $ba_setting['captionBG'] && '#fff' !== $ba_setting['captionBG']) {
            $slider_caption_color = substr($ba_setting['captionBG'], strpos($ba_setting['captionBG'], '#') + 1);
            ?>
            <style type="text/css" id="baslider-<?php echo $baid; ?>">
                .slides-<?php echo $baid;?> .ba-before {
                    background: <?php echo baslider_get_color( $slider_caption_color, 0.3 )?> !important;
                }

                .slides-<?php echo $baid;?> .ba-after {
                    background: <?php echo baslider_get_color( $slider_caption_color, 0.3)?> !important;
                }

                .slides-<?php echo $baid;?> .ba-title {
                    background: <?php echo baslider_get_color( $slider_caption_color , 0.8 )?> !important;
                }
            </style>
        <?php }
        $sliderAlignment = '';
        if (isset($ba_setting['sliderAlignment'])) {
            if ('center' == sanitize_text_field( $ba_setting['sliderAlignment']) ){
                $sliderAlignment = "margin: 0 auto;";
            } elseif ('right' == sanitize_text_field( $ba_setting['sliderAlignment']) ) {
                $sliderAlignment = "margin-right: 0; margin-left: auto; ";
            } else {
                $sliderAlignment = "margin-left: 0; margin-right: auto;";
            }
        } ?>

        <div id="<?php echo $baid; ?>" class="baslider-main"
             style="width:<?php echo esc_attr($ba_setting['width']); ?>px; border: <?php echo esc_attr($ba_setting['borderWidth']); ?>px solid <?php echo esc_attr($ba_setting['borderColor']); ?>;<?php echo esc_attr($sliderAlignment); ?>">
            <ul class="slides-<?php echo $baid; ?>">
                <?php
                foreach ($slider_array as $slider) {
                    $before_caption = (isset($slider['caption_before']) && $slider['caption_before'] != '') ? stripslashes($slider['caption_before']) : 'Before';
                    $after_caption = (isset($slider['caption_after']) && $slider['caption_after'] != '') ? stripslashes($slider['caption_after']) : 'After';
                    if (is_numeric($slider['before'])) {
                        $slider_before_img = wp_get_attachment_image_src($slider['before'], 'full');
                        $slider_before_img = $slider_before_img[0];
                    } else
                        $slider_before_img = $slider['before'];

                    if (is_numeric($slider['after'])) {
                        $slider_after_img = wp_get_attachment_image_src($slider['after'], 'full');
                        $slider_after_img = $slider_after_img[0];
                    } else
                        $slider_after_img = $slider['after'];

                    ?>
                    <li class="baslideli">
                        <div class="beforeAfterSlidebar <?php if ($ba_setting['type'] == 'traditional') echo 'traditional_slider'; ?>">
                            <div class="bottomImage" <?php if ($ba_setting['type'] == 'traditional') echo 'style="width:50% !important;"'; ?>>
                                <img src="<?php echo esc_url( $slider_before_img ); ?>"
                                     alt="Before-<?php echo stripslashes($slider['caption']); ?>">
                            </div>
                            <div class="topImage" style="border-left-color:<?php echo esc_attr( $ba_setting['handelColor'] );
                            if ($ba_setting['type'] == 'traditional') echo '; width:50% !important; left:50% !important;'; ?>;-webkit-transition: <?php echo esc_attr( $ba_setting['handelSpeed'] ); ?>ms;-moz-transition-duration: <?php echo esc_attr( $ba_setting['handelSpeed'] ); ?>ms;-o-transition-duration: <?php echo esc_attr( $ba_setting['handelSpeed'] ); ?>ms;-ms-transition-duration: <?php echo esc_attr( $ba_setting['handelSpeed'] ); ?>ms;transition-duration: <?php echo esc_attr( $ba_setting['handelSpeed'] ); ?>ms; <?php echo esc_attr( $hndshdw ); ?>">
                                <img class="topImg"
                                     style="-webkit-transition: <?php echo esc_attr( $ba_setting['handelSpeed'] ); ?>ms;-moz-transition-duration: <?php echo esc_attr( $ba_setting['handelSpeed'] ); ?>ms;-o-transition-duration: <?php echo esc_attr( $ba_setting['handelSpeed'] ); ?>ms;-ms-transition-duration: <?php echo esc_attr( $ba_setting['handelSpeed'] ); ?>ms;transition-duration: <?php echo esc_attr( $ba_setting['handelSpeed'] ); ?>ms;"
                                     src="<?php echo esc_url( $slider_after_img ); ?>"
                                     alt="After-<?php echo stripslashes($slider['caption']) ?>">
                            </div>
                            <h5 style="text-align:<?php echo esc_attr( $ba_setting['captionAlignment'] ); ?>;color:<?php echo esc_attr( $ba_setting['captionColor'] ) ?> !important;">
                                <span class="ba-before" <?php echo $bacptd; ?>><?php _e($before_caption, 'baslider');?></span>
                                <span class="ba-title" style="font-size:<?php echo $ba_setting['captionSize']; ?>px; <?php echo $cptd; ?>"><?php echo stripslashes($slider['caption']) ?></span>
                                <span class="ba-after" <?php echo $bacptd; ?>><?php _e($after_caption, 'baslider'); ?></span>
                            </h5></div>
                    </li>
                    <?php
                } ?>
            </ul>

            <div class="ba-outside"
                 style="width:<?php echo( esc_attr( $ba_setting['width'] ) + ( esc_attr( $ba_setting['borderWidth'] ) * 2 ) ); ?>px;<?php echo esc_attr( $ctrld ); ?>">
                <span id="slider-next-<?php echo esc_attr( $baid ); ?>" class="slide-nex"></span><span
                        id="slider-prev-<?php echo esc_attr( $baid ); ?>" class="slide-pre"></span>
            </div>
        </div>
        <div style="clear:both"></div>
        <script type="text/javascript">
            (function ($) {
                <?php $temp_id = str_replace('-', '_', $slider_name); ?>
                var bxslider_<?php echo esc_js( $temp_id ) ?>;
                jQuery(document).ready(function () {
                    var <?php echo esc_js( $slider_name ) ?>_l		= get_width('#<?php echo esc_attr( $baid );?>');
                    var <?php echo esc_js( $slider_name ) ?>_r		= get_width('#<?php echo esc_attr( $baid );?>');
                    var <?php echo esc_js( $slider_name ) ?>_l		= <?php echo esc_js( $slider_name ) ?>_l - 2;
                    var <?php echo esc_js( $slider_name ) ?>_r		= <?php echo esc_js( $slider_name ) ?>_r;
                    var <?php echo esc_js( $slider_name ) ?>_auto	= <?php echo esc_js( $atply );?>;
                    var <?php echo esc_js( $slider_name ) ?>_delay	= '<?php echo esc_attr( $ba_setting['delay'] ) ?>';
                    var <?php echo esc_js( $slider_name ) ?>_effect	= '<?php echo esc_attr( $efct );?>';
                    var <?php echo esc_js( $slider_name ) ?>_pager	= <?php echo esc_js( $pgntd );?>;
                    reset_images('<?php echo esc_attr( $baid ) ?>');
                    bxslider_<?php echo esc_js( $temp_id ) ?> = start_slider('<?php echo esc_attr( $baid ) ?>', <?php echo esc_js( $slider_name ) ?>_delay, <?php echo esc_js( $slider_name ) ?>_auto, <?php echo esc_js( $slider_name ) ?>_effect, <?php echo esc_js( $slider_name ) ?>_l, <?php echo esc_js( $slider_name ) ?>_r, <?php echo esc_js( $slider_name ) ?>_pager);
                });
                jQuery(window).on("resize", function (event) {
                    reset_images('<?php echo esc_attr( $baid ) ?>');
                });
                jQuery(window).load(function (event) {
                    jQuery(window).trigger('resize');
                });
                jQuery(".wpb_accordion .wpb_accordion_wrapper").on("accordionactivate", function (event, ui) {
                    bxslider_<?php echo esc_js( $temp_id ) ?>.redrawSlider();
                    reset_images('<?php echo esc_attr( $baid ) ?>');
                });
                jQuery(document).on("ultAdvancedTabClicked", function () {
                    bxslider_<?php echo esc_js( $temp_id ) ?>.destroySlider();
                    var <?php echo esc_js( $slider_name ) ?>_l      = get_width('#<?php echo esc_attr( $baid );?>');
                    var <?php echo esc_js( $slider_name ) ?>_r      = get_width('#<?php echo esc_attr( $baid );?>');
                    var <?php echo esc_js( $slider_name ) ?>_l      = <?php echo esc_js( $slider_name ) ?>_l - 2;
                    var <?php echo esc_js( $slider_name ) ?>_r      = <?php echo esc_js( $slider_name ) ?>_r;
                    var <?php echo esc_js( $slider_name ) ?>_auto   = <?php echo esc_js( $atply );?>;
                    var <?php echo esc_js( $slider_name ) ?>_delay  = '<?php echo esc_attr( $ba_setting['delay'] ) ?>';
                    var <?php echo esc_js( $slider_name ) ?>_effect = '<?php echo esc_attr( $efct );?>';
                    var <?php echo esc_js( $slider_name ) ?>_pager  = <?php echo esc_js( $pgntd );?>;
                    reset_images('<?php echo esc_attr( $baid ) ?>');
                    start_slider('<?php echo esc_attr( $baid ) ?>', <?php echo esc_js( $slider_name ) ?>_delay, <?php echo esc_js( $slider_name ) ?>_auto, <?php echo esc_js( $slider_name ) ?>_effect, <?php echo esc_js( $slider_name ) ?>_l, <?php echo esc_js( $slider_name ) ?>_r, <?php echo esc_js( $slider_name ) ?>_pager);
                });
                jQuery(document).ajaxComplete(function (e, xhr, settings) {
                    var <?php echo esc_js( $slider_name ) ?>_l      = get_width('#<?php echo esc_attr( $baid );?>');
                    var <?php echo esc_js( $slider_name ) ?>_r      = get_width('#<?php echo esc_attr( $baid );?>');
                    var <?php echo esc_js( $slider_name ) ?>_l      = <?php echo esc_js( $slider_name ) ?>_l - 2;
                    var <?php echo esc_js( $slider_name ) ?>_r      = <?php echo esc_js( $slider_name ) ?>_r;
                    var <?php echo esc_js( $slider_name ) ?>_auto   = <?php echo esc_js( $atply );?>;
                    var <?php echo esc_js( $slider_name ) ?>_delay  = '<?php echo esc_attr( $ba_setting['delay'] ) ?>';
                    var <?php echo esc_js( $slider_name ) ?>_effect = '<?php echo esc_attr( $efct );?>';
                    var <?php echo esc_js( $slider_name ) ?>_pager  = <?php echo esc_js( $pgntd );?>;
                    reset_images('<?php echo esc_attr( $baid ) ?>');
                    start_slider('<?php echo esc_attr( $baid ) ?>', <?php echo esc_js( $slider_name ) ?>_delay, <?php echo esc_js( $slider_name ) ?>_auto, <?php echo esc_js( $slider_name ) ?>_effect, <?php echo esc_js( $slider_name ) ?>_l, <?php echo esc_js( $slider_name ) ?>_r, <?php echo esc_js( $slider_name ) ?>_pager);
                });
            })(jQuery);
        </script>
        <?php
    }
}

// Instantiat the class
if (class_exists('ba_slider')) {
    $ba_slider = new ba_slider();
}
// Register Widget for Before After Slider
require_once BASLIDERv1_DIR . 'functions.widget.php';


// bsf core
$bsf_core_version_file = realpath(dirname(__FILE__) . '/admin/bsf-core/version.yml');
if (is_file($bsf_core_version_file)) {
    global $bsf_core_version, $bsf_core_path;
    $bsf_core_dir = realpath(dirname(__FILE__) . '/admin/bsf-core/');
    $version = file_get_contents($bsf_core_version_file);
    if (version_compare($version, $bsf_core_version, '>')) {
        $bsf_core_version = $version;
        $bsf_core_path = $bsf_core_dir;
    }
}
add_action('init', 'bsf_core_load', 999);
if (!function_exists('bsf_core_load')) {
    function bsf_core_load()
    {
        global $bsf_core_version, $bsf_core_path;
        if (is_file(realpath($bsf_core_path . '/index.php'))) {
            include_once realpath($bsf_core_path . '/index.php');
        }
    }
}
?>