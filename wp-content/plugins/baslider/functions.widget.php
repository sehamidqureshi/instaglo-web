<?php
//Register Slider as a widget			
add_action( 'widgets_init', 'baslider_register_widgets' );
function baslider_register_widgets() {
	register_widget( 'BASlider_Widget' );
}

//Register Sidebar as Widget
class BASlider_Widget extends WP_Widget {
	function __construct() {
		// Instantiate the parent object
		parent::__construct(
			'baslider-widget', // Base ID
			'BASlider Widget', // Name
			array( 'description' => __( 'Before After Gallery', 'baslider' ), ) // Args
		);
	}

	function widget( $args, $instance ) {
		// Widget output
		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'Before After Slider', 'baslider' );
		}

		if ( isset( $instance['baslider'] ) ) {
			$baslider = $instance['baslider'];
		} else {
			$baslider = __( 'slider1', 'baslider' );
		}

		if ( isset( $instance['description1'] ) ) {
			$description1 = $instance['description1'];
		} else {
			$description1 ='';
		}

		if ( isset( $instance['description2'] ) ) {
			$description2 = $instance['description2'];
		} else {
			$description2 ='';
		}

		wp_enqueue_script( 'baslider' );
		wp_enqueue_script( 'baslider_event' );
		wp_enqueue_style( 'baslider_main_style' );
		add_action( 'wp_footer', array( 'ba_slider', 'scripts_footer' ) );

		extract( $args );

		echo $before_widget;

		echo $before_title . $title . $after_title;

		echo '<p>' . $description1 . '</p>';
		display_baslider( $baslider ); //check

		echo '<p>' . $description2 . '</p>';
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		// Save widget options
		$instance                 = array();
		$instance['title']        = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['baslider']     = $new_instance['baslider'];
		$instance['description1'] = $new_instance['description1'];
		$instance['description2'] = $new_instance['description2'];

		return $instance;
	}

	function form( $instance ) {

		if ( isset( $instance['title'] ) ) {
			$title = $instance['title'];
		} else {
			$title = __( 'Before After Slider', 'baslider' );
		}

		if ( isset( $instance['baslider'] ) ) {
			$baslider = $instance['baslider'];
		} else {
			$baslider = __( 'slider1', 'baslider' );
		}

		if ( isset( $instance['description1'] ) ) {
			$description1 = $instance['description1'];
		} else {
			$description1 = '';
		}

		if ( isset( $instance['description2'] ) ) {
			$description2 = $instance['description2'];
		} else {
			$description2 = '';
		}

		// Output admin widget options form
		?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'baslider' ); ?>:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'title' ); ?>"
			       name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>"
			       style="width:95%;"/></p>

		<p>
			<label
				for="<?php echo $this->get_field_id( 'description1' ); ?>"><?php _e( 'Description 1 (optional)', 'baslider' ); ?>
				:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'description1' ); ?>"
			       name="<?php echo $this->get_field_name( 'description1' ); ?>"
			       value="<?php echo esc_attr( $description1 ); ?>" style="width:95%;"/>
		</p>

		<p><label><?php _e( 'Choose Slider', 'baslider' ); ?>:</label><br/>
			<select id="<?php echo $this->get_field_name( 'baslider' ); ?>"
			        name="<?php echo $this->get_field_name( 'baslider' ); ?>">
				<?php
				$sliders_data = get_option( 'baslider' );
				foreach ( $sliders_data as $data ) {
					?>
					<option value="<?php echo esc_attr( $data ); ?>" <?php if ( $data == $baslider ) {
						echo 'selected="selected"';
					} ?>><?php echo esc_attr( $data ); ?></option>
					<?php
				}
				?>
			</select>
		</p>
		<p>
			<label
				for="<?php echo $this->get_field_id( 'description2' ); ?>"><?php _e( 'Description 2 (optional)', 'baslider' ); ?>
				:</label>
			<input type="text" id="<?php echo $this->get_field_id( 'description2' ); ?>"
			       name="<?php echo $this->get_field_name( 'description2' ); ?>"
			       value="<?php echo esc_attr( $description2 ); ?>" style="width:95%;"/></p>
		<?php
	}
}

// Use shortcodes in text widgets.
add_filter( 'widget_text', 'do_shortcode' );