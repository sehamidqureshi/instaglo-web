# Copyright (C) 2019 Brainstorm Force
# This file is distributed under the GPL version 2 or later - http://www.gnu.org/licenses/old-licenses/gpl-2.0.html.
msgid ""
msgstr ""
"Project-Id-Version: MultiPurpose Before After Slider 2.7.1\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/index\n"
"POT-Creation-Date: 2019-03-04 08:37:15+00:00\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2019-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"X-Generator: grunt-wp-i18n 0.5.4\n"
"X-Poedit-KeywordsList: "
"__;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_"
"attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Country: United States\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-Basepath: ../\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-Bookmarks: \n"
"X-Textdomain-Support: yes\n"

#: add_image_set.php:22
msgid "Add / Modify Images"
msgstr ""

#: add_image_set.php:31 slider_settings.php:25
msgid "Shortcode &mdash; "
msgstr ""

#: add_image_set.php:41
msgid "Slider Settings"
msgstr ""

#: add_image_set.php:93 add_slider.php:62
msgid "Edit"
msgstr ""

#: add_image_set.php:94 add_slider.php:65
msgid "Delete"
msgstr ""

#: add_image_set.php:98
msgid "Cancel"
msgstr ""

#: add_image_set.php:117
msgid "Upload New Set"
msgstr ""

#: add_image_set.php:119 add_image_set.php:178
msgid "Please make sure all images you upload in same slider are of same dimentions"
msgstr ""

#: add_image_set.php:124 add_image_set.php:181 slider_settings.php:195
msgid "Caption"
msgstr ""

#: add_image_set.php:134 add_image_set.php:191
msgid "Before Image:"
msgstr ""

#: add_image_set.php:142 add_image_set.php:157 add_image_set.php:199
#: add_image_set.php:214 functions.widget.php:102
msgid "Title"
msgstr ""

#: add_image_set.php:149 add_image_set.php:206
msgid "After Image:"
msgstr ""

#: add_image_set.php:165
msgid "Upload Set"
msgstr ""

#: add_image_set.php:176
msgid "Edit Set"
msgstr ""

#: add_image_set.php:222
msgid "Update Set"
msgstr ""

#: add_image_set.php:282
msgid "Set Added Successfully !"
msgstr ""

#: add_image_set.php:284
msgid "Please provide both images with caption."
msgstr ""

#: add_image_set.php:308
msgid "Set Edited Successfully !"
msgstr ""

#: add_image_set.php:328
msgid "Set Deleted Successfully !"
msgstr ""

#: add_slider.php:10 functions.widget.php:24 functions.widget.php:79
msgid "Before After Slider"
msgstr ""

#: add_slider.php:43
msgid "Create new BASlider"
msgstr ""

#: add_slider.php:94
msgid "Slider '+ slidername +' is successfully added !"
msgstr ""

#: add_slider.php:96
msgid "Slider '+ slidername +' could not be added."
msgstr ""

#: add_slider.php:98
msgid "Slider '+ slidername +' is already exists. Try another name."
msgstr ""

#: add_slider.php:100
msgid "Please provide a valid slider name."
msgstr ""

#: add_slider.php:117
msgid "Slider Deleted Successfully !"
msgstr ""

#: admin/bsf-core/BSF_Envato_Activate.php:178
msgid ""
"Updates & Support Registration - <span class=\"not-active\">Not "
"Active!</span>"
msgstr ""

#: admin/bsf-core/BSF_Envato_Activate.php:179
msgid ""
"Click on the button below to activate your license and subscribe to our "
"newsletter."
msgstr ""

#: admin/bsf-core/BSF_License_Manager.php:332
msgid "Product id is missing."
msgstr ""

#: admin/bsf-core/BSF_License_Manager.php:410
msgid ""
"<p>Click on the button below to activate your license and subscribe to our "
"newsletter.</p>"
msgstr ""

#: admin/bsf-core/BSF_License_Manager.php:412
msgid "<p>Enter your purchase key and activate automatic updates.</p>"
msgstr ""

#: admin/bsf-core/BSF_License_Manager.php:439
msgid "Your license is active."
msgstr ""

#: admin/bsf-core/BSF_License_Manager.php:549
#: admin/bsf-core/BSF_License_Manager.php:564
msgid "Activate License"
msgstr ""

#: admin/bsf-core/BSF_License_Manager.php:552
msgid "License"
msgstr ""

#: admin/bsf-core/BSF_License_Manager.php:565
msgid "Deactivate License"
msgstr ""

#: admin/bsf-core/BSF_License_Manager.php:567
msgid "Your license is not active!"
msgstr ""

#: admin/bsf-core/BSF_License_Manager.php:568
msgid "Your license is activated!"
msgstr ""

#: admin/bsf-core/BSF_License_Manager.php:614
msgid "Close"
msgstr ""

#: admin/bsf-core/BSF_Update_Manager.php:573
msgid ""
" <br>This plugin is came bundled with the <i>%1$s</i>. For receiving "
"updates, you need to register license of <i>%2$s</i> <a "
"href=\"%3$s\">here</a>."
msgstr ""

#: admin/bsf-core/BSF_Update_Manager.php:575
msgid " <i>Click <a href=\"%1$s\">here</a> to activate your license.</i>"
msgstr ""

#: admin/bsf-core/BSF_Update_Manager.php:639
#: admin/bsf-core/BSF_Update_Manager.php:666
msgid ""
"Click <a target=\"_blank\" href=\"%1$1s\">here</a> to activate license of "
"<i>%2$2s</i> to receive automatic updates."
msgstr ""

#: admin/bsf-core/BSF_Update_Manager.php:645
msgid "Activate license of <i>%2s</i> to receive automatic updates."
msgstr ""

#: admin/bsf-core/BSF_Update_Manager.php:651
msgid ""
"This plugin is came bundled with the <i>%1$1s</i>. For receiving updates, "
"you need to register license of <i>%2$2s</i> <a target=\"_blank\" "
"href=\"%3$3s\">here</a>."
msgstr ""

#: admin/bsf-core/auto-update/admin-functions.php:625
msgid "Please"
msgstr ""

#: admin/bsf-core/auto-update/admin-functions.php:625
msgid "activate"
msgstr ""

#: admin/bsf-core/auto-update/admin-functions.php:625
msgid "your copy of the"
msgstr ""

#: admin/bsf-core/auto-update/admin-functions.php:625
msgid "to get update notifications, access to support features & other resources!"
msgstr ""

#: admin/bsf-core/auto-update/admin-functions.php:672
msgid "Something went wrong. Please try again!"
msgstr ""

#: admin/bsf-core/auto-update/index.php:23
msgid "Upgrading Extension"
msgstr ""

#: admin/bsf-core/auto-update/index.php:40
msgid "Back to Registration"
msgstr ""

#: admin/bsf-core/auto-update/index.php:234
msgid "Let's Get Started!"
msgstr ""

#: admin/bsf-core/auto-update/index.php:237
msgid ""
"Please register using the form below and get instant access to our support "
"portal, updates, extensions and more!"
msgstr ""

#: admin/bsf-core/auto-update/index.php:248
msgid "Your Name"
msgstr ""

#: admin/bsf-core/auto-update/index.php:253
msgid "Your Email Address"
msgstr ""

#: admin/bsf-core/auto-update/index.php:259
msgid "Verify Email Address"
msgstr ""

#: admin/bsf-core/auto-update/index.php:266
msgid "Receive important news, updates & freebies on email."
msgstr ""

#: admin/bsf-core/auto-update/index.php:273
msgid "Register and Proceed"
msgstr ""

#: admin/bsf-core/auto-update/index.php:280
msgid "We respect your privacy & of course you can unsubscribe at any time."
msgstr ""

#: admin/bsf-core/auto-update/index.php:328
msgid "Welcome to %s"
msgstr ""

#: admin/bsf-core/auto-update/index.php:331
msgid ""
"Validate your purchase keys and get eligible for receiving one click "
"updates, extensions and freebies."
msgstr ""

#: admin/bsf-core/auto-update/index.php:391
msgid "Licenses"
msgstr ""

#: admin/bsf-core/auto-update/index.php:394
msgid "Help"
msgstr ""

#: admin/bsf-core/auto-update/index.php:398
msgid "Debug"
msgstr ""

#: admin/bsf-core/auto-update/index.php:400
msgid "System Info"
msgstr ""

#: admin/bsf-core/auto-update/index.php:630
msgid "Enable Developer access"
msgstr ""

#: admin/bsf-core/auto-update/index.php:631
msgid "Read more about developer access "
msgstr ""

#: admin/bsf-core/auto-update/index.php:633
msgid "here"
msgstr ""

#: admin/bsf-core/auto-update/index.php:642
msgid "Activate your license to enable!"
msgstr ""

#: admin/bsf-core/auto-update/index.php:667
msgid "Check if there are updates available of plugins by %s."
msgstr ""

#: admin/bsf-core/auto-update/index.php:672
msgid "Check Updates Now"
msgstr ""

#: admin/bsf-core/auto-update/index.php:682
msgid ""
"Having any trouble using our products? Head to our support center to get "
"your issues resolved."
msgstr ""

#: admin/bsf-core/auto-update/index.php:685
msgid "Request Support"
msgstr ""

#: admin/bsf-core/auto-update/index.php:706
msgid "Reset %s Registration"
msgstr ""

#: admin/bsf-core/auto-update/index.php:719
msgid "Check Bundled Products"
msgstr ""

#: admin/bsf-core/auto-update/index.php:728
msgid "%s Menu in Settings?"
msgstr ""

#: admin/bsf-core/auto-update/index.php:730
msgid "Move the %s menu to WordPress settings?"
msgstr ""

#: admin/bsf-core/auto-update/index.php:733
msgid "Update"
msgstr ""

#: admin/bsf-core/index.php:195 admin/bsf-core/index.php:196
msgid "Extensions"
msgstr ""

#: admin/bsf-core/index.php:328
msgid "iMedica Extensions"
msgstr ""

#: admin/bsf-core/index.php:825
msgid " requires following plugins to be active : "
msgstr ""

#: admin/bsf-core/index.php:835
msgid " requires following plugins to be installed and activated : "
msgstr ""

#: admin/bsf-core/index.php:844
msgid "Begin activating plugins"
msgstr ""

#: admin/bsf-core/index.php:848
msgid "Begin installing plugins"
msgstr ""

#: admin/bsf-core/index.php:851
msgid "Dismiss This Notice"
msgstr ""

#: admin/bsf-core/index.php:877
msgid ""
"Looks like you are on a WordPress Multisite, you will need to install and "
"network activate %1$s Brainstorm Updater for Multisite %2$s plugin. "
"Download it from %3$s here %4$s"
msgstr ""

#: admin/bsf-core/index.php:1194
msgid "WordPress Environment"
msgstr ""

#: admin/bsf-core/index.php:1298
msgid "Server Environment"
msgstr ""

#: admin/bsf-core/index.php:1402
msgid "Theme Information"
msgstr ""

#: admin/bsf-core/index.php:1422
msgid "Installed Plugins"
msgstr ""

#: admin/bsf-core/plugin-installer/admin-functions.php:173
msgid "You do not have sufficient permissions to install plugins for this site."
msgstr ""

#: admin/bsf-core/plugin-installer/admin-functions.php:235
msgid "Error! Can't connect to filesystem"
msgstr ""

#: admin/bsf-core/plugin-installer/admin-functions.php:246
msgid "Downloading package from Server"
msgstr ""

#: admin/bsf-core/plugin-installer/admin-functions.php:247
msgid "Removing old plugin, if exists"
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:9
msgid "Installing Extension"
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:16
msgid "Manage plugin here"
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:60
msgid "Extensions "
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:66
msgid "Available Extensions"
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:141
#: admin/bsf-core/plugin-installer/index.php:260
msgid "Downloading"
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:182
#: admin/bsf-core/plugin-installer/index.php:300
msgid "Install"
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:187
#: admin/bsf-core/plugin-installer/index.php:304
msgid "Validate Purchase"
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:198
#: admin/bsf-core/plugin-installer/index.php:315
msgid "Installed"
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:211
msgid "All available extensions have been installed!"
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:219
msgid "Installed Extensions"
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:328
msgid "No extensions installed yet!"
msgstr ""

#: admin/bsf-core/plugin-installer/index.php:337
msgid "No extensions available yet!"
msgstr ""

#: functions.widget.php:15
msgid "Before After Gallery"
msgstr ""

#: functions.widget.php:30 functions.widget.php:85
msgid "slider1"
msgstr ""

#: functions.widget.php:109
msgid "Description 1 (optional)"
msgstr ""

#: functions.widget.php:116
msgid "Choose Slider"
msgstr ""

#: functions.widget.php:133
msgid "Description 2 (optional)"
msgstr ""

#: index.php:212
msgid "Before After"
msgstr ""

#: index.php:218
msgid "Content"
msgstr ""

#: index.php:223
msgid "Slider"
msgstr ""

#: index.php:227
msgid "Select the Before After Slider you want to display."
msgstr ""

#: slider_settings.php:16
msgid "Settings"
msgstr ""

#: slider_settings.php:35
msgid "Add / Edit Sets"
msgstr ""

#: slider_settings.php:48
msgid "Slider Type"
msgstr ""

#: slider_settings.php:69
msgid "Slider Width"
msgstr ""

#: slider_settings.php:73
msgid "Slider Alignment"
msgstr ""

#: slider_settings.php:75 slider_settings.php:231
msgid "Left"
msgstr ""

#: slider_settings.php:76 slider_settings.php:232
msgid "Center"
msgstr ""

#: slider_settings.php:77 slider_settings.php:233
msgid "Right"
msgstr ""

#: slider_settings.php:81
msgid "Height"
msgstr ""

#: slider_settings.php:85
msgid "Slider Autoplay"
msgstr ""

#: slider_settings.php:91
msgid "TRUE"
msgstr ""

#: slider_settings.php:92
msgid "FALSE"
msgstr ""

#: slider_settings.php:99
msgid "Autoplay Speed"
msgstr ""

#: slider_settings.php:100 slider_settings.php:121
msgid "in MicroSeconds"
msgstr ""

#: slider_settings.php:103
msgid "Slide Effect"
msgstr ""

#: slider_settings.php:120
msgid "Comparison Bar Speed"
msgstr ""

#: slider_settings.php:124
msgid "Comparison Bar Color"
msgstr ""

#: slider_settings.php:132
msgid "Comparison Bar Shadow"
msgstr ""

#: slider_settings.php:138 slider_settings.php:170 slider_settings.php:184
#: slider_settings.php:201 slider_settings.php:243
msgid "SHOW"
msgstr ""

#: slider_settings.php:139 slider_settings.php:171 slider_settings.php:185
#: slider_settings.php:202 slider_settings.php:244
msgid "HIDE"
msgstr ""

#: slider_settings.php:149
msgid "Border Width"
msgstr ""

#: slider_settings.php:153
msgid "Border Color"
msgstr ""

#: slider_settings.php:164
msgid "Next / Previous Arrows"
msgstr ""

#: slider_settings.php:178
msgid "Paginations"
msgstr ""

#: slider_settings.php:209
msgid "Caption Size"
msgstr ""

#: slider_settings.php:213
msgid "Caption Color"
msgstr ""

#: slider_settings.php:221
msgid "Caption Background Color"
msgstr ""

#: slider_settings.php:229
msgid "Caption Alignment"
msgstr ""

#: slider_settings.php:237
msgid "Before / After Caption"
msgstr ""

#: slider_settings.php:257
msgid "Update Settings"
msgstr ""

#: slider_settings.php:259
msgid "Restore Settings"
msgstr ""

#: slider_settings.php:288
msgid "Settings Updated Successfully !"
msgstr ""

#: slider_settings.php:290
msgid "No settings were updated !"
msgstr ""

#: slider_settings.php:307
msgid "Settings Restored Successfully !"
msgstr ""

#: slider_settings.php:309
msgid "Error occured while restoring settings !"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "MultiPurpose Before After Slider"
msgstr ""

#. Author URI of the plugin/theme
msgid "https://www.brainstormforce.com"
msgstr ""

#. Description of the plugin/theme
msgid "Compare images with ease!"
msgstr ""

#. Author of the plugin/theme
msgid "Brainstorm Force"
msgstr ""