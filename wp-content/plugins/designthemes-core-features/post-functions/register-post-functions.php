<?php
if( !class_exists( 'DTCorePostFunctions' ) ) {

	class DTCorePostFunctions {

		function __construct() {
			add_action( 'wp_ajax_meni_post_rating_like', array( $this, 'meni_post_rating_like' ) );
			add_action( 'wp_ajax_nopriv_meni_post_rating_like', array( $this, 'meni_post_rating_like' ) );
		}

		function meni_post_rating_like() {

			$out = '';
			$postid = $_REQUEST['post_id'];
			$nonce = $_REQUEST['nonce'];
			$action = $_REQUEST['doaction'];
			$arr_pids = array();

			if ( wp_verify_nonce( $nonce, 'rating-nonce' ) && $postid > 0 ) {
		
				$post_meta = get_post_meta ( $postid, '_dt_post_settings', TRUE );
				$post_meta = is_array ( $post_meta ) ? $post_meta : array ();
				$var_count = ($action == 'like') ? 'like_count' : 'unlike_count';

				if( isset( $_COOKIE['arr_pids'] ) ) {

					// article voted already...
					if( in_array( $postid, explode(',', $_COOKIE['arr_pids']) ) ) {

						$out = esc_html__('Already', 'meni');

					} else {
						// article first vote...
						$v = array_key_exists($var_count, $post_meta) ?  $post_meta[$var_count] : 0;
						$v = $v + 1;
						$post_meta[$var_count] = $v;
						update_post_meta( $postid, '_dt_post_settings', $post_meta );

						$out = $v;

						$arr_pids = explode(',', $_COOKIE['arr_pids']);
						array_push( $arr_pids, $postid);
						setcookie( "arr_pids", implode(',', $arr_pids ), time()+1314000, "/" );
					}
				} else {

					// site first vote...
					$v = array_key_exists($var_count, $post_meta) ?  $post_meta[$var_count] : 0;
					$v = $v + 1;
					$post_meta[$var_count] = $v;
					update_post_meta( $postid, '_dt_post_settings', $post_meta );

					$out = $v;

					array_push( $arr_pids, $postid);
					setcookie( "arr_pids", implode(',', $arr_pids ), time()+1314000, "/" );
				}
			} else {
				$out = esc_html__('Security check', 'meni');
			}

			echo do_shortcode($out);
			
			die();
		}
	}
}