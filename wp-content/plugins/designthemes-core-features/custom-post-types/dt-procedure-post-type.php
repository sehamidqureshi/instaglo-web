<?php if( !class_exists('DTProcedurePostType') ) {

	class DTProcedurePostType {

		function __construct() {

			add_action ( 'init', array( $this, 'dt_register_cpt' ), 5 );

			add_filter( 'meni_vc_default_cpt', array( $this, 'dt_enable_vc' ) );

			add_filter ( 'cs_metabox_options', array ( $this, 'dt_procedure_cs_metabox_options' ), 100 );

			add_filter ( 'cs_framework_options', array ( $this, 'dt_procedure_cs_framework_options' ) );

			add_filter( 'meni_header_footer_default_cpt', array( $this, 'add_header_footer_metabox' ) );
		}

		function dt_register_cpt() {

			$procedure_slug = meni_cs_get_option( 'single-procedure-slug', 'procedure' );
			$singular_name  = meni_cs_get_option( 'singular-procedure-name', __('Procedure', 'designthemes-core') );
			$plural_name    = meni_cs_get_option( 'plural-procedure-name', __('Procedures', 'designthemes-core') );


			$labels = array (
				'name'               => 	$plural_name,
				'all_items'          => 	__ ( 'All ', 'designthemes-core' ) . $plural_name,
				'singular_name'      => 	$singular_name,
				'add_new'            => 	__ ( 'Add New', 'designthemes-core' ),
				'add_new_item'       => 	__ ( 'Add New ', 'designthemes-core' ) . $singular_name,
				'edit_item'          => 	__ ( 'Edit ', 'designthemes-core' ) . $singular_name,
				'new_item'           => 	__ ( 'New ', 'designthemes-core' ) . $singular_name,
				'view_item'          => 	__ ( 'View ', 'designthemes-core' ) . $singular_name,
				'search_items'       =>	__ ( 'Search ', 'designthemes-core' ) . $plural_name,
				'not_found'          => 	__ ( 'No ', 'designthemes-core' ) . $plural_name . __ ( ' found', 'designthemes-core' ),
				'not_found_in_trash' => __ ( 'No ', 'designthemes-core' ) . $plural_name . __ ( ' found in Trash', 'designthemes-core' ),
				'parent_item_colon'  => 	__ ( 'Parent ', 'designthemes-core' ) . $singular_name . ':',
				'menu_name'          => 	$plural_name
			);

			$args = array(
				'labels'        => $labels,
				'public'        => true,
				'menu_position' => 22,
				'hierarchical'  => true,
				'show_in_rest'  => true,
				'supports'      => array( 'title', 'editor', 'page-attributes','revisions'),
				'rewrite'       => array ( 'slug' => $procedure_slug ),
			);

			register_post_type( 'dt_procedures', $args );
		}

		function dt_enable_vc( $default ) {

			array_push( $default, 'dt_procedures' );

			return $default;
		}		

		function dt_procedure_cs_metabox_options( $options ) {

			$meta_layout_section =array(
				'name'   => 'layout_section',
				'title'  => esc_html__('Layout', 'designthemes-core'),
				'icon'   => 'fa fa-columns',
				'fields' =>  array(
					array(
						'id'         => 'layout',
						'type'       => 'image_select',
						'title'      => esc_html__('Page Layout', 'designthemes-core' ),
						'options'    => array(
							'global-site-layout' => MENI_THEME_URI . '/cs-framework-override/images/admin-option.png',
							'content-full-width' => MENI_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
							'with-left-sidebar'  => MENI_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
							'with-right-sidebar' => MENI_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
							'with-both-sidebar'  => MENI_THEME_URI . '/cs-framework-override/images/both-sidebar.png'
						),
						'default'    => 'global-site-layout',
						'attributes' => array( 'data-depend-id' => 'page-layout' )
					),
					array(
						'id'         => 'show-standard-sidebar-left',
						'type'       => 'switcher',
						'title'      => esc_html__('Show Standard Left Sidebar', 'designthemes-core' ),
						'dependency' => array( 'page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
						'default'    => true,
					),
					array(
						'id'         => 'widget-area-left',
						'type'       => 'select',
						'title'      => esc_html__('Choose Left Widget Areas', 'designthemes-core' ),
						'class'      => 'chosen',
						'options'    => meni_custom_widgets(),
						'dependency' => array( 'page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
						'attributes' => array( 
							'multiple'  => 'multiple',
							'data-placeholder' => esc_html__('Select Left Widget Areas','designthemes-core'),
							'style' => 'width: 400px;'
						),
					),
					array(
						'id'         => 'show-standard-sidebar-right',
						'type'       => 'switcher',
						'title'      => esc_html__('Show Standard Right Sidebar', 'designthemes-core' ),
						'dependency' => array( 'page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
						'default'    => true,
					),
					array(
						'id'         => 'widget-area-right',
						'type'       => 'select',
						'title'      => esc_html__('Choose Right Widget Areas', 'designthemes-core' ),
						'class'      => 'chosen',
						'options'    => meni_custom_widgets(),
						'dependency' => array( 'page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
						'attributes' => array( 
							'multiple' => 'multiple',
							'data-placeholder' => esc_html__('Select Right Widget Areas','designthemes-core'),
							'style' => 'width: 400px;'
						),
					)
				)
			);

			$meta_breadcrumb_section = array(
				'name'   => 'breadcrumb_section',
				'title'  => esc_html__('Breadcrumb', 'designthemes-core'),
				'icon'   => 'fa fa-sitemap',
				'fields' =>  array(					
				    array(
						'id'      => 'enable-sub-title',
						'type'    => 'switcher',
						'title'   => esc_html__('Show Breadcrumb', 'designthemes-core' ),
						'default' => true
				    ),
				    array(
						'id'         => 'breadcrumb_position',
						'type'       => 'select',
						'title'      => esc_html__('Position', 'designthemes-core' ),
						'default'    => 'header-top-relative',	
						'dependency' => array( 'enable-sub-title', '==', 'true' ),
						'options'    => array(
							'header-top-absolute' => esc_html__('Behind the Header','designthemes-core'),
							'header-top-relative' => esc_html__('Default','designthemes-core'),
				    	),
					),    
					array(
						'id'         => 'breadcrumb_background',
						'type'       => 'background',
						'title'      => esc_html__('Background', 'designthemes-core' ),
						'dependency' => array( 'enable-sub-title', '==', 'true' ),
					),
				)
			);

			$options[] = array(
			  'id'        => '_custom_settings',
			  'title'     => "Options",
			  'post_type' => 'dt_procedures',
			  'context'   => 'normal',
			  'priority'  => 'default',
			  'sections'  => array( $meta_layout_section, $meta_breadcrumb_section )
			);

			return $options;
		}

		function dt_procedure_cs_framework_options( $options ) {

			$options[] = array(

				'name'   => 'procedures',
				'title'  => esc_html__('Procedures', 'designthemes-core'),
				'icon'   => 'fa fa-cubes',
				'fields' => array(

					// ----------------------------------------
					// a option sub section for permalinks    -
					// ----------------------------------------
					array(
						'type'    => 'subheading',
						'content' => esc_html__( 'Permalinks', 'designthemes-core' ),
					),
					array(
						'id'    => 'single-procedure-slug',
						'type'  => 'text',
						'title' => esc_html__('Single Procedure slug', 'designthemes-core'),
						'after' => '<p class="cs-text-info">'.esc_html__('Do not use characters not allowed in links. Use, eg. procedure-item ', 'designthemes-core').'<br> <b>'.esc_html__('After made changes save permalinks.', 'designthemes-core').'</b></p>',
					),
					array(
						'id'    => 'singular-procedure-name',
						'type'  => 'text',
						'title' => esc_html__('Singular Procedure Name', 'designthemes-core'),
						'after' => '<p class="cs-text-info">'.esc_html__('By default "Procedure", save options & reload.', 'designthemes-core').'</p>',
					),
					array(
						'id'    => 'plural-procedure-name',
						'type'  => 'text',
						'title' => esc_html__('Plural Procedure Name', 'designthemes-core'),
						'after' => '<p class="cs-text-info">'.esc_html__('By default "Procedures". save options & reload.', 'designthemes-core').'</p>',
					),
				),
			);

			return $options;
		}

		function add_header_footer_metabox( $posts ) {

			array_push( $posts, 'dt_procedures' );

			return $posts;
		}
	}
}