<?php
if (! class_exists ( 'DTCoreCustomPostTypes' )) {

	/**
	 *
	 * @author iamdesigning11
	 *        
	 */
	class DTCoreCustomPostTypes {
		
		function __construct() {
			
			// Header Template Post Type
			require_once plugin_dir_path ( __FILE__ ) . '/header/dt-header-post-type.php';
			if( class_exists('DTHeaderPostType') ){
				new DTHeaderPostType();
			}
			
			// Footer Template Post Type
			require_once plugin_dir_path ( __FILE__ ) . '/footer/dt-footer-post-type.php';
			if( class_exists('DTFooterPostType') ){
				new DTFooterPostType();
			}

			// Mega Menu Template Post Type
			require_once plugin_dir_path ( __FILE__ ) . '/menu/dt-mega-menu-post-type.php';
			if( class_exists('DTMegaMenuPostType') ){
				new DTMegaMenuPostType();
			}

			// Procedure Post Type
			require_once plugin_dir_path ( __FILE__ ) . '/dt-procedure-post-type.php';
			if( class_exists('DTProcedurePostType') ){
				new DTProcedurePostType();
			}

			// Package Post Type
			require_once plugin_dir_path ( __FILE__ ) . '/dt-package-post-type.php';
			if( class_exists('DTPackagePostType') ){
				new DTPackagePostType();
			}
			
			// Add Hook into the 'template_include' filter
			add_filter ( 'template_include', array ( $this, 'dt_template_include' ) );			
		}

		/**
		 * To load procedure pages in front end
		 *
		 * @param string $template        	
		 * @return string
		 */
		function dt_template_include($template) {

			if (is_singular( 'dt_procedures' )) {
				if (! file_exists ( get_stylesheet_directory () . '/single-dt_procedure.php' )) {
					$template = plugin_dir_path ( __FILE__ ) . 'templates/single-dt_procedure.php';
				}
			}

			if (is_singular( 'dt_packages' )) {
				if (! file_exists ( get_stylesheet_directory () . '/single-dt_package.php' )) {
					$template = plugin_dir_path ( __FILE__ ) . 'templates/single-dt_package.php';
				}
			}			 
			return $template;
		}		
	}
}
?>