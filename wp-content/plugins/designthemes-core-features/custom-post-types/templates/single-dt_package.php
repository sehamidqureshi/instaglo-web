<?php get_header();

    $settings = get_post_meta($post->ID,'_custom_settings',TRUE);
    $settings = is_array( $settings ) ?  array_filter( $settings )  : array();

	if( empty($settings) || ( array_key_exists( 'layout', $settings ) && $settings['layout'] == 'global-site-layout' ) ) {
		$settings['enable-sub-title']            = true;
		$settings['layout']                      = cs_get_option( 'site-global-sidebar-layout' );
		$settings['show-standard-sidebar-left']  = true;
		$settings['show-standard-sidebar-right'] = true;
    }

	$header_class      = '';
	$global_breadcrumb = cs_get_option( 'show-breadcrumb' );

    if( !empty( $global_breadcrumb ) ) {
        if( isset( $settings['enable-sub-title'] ) && $settings['enable-sub-title'] ) {
            $header_class = isset( $settings['breadcrumb_position'] ) ? $settings['breadcrumb_position'] : '';
		}
	}?>
<!-- ** Header Wrapper ** -->
<div id="header-wrapper" class="<?php echo esc_attr($header_class); ?>">

    <!-- **Header** -->
    <header id="header">

        <div class="container"><?php
            /**
             * meni_header hook.
             * 
             * @hooked meni_vc_header_template - 10
             *
             */
            do_action( 'meni_header' ); ?>
        </div>
    </header><!-- **Header - End ** -->

    <!-- ** Breadcrumb ** -->
    <?php 
        # Global Breadcrumb
        if( !empty( $global_breadcrumb ) ) {
            if( isset( $settings['enable-sub-title'] ) && $settings['enable-sub-title'] ) {
                $breadcrumbs = array();
                $bstyle = meni_cs_get_option( 'breadcrumb-style', 'default' );

                if( $post->post_parent ) {
                    $parent_id  = $post->post_parent;
                    $parents = array();

                    while( $parent_id ) {
                        $page = get_page( $parent_id );
                        $parents[] = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
                        $parent_id  = $page->post_parent;
                    }

                    $parents = array_reverse( $parents );
                    $breadcrumbs = array_merge_recursive($breadcrumbs, $parents);
                }

                $breadcrumbs[] = the_title( '<span class="current">', '</span>', false );
				$bcsettings = isset( $settings['breadcrumb_background'] ) ? $settings['breadcrumb_background'] : array();
                $style = meni_breadcrumb_css( $bcsettings );

                meni_breadcrumb_output ( the_title( '<h1>', '</h1>',false ), $breadcrumbs, $bstyle, $style );
            }
        }
    ?><!-- ** Breadcrumb End ** -->
</div><!-- ** Header Wrapper - End ** -->

<!-- **Main** -->
<div id="main">

    <!-- ** Container ** -->
    <div class="container"><?php
        $page_layout  = array_key_exists( "layout", $settings ) ? $settings['layout'] : "content-full-width";
        $layout = meni_page_layout( $page_layout );
        extract( $layout );

        if ( $show_sidebar ) {
            if ( $show_left_sidebar ) {?>                
                <!-- Secondary Left -->
                <section id="secondary-left" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php
                    meni_show_sidebar( 'dt_packages', $post->ID, 'left' );
                ?></section><!-- Secondary Left End --><?php
            }
        }?>

        <!-- Primary -->
        <section id="primary" class="<?php echo esc_attr( $page_layout );?>"><?php
            if( have_posts() ) {
                while( have_posts() ) {
                    the_post(); ?>
                    <!-- #post-<?php the_ID(); ?> -->
                    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>><?php
                    	the_content(); 
                    	wp_link_pages( array(	
							'before'         =>	'<div class="page-link">',
							'after'          =>	'</div>',
							'link_before'    =>	'<span>',
							'link_after'     =>	'</span>',
							'next_or_number' =>	'number',
							'pagelink'       =>	'%',
							'echo'           =>	1 
                    	) );
                    	edit_post_link( esc_html__( ' Edit ','meni' ) );?>
                    </div><!-- #post-<?php the_ID(); ?> --><?php
                }
            }?>
        </section><!-- Primary End --><?php

        if ( $show_sidebar ) {
            if ( $show_right_sidebar ) {?>
                <!-- Secondary Right -->
                <section id="secondary-right" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php
                    meni_show_sidebar( 'dt_packages', $post->ID, 'right' );
                ?></section><!-- Secondary Right End --><?php
            }
        }?>    	
    </div><!-- ** Container End ** -->
    
</div><!-- **Main - End ** -->    
<?php get_footer(); ?>