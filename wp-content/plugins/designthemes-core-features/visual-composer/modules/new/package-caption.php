<?php
vc_map( array(
	"name"   => esc_html__( "Package Caption", 'designthemes-core' ),
	"base"   => "dt_sc_package_caption",
	"icon"   => "dt_sc_package_caption",
	"params" => array(
		array(
			"type"        => "textfield",
			"heading"     => esc_html__( "Title", "designthemes-class" ),
			"param_name"  => "title",
			"value"       => 'Lorem ipsum dolor sit amet',
			'save_always' => true,
			"description" => esc_html__( 'Enter title of bmi calculator.', 'designthemes-class' ),
		),
		array(
			"type"        => "textfield",
			"heading"     => esc_html__( "Sub Title", "designthemes-class" ),
			"param_name"  => "sub_title",
			"value"       => 'Lorem ipsum sit',
			'save_always' => true,
			"description" => esc_html__( 'Enter title of bmi calculator.', 'designthemes-class' ),
		),
        array(
			'type'        => 'attach_image',
			'param_name'  => 'image',
			'heading'     => esc_html__( 'Image', 'designthemes-core' ),
			'save_always' => true,
        ),		
  		array(
			'type'       => 'textarea_html',
			'heading'    => esc_html__( 'Content', 'designthemes-class' ),
			'param_name' => 'content',
			'value'      => '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor</p>
			<ul> <li>Nullam congue nibh</li> <li>Vivamus suscipit ante</li> <li>Fusce cursus purus</li></ul>'
  		),
  		array(
			'type'        => 'checkbox',
			'heading'     => esc_html__( 'Switch Image and Content', 'designthemes-core' ),
			'param_name'  => 'switch_content',
			'value'       => array( esc_html__( 'Yes', 'designthemes-core' ) => 'yes' ),
			'std'         => 'yes',
			'save_always' => true,
        ),  		
  		array(
			'type'        => 'vc_link',
			'heading'     => esc_html__( 'URL (Link)', 'designthemes-core' ),
			'param_name'  => 'link',
			'description' => esc_html__( 'Add link to button', 'designthemes-core' ),
		),  		
        array(
			'type'       => 'css_editor',
			'heading'    => esc_html__( 'Css', 'designthemes-core' ),
			'param_name' => 'css',
			'group'      => esc_html__( 'Design options', 'designthemes-core' ),
        ),
        array(
			"type"        => "textfield",
			"heading"     => esc_html__( "Extra class name", 'designthemes-core' ),
			"param_name"  => "class",
			'description' => esc_html__('Style particular element differently - add a class name and refer to it in custom CSS','designthemes-core'),
			'group'       => esc_html__( 'Design options', 'designthemes-core' ),
        ),
        array(
			'type'       => 'animation_style',
			'heading'    => __( 'CSS Animation', 'designthemes-core' ),
			'param_name' => 'css_animation',
			'value'      => '',
			'group'      => esc_html__( 'Design options', 'designthemes-core' ),
			'settings'   => array(
				'type'   => 'in',
				'custom' => array(
					array(
						'label'  => __( 'Default', 'designthemes-core' ),
						'values' => array(
							__( 'Top to bottom', 'designthemes-core' )      => 'top-to-bottom',
							__( 'Bottom to top', 'designthemes-core' )      => 'bottom-to-top',
							__( 'Left to right', 'designthemes-core' )      => 'left-to-right',
							__( 'Right to left', 'designthemes-core' )      => 'right-to-left',
							__( 'Appear from center', 'designthemes-core' ) => 'appear',
						),
					),
				),
			),
			'description' => __( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'designthemes-core' ),
        ),
        array(
			"type"             => "textfield",
			"heading"          => __("Animation delay ( optional )", 'designthemes-core'),
			"edit_field_class" => 'vc_col-sm-6 vc_column',
			"param_name"       => "delay",
			"value"            => "0",
			"description"      => __("Set the animation delay ( e.g 200 )", 'designthemes-core'),
			'group'            => esc_html__( 'Design options', 'designthemes-core' ),
        ),  				
	)
) );		