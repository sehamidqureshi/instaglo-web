<?php

if (! class_exists ( 'DTMCSubscribe' ) ) {
    
    class DTMCSubscribe extends DTBaseSC {

        function __construct() {

            add_shortcode( 'dt_sc_mc_subscribe', array( $this, 'dt_sc_mc_subscribe' ) );

            add_action( 'wp_ajax_dt_mailchimp_subscribe', array( $this, 'dt_mailchimp_subscribe' ) );
            add_action( 'wp_ajax_nopriv_dt_mailchimp_subscribe', array( $this, 'dt_mailchimp_subscribe' ) );
        }

        function dt_mailchimp_subscribe() {

            $out = '';
            $apiKey = $_REQUEST['apikey'];
            $listId = $_REQUEST['listid'];

            if($apiKey != '' && $listId != '') {

                $data = array( 'email' => sanitize_email($_REQUEST['email']), 'status' => 'subscribed' );

                if( $this->check_membership($data, $apiKey, $listId) ) {

                    $out = '<span class="error-msg"><b>'.esc_html__('Error:', 'designthemes-core')
                        .'</b> '
                        . esc_html__('You have already subscribed with us !', 'designthemes-core').'</span>';
                } else {
                    $out = $this->register_member($data, $apiKey, $listId);
                }
            } else {

                $out = '<span class="error-msg"><b>'.esc_html__('Error:', 'designthemes-core').'</b> '.esc_html__('Please make sure valid mailchimp details are provided.', 'designthemes-core').'</span>';
            }

            echo wp_kses( $out, array( 'span' => array( 'class' => array() ), 'b' => array() ) );
            die();            
        }

        function check_membership( $data, $apiKey, $listId ) {

            $memberId = md5(strtolower($data['email']));
            $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
            $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members';
            $json = json_encode(array( 'email_address' => $data['email'], 'status' => $data['status'], ));

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $result = curl_exec($ch);
            curl_close($ch);

            $result_decode = json_decode( $result, true );
            foreach($result_decode['members'] as $key => $value) {
                if($value['email_address'] == $data['email']) {
                    return true;
                }
            }

            return false;
        }

        function register_member( $data, $apiKey, $listId ) {
            $memberId = md5(strtolower($data['email']));
            $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
            $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listId . '/members/' . $memberId;
            $json = json_encode( array( 'email_address' => $data['email'], 'status' => $data['status'] ));

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 10);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

            $result = curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);

            $result_decode = json_decode($result, true);
            if($httpCode == 200) {
                $out = '<span class="success-msg">'.esc_html__('Success! Please check your inbox or spam folder.', 'designthemes-core').'</span>';
            } else {
                $out = '<span class="error-msg"><b>'.$result_decode['title'].':</b> '.$result_decode['detail'].'</span>';
            }

            return $out;
        }        

        function dt_generate_css( $attrs ) {

            $css = '';
            $attrs['el_id'] = 'dt-'.$attrs['el_id'];

            if( !empty( $attrs['height'] ) ) {
                $css .= "\n".'div#'.esc_attr( $attrs['el_id'] ).' form .email-field-wrap, div#'.esc_attr( $attrs['el_id'] ).' form .btn-wrap { height:'.$attrs['height'].'px;}'."\n";
            }

            # Default State
                
                # Field                
                    $field_css = '';
                
                    # Field Text Color
                        $_field_color = '';
                        if( $attrs['input-color'] == 'custom' &&  !empty( $attrs['input-custom-color'] ) ) {
                            $_field_color = $attrs['input-custom-color'];
                        } else {
                            $_field_color = $this->dt_current_skin( $attrs['input-color'] );
                        }

                        if( !empty( $_field_color ) ) {
                            $field_css .= 'color:'.$_field_color.';';                            
                        }

                    # Fields BG Color
                    if( $attrs['input-shape'] == 'filled' ) {

                        $_field_bg_color = '';
                        if( $attrs['input-bg-color'] == 'custom' &&  !empty( $attrs['input-custom-bg-color'] ) ) {
                            $_field_bg_color = $attrs['input-custom-bg-color'];
                        } else {
                            $_field_bg_color = $this->dt_current_skin( $attrs['input-bg-color'] );
                        }

                        if( !empty( $_field_bg_color ) ) {
                            $field_css .= 'background-color:'.$_field_bg_color.';';
                        }
                    }

                    # Field Border Color
                    if( $attrs['input-shape'] == 'filled' || $attrs['input-shape'] == 'bordered' ) {

                        $field_css .= 'border-style:solid; border-width:1px;';

                        $_field_border_color = '';
                        if( $attrs['input-border-color'] == 'custom' &&  !empty( $attrs['input-custom-border-color'] ) ) {
                            $_field_border_color = $attrs['input-custom-border-color'];
                        } else {
                            $_field_border_color = $this->dt_current_skin( $attrs['input-border-color'] );
                        }

                        if( !empty( $_field_border_color ) ) {
                            $field_css .= 'border-color:' . $_field_border_color . ';';
                        }
                    }

                    if( !empty( $field_css ) ) {
                        $css .= "\n".'div#'.esc_attr( $attrs['el_id'] ).' > form > div {'.$field_css.'}'."\n";
                    }                                    
                # Field
                
                # Field Icon
                    $field_icon_css = '';
                    if( $attrs['use_icon'] == 'yes' ) {

                        # Field Icon Color
                            $_field_i_color = '';
                            if( $attrs['icon-color'] == 'custom' &&  !empty( $attrs['icon-custom-color'] ) ) {
                                $_field_i_color = $attrs['icon-custom-color']; 
                            } else {
                                $_field_i_color .= $this->dt_current_skin( $attrs['icon-color'] );
                            }

                            if( !empty( $_field_i_color ) ) {
                                $field_icon_css .= 'color:'. $_field_i_color.';';                                
                            }

                        # Field Icon BG Color
                            $_field_i_bg_color = '';
                            if( $attrs['icon-bg-color'] == 'custom' &&  !empty( $attrs['icon-custom-bg-color'] ) ) {
                                $_field_i_bg_color = $attrs['icon-custom-bg-color'];
                            } else {
                                $_field_i_bg_color = $this->dt_current_skin( $attrs['icon-bg-color'] );
                            }

                            if( !empty( $_field_i_bg_color ) ) {
                                $field_icon_css .= 'background-color:'.$_field_i_bg_color.';';
                            }

                        # Field Icon Border
                        $field_icon_css .= 'border-style:solid; border-width:1px;';

                            # Field Icon Border Color
                                $_field_i_border_color = '';
                                if( $attrs['icon-border-color'] == 'custom' &&  !empty( $attrs['icon-custom-border-color'] ) ) {
                                    $_field_i_border_color = $attrs['icon-custom-border-color'];
                                } else {
                                    $_field_i_border_color = $this->dt_current_skin( $attrs['icon-border-color'] );
                                }

                                if( !empty( $_field_i_border_color ) ) {
                                    $field_icon_css .= 'border-color:'.$_field_i_border_color.';';
                                }

                        if( !empty( $field_icon_css ) ) {
                            $css .= "\n".'div#'.esc_attr( $attrs['el_id'] ).' > form > div.email-field-wrap i {'.$field_icon_css.'}'."\n";
                        }
                    } 
                # Field Icon  

                # Button
                    # Icon
                        $i_color = '';
                        if( $attrs['btn_style'] == 'icon-only' || $attrs['btn_style'] == 'text-icon' ) {

                            $_i_color = '';
                            if( $attrs['btn-color'] == 'custom' &&  !empty( $attrs['btn-custom-color'] ) ) {
                                $_i_color = $attrs['btn-custom-color'];
                            } else {
                                $_i_color = $this->dt_current_skin( $attrs['btn-color'] );
                            }

                            if( !empty( $_i_color ) ) {
                                $i_color .= 'color:'.$_i_color.';';
                            }

                            if( !empty( $i_color ) ) {
                                $css .= "\n".'div#'.esc_attr( $attrs['el_id'] ).' .btn-wrap i {'.$i_color.'}'."\n";
                            }
                        }

                    # Button Item
                        $item_css = '';

                        # Button Text Color
                            $_btn_txt_color = '';
                            if( $attrs['btn-color'] == 'custom' &&  !empty( $attrs['btn-custom-color'] ) ) {
                                $_btn_txt_color = $attrs['btn-custom-color'];
                            } else {
                                $_btn_txt_color = $this->dt_current_skin( $attrs['btn-color'] );
                            }

                            if( !empty( $_btn_txt_color ) ) {
                                $item_css .= 'color:'.$_btn_txt_color.';';
                            }

                        # Button Background Color
                            if( $attrs['btn_shape'] == 'filled' ) {

                                $_btn_bg_color = '';
                                if( $attrs['btn-bg-color'] == 'custom' &&  !empty( $attrs['btn-custom-bg-color'] ) ) {
                                    $_btn_bg_color = $attrs['btn-custom-bg-color'];
                                } else {
                                    $_btn_bg_color = $this->dt_current_skin( $attrs['btn-bg-color'] );
                                }

                                if( !empty( $_btn_bg_color ) ) {
                                    $item_css .= 'background-color:'. $_btn_bg_color.';';
                                }
                            }

                        # Button Border Color
                            if( $attrs['btn_shape'] == 'filled' || $attrs['btn_shape'] == 'bordered' ) {

                                $_btn_border_color = '';
                                $item_css .= 'border-style:solid; border-width:1px;';

                                if( $attrs['btn-border-color'] == 'custom' &&  !empty( $attrs['btn-custom-border-color'] ) ) {
                                    $_btn_border_color = $attrs['btn-custom-border-color'];
                                } else {
                                    $_btn_border_color = $this->dt_current_skin( $attrs['btn-border-color'] );
                                } 

                                if( !empty( $_btn_border_color ) ) {
                                    $item_css .= 'border-color:'. $_btn_border_color.';';
                                }                                                           
                            }

                    if( !empty( $item_css ) ) {
                        $css .= "\n".'div#'.esc_attr( $attrs['el_id'] ).' .btn-wrap > div {'.$item_css.'}'."\n";
                    }

            # Hover Color
                
                # Field
                    $field_h_css = '';

                    # Field Text Color
                        $_field_h_color = '';
                        if( $attrs['input-hover-color'] == 'custom' &&  !empty( $attrs['input-hover-custom-color'] ) ) {
                            $_field_h_color = $attrs['input-hover-custom-color'];
                        } else {
                            $_field_h_color = $this->dt_current_skin( $attrs['input-hover-color'] );
                        }

                        if( !empty( $_field_h_color ) ) {
                            $field_h_css .= 'color:'.$_field_h_color.';';                            
                        }

                    # Fields BG Color
                        if( $attrs['input-shape'] == 'filled' ) {
                            $_field_h_bg_color = '';
                            if( $attrs['input-hover-bg-color'] == 'custom' &&  !empty( $attrs['input-hover-custom-bg-color'] ) ) {
                                $_field_h_bg_color = $attrs['input-hover-custom-bg-color'];
                            } else {
                                $_field_h_bg_color = $this->dt_current_skin( $attrs['input-hover-bg-color'] );
                            }
                            if( !empty( $_field_h_bg_color ) ) {
                                $field_h_css .= 'background-color:'.$_field_h_bg_color.';';
                            }
                        }

                    # Field Border Color
                        if( $attrs['input-shape'] == 'filled' || $attrs['input-shape'] == 'bordered' ) {
                            $field_h_css .= 'border-style:solid; border-width:1px;';
                            $_field_h_border_color = '';

                            if( $attrs['input-hover-border-color'] == 'custom' &&  !empty( $attrs['input-hover-custom-border-color'] ) ) {
                                $_field_h_border_color = $attrs['input-hover-custom-border-color'];
                            } else {
                                $_field_h_border_color = $this->dt_current_skin( $attrs['input-hover-border-color'] );
                            }
                            if( !empty( $_field_h_border_color ) ) {
                                $field_h_css .= 'border-color:'.$_field_h_border_color.';';
                            }
                        }                                                

                    if( !empty( $field_h_css ) ) {
                        $css .= "\n".'div#'.esc_attr( $attrs['el_id'] ).' > form > div:hover {'.$field_h_css.'}'."\n";
                    }
                # Field

                # Field Icon
                    $field_icon_h_css = '';
                    if( $attrs['use_icon'] == 'yes' ) {                        

                        # Field Icon Color
                            $_field_icon_h_color = '';
                            if( $attrs['icon-hover-color'] == 'custom' &&  !empty( $attrs['icon-hover-custom-color'] ) ) {
                                $_field_icon_h_color = $attrs['icon-hover-custom-color'];
                            } else {
                                $_field_icon_h_color = $this->dt_current_skin( $attrs['icon-hover-color'] );
                            }
                            if( !empty( $_field_icon_h_color ) ) {
                                $field_icon_h_css .= 'color:'.$_field_icon_h_color.';';
                            }

                        # Field Icon BG Color
                            $_field_icon_h_bg_color = '';
                            if( $attrs['icon-hover-bg-color'] == 'custom' &&  !empty( $attrs['icon-hover-custom-bg-color'] ) ) {
                                $_field_icon_h_bg_color = $attrs['icon-hover-custom-bg-color'];
                            } else {
                                $_field_icon_h_bg_color = $this->dt_current_skin( $attrs['icon-hover-bg-color'] );
                            }
                            if( !empty( $_field_icon_h_bg_color ) ) {
                                $field_icon_h_css .= 'background-color:'.$_field_icon_h_bg_color.';';
                            }

                        # Field Icon Border Color
                            $field_icon_h_css .= 'border-style:solid; border-width:1px;';
                            $_field_icon_h_border_color = '';

                            if( $attrs['icon-hover-border-color'] == 'custom' &&  !empty( $attrs['icon-hover-custom-border-color'] ) ) {
                                $_field_icon_h_border_color = $attrs['icon-hover-custom-border-color'];
                            } else {
                                $_field_icon_h_border_color = $this->dt_current_skin( $attrs['icon-hover-border-color'] );
                            }
                            if( !empty( $_field_icon_h_border_color ) ) {
                                $field_icon_h_css .= 'border-color:'.$_field_icon_h_border_color.';';
                            }                            

                        if( !empty( $field_icon_h_css ) ) {
                            $css .= "\n".'div#'.esc_attr( $attrs['el_id'] ).' > form > div.email-field-wrap i:hover {'.$field_icon_h_css.'}'."\n";
                        }
                    }
                # Field Icon

                # Button
                     # Icon
                        $i_h_color = '';
                        if( $attrs['btn_style'] == 'icon-only' || $attrs['btn_style'] == 'text-icon' ) {

                            if( $attrs['btn-hover-color'] == 'custom' &&  !empty( $attrs['btn-hover-custom-color'] ) ) {
                                $i_h_color = $attrs['btn-hover-custom-color'];
                            } else {
                                $i_h_color = $this->dt_current_skin( $attrs['btn-hover-color'] );
                            }

                            if( !empty( $i_color ) ) {
                                $css .= "\n".'div#'.esc_attr( $attrs['el_id'] ).' .btn-wrap:hover i {color:'.$i_h_color.';}'."\n";
                            }
                        }

                    # Button Item
                        $item_h_css       = '';
                        $_btn_h_txt_color = '';

                        # Button Text Color
                            if( $attrs['btn-hover-color'] == 'custom' &&  !empty( $attrs['btn-hover-custom-color'] ) ) {
                                $_btn_h_txt_color = $attrs['btn-hover-custom-color'];
                            } else {
                                $_btn_h_txt_color = $this->dt_current_skin( $attrs['btn-hover-color'] );
                            }
                            if( !empty( $_btn_h_txt_color ) ) {
                                $item_h_css .= 'color:'.$_btn_h_txt_color.';';
                            }

                        # Button Background Color
                            if( $attrs['btn_shape'] == 'filled' ) {

                                $_btn_h_bg_color = '';
                                if( $attrs['btn-hover-bg-color'] == 'custom' &&  !empty( $attrs['btn-hover-custom-bg-color'] ) ) {
                                    $_btn_h_bg_color = $attrs['btn-hover-custom-bg-color'];
                                } else {
                                    $_btn_h_bg_color = $this->dt_current_skin( $attrs['btn-hover-bg-color'] );
                                }

                                if( !empty( $_btn_h_bg_color ) ) {
                                    $item_h_css .= 'background-color:'.$_btn_h_bg_color.';';
                                }
                            }

                        # Button Border Color
                            if( $attrs['btn_shape'] == 'filled' || $attrs['btn_shape'] == 'bordered' ) {

                                $_btn_h_border_color = '';

                                $item_h_css .= 'border-style:solid; border-width:1px;';
                                if( $attrs['btn-hover-border-color'] == 'custom' &&  !empty( $attrs['btn-hover-custom-border-color'] ) ) {
                                    $_btn_h_border_color = $attrs['btn-hover-custom-border-color'];
                                } else {
                                    $_btn_h_border_color = $this->dt_current_skin( $attrs['btn-hover-border-color'] );
                                }

                                if( !empty( $_btn_h_border_color ) ) {
                                    $item_h_css .= 'border-color:'.$_btn_h_border_color.';';                                    
                                }
                            }                            

                    if( !empty( $item_h_css ) ) {
                        $css .= "\n".'div#'.esc_attr( $attrs['el_id'] ).' .btn-wrap:hover > div {'.$item_h_css.'}'."\n";
                    }                                                    

            return $css;
        }

        function dt_sc_mc_subscribe( $attrs, $content = null ) {

            extract ( shortcode_atts ( array (
                'el_id'    => '',
                'listid'   => '',
                'radius'   => '',
                'height'   => '',
                'display'  => '',
                'gap'      => '',
                'el_class' => '',

                # INPUT
                    'input_label' => '',
                    'placeholder' => '',
                    'input-shape' => '',
                    'input-color' => '',
                    'input-custom-color' => '',

                    'input-bg-color' => '',
                    'input-custom-bg-color' => '',

                    'input-border-color' => '',
                    'input-custom-border-color' => '',

                    'input-hover-color' => '',
                    'input-hover-custom-color' => '',

                    'input-hover-bg-color' => '',
                    'input-hover-custom-bg-color' => '',

                    'input-hover-border-color' => '',
                    'input-hover-custom-border-color' => '',

                    'use_icon' => '',
                    'icon_alignment' => '',
                    'icon_type' => '',
                    'icon_type_entypo' => '',
                    'icon_type_fontawesome' => '',
                    'icon_type_icon_moon_line' => '',
                    'icon_type_icon_moon_solid' => '',
                    'icon_type_icon_moon_ultimate' => '',
                    'icon_type_linecons' => '',
                    'icon_type_material_design_iconic_font' => '',
                    'icon_type_material' => '',
                    'icon_type_monosocial' => '',
                    'icon_type_openiconic' => '',
                    'icon_type_pe_icon_7_stroke' => '',
                    'icon_type_stroke' => '',
                    'icon_type_typicons' => '',

                    'icon-color' => '',
                    'icon-custom-color' => '',

                    'icon-bg-color' => '',
                    'icon-custom-bg-color' => '',

                    'icon-border-color' => '',
                    'icon-custom-border-color' => '',

                    'icon-hover-color' => '',
                    'icon-hover-custom-color' => '',

                    'icon-hover-bg-color' => '',
                    'icon-hover-custom-bg-color' => '',

                    'icon-hover-border-color' => '',
                    'icon-hover-custom-border-color' => '',

                # Submit Button
                    'btn_shape' => '',
                    'btn_style' => '',
                    'btn_label' => '',
                    'btn_layout' => '',
                    'btn_icon_type' => '',
                    'btn_icon_type_entypo' => '',
                    'btn_icon_type_fontawesome' => '',
                    'btn_icon_type_icon_moon_line' => '',
                    'btn_icon_type_icon_moon_solid' => '',
                    'btn_icon_type_icon_moon_ultimate' => '',
                    'btn_icon_type_linecons' => '',
                    'btn_icon_type_material_design_iconic_font' => '',
                    'btn_icon_type_material' => '',
                    'btn_icon_type_monosocial' => '',
                    'btn_icon_type_openiconic' => '',
                    'btn_icon_type_pe_icon_7_stroke' => '',
                    'btn_icon_type_stroke' => '',
                    'btn_icon_type_typicons' => '',

                    'btn-color' => '',
                    'btn-custom-color' => '',

                    'btn-bg-color' => '',
                    'btn-custom-bg-color' => '',

                    'btn-border-color' => '',
                    'btn-custom-border-color' => '',

                    'btn-hover-color' => '',
                    'btn-hover-custom-color' => '',

                    'btn-hover-bg-color' => '',
                    'btn-hover-custom-bg-color' => '',

                    'btn-hover-border-color' => '',
                    'btn-hover-custom-border-color' => '',

                'css' => '',
                'align' => '',

                'css_animation' => '',
                'delay' => '0',
            ), $attrs ) );

            $out = '';
            $api_key = cs_get_option( 'mailchimp-key' );

            if( !empty( $api_key ) ) {

                $el_id = 'dt-'.$el_id;
                $css_classes = array(
                    $radius,
                    $display,
                    $gap, 
                    'dt-mc-subscribe',
                    'align-'.$align,
                    $el_class,
                    $this->getCSSAnimation($css_animation),
                    vc_shortcode_custom_css_class( $css ),
                );
                $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), 'dt_sc_mc_subscribe', $attrs ) );

                # Custom CSS
                $custom_css = '';
                $custom_css .= $this->dt_generate_css( $attrs );                 

                # Email
                    $field  = '';
                    $field .= '<div class="email-field-wrap">';
                    $field .= ' <div>';
                        $field .= !empty( $input_label ) ? '<label>'.esc_html( $input_label ).'</label>' : '';

                        if( $use_icon == 'yes' ) {
                            vc_icon_element_fonts_enqueue( $icon_type );
                            $field .= '<i class="'.${'icon_type_'.$icon_type}.' '.$icon_alignment.'"> </i>';
                        }

                        $field .= '<input type="email" name="dt_mc_emailid" required="required" ';
                        $field .= !empty( $placeholder ) ? 'placeholder="'.esc_attr( $placeholder ).'" />' : '/>';
                    $field .= ' </div>';
                    $field .= '</div>';

                # Button
                    $button = '';
                    $button .= '<div class="btn-wrap '.$btn_layout.' '.$btn_style.'">';
                    $button .= ' <div>';

                        if( $btn_style == 'text-icon' || $btn_style == 'icon-only' ) {

                            vc_icon_element_fonts_enqueue( $btn_icon_type );
                            
                            $button .= '<i class="'.${'btn_icon_type_'.$btn_icon_type}.'"> </i>';
                            if( $btn_style == 'icon-only' ) {
                                $button .= "<input type='submit' name='mc_submit' value=''>";
                            }
                        }

                        if( $btn_style == 'text-only' || $btn_style == 'text-icon' ) {
                            $button .= "<input type='submit' name='mc_submit' value='{$btn_label}'>";
                        }
                    $button .= ' </div>';
                    $button .= '</div>';


                $out .= '<div id="' . esc_attr($el_id) . '" class="' . esc_attr($css_class) . '" data-delay="' . $delay . '">';
                $out .= '   <div class="dt-subscribe-msg"> </div>';
                $out .= '   <form name="dt-subscribe" method="post">';
                $out .= "       <input type='hidden' name='dt_mc_listid' value='$listid' />";
                $out .= "       <input type='hidden' name='dt_mc_apikey' value='".$api_key."'/>";
                $out .=         $field;
                $out .=         $button;
                $out .= '   </form>';
                $out .= '</div>';

                if( !empty( $custom_css ) ) {
                    $this->dt_print_css( $custom_css ); 
                }
            } else {
                $out .= '<div class="vc_message_box vc_message_box-standard vc_message_box-rounded vc_color-warning">';
                $out .= '    <div class="vc_message_box-icon"> <i class="fas fa-exclamation-triangle"></i> </div> ';
                $out .= '    <p> <b>Oops!</b> <br/> '. __( 'It looks like you forgot to set your Mailchimp API key.', 'designthemes-core') .'</p>';
                $out .= '</div>';
            }

            return $out;
        }
    }
}

new DTMCSubscribe();