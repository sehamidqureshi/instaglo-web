<?php
if (! class_exists ( 'DTMeniPackageCaption' ) ) {
    
    class DTMeniPackageCaption extends DTBaseSC {

        function __construct() {

            add_shortcode( 'dt_sc_package_caption', array( $this, 'dt_sc_package_caption' ) );
        }
        function dt_sc_package_caption( $attrs, $content = null ) {

            extract ( shortcode_atts ( array (
            	'title'          => '',
            	'sub_title'      => '',
            	'image'          => '',
            	'switch_content' => '',
            	'link'           => '',
            	'css'            => '',
            	'class'          => '',
            	'css_animation'  => '',
            	'delay'          => '',            	
            ), $attrs ) );

            $css_classes = array( 
                'dt-sc-meni-package-caption',
                $class,
                $this->getCSSAnimation( $css_animation ),
                vc_shortcode_custom_css_class( $css ),
            );

			if( $switch_content ) {
				$css_classes[] = 'alternate';
			}

            $css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), 'dt_sc_package_caption', $attrs ) );

            $default_src = vc_asset_url( 'vc/no_image.png' );

			$img = wp_get_attachment_image_url( $image, 'full' );

			if ( ! $img ) {
				$img = $default_src;
			}

			$content = $this->dt_sc_helper( $content );

            ob_start();
		    echo '<div  class="' . esc_attr($css_class) . '" data-delay="' . $delay . '">';
		    	echo '<div class="dt-sc-meni-image">';
					echo '<div class="" style="background-image:url('.$img.')"></div>';
		    	echo '</div>';
		    	echo '<div class="dt-sc-meni-content-wrapper">';
		    		echo !empty( $title ) ? '<h5>'.$title.'</h5>' : '';
		    		echo !empty( $sub_title ) ? '<h6>'.$sub_title.'</h6>' : '';
		    		if( !empty( $content ) ) {
			    		echo '<div class="dt-sc-meni-content">';
			    			echo $content;
			    		echo '</div>';
		    		}

		    		$url = vc_build_link( $link );
		    		if ( strlen( $link ) > 0 && strlen( $url['url'] ) > 0 ) {
		    			echo '<a class="dt-sc-button bordered small" href="' . esc_attr( $url['url'] ) . '" title="' . esc_attr( $url['title'] ) . '" target="'.( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self' ) . '">'. esc_html( $url['title'] ) .'</a>';
					}
		    	echo '</div>';		    	
		    echo '</div>';
            $output = ob_get_clean();

            return $output;
        }
    }
}

new DTMeniPackageCaption();