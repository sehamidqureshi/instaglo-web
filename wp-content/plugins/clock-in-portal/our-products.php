<div class="wrap elw-page-welcome about-wrap seting-page">

    <div class="row col-md-12 settings">
        <div class=" col-md-4">
            <div class="col-md-6">
                <?php
					echo '<img src="' . plugins_url( 'image/logo.png', __FILE__ ) . '" class="img-responsive" alt="img"/> ';
				?>
            </div>
        </div>

        <div class=" col-md-6">
            <h2><span class="elw_shortcode_heading"><?php esc_html_e('Responsive WordPress Themes & Plugins', CIP_FREE_TXTDM ); ?></span></h2>
        </div>
    </div>

    </header>
    <!-- Header -->
    <!-- Themes & Plugin -->
    <div class="col-md-12  product-main-cont">
        <ul class="nav nav-tabs product-tbs">
            <li class="active nav-item"><a class="nav-link" data-toggle="tab" href="#themesd"><?php esc_html_e('Themes', CIP_FREE_TXTDM ); ?> </a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#pluginsd"><?php esc_html_e('Plugins', CIP_FREE_TXTDM ); ?></a></li>
            <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#offers"><?php esc_html_e('Offers', CIP_FREE_TXTDM ); ?></a></li>
        </ul>

        <div class="tab-content">
            <div id="themesd" class="tab-pane fade in active">
                <div class="space  theme active">

                    <div class=" p_head theme">
                        <h1 class="section-title"><?php esc_html_e('WordPress Themes', CIP_FREE_TXTDM ); ?></h1>

                    </div>

                    <div class="row p_plugin blog_gallery">
                        <div class="col-xs-12 col-sm-4 col-md-5 p_plugin_pic">
                            <div class="img-thumbnail">

                                <?php
						echo '<img src="' . plugins_url( 'image/2.png', __FILE__ ) . '" class="img-responsive" alt="img"/> ';
					?>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 p_plugin_desc">
                            <div class="row p-box">
                                <h2><a href=""><?php esc_html_e('Eatos - Restaurant Theme', CIP_FREE_TXTDM ); ?></a></h2>
                                <p><strong><?php esc_html_e('Tags:', CIP_FREE_TXTDM ); ?> </strong><?php esc_html_e('Customize Front Page, Multilingual, Complete Documentation, Theme Option Panel, Unlimited Color Skins, Multiple Background Patterns, Multiple Theme Templates, 5 Portfolio Layout, 3 Page Layout and many more.', CIP_FREE_TXTDM ); ?></p>
                                <div>
                                    <p><strong><?php esc_html_e('Description:', CIP_FREE_TXTDM ); ?> </strong> <?php esc_html_e('You can use this multipurpose WordPress theme is flexible enough to let you build a website of your imagination effortlessly..', CIP_FREE_TXTDM ); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-2 p_plugin_pic">
                            <div class="price">
                                <span class="currency"><?php esc_html_e('USD', CIP_FREE_TXTDM ); ?></span>
                                <span class="price-number"><?php esc_html_e('$', CIP_FREE_TXTDM ); ?><span><?php esc_html_e('11', CIP_FREE_TXTDM ); ?></span></span>
                            </div>
                            <div class="btn-group-vertical">
                                <a class="btn btn-primary btn-lg" href="https://weblizar.com/themes/eatos-premium-restaurant-theme/"><?php esc_html_e('Demo', CIP_FREE_TXTDM ); ?></a>
                                <a class="btn btn-success btn-lg" href="https://weblizar.com/themes/eatos-premium-restaurant-theme/"><?php esc_html_e('Buy Now', CIP_FREE_TXTDM ); ?></a>
                            </div>
                        </div>
                    </div>

                    <div class="row p_plugin blog_gallery">
                        <div class="col-xs-12 col-sm-4 col-md-5 p_plugin_pic">
                            <div class="img-thumbnail">
                                <?php
						echo '<img src="' . plugins_url( 'image/1.png', __FILE__ ) . '" class="img-responsive" alt="img"/> ';
					?>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 p_plugin_desc">
                            <div class="row p-box">
                                <h2><a href=""><?php esc_html_e('Nineteen Premium Theme', CIP_FREE_TXTDM ); ?> </a></h2>
                                <p><strong><?php esc_html_e('Tags:', CIP_FREE_TXTDM ); ?> </strong><?php esc_html_e('clean, responsive, portfolio, blog, e-commerce, Business, WordPress, dark, real estate, shop, restaurant, ele…', CIP_FREE_TXTDM ); ?></p>
                                <div>
                                    <p><strong><?php esc_html_e('Description:', CIP_FREE_TXTDM ); ?> </strong> <?php esc_html_e('Creative agencies, financial advisors, business development institutions, investment centers and other local business foundations can make the best out of Nineteen Premium template.', CIP_FREE_TXTDM ); ?> </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-2 p_plugin_pic">
                            <div class="price">
                                <span class="currency"><?php esc_html_e('USD', CIP_FREE_TXTDM ); ?></span>
                                <span class="price-number"><?php esc_html_e('$', CIP_FREE_TXTDM ); ?><span><?php esc_html_e('20', CIP_FREE_TXTDM ); ?></span></span>
                            </div>
                            <div class="btn-group-vertical">
                                <a class="btn btn-primary btn-lg" href="https://weblizar.com/themes/nineteen-premium-theme-for-business/"><?php esc_html_e('Demo', CIP_FREE_TXTDM ); ?></a>
                                <a class="btn btn-success btn-lg" href="https://weblizar.com/themes/nineteen-premium-theme-for-business/"><?php esc_html_e('Buy Now', CIP_FREE_TXTDM ); ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="row p_plugin blog_gallery">
                        <div class="col-xs-12 col-sm-4 col-md-5 p_plugin_pic">
                            <div class="img-thumbnail">
                                <?php
						echo '<img src="' . plugins_url( 'image/explora.jpg', __FILE__ ) . '" class="img-responsive" alt="img"/> ';
					?>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 p_plugin_desc">
                            <div class="row p-box">
                                <h2><a href=""><?php esc_html_e('Explora- Ultimate Multi-Purpose WordPress Theme', CIP_FREE_TXTDM ); ?></a></h2>
                                <p><strong><?php esc_html_e('Tags:', CIP_FREE_TXTDM ); ?> </strong><?php esc_html_e('clean, responsive, portfolio, blog, e-commerce, Business, WordPress, Corporate, dark, real estate, shop, restaurant, ele…', CIP_FREE_TXTDM ); ?></p>
                                <div>
                                    <p><strong><?php esc_html_e('Description:', CIP_FREE_TXTDM ); ?> </strong> <?php esc_html_e('Explora Premium is a multi-purpose responsive theme coded & designed with a lot of care and love. You can use it for your business, portfolio, blogging or any type of site. ', CIP_FREE_TXTDM ); ?></div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-2 p_plugin_pic">
                            <div class="price">
                                <span class="currency"><?php esc_html_e('USD', CIP_FREE_TXTDM ); ?></span>
                                <span class="price-number"><?php esc_html_e('$', CIP_FREE_TXTDM ); ?><span><?php esc_html_e('25', CIP_FREE_TXTDM ); ?></span></span>
                            </div>
                            <div class="btn-group-vertical">
                                <a class="btn btn-primary btn-lg" href="https://weblizar.com/explora-premium/"><?php esc_html_e('Demo', CIP_FREE_TXTDM ); ?></a>
                                <a class="btn btn-success btn-lg" href="https://weblizar.com/explora-premium/"><?php esc_html_e('Buy Now', CIP_FREE_TXTDM ); ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="row p_plugin blog_gallery">
                        <div class="col-xs-12 col-sm-4 col-md-5 p_plugin_pic">
                            <div class="img-thumbnail">
                                <?php
						echo '<img src="' . plugins_url( 'image/Guardian.jpg', __FILE__ ) . '" class="img-responsive" alt="img"/> ';
					?>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 p_plugin_desc">
                            <div class="row p-box">
                                <h2><a href=""><?php esc_html_e('Guardian- Corporate Business Theme', CIP_FREE_TXTDM ); ?></a></h2>
                                <p><strong><?php esc_html_e('Tags: ', CIP_FREE_TXTDM ); ?></strong><?php esc_html_e('Multiple Background Patterns, Rich color changer, Boxed/wide layout styles, Additional Page, WPML & Woo Commerce.', CIP_FREE_TXTDM ); ?></p>
                                <div>
                                    <p><strong><?php esc_html_e('Description: ', CIP_FREE_TXTDM ); ?></strong> <?php esc_html_e('Guardian Premium Theme is a super professional one page WordPress theme for modern businesses. Ideal for creative agencies, startups, small businesses, and freelancers and best of all it is so easy to use that you can have your website up in minutes.', CIP_FREE_TXTDM ); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-2 p_plugin_pic">
                            <div class="price">
                                <span class="currency"><?php esc_html_e('USD', CIP_FREE_TXTDM ); ?></span>
                                <span class="price-number"><?php esc_html_e('$', CIP_FREE_TXTDM ); ?><span><?php esc_html_e('39', CIP_FREE_TXTDM ); ?></span></span>
                            </div>
                            <div class="btn-group-vertical">
                                <a class="btn btn-primary btn-lg" href="https://weblizar.com/themes/guardian-premium-theme/"><?php esc_html_e('Detail', CIP_FREE_TXTDM ); ?></a>
                                <a class="btn btn-success btn-lg" href="https://weblizar.com/themes/guardian-premium-theme/"><?php esc_html_e('Buy Now', CIP_FREE_TXTDM ); ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="row p_plugin blog_gallery">
                        <div class="col-xs-12 col-sm-4 col-md-5 p_plugin_pic">
                            <div class="img-thumbnail">
                                <?php
						echo '<img src="' . plugins_url( 'image/Enigma.jpg', __FILE__ ) . '" class="img-responsive" alt="img"/> ';
					?>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 p_plugin_desc">
                            <div class="row p-box">
                                <h2><a href=""><?php esc_html_e('Enigma- Modern & Clean Designed Multi-Purpose WordPress Theme', CIP_FREE_TXTDM ); ?></a></h2>
                                <p><strong><?php esc_html_e('Tags: ', CIP_FREE_TXTDM ); ?></strong><?php esc_html_e('clean, responsive, portfolio, blog, e-commerce, Business, WordPress, Corporate, dark, real estate, shop, restaurant.', CIP_FREE_TXTDM ); ?></p>
                                <div>
                                    <p><strong><?php esc_html_e('Description: ', CIP_FREE_TXTDM ); ?></strong><?php esc_html_e(' Enigma is a Full Responsive Multi-Purpose Theme suitable for Business , corporate office and others .Cool Blog Layout and full width page also present.', CIP_FREE_TXTDM ); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-2 p_plugin_pic">
                            <div class="price">
                                <span class="currency">USD</span>
                                <span class="price-number">$<span>39</span></span>
                            </div>
                            <div class="btn-group-vertical">
                                <a class="btn btn-primary btn-lg" href="https://weblizar.com/themes/enigma-premium/"><?php esc_html_e('Detail', CIP_FREE_TXTDM ); ?></a>
                                <a class="btn btn-success btn-lg" href="https://weblizar.com/themes/enigma-premium/"><?php esc_html_e('Buy Now', CIP_FREE_TXTDM ); ?></a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!----Plugin----->

            <div id="pluginsd" class="tab-pane fade">
                <div class="p_head theme">

                    <h1 class="section-title"><?php esc_html_e('WordPress Plugins', CIP_FREE_TXTDM ); ?></h1>

                </div>
                <div class="row p_plugin blog_gallery">
                    <div class="col-xs-12 col-sm-4 col-md-5 p_plugin_pic">
                        <div class="img-thumbnail">
                            <?php
						echo '<img src="' . plugins_url( 'image/Clockin-Pro.jpg', __FILE__ ) . '" class="img-responsive" alt="Clockin"/> ';
					?>

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 p_plugin_desc">
                        <div class="row p-box">
                            <h2><a href=""><?php esc_html_e('Clockin Portal Pro', CIP_FREE_TXTDM ); ?></a></h2>
                            <p><strong><?php esc_html_e('Features: ', CIP_FREE_TXTDM ); ?></strong>
                                <ul>
                                    <li><?php esc_html_e('Set Your TimeZone', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Salary status (Monthly or Hourly)', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('IP Restriction', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Shift Management', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Holiday Listing', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Leave Management Module', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Event Management Module....etc many more.', CIP_FREE_TXTDM ); ?></li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2 p_plugin_pic">
                        <div class="price">
                            <span class="currency"><?php esc_html_e('USD', CIP_FREE_TXTDM ); ?></span>
                            <span class="price-number"><span><?php esc_html_e('$14', CIP_FREE_TXTDM ); ?></span></span>
                        </div>
                        <div class="btn-group-vertical">
                            <a target="_blank" class="btn btn-primary btn-lg" href="https://weblizar.com/plugins/clockin-pro-plugin/"><?php esc_html_e('Demo', CIP_FREE_TXTDM ); ?></a>
                            <a class="btn btn-success btn-lg" href="https://weblizar.com/plugins/clockin-pro-plugin/"><?php esc_html_e('Buy Now', CIP_FREE_TXTDM ); ?></a>
                        </div>
                    </div>
                </div>
                <div class="row p_plugin blog_gallery">
                    <div class="col-xs-12 col-sm-4 col-md-5 p_plugin_pic">
                        <div class="img-thumbnail">
                            <?php
						echo '<img src="' . plugins_url( 'image/hrm.jpg', __FILE__ ) . '" class="img-responsive" alt="img"/> ';
					?>

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 p_plugin_desc">
                        <div class="row p-box">
                            <h2><a href=""><?php esc_html_e('Employee And HR Management WordPress Plugin', CIP_FREE_TXTDM ); ?></a></h2>
                            <p><strong> <?php esc_html_e('Features:', CIP_FREE_TXTDM ); ?></strong>
                                <ul>
                                    <li><?php esc_html_e('Admin Centralized Dashboard', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Department & Designation Management', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Shifts Management', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Staff Management', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Generate Attandance Reports', CIP_FREE_TXTDM ); ?></li>
                                   
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2 p_plugin_pic">
                        <div class="price">
                            <span class="currency"><?php esc_html_e('USD', CIP_FREE_TXTDM ); ?></span>
                            <span class="price-number"><span><?php esc_html_e('$19', CIP_FREE_TXTDM ); ?></span></span>
                        </div>
                        <div class="btn-group-vertical">
                            <a target="_blank" class="btn btn-primary btn-lg" href="https://weblizar.com/plugins/employee-and-hr-management-wordpress-plugin/"><?php esc_html_e('Demo', CIP_FREE_TXTDM ); ?></a>
                            <a class="btn btn-success btn-lg" href="https://weblizar.com/plugins/employee-and-hr-management-wordpress-plugin/"><?php esc_html_e('Buy Now', CIP_FREE_TXTDM ); ?></a>
                        </div>
                    </div>
                </div>
                <div class="row p_plugin blog_gallery">
                    <div class="col-xs-12 col-sm-4 col-md-5 p_plugin_pic">
                        <div class="img-thumbnail">
                            <?php
						echo '<img src="' . plugins_url( 'image/school.jpg', __FILE__ ) . '" class="img-responsive" alt="img"/> ';
					?>

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 p_plugin_desc">
                        <div class="row p-box">
                            <h2><a href=""><?php esc_html_e('School Manaegment WordPress Plugin', CIP_FREE_TXTDM ); ?></a></h2>
                            <p><strong> <?php esc_html_e('Features:', CIP_FREE_TXTDM ); ?></strong>
                                <ul>
                                    <li><?php esc_html_e('Class & Sections [ Subjects - Study Material - Attendance]', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Fee Generation', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Expense Management', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Income Management', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Exams - Time Table', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Much more.', CIP_FREE_TXTDM ); ?></li>
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2 p_plugin_pic">
                        <div class="price">
                            <span class="currency"><?php esc_html_e('USD', CIP_FREE_TXTDM ); ?></span>
                            <span class="price-number"><span><?php esc_html_e('$15', CIP_FREE_TXTDM ); ?></span></span>
                        </div>
                        <div class="btn-group-vertical">
                            <a target="_blank" class="btn btn-primary btn-lg" href="https://weblizar.com/plugins/school-management/"><?php esc_html_e('Demo', CIP_FREE_TXTDM ); ?></a>
                            <a class="btn btn-success btn-lg" href="https://weblizar.com/plugins/school-management/"><?php esc_html_e('Buy Now', CIP_FREE_TXTDM ); ?></a>
                        </div>
                    </div>
                </div>
                <div class="row p_plugin blog_gallery">
                    <div class="col-xs-12 col-sm-4 col-md-5 p_plugin_pic">
                        <div class="img-thumbnail">
                            <?php
						echo '<img src="' . plugins_url( 'image/Multi_Institute_Preview.jpg', __FILE__ ) . '" class="img-responsive" alt="img"/> ';
					?>

                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 p_plugin_desc">
                        <div class="row p-box">
                            <h2><a href=""><?php esc_html_e('Multi Institute Management', CIP_FREE_TXTDM ); ?> <?php esc_html_e('$', CIP_FREE_TXTDM ); ?></a></h2>
                            <p><strong><?php esc_html_e('Features: ', CIP_FREE_TXTDM ); ?> </strong>
                                <ul>
                                    <li><?php esc_html_e('Student Manaegment', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Staff Manaegment', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Course Manaegment', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Invoice - ID - Certificate Printing', CIP_FREE_TXTDM ); ?></li>
                                    <li> <?php esc_html_e('Multiple Custom Fee', CIP_FREE_TXTDM ); ?></li>
                                    <li><?php esc_html_e('Paypal - Razorpay supported', CIP_FREE_TXTDM ); ?></li>
                                    
                                </ul>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-3 col-md-2 p_plugin_pic">
                        <div class="price">
                            <span class="currency"><?php esc_html_e('USD', CIP_FREE_TXTDM ); ?></span>
                            <span class="price-number"><span><?php esc_html_e('$35', CIP_FREE_TXTDM ); ?></span></span>
                        </div>
                        <div class="btn-group-vertical">
                            <a target="_blank" class="btn btn-primary btn-lg" href="https://weblizar.com/plugins/multi-institute-management/"><?php esc_html_e('Demo', CIP_FREE_TXTDM ); ?></a>
                            <a class="btn btn-success btn-lg" href="https://weblizar.com/plugins/multi-institute-management/"><?php esc_html_e('Buy Now', CIP_FREE_TXTDM ); ?></a>
                        </div>
                    </div>
                </div>

               

                
               

            </div>

            <!--offers-->
            <div id="offers" class="tab-pane fade">
                <div class=" p_head theme">
                    <h1 class="section-title"><?php esc_html_e('Weblizar Offers', CIP_FREE_TXTDM ); ?></h1>

                </div>

                <div class="row p_plugin blog_gallery">
                    <div class="col-xs-12 col-sm-4 col-md-12 p_plugin_pic">
                        <div class="img-thumbnail">
						<a class="btn btn-primary title_offers" href="https://weblizar.com/offers/">
                            <?php
									echo '<img src="' . plugins_url( 'image/offer.jpg', __FILE__ ) . '" class="img-responsive" alt="offer"/> ';
								?></a>

                        </div>

                    </div>
                    
                </div>

            </div>
        </div>